<?php
//	#	Debug mode
	$_debug = true;
	// $_debug = false;

	if($_debug) $_loadTime = microtime(true);
//	#	Set the PHP error reporting level. 
	error_reporting(E_ALL | E_STRICT);

//	#	Set ini PHP error reporting.
	ini_set('display_errors', 1);

//	#	Content Type
	header('Content-Type: text/html; charset=UTF-8');

//	#	Set Local 
	setlocale(LC_ALL, "en_US.UTF-8");

//	#	Start Session
	session_start();

//	#	Main Defines
	define('__JAMP__', [
		'ext' => '.php',
		'sep' => DIRECTORY_SEPARATOR,
		'root' => __DIR__.DIRECTORY_SEPARATOR,
		'app' => __DIR__.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR,
        'user' => __DIR__.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR,
		'public' => __DIR__.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR,
		'basic' => __DIR__.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'_basic'.DIRECTORY_SEPARATOR,
		'view' => __DIR__.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR,
		'lang' => __DIR__.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR,
		'css' => 'public'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR,
		'js' => 'public'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR,
		'images' => 'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR,
		'uploads' => 'uploads'.DIRECTORY_SEPARATOR
	]);

	require_once ( "app/config/config.php" );
	require_once ( "app/class/system/jampDebug.php" );
	require_once ( "app/class/system/cronModel.php" );
	require_once ( "cron/day.class.php" );
	require_once ( "cron/archive.class.php" );
	require_once ( "cron/request.class.php" );
	require_once ( "cron/status.class.php" );

	$class = new day( $_debug, $glConfig );

//	#	Debug mode
// echo 'cron ran '.date("Y-m-d H:i:s");
if($_debug) echo '<br>'.$class->translate('_debugLoadTime') . number_format(microtime(true) - $_loadTime, 10) . $class->translate('_debugSeconds');

?>