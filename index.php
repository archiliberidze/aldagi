<?php
//	#Debug mode
	// $_debug = true;
	 $_debug = false;
	if($_debug) $_loadTime = microtime(true);
//	#	Set the PHP error reporting level. 
	error_reporting(E_ALL | E_STRICT);

//	#	Set ini PHP error reporting.
	ini_set('display_errors', 1);

//	#	Content Type
	header('Content-Type: text/html; charset=UTF-8');

//	#	Set Local 
	setlocale(LC_ALL, "en_US.UTF-8");

//	#	Start Session
	session_start();

//	#	Main Defines
	define('__JAMP__', [
		'ext' => '.php',
		'sep' => DIRECTORY_SEPARATOR,
		'root' => __DIR__.DIRECTORY_SEPARATOR,
        'user' => __DIR__.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR,
		'public' => __DIR__.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR,
		'basic' => __DIR__.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'_basic'.DIRECTORY_SEPARATOR,
		'view' => __DIR__.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR,
		'lang' => __DIR__.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR,
		'css' => 'public'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR,
		'js' => 'public'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR,
		'images' => 'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR,
		'uploads' => 'uploads'.DIRECTORY_SEPARATOR
	]);

//	#	Include all class files
	spl_autoload_register( function ( $class ) {
	    try {
	    	// System
            $sysclass = str_replace("\\", DIRECTORY_SEPARATOR, $class);
            if ( file_exists("app/class/". $sysclass . ".php") ) {
	            require_once ("app/class/". $sysclass . ".php");
	        }
	        // User
	        if ( file_exists("app/class/user/". $class . ".php") ) {
	            require_once ("app/class/user/". $class . ".php");
	        }
	    } catch ( Exception $e ) {
	        echo "Class load faled: ".$e->getMessage();
	        die();
	    }
	});

//  #   Config 
	try {
		require 'app/config/config.php';
	} catch (Exception $e) {
		echo "Config load faled: ".$e->getMessage();
		die();
	}

//	#	Core start
	use system\jampCore;
	$class = new jampCore($glConfig, $_debug);
	unset($glConfig);

//	#	Build page 
	include_once( __JAMP__['basic'] . '_top' . __JAMP__['ext'] );

	foreach ($class->plan['plan'] as $key => $value) {
		$class->compiler( __JAMP__['view'] . $value . __JAMP__['ext'] );
	}

	include_once( __JAMP__['basic'] . '_bottom' . __JAMP__['ext'] );

//	#	Debug mode
	if($_debug) echo $class->translate('_debugLoadTime') . number_format(microtime(true) - $_loadTime, 10) . $class->translate('_debugSeconds');
?>