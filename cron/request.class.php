<?php if(!defined('__JAMP__')) exit( "Direct access not permitted." );

/**
 * 
 */

use DateTime as _date;

class request extends system\cronModel{

	private $_request		= 	null;

	function __construct( $debug, $config, $tables){

		parent::__construct( $debug, $config, $tables );

	}

	function sendRequest($data)
    {
        $dbNames = $this->dbName();

        foreach ($dbNames as $db) {
            foreach ($data as $k => $v) {
                $data[$k] = '("' . $v->users . '", \'' . $v->content . '\')';
            }
            $this->complicatedQuery(
                ['requests'],
                'INSERT INTO {table0} (users, content) VALUES ' . rtrim(implode(',', $data), ',')
            );
        }
    }

    function dbName(){
        return $this->complicatedQuery(
            [NULL],
            'SELECT company.name FROM users
                    LEFT JOIN company ON users.company_id = company.id
                    WHERE users.is_company = 1'
        );
    }
}

?>