<?php if(!defined('__JAMP__')) exit( "Direct access not permitted." );

/**
 * 
 */

use DateTime as _date;

class status extends system\cronModel{

	private $now				=	null;

	private $data				=	null;

	private $_days				= 	null;

	private $_status			= 	null;

	private $_update			= 	'';


	function __construct($debug, $config, $tables){
		parent::__construct($debug, $config, $tables);

	}

	function gatherData($data){

		$this->data[] = $data;

	}


	function processData(){

		$this->checkStatus();
		
		$this->processUpdate();

	}





	function checkStatus(){
		if($this->data){
			foreach ($this->data as $v) {
				if($v->dayId==null)break;

				$this->now 			=	 new _date();
				$this->now->setTimeZone( new DateTimeZone($v->settings->timezone) );

				$workHours = $v->plan->shift->day->start->diff($v->plan->shift->day->end);

				$v->status = $this->processStatus($v->status);

				if( $v->checkIn == null && !in_array('502', $v->status) ){
					$this->statusLate($v);
				}


				// CheckExtra Hours
				if( $v->checkIn !== null && $v->checkOut == null && !in_array('506', $v->status) ){
					$checkIn = new _date($v->checkIn);
					$checkIn->add($workHours);
					if( $v->settings->end->paid && $this->now->diff($checkIn) > new DateInterval('PT'.$v->settings->end->after.'M')){
						$this->statusExtra($v);
					}
				}
				
				// Check Being Late On Lunch
				if( $v->plan->shift->lunch && $v->lunchStart && $v->lunchEnd == null && !in_array('508', $v->status) ){
					$lunchEndTime = new _date($v->lunchStart);
					$lunchEndTime->modify('+ '. $v->plan->shift->lunch->duration .' minutes');
					if( $this->now > $lunchEndTime ){
						$this->statusLateLunch($v);
					}
				}
			}
		}
	}

	function processStatus($status){
		if( strpos($status, ',') !== false ){
			return explode(',', $status);
		}
		return [$status];
	}

	function statusLate($day){
		$startMax = $day->plan->shift->day->end;
		$startMax->modify('+ '. $day->plan->settings->dayRange .' minutes');
		if ( $this->now > $startMax ) {
			$day->status = array_diff($day->status, [501]);
			$day->status[] = 502;
			$this->writeUpdate($day->status, $day->dayId);
		}
		else if( 
			$this->now->diff($startMax) <= new DateInterval('PT'.$day->settings->start->after.'M') && !in_array('501', $day->status) 
		){
			$day->status[] = 501;
			$this->writeUpdate($day->status, $day->dayId);
		}
	}

	function statusExtra($day){
		$day->status[] = 506;
		$this->writeUpdate($day->status, $day->dayId);
	}

	function statusLateLunch($day){
		$day->status[] = 508;
		$this->writeUpdate($day->status, $day->dayId);
	}

	function writeUpdate($status, $dayId){
		$this->_update .=	'('. $dayId .',"'. implode(',', $status) .'"),';
	}

	function processUpdate(){
        $dbNames = $this->dbName();

        foreach ($dbNames as $db) {
            if ($this->_update) {
                $this->_update = rtrim($this->_update, ',');
                $this->complicatedQuery(
                    ['day'],
                    'INSERT INTO {table0} (id,status) VALUES ' . $this->_update . '
				ON DUPLICATE KEY UPDATE status = VALUES(status)'
                );
            }
        }
	}

    function dbName(){
        return $this->complicatedQuery(
            [NULL],
            'SELECT company.name FROM users
                    LEFT JOIN company ON users.company_id = company.id
                    WHERE users.is_company = 1'
        );
    }
}

?>