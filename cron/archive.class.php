<?php if(!defined('__JAMP__')) exit( "Direct access not permitted." );

/**
 * 
 */

use DateTime 		as _date;
use request 		as _request;

class archive extends system\cronModel{

	private $data				= 	null;

	private $_unset				= 	'';
	private $_insert			= 	'';

	private $_request			= 	null;
	private $_requestData		= 	[];

	private $log				= 	null;

	function __construct( $debug, $config, $tables ){

		parent::__construct( $debug, $config, $tables );

		$this->_request = new _request( $debug, $config, $tables);

	}

	function gatherData($data){

		$this->data[] = $data;

	}

	function processData(){
		if( $this->data ){
			foreach ($this->data as $data) {

				if( $data->checkIn == null ){

					$this->_requestData[] = (object)[
						'users'	=>	$data->managers,
						'content'	=>	json_encode([
							'userId'	=>	$data->id,
							'date'		=>	$data->date,
							'type'		=>	1,
							'category'	=>	1
						])
					];
				}
				elseif( $data->checkOut == null ){
					$this->_requestData[] = (object)[
						'users'	=>	$data->managers,
						'content'	=>	json_encode([
							'userId'	=>	$data->id,
							'date'		=>	$data->date,
							'type'		=>	1,
							'category'	=>	1
						])
					];
					$data->checkIn		=	new _date( $data->checkIn );
				}
				else{
					$data->checkIn		=	new _date( $data->checkIn );
					$data->checkOut		=	new _date( $data->checkOut );
				}

				if( $data->lunchStart 	!= null )	$data->lunchStart	=	new _date( $data->lunchStart );
				if( $data->lunchEnd		!= null )	$data->lunchEnd		=	new _date( $data->lunchEnd );

				// 
				$this->_unset.= $data->dayId . ',';


				$this->processSummary($data);
			}

			$this->moveDataToArchive();

			$this->processRequests();
		}
	}


	function processSummary( $data ){
		if( $data->checkOut && $data->checkIn ){
			$userDay =  $data->checkOut->diff($data->checkIn);

			$fi = $this->plan->settings->dayRange;
			$st = $this->plan->settings->startMin;
			$workHours = $fi->diff($st);

			$workTime =  $data->checkOut->diff($data->checkIn);
			if( (($userDay->h * 60 + $userDay->i) - ($workHours->h * 60 + $workHours->i)) > $this->settings->end->after && $this->settings->extra->after ){
				$workTime = $workHours;
				$workTimeNew  = new _date ($workHours->format("%H:%I:%S"));
				$userDayNew  = new _date ($userDay->format("%H:%I:%S"));
				$extraTime = $workTimeNew->diff($userDayNew);

				// 
				$reason = 'extra time';
				$this->_request->sendRequest($reason, $data);
			}
		}

		$this->_insert.= 	'(
			NULL, "'.
			$data->date 	.'", '.
			$data->id	.	', \''.
			json_encode([
				'checkIn' 		=> 	( ($data->checkIn ) ? $data->checkIn->format('H:i:s') : null),
				'lunchStart' 	=> 	( ($data->lunchStart ) ? $data->lunchStart->format('H:i:s') : null),
				'lunchEnd' 		=> 	( ($data->lunchEnd ) ? $data->lunchEnd->format('H:i:s') : null),
				'checkOut' 		=> 	( ($data->checkOut ) ? $data->checkOut->format('H:i:s') : null)
			])				.	'\', \''.
			json_encode([
				'lunchTime'		=>	( ($data->lunchEnd && $data->lunchStart ) ? $data->lunchEnd->diff($data->lunchStart)->format("%H:%I:%S") : null),
				'totalTime'		=>	( ($data->checkOut && $data->checkIn ) ? $data->checkOut->diff($data->checkIn)->format("%H:%I:%S") : null),
				'workTime'		=>	( ( isset($workTime) ) ? $workTime->format("%H:%I:%S") : null),
				'extraTime'		=>	( ( isset($extraTime) ) ? $extraTime->format("%H:%I:%S") : '00:00:00'),
			 	'vacationTime'	=>	''
			])				.	'\', "'.
			$data->log 		.	'",
			0
			),'
		;
	}

	function moveDataToArchive(){
	    $dbNames = $this->dbName();

	    foreach($dbNames as $db) {
            $this->complicatedQuery(
                ['`'.$db->name.'`.archive'],
                'INSERT INTO {table0} (`id`, `date`, `userId`, `data`, `summary`, `log`, `_deleted`) VALUES ' . rtrim($this->_insert, ',')
            );
            $this->complicatedQuery(
                ['`'.$db->name.'`.day'],
                'DELETE from {table0} WHERE id IN (' . rtrim($this->_unset, ',') . ');'
            );
        }
	}	

	function processRequests(){
		$this->_request->sendRequest( $this->_requestData );
	}

	function dbName(){
        return $this->complicatedQuery(
            [NULL],
            'SELECT company.name FROM users
                    LEFT JOIN company ON users.company_id = company.id
                    WHERE users.is_company = 1'
        );
	}
}

?>