<?php if(!defined('__JAMP__')) exit( "Direct access not permitted." );

/**
 * 
 */

use DateTime 		as _date;
use archive 		as _archive;
use request 		as _request;
use status 			as _status;

class day extends system\cronModel{

	public $_config				=	null;
	private $_debug				=	null;
	private $now				=	null;
	private $_schedule 			= 	null;
	private $_archive			= 	null;
	private $_request			= 	null;
	private $dataDay			=	'';
	private $dataRecount		=	'';

	function __construct( $debug, $config ){

		$this->_debug 	=	$debug;
		$this->_config 	=	$config;

		parent::__construct( $this->_debug, $this->_config );


		$this->_archive 	= 	new _archive( $this->_debug, $this->_config, $this->tables );
		$this->_status 		= 	new _status( $this->_debug, $this->_config, $this->tables);


        $this->verifyCompany();
        $this->setSchedule();


    }


	function setSchedule()
    {
        $dbNames = $this->dbName();
        foreach ($dbNames as $db) {
            $db = 'a-jamp-timepunch'.'_'.$db->name;
            $this->data['result'] = $this->updateRecords(null, "INSERT INTO `$db`.day (userId, date, shiftId)
    (
        SELECT `$db`.users.id, CURDATE(), `$db`.shift.id
        FROM `$db`.users
        JOIN `$db`.schedule ON `$db`.users.scheduleId=`$db`.schedule.id AND `$db`.schedule.StartDate<=curdate()
        JOIN `$db`.shift ON `$db`.shift.scheduleId=`$db`.schedule.id
        WHERE curdate() NOT IN (SELECT IFNULL(`$db`.day.date,curdate()) FROM `$db`.day WHERE `$db`.day.userId=`$db`.users.id AND `$db`.day.date=curdate() ) AND  `$db`.shift.weekDay=WEEKDAY(CURDATE())+1 AND  `$db`.shift.weekId=IF(CEIL(DATEDIFF(CURDATE(),`$db`.schedule.StartDate)/7)% (SELECT COUNT(DISTINCT `$db`.shift.weekId) FROM `$db`.shift WHERE `$db`.shift.scheduleId=`$db`.schedule.id)=0,(SELECT COUNT(DISTINCT `$db`.shift.weekId) FROM `$db`.shift WHERE `$db`.shift.scheduleId=`$db`.schedule.id),CEIL(DATEDIFF(CURDATE(),`$db`.schedule.StartDate)/7)% (SELECT COUNT(DISTINCT `$db`.shift.weekId) FROM `$db`.shift WHERE `$db`.shift.scheduleId=`$db`.schedule.id))
        GROUP BY `$db`.users.id
    )
");
            echo json_encode($this->data);
        }
    }
// 'UPDATE {table0} set `settings` = json_set(`settings`, "$.recount", "'.$date->format("Y-m-d H:i:s").'") WHERE id='.$this->userId

    function dbName(){
        return $this->complicatedQuery(
            [NULL],
            'SELECT company.name FROM users
                    LEFT JOIN company ON users.company_id = company.id
                    WHERE users.is_company = 1'
        );
    }



    ######################### If new company isnt verified it will be delayted Start ###########

    function verifyCompany(){
        $rows = $this->complicatedQuery(
            [NULL],
            'SELECT b.name, b.id as companyId, a.id FROM `a-jamp-timepunch`.users as a
                    LEFT JOIN `a-jamp-timepunch`.company as b ON a.company_id = b.id
                    WHERE a.verify != "true" && is_company = 1'
        );
        foreach($rows as $item){
            $newDb = 'a-jamp-timepunch_'.$item->name;
            $this->complicatedQuery(
                [NULL],
                'DROP DATABASE IF EXISTS `'.$newDb.'`'
            );

            $this->complicatedQuery(
                [NULL],
                'DELETE FROM `a-jamp-timepunch`.company WHERE id = "'.$item->companyId.'"'
            );

            $this->complicatedQuery(
                [NULL],
                'DELETE FROM `a-jamp-timepunch`.users WHERE id = "'.$item->id.'"'
            );
        }
    }
    ######################### If new company isnt verified it will be delayted End ###########
}

?>