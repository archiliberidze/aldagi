<?php

use system\jampModel;

if (!defined('__JAMP__')) {
    header('HTTP/1.0 404 Not Found');
    die();
}

class reports extends system\jampModel
{
    function index() {
        $im = $this->complicatedQuery(['users'],'SELECT `is_company`, `controls-users` as controlUsers, users FROM users
                                                        LEFT JOIN groups ON FIND_IN_SET(users.groups, groups.id)
                                                        WHERE users.id = "'.$this->session->userId.'"', 1);

        if($im->controlUsers != ''){
            $allUsersControl = ($im->users != '')?$im->users.','.$im->controlUsers : $im->controlUsers;
        }else{
            $allUsersControl = $im->users;
        }

        ($allUsersControl != '')? $allUsersControl = $this->session->userId.','.$allUsersControl : $allUsersControl = $this->session->userId;

        ($im->is_company == 1)? $company = '&& is_company = 0' : $company = '&& users.id IN ('.$allUsersControl.')';


        if (isset($this->post)) {
//            echo '<pre>';
//            print_r($this->post);
//            echo '</pre>';
            if (isset($this->post->ids)) {

                $impIds = implode(', ', $this->post->ids);
                if(empty($this->post->day_period)) {

//                    $dateFormat = date('Y-m-d', strtotime($this->post->datepicker));
                    $datepickerEx = str_replace('/', '-', explode(' - ', $this->post->datepicker));
                    $startDate = date('Y-m-d', strtotime($datepickerEx[0]));
                    $endDate = date('Y-m-d', strtotime("$datepickerEx[1]"));

                    $pickerdate = ' DATE_FORMAT(day.date, "%m-%d-%Y") >= "'.$datepickerEx[0].'" && DATE_FORMAT(day.date, "%m-%d-%Y") <= "'.$datepickerEx[1].'"';
                    $pickerdateAnd = " && ";
                }else{
                    $lastWeekDays = $this->post->day_period + date('w');
                    $pastDays = date("Y-m-d", strtotime("-".$lastWeekDays." day"));
                    $exception = date("Y-m-d", strtotime("-".date('w')." day"));

                    $pickerdate = 'day.date >=  "'.$pastDays.'" && day.date <= "'.$exception.'"';
                    $pickerdateAnd = ' && ';
                }

                if(isset($this->post->group_by_date)){
                    $this->data['shift'] = $this->complicatedQuery(
                        ['day', 'shift'],
                        'SELECT day.date, day.userId, shift.id, shift.clockIn, clockOut FROM day
                            LEFT JOIN shift ON day.shiftId = shift.id
                            WHERE '.$pickerdate.' && day.userId IN (' . $impIds . ') && shift.clockIn != "" && shift.clockOut != ""
                            '
                    );

                    $this->data['day'] = $this->complicatedQuery(
                        ['users', 'day'],
                        'SELECT users.id as user_id,  users.name, users.last_name, users.overtime, day.date, day.before_extratime, day.after_extratime, day.id as day_id, day.log, GROUP_CONCAT(day_times.clockIn) as day_time_clockIn, GROUP_CONCAT(day_times.id) as day_time_id, GROUP_CONCAT(IFNULL(day_times.clockOut,"")) as day_times_clockOut,
                            user_group.title as group_title,
                            GROUP_CONCAT(day_times.start_lunch) as day_times_startLunch, GROUP_CONCAT(day_times.end_lunch) as day_times_endLunch, GROUP_CONCAT(day_times.clockOutStatus) as clockOutStatus, shift.*, (SELECT GROUP_CONCAT(positions.name) FROM positions WHERE FIND_IN_SET(users.id, positions.users) ) as position_name,
                            IFNUll((SELECT id FROM dayrequest WHERE dayId=day.id && (FIND_IN_SET(1, dayrequest.status) OR FIND_IN_SET(2, dayrequest.status)) LIMIT 1),"") as request_day_id
                            FROM users
                            LEFT JOIN  day ON users.id = day.userId
                            JOIN day_times ON day.id = day_times.dayId 
                            LEFT JOIN ( SELECT users.id as user_id,
                                          GROUP_CONCAT(groups.title) as title
                                        FROM users
                                        LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                        WHERE users._deleted = 0 GROUP BY users.id
                             ) AS user_group ON user_group.user_id=users.id 
                            JOIN shift ON day.shiftId = shift.id 
                            WHERE   users._deleted = 0 && day._deleted = 0 && users.id IN (' . $impIds . ') '.$pickerdateAnd.' '.$pickerdate.' 
                            GROUP BY day.id
                            order by users.last_name, users.id ASC
                            '
                    );
                    $this->day_group_by_date();
                    die();
                }



                if(isset($this->post->group_by_employees)){
                    $this->data['shift'] = $this->complicatedQuery(
                        ['day', 'shift'],
                        'SELECT day.date, day.userId, shift.id, shift.clockIn, clockOut FROM day
                            LEFT JOIN shift ON day.shiftId = shift.id
                            WHERE '.$pickerdate.' && day.userId IN (' . $impIds . ') && shift.clockIn != "" && shift.clockOut != ""
                            '
                    );

                    $this->data['day'] = $this->complicatedQuery(
                        ['users', 'day'],
                        'SELECT users.id as user_id,  users.name, users.last_name, users.overtime, day.date, day.before_extratime, day.after_extratime, day.id as day_id, day.log, GROUP_CONCAT(day_times.clockIn) as day_time_clockIn, GROUP_CONCAT(day_times.id) as day_time_id, GROUP_CONCAT(IFNULL(day_times.clockOut,"")) as day_times_clockOut,
                            user_group.title as group_title,
                            GROUP_CONCAT(day_times.start_lunch) as day_times_startLunch, GROUP_CONCAT(day_times.end_lunch) as day_times_endLunch, GROUP_CONCAT(day_times.clockOutStatus) as clockOutStatus, shift.*, (SELECT GROUP_CONCAT(positions.name) FROM positions WHERE FIND_IN_SET(users.id, positions.users) ) as position_name,
                            IFNUll((SELECT id FROM dayrequest WHERE dayId=day.id && (FIND_IN_SET(1, dayrequest.status) OR FIND_IN_SET(2, dayrequest.status)) LIMIT 1),"") as request_day_id
                            FROM users
                            LEFT JOIN  day ON users.id = day.userId
                            JOIN day_times ON day.id = day_times.dayId 
                            LEFT JOIN ( SELECT users.id as user_id,
                                          GROUP_CONCAT(groups.title) as title
                                        FROM users
                                        LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                        WHERE users._deleted = 0 GROUP BY users.id
                             ) AS user_group ON user_group.user_id=users.id 
                            JOIN shift ON day.shiftId = shift.id 
                            WHERE   users._deleted = 0 && day._deleted = 0 && users.id IN (' . $impIds . ') '.$pickerdateAnd.' '.$pickerdate.' 
                            GROUP BY day.id
                            order by users.last_name, users.id ASC
                            '
                    );
                    $this->day_group_by_employees();
                    die();
                }


                $this->data['shift'] = $this->complicatedQuery(
                ['day', 'shift'],
                    'SELECT day.date, day.userId, shift.id, shift.clockIn, clockOut FROM day
                            LEFT JOIN shift ON day.shiftId = shift.id
                            WHERE '.$pickerdate.' && day.userId IN (' . $impIds . ') && shift.clockIn != "" && shift.clockOut != ""
                            '
                );

                $this->data['day'] = $this->complicatedQuery(
                    ['users', 'day'],
                    'SELECT users.id as user_id,  users.name, users.last_name, users.overtime, day.date, day.before_extratime, day.after_extratime, day.id as day_id, day.log, GROUP_CONCAT(day_times.clockIn) as day_time_clockIn, GROUP_CONCAT(day_times.id) as day_time_id, GROUP_CONCAT(IFNULL(day_times.clockOut,"")) as day_times_clockOut,
                            user_group.title as group_title,
                            GROUP_CONCAT(day_times.start_lunch) as day_times_startLunch, GROUP_CONCAT(day_times.end_lunch) as day_times_endLunch, GROUP_CONCAT(day_times.clockOutStatus) as clockOutStatus, shift.*, (SELECT GROUP_CONCAT(positions.name) FROM positions WHERE FIND_IN_SET(users.id, positions.users) ) as position_name,
                            IFNUll((SELECT id FROM dayrequest WHERE dayId=day.id && (FIND_IN_SET(1, dayrequest.status) OR FIND_IN_SET(2, dayrequest.status)) LIMIT 1),"") as request_day_id
                            FROM users
                            LEFT JOIN  day ON users.id = day.userId
                            JOIN day_times ON day.id = day_times.dayId 
                            LEFT JOIN ( SELECT users.id as user_id,
                                          GROUP_CONCAT(groups.title) as title
                                        FROM users
                                        LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                        WHERE users._deleted = 0 GROUP BY users.id
                             ) AS user_group ON user_group.user_id=users.id 
                            JOIN shift ON day.shiftId = shift.id 
                            WHERE   users._deleted = 0 && day._deleted = 0 && users.id IN (' . $impIds . ') '.$pickerdateAnd.' '.$pickerdate.' 
                            GROUP BY day.id
                            order by users.last_name, users.id ASC
                            '
                );


                if (!empty($this->data['day'])) {
                    foreach ($this->data['day'] as $dayIds) {
                        $dayIdsArr[] = $dayIds->id;
                    }
                    $implodeIds = implode(',', $dayIdsArr);

                    $dayReq = $this->selectRows('dayrequest', 'dayId', 'dayId IN ("' . $implodeIds . '")', null);
                    $arr[] = '';
                    if (!empty($dayReq)) {
                        foreach ($dayReq as $reqIds) {
                            $arr[] = $reqIds->dayId;
                        }
                    }
                    $this->data['dayReq'] = $arr;
                    $list['html_data'] = $this->report_generation();
                    print_r($list['html_data']);
                }
            }

            // pay overtimes
            if (isset($this->post->before_extratime)) {
                $this->beforeExtratime();
            }

            if(isset($this->post->after_extratime)){
                $this->afterExtratime();
            }

            if (isset($this->post->logDay)) {
                $ex = explode('-', $this->post->logDay);

                $this->data['day'] = $this->complicatedQuery(
                    ['dayrequest'],
                    'SELECT dayrequest.*, day.userId, day.date ,users_t.name, users_t.last_name
                        FROM dayrequest
                        JOIN day ON dayrequest.dayId = day.id
                        JOIN (SELECT users.name, users.last_name, dayrequest.id  as id
                             FROM dayrequest
                             LEFT JOIN users ON dayrequest.toUser = users.id
                        ) as users_t ON users_t.id=dayrequest.id
                        WHERE dayrequest.dayId = "' . $ex[1] . '" && (FIND_IN_SET(1, dayrequest.status) OR FIND_IN_SET(2, dayrequest.status))'
                );

                isset($this->data['day'][0])?$touserID = $this->data['day'][0]->toUser : $touserID = '';
                isset($this->data['day'][0])?$userId = $this->data['day'][0]->userId : $userId = '';
                $this->data['user_info'] = $this->selectRows('users', '*', 'id = "'.$touserID.'"', null, 1);
                $this->data['user_id'] = $this->selectRows('users', 'name, last_name', 'id = "'.$userId.'"', null, 1);

                $this->user_log_modal();
                die();
            }

            if (isset($this->post->updateDay)) {
                $this->updateDay();
            }

            if (isset($this->post->groupId)) {
                $this->sort_user_by_group($company);
            }

            if (isset($this->post->username_user_id) && !empty($this->post->username_user_id)) {

                $this->data['users'] = $this->complicatedQuery(
                    ['users', 'positions'],
                    'SELECT users.last_name, users.image, users.phone_number, schedule.title as shcedule_title, users.address, users.zip_code, users.active_since, users.id as table_user_id, users.name as user_name, users.last_name, positions.name as positionName, user_group.title as group_title
                    FROM users 
                    LEFT JOIN ( SELECT users.id as user_id,
                                  GROUP_CONCAT(groups.title) as title
                                FROM users
                                LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                WHERE users._deleted = 0 GROUP BY users.id
                     ) AS user_group ON user_group.user_id=users.id 
                     LEFT JOIN ( SELECT users.id as user_id,
                                   positions.id as id,
                                  GROUP_CONCAT(positions.name) as name
                                FROM users
                                LEFT JOIN positions ON FIND_IN_SET(users.id, positions.users)
                                WHERE users._deleted = 0 GROUP BY users.id
                     ) AS positions ON positions.user_id=users.id 
                     LEFT JOIN schedule ON users.scheduleId = schedule.id                     
                    WHERE users._deleted = 0 && users.id = "'.$this->post->username_user_id.'" GROUP BY users.id ORDER BY users.last_name ASC', 1
                );


//                Todo select next schedule amd other
//                echo '<pre>';
//                print_r($this->data['users']);
//                echo '</pre>';




                $list['user_info_modal'] = $this->user_info_modal();
                print_r($list['user_info_modal']);
            }

            if(isset($this->post->generate_excel)){
//                echo '<pre>';
//                print_r($this->post);
//                echo '</pre>';
//                die('asd');
                $exUserId = explode('_', $this->post->user_id);

                if(empty($this->post->day_period)) {

//                    $dateFormat = date('Y-m-d', strtotime($this->post->datepicker));
                    $datepickerEx = str_replace('/', '-', explode(' - ', $this->post->datepicker));
                    $startDate = date('Y-m-d', strtotime($datepickerEx[0]));
                    $endDate = date('Y-m-d', strtotime("$datepickerEx[1]"));

                    $pickerdate = ' DATE_FORMAT(day.date, "%m-%d-%Y") >= "'.$datepickerEx[0].'" && DATE_FORMAT(day.date, "%m-%d-%Y") <= "'.$datepickerEx[1].'"';
                    $pickerdateAnd = " && ";
                }else{
                    $lastWeekDays = $this->post->day_period + date('w');
                    $pastDays = date("Y-m-d", strtotime("-".$lastWeekDays." day"));
                    $exception = date("Y-m-d", strtotime("-".date('w')." day"));

                    $pickerdate = 'day.date >=  "'.$pastDays.'" && day.date <= "'.$exception.'"';
                    $pickerdateAnd = ' && ';
                }

                $this->data['shift'] = $this->complicatedQuery(
                    ['day', 'shift'],
                    'SELECT day.date, day.userId, shift.id, shift.clockIn, clockOut FROM day
                            LEFT JOIN shift ON day.shiftId = shift.id
                            WHERE '.$pickerdate.' && day.userId = "'.$exUserId[1].'" && shift.clockIn != "" && shift.clockOut != ""
                            '
                );

                $this->data['excel'] = $this->complicatedQuery(
                    ['users', 'day'],
                    'SELECT users.id as user_id,  users.name, users.last_name, users.overtime, day.date, day.before_extratime, day.after_extratime, day.id as day_id, day.log, GROUP_CONCAT(day_times.clockIn) as day_time_clockIn, GROUP_CONCAT(day_times.id) as day_time_id, GROUP_CONCAT(IFNULL(day_times.clockOut,"")) as day_times_clockOut,
                            user_group.title as group_title,
                            GROUP_CONCAT(day_times.start_lunch) as day_times_startLunch, GROUP_CONCAT(day_times.end_lunch) as day_times_endLunch, GROUP_CONCAT(day_times.clockOutStatus) as clockOutStatus, shift.*, (SELECT GROUP_CONCAT(positions.name) FROM positions WHERE FIND_IN_SET(users.id, positions.users) ) as position_name,
                            IFNUll((SELECT id FROM dayrequest WHERE dayId=day.id && (FIND_IN_SET(1, dayrequest.status) OR FIND_IN_SET(2, dayrequest.status)) LIMIT 1),"") as request_day_id
                            FROM users
                            LEFT JOIN  day ON users.id = day.userId
                            JOIN day_times ON day.id = day_times.dayId 
                            LEFT JOIN ( SELECT users.id as user_id,
                                          GROUP_CONCAT(groups.title) as title
                                        FROM users
                                        LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                        WHERE users._deleted = 0 GROUP BY users.id
                             ) AS user_group ON user_group.user_id=users.id 
                            JOIN shift ON day.shiftId = shift.id 
                            WHERE   users._deleted = 0 && day._deleted = 0 && users.id = "'.$exUserId[1].'" '.$pickerdateAnd.' '.$pickerdate.' 
                            GROUP BY day.id
                            order by users.last_name, users.id ASC
                            '
                );

                $this->excel_generate();
            }


            die();
        }

        $this->data['groups'] = $this->selectRows('groups', 'title, users, id', '_deleted = 0', null);
        $this->data['users'] = $this->complicatedQuery(
            ['users', 'positions'],
            'SELECT users.*, users.id as table_user_id, users.name as user_name, users.last_name, positions.*, positions.id as positionId, positions.name as positionName, user_group.title as group_title
                    FROM users
                    LEFT JOIN ( SELECT users.id as user_id,
                                  GROUP_CONCAT(groups.title) as title
                                FROM users
                                LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                WHERE users._deleted = 0 GROUP BY users.id
                     ) AS user_group ON user_group.user_id=users.id
                     LEFT JOIN ( SELECT users.id as user_id,
                                    GROUP_CONCAT(positions.id) as id,
                                    GROUP_CONCAT(positions.name) as name
                                FROM users
                                LEFT JOIN positions ON FIND_IN_SET(users.id, positions.users)
                                WHERE users._deleted = 0 GROUP BY users.id
                     ) AS positions ON positions.user_id=users.id
                    WHERE users._deleted = 0 '.$company.' GROUP BY users.id ORDER BY users.last_name ASC'
        );
    }

    function plan()
    {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css",
                "https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css",
                "header/header.css",
                "header/glyphter.css",
                "footer.css",
                "frame.css",
                "reports/glyphter.css",
                "reports/style.css",
            ],
            'js' => [
                "https://cdn.jsdelivr.net/jquery/latest/jquery.min.js",
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js",
                "reports.js",
                "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js",
                "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
                "https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js",
                "https://cdn.jsdelivr.net/jquery/latest/jquery.min.js",
                "https://cdn.jsdelivr.net/momentjs/latest/moment.min.js",
                "https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js",
                "https://cdn.jsdelivr.net/npm/sweetalert2@9",
                "basic.js"
            ],
            'plan' => [
                'header',
                'reports',
                'footer'
            ]
        ];
        return $list;
    }


// if update day times create new record on dayrequest and create record in requestmessage


    public function getMenages () {
        return $this->customQuery("SELECT users FROM groups WHERE role = 1", 2);
    }

    public function getGroupMenage ($groupId='', $userId=null){
    $find_group_id = '';
	if (!is_null($groupId) && !empty($groupId))$find_group_id = "FIND_IN_SET($groupId, groups) ||";
//        return $this->customQuery("SELECT id FROM users WHERE (FIND_IN_SET($groupId, groups) || is_company = 1) AND _deleted = 0", 2);
        return $this->customQuery( 'SELECT id FROM users WHERE ('.$find_group_id.' is_company = 1 || FIND_IN_SET("'.$userId.'", `controls-users`)) AND _deleted = 0', 2);
    }

    public function updateDay()
    {
        $types = ['checkIn' => 'checkIn', 'checkOut' => 'checkOut', 'lunchStart' => 'lunchStart', 'lunchEnd' => 'lunchEnd'];
        $groupMenage = null;
        if (trim($this->post->message) == "" || !in_array($this->post->type, $types)) {
            return null;
        }

        $this->data['success'] = 'Employeer success added!';
        echo 'asdasd';
        $day = $this->complicatedQuery(
            ['day'],
            'SELECT day.userId, day.id, day_times.id AS dayTime
                        FROM day 
                            LEFT JOIN day_times ON day.id = day_times.dayId 
                    WHERE day._deleted = 0 AND day_times.id = ?', 1, [
                $this->post->updateDay
            ]
        );

        if (empty($day)) return null;
        $groupId = '';
        $groupId = $this->complicatedQuery(['groups'], "SELECT id FROM groups WHERE FIND_IN_SET(?, users) ", true, [$day->userId]);
//        if ($groupId) $groupMenage = $this->getGroupMenage($groupId->id, $day->userId);
        $groupMenage = $this->getGroupMenage((isset($groupId->id) && !empty($groupId->id))?$groupId->id:'' , $day->userId);

//        if (is_null($groupMenage) || empty($groupMenage)) $groupMenage = $this->getMenages();


        $dayRequest = $this->complicatedQuery(
            ['dayrequest'],
            'SELECT id FROM dayrequest WHERE dayId = '.$day->id.' AND '. $this->post->type .' IS NOT NULL AND fromUser = '.$day->userId.' AND toUser IN ('.  $groupMenage[0] .') ',
            true
        );

        if ($dayRequest == false) {

            $status = "0";
            if ($this->post->urgent != "false") {
                $status .= ',3';
            }

            $toManagers = implode(',', $groupMenage);
            $groupMenage = explode(',', $groupMenage[0]);
            if (empty($groupMenage)) return null;
            /*foreach ($groupMenage as $key => $userId) {*/
                $reqId = $this->complicatedQuery(
                    ['dayrequest'],
                     'INSERT INTO dayrequest (dayId, dayTimeId, '.$this->post->type.', fromUser, toUser, toManagers, status) VALUES (?, ?, ?, ?, ?, ?, ?)',
                    1,[$this->post->day_id, $this->post->updateDay, $this->post->time, $this->session->userId, $groupMenage[0], $toManagers, $status],true);

                $this->complicatedQuery(
                    ['requestmessage'],
                    'INSERT INTO requestmessage (dayReqId, message, fromUser, toUser, toManagers, reqId, sentTime) VALUES(?, ?, ?, ?, ?, ?, CONVERT_TZ(NOW(), @@session.time_zone, ?))',
                    1, [
                        $this->post->day_id,
                        $this->post->message,
                        $this->session->userId,
                        $groupMenage[0],
                        $toManagers,
                        $reqId,
                        $this->selectUserCompanyTimeZone($this->getSession('id'))
                    ], true);
            }
        /*}*/

        die();
    }

// if overtime checked its approved for paid else declined
    public function beforeExtratime()
    {
        $day = $this->selectRows('day', '*', '_deleted = 0 && id = "' . $this->post->dayId . '"', null, 1);

        $allMinutes = 0;
        if($this->post->before_extratime == 'decline'){
            $approve = 0;
        }else{
            $approve = 1;
            $allMinutes = $this->post->before_extratime;
        }


        if ($approve >= 1) {
            $log = $day->log . '\n Extra time approved (Before the shift)';
        } else {
            $log = $day->log . '\n Extra time canceled (Before the shift)';
        }
        $this->updateRecords('day', 'before_extratime = "' . $allMinutes . '", log = "' . $log . '"', $this->post->dayId . '');
    }

    public function afterExtratime(){
        $day = $this->selectRows('day', '*', '_deleted = 0 && id = "' . $this->post->dayId . '"', null, 1);

        $allMinutes = 0;
        if($this->post->after_extratime == 'decline'){
            $approve = 0;
        }else{
            $approve = 1;
            $allMinutes = $this->post->after_extratime;
        }


        if ($approve >= 1) {
            $log = $day->log . '\n Extra time approved (Afteк the shift)';
        } else {
            $log = $day->log . '\n Extra time canceled (After the shift)';
        }
        $this->updateRecords('day', 'after_extratime = "' . $allMinutes . '", log = "' . $log . '"', $this->post->dayId . '');
    }


    // sorting user by selected group
    public function sort_user_by_group($company)
    {if ($this->post->groupId == 0) {
        $groups = '';
    } else {
        $groups = ' AND FIND_IN_SET('.$this->post->groupId.',user_group.group_id)';
    }

        $this->data['users'] = $this->complicatedQuery(
            ['users', 'positions'],
            'SELECT users.*, users.id as table_user_id, users.name as user_name, users.last_name, positions.*, positions.id as positionId, positions.name as positionName, user_group.title as group_title
                    FROM users
                             LEFT JOIN ( SELECT users.id as user_id, GROUP_CONCAT(groups.id) as group_id, GROUP_CONCAT(groups.users) as group_users,
                                           GROUP_CONCAT(groups.title) as title
                                    FROM users
                                             LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                    WHERE users._deleted = 0 GROUP BY users.id
                                  ) AS user_group ON user_group.user_id=users.id
                             LEFT JOIN ( SELECT users.id as user_id,
                                           GROUP_CONCAT(positions.id) as id,
                                           GROUP_CONCAT(positions.name) as name
                                    FROM positions
                                             JOIN users ON FIND_IN_SET(users.id, positions.users)
                                    WHERE users._deleted = 0 GROUP BY users.id
                    ) AS positions ON positions.user_id=users.id
                    WHERE users._deleted = 0 '.$company.' '.$groups.'
                    GROUP BY users.id ORDER BY users.last_name ASC'
        );


        $this->data['groups'] = $this->selectRows('groups', 'title, users, id', '_deleted = 0', null);
        $list['users'] = $this->select_user();
        print_r($list['users']);
    }

    function report_generation()
    {
        include "app/class/includes/report_function.php";
    }

    function select_user()
    {
        include "app/class/includes/select-group-users.php";
    }

    function user_info_modal()
    {
        include "app/class/includes/reports/user-info-modal.php";
    }

    function day_group_by_date()
    {
        include "app/class/includes/reports/day-group-by-date.php";
    }

    function day_group_by_employees()
    {
        include "app/class/includes/reports/day-group-by-employees.php";
    }

    function user_log_modal()
    {
        include "app/class/includes/reports/user_log_modal.php";
    }

    function excel_generate(){
        include "app/class/includes/reports/excel.php";
    }
}
