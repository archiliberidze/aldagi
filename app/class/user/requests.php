<?php if(!defined('__JAMP__')) {header('HTTP/1.0 404 Not Found'); die();}

use DateTime as _date;
use system\jampModel;

class requests extends system\jampModel{

    /*
     * default page to load
     */
    public $defaultPage = 'inbox'; // index or sent

    /*
     * template list
     */
    private $templates = [
        'Messages'   => 'app/class/includes/chat_function.php',
        'Pagination' => 'app/class/includes/pagination.php'
    ];

    /*
     * dayrequest status Ids
     */
    private $dayRequestStatus = [
        'underReview' => 0,
        'approved'    => 1,
        'declined'    => 2,
        'urgent'      => 3,
        'read'        => 4
    ];

    /*
     * message status Ids
     */
    private $messageStatus = [
        'userComment ' => null,
        'approved'     => 1,
        'denied'       => 2,
        'edit'         => 3
    ];

    /*
     * table day, requestMessage messages and log row. (add languages...?)
     */
    private $autoLogMessages = [
        'approved'        => 'Approved the request ',
        'denied'          => 'Denied the request ',
        'edit'            => 'Changed the ',
        'timeChangeStart' => 'The requests for time change ',
        'timeChangeEnd'   => ' was edited '
    ];

    /*
     * dayRequest and day table rows (this is needed because the row name are coming from js)
     */
    private $tableRows = [
        'Check In'    => 'checkIn',
        'Check Out'   => 'checkOut',
        'Lunch Start' => 'lunchStart',
        'Lunch End'   => 'lunchEnd'
    ];

    /*
     *
     */
    private $messageUpdates = [
        'no_update' => ["update" => 0]
    ];

    /*
     * changing from index to sent messages.
     */
    private $requestInboxTypes = [
        'inbox' => ' AND request.toUser = ',
        'sent'  => ' AND request.fromUser = '
    ];

    /*
     *  list of menu links and names. (add languages...?)
     */
    public $menu = [
        "all"      => "All",
        "urgent"   => "Urgent",
        "approved" => "Approved",
        "declined" => "Declined",
        "unread"   => "Unread",
        "read"     => "Read"
    ];

    /*
     * default select list for requests...
     */
    protected $selectList = array(
        'request.checkIn     AS requestCheckIn',
        'request.checkOut    AS requestCheckOut',
        'request.lunchStart  AS requestLunchStart',
        'request.lunchEnd    AS requestLunchEnd',
        'request.toManagers    AS toManagers',

        'dayTime.clockIn     AS checkIn',
        'dayTime.clockOut    AS clockOut',
        'dayTime.start_lunch AS startLunchId',
        'dayTime.end_lunch   AS endLunchId',

        'request.dayId       AS requestsDayId',
        'request.id          AS requestId',
        'request.oldTime     AS oldTime',
        'day.id              AS dayID',
        '`users`.username    AS username',
        'day.userId          AS dayUserId',
        'day.date            AS dayDate',
        'dayTime.id          AS dayTimeId',
        'request.fromUser',
        'request.toUser',
        '`users`.id',
        '`users`.name        AS name',
        'request.`status`',
        '(SELECT username FROM users WHERE id = request.toUser) AS toUserName',
        '(SELECT name FROM users WHERE id = request.toUser) AS toName'
    );

    /*
     * all of the info needed for pagination to work properly
     */
    public $pagination = [
        'allReports'          => 0,
        'allPages'            => 0,
        'maxReportsOnOnePage' => 9,
        'currentPage'         => 1
    ];

    /**
     * @return void|null
     * request handler, if there is a post request, Id and type it will call a method.
     * if not it will load only list of requests.
     * main page under requests.
     */
    public function index() {

        /* FOR TESTING ONLY........................................*/
        if (!$this->getSession('userId')) {
            $this->setSession('userId', 3);
        }
        /* FOR TESTING ONLY........................................*/


        if (isset($this->post) && (isset($this->post->ids) || isset($this->post->id))) {
            switch ($this->post->type) {
                case 'messages' :
                    $this->setMessages();
                    break;

                case 'edit' :
                    // check if the user is admin

                    $this->approved($this->post->id, 1);
                    $this->changeTime($this->post->id, $this->post->dayId, $this->post->row, $this->post->newTime, $this->post->oldTime);
                    break;

                case 'save' :
                    if (empty($this->post->message) || trim($this->post->message == '')) $this->setMessages();

                    $this->saveMessages($this->post->ids, $this->post->message);
                    $this->setMessages();
                    break;

                case 'approved' :
                    // check if the user is admin

                    $changeTest = $this->testExistingStatus($this->post->id, 'approved');

                    if (is_null($changeTest)) die(); // error needs to be added

                    $this->changeOriginalTime($this->post->id, $this->post->dayId);
                    $this->approved($this->post->id, $this->post->status);
                    $this->autoLog('approved', $this->post->dayId, $this->post->id, $this->post->timeLog);
                    break;

                case 'denied' :
                    // check if the user is admin

                    $changeTest = $this->testExistingStatus($this->post->id, 'declined');

                    if (is_null($changeTest)) die(); // error needs to be added

                    $this->revertTime($this->post->id, $this->post->status, $this->post->dayId);
                    $this->denied($this->post->id, $this->post->status);
                    $this->autoLog('denied', $this->post->dayId, $this->post->id, $this->post->timeLog);
                    break;

                case 'update' :
                    $this->isMessageUpdate($this->post->ids, $this->post->lastUpdateDataAndTime);
                    $this->messageUpdates['no_update']['ids'] = $this->post->ids;

                    if (empty($this->data['messageUpdate'])) {
                        echo json_encode($this->messageUpdates['no_update']);
                        die();
                    } else {
                        $this->setMessages();
                        die();
                    }
                    break;

                default:
                    die('404 ERROR.');
                    break;
            }
            die ();
        }
        $this->data['menu']        = $this->menu;
        $this->data['urgent']      = $this->isUrgent();
        $this->data['defaultPage'] = $this->defaultPage;

        $this->getAllRequests(true);
        $this->pagination['allReports']  = $this->data['requests'][0]->Count;
        $this->pagination['allPages']    = ceil($this->pagination['allReports'] / $this->pagination['maxReportsOnOnePage']);
        $this->pagination['currentPage'] = $this->getCurrentPage();
        $this->pagination['template']    = $this->templates['Pagination'];
        $this->data['pagination']        = $this->pagination;

        $this->getAllRequests();
    }

    /**
     * @return int
     * test if the page number is set if not it will return default page number one...
     */
    public function getCurrentPage () {
        if (!isset($this->url[3])) return 1;

        return is_string($this->url[3]) ? $this->url[3] : 1;
    }

    // cancel chenged time
    private function revertTime($id , $status, $dayId){
        $dayrequest = $this->complicatedQuery(
            ['dayrequest'],
            'SELECT dayId, dayTimeId, checkIn, checkOut, lunchStart, lunchEnd, oldTime FROM dayrequest WHERE id = "'.$id.'"',1
        );

        if(isset($dayrequest->oldTime) && !empty($dayrequest->oldTime)){

            $dayDate = $this->complicatedQuery(
                ['day'],
                'SELECT `date` FROM `day` WHERE id = "'.$dayId.'"',1
            );

            $where = 'id = "'.$dayrequest->dayTimeId.'"';
            if($dayrequest->checkIn != '' && !is_null($dayrequest->checkIn)){
                $change = 'clockIn =  DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->oldTime.':%s")';
            }elseif($dayrequest->checkOut != '' && !is_null($dayrequest->checkOut)){
                $lastDay = $this->complicatedQuery(
                    ['day'],
                    'SELECT `id` FROM `day_times` WHERE dayId = "'.$dayrequest->dayId.'" && clockOutStatus = 1',1
                );
                $where = 'id = "'.$lastDay->id.'"';

                $change = 'clockOut =  DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->oldTime.':%s")';
            }elseif($dayrequest->lunchStart != '' && !is_null($dayrequest->lunchStart)){
                $change = 'clockOut =  DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->oldTime.':%s")';
            }elseif($dayrequest->lunchEnd != '' && !is_null($dayrequest->lunchEnd)){
                $change = 'clockIn =  DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->oldTime.':%s")';
            }

            if(isset($change)) {
                $this->complicatedQuery(
                    ['day_times'],
                    "UPDATE day_times SET $change WHERE $where"
                );
            }
        }
    }

    // Change origin time

    private function changeOriginalTime($id, $dayId){
        $dayrequest = $this->complicatedQuery(
            ['dayrequest'],
            'SELECT dayTimeId, checkIn, checkOut, lunchStart, lunchEnd FROM dayrequest WHERE id = "'.$id.'"',1
        );

        $dayDate = $this->complicatedQuery(
            ['day'],
            'SELECT `date` FROM `day` WHERE id = "'.$dayId.'"',1
        );

        if($dayrequest->checkIn != '' && !is_null($dayrequest->checkIn)){
            $dayOldTime = $this->complicatedQuery(
                ['day'],
                "SELECT `clockIn` FROM `day_times` WHERE id = $dayrequest->dayTimeId",1
            );
            $oldTime = "'.$dayOldTime->clockIn.'";

            $change = 'clockIn =  DATE_FORMAT(clockIn, "%Y-%m-%d '.$dayrequest->checkIn.':%s")';
            $this->complicatedQuery(
                ['dayrequest'],
                "UPDATE day_times SET $change WHERE id = $dayrequest->dayTimeId"
            );
        }elseif($dayrequest->checkOut != ''){
            $dayOldTime = $this->complicatedQuery(
                ['day'],
                'SELECT `clockOut` FROM `day_times` WHERE dayId = "'.$dayId.'" && start_lunch = 0',1
            );
            $oldTime = "'.$dayOldTime->clockOut.'";

            $change = 'clockOut = DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->checkOut.':%s"), clockOutStatus = 1';
            $this->complicatedQuery(
                ['dayrequest'],
                "UPDATE day_times SET $change WHERE dayId = $dayId && start_lunch = 0"
            );
        }elseif($dayrequest->lunchStart != ''){
            $dayClockOut = $this->complicatedQuery(
                ['day_times'],
                'SELECT id, clockOut, clockOutStatus FROM day_times WHERE dayId = "'.$dayId.'" && clockOutStatus = 1',1
            );

            if(isset($dayClockOut->id) && !empty($dayClockOut->id)){

                $dayLunch = $this->complicatedQuery(
                    ['day_times'],
                    'SELECT id FROM day_times WHERE dayId = "'.$dayId.'" && start_lunch = 1',1
                );

                $dayOldTime = $this->complicatedQuery(
                    ['day_times'],
                    'SELECT `clockOut` FROM `day_times` WHERE id = "'.$dayrequest->dayTimeId.'"',1
                );
                $oldTime = "'.$dayOldTime->clockOut.'";

                if(isset($dayLunch->id) && !empty($dayLunch->id)){
                    $change = 'clockOut = DATE_FORMAT(clockOut, "' . $dayDate->date . ' ' . $dayrequest->lunchStart . ':%s")';
                    $this->complicatedQuery(
                        ['day_times'],
                        "UPDATE day_times SET $change WHERE id = $dayrequest->dayTimeId"
                    );
                }else {
                    $change = 'clockOut = DATE_FORMAT(clockOut, "' . $dayDate->date . ' ' . $dayrequest->lunchStart . ':%s"), start_lunch = 1, clockOutStatus = 0';
                    $this->complicatedQuery(
                        ['day_times'],
                        "UPDATE day_times SET $change WHERE id = $dayClockOut->id"
                    );

                    $this->complicatedQuery(
                        ['day_times'],
                        "INSERT INTO day_times (clockOut, clockOutStatus, dayId) VALUES ('" . $dayClockOut->clockOut . "', 1, '" . $dayId . "')"
                    );
                }
            }else{
                $dayTimeId = $this->complicatedQuery(
                    ['dayrequest'],
                    'SELECT dayTimeId FROM dayrequest WHERE id = "'.$id.'"',1
                );

                $dayOldTime = $this->complicatedQuery(
                    ['day'],
                    'SELECT `clockOut` FROM `day_times` WHERE id = "'.$dayTimeId->dayTimeId.'"',1
                );
                $oldTime = "'.$dayOldTime->clockOut.'";

                $change = 'clockOut = DATE_FORMAT(clockIn, "'.$dayDate->date.' '.$dayrequest->lunchStart.':%s"), start_lunch = 1';
                $this->complicatedQuery(
                    ['day_times'],
                    "UPDATE day_times SET $change WHERE id = '".$dayTimeId->dayTimeId."'"
                );
            }
        }elseif($dayrequest->lunchEnd != ''){
            $dayClockOut = $this->complicatedQuery(
                ['day_times'],
                'SELECT id, clockOut FROM day_times WHERE dayId = "'.$dayId.'" && clockOutStatus = 1',1
            );

            $dayTimeId = $this->complicatedQuery(
                ['dayrequest'],
                'SELECT dayTimeId FROM dayrequest WHERE id = "'.$id.'"',1
            );

            $dayOldTime = $this->complicatedQuery(
                ['day'],
                'SELECT `clockOut` FROM `day_times` WHERE id = "'.$dayTimeId->dayTimeId.'"',1
            );
            $oldTime = "'.$dayOldTime->clockOut.'";

            if(isset($dayClockOut->id) && !empty($dayClockOut->id)){
                $change = 'clockIn = DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->lunchEnd.':%s"), end_lunch = 1';
                $this->complicatedQuery(
                    ['day_times'],
                    "UPDATE day_times SET $change WHERE id = '".$dayClockOut->id."'"
                );
            }else{
                $change = 'day_times (clockIn, end_lunch, dayId) VALUES (DATE_FORMAT("'.$dayDate->date.'", "'.$dayDate->date.' '.$dayrequest->lunchEnd.':%s"), 1, "'.$dayId.'")';
                $this->complicatedQuery(
                    ['day_times'],
                    "INSERT INTO $change"
                );
            }
        }

        $this->complicatedQuery(
            ['day_times'],
            "UPDATE dayrequest SET oldTime = $oldTime WHERE id = '".$id."'"
        );
    }

    /**
     * @param null $requestId
     * @param null $type
     * @return bool|null
     * tests the existing status if the new status matches it will return null.
     */
    private function testExistingStatus ($requestId=null, $type=null) {
        if (!isset($this->dayRequestStatus[$type]) || is_null($requestId) || is_null($type)) return null;

        $status = $this->dayRequestStatus[$type];
        $condition = " id = $requestId AND status LIKE '%$status%' ";
        $val = $this->selectRows('dayrequest', 'status', $condition, null, 1);

        if (!empty($val)) return null;

        return true;
    }

    /**
     * @return array|bool|mixed
     * tests if there is a urgent message. (for the menu).
     */
    private function isUrgent () {
        return $this->selectRows('dayrequest','status',' status LIKE "%'. $this->dayRequestStatus['urgent'] .'%" AND status NOT LIKE "%'. $this->dayRequestStatus['read'] .'%" ','','');
    }

    /**
     * @param null $type
     * @param null $dayId
     * @param null $timeLog
     * @param null $requestId
     * @return null|null
     * adds logs(messages) in a table day and in requestMessage(as a message).
     */
    private function autoLog ($type=null, $dayId=null, $requestId=null, $timeLog=null) {
        if (is_null($type) || is_null($requestId) || is_null($dayId) || is_null($timeLog) || !in_array($type, array_keys($this->autoLogMessages))) return null;

        $message = $this->autoLogMessages[$type];
        $message .= $timeLog;

        $logStatus = null;

        if (isset($this->messageStatus[$type])) {
            $logStatus = $this->messageStatus[$type];
        }

        $this->saveMessages([$dayId, $requestId], $message, $logStatus);
        $this->addLogToDay($dayId, $message, $requestId);
    }

    /**
     * @param null $requestId
     * @param null $Id
     * @param null $row
     * @param null $newTime
     * @param null $oldTime
     * @return null|null
     * changes request time.
     */
    private function changeTime ($requestId=null, $Id=null, $row=null, $newTime=null, $oldTime=null) {
        if (is_null($requestId) || is_null($Id) || is_null($newTime) || is_null($row) || is_null($oldTime)) return null;
        $row = trim($row);

        if (isset($this->tableRows[$row])) { // testing if the row exists

            $row = $this->tableRows[$row];

            $this->complicatedQuery(['dayrequest'],'UPDATE `dayrequest` SET ' . $row . ' = :newTime, oldTime=:oldTime, `log_edit_time` = CURRENT_TIMESTAMP() WHERE id=:requestId AND dayId=:dayId', 1, [
                'newTime' => $newTime, 'requestId' => $requestId, 'dayId' => $Id, 'oldTime' => trim($this->post->oldTime)
            ]);

            $this->complicatedQuery(['day'],'UPDATE `day_times` SET ' . $this->post->field . ' = :newTime WHERE Id=:dayTimeId', 1, [
                'newTime' => $this->post->timeAndDate . ' ' . $newTime, 'dayTimeId' => $this->post->dayTimeId
            ]);

            $timeLog = $row . ' time: From ' . $oldTime . ' to ' . $newTime;
            $this->autoLog('edit', $Id, $requestId, $timeLog);
        }
    }

    /**
     * @param null $dayId
     * @param null $message
     * @return null|null
     * concatenate (combines) existing log and new message in a day table.
     */
    private function addLogToDay ($dayId=null, $message=null, $requestId=null) {
        if (is_null($dayId) || is_null($message) || is_null($requestId)) return null;

        $this->complicatedQuery(['dayrequest'],'UPDATE `dayrequest` SET `log_edit_time` = CURRENT_TIMESTAMP(), oldTime=:oldTime WHERE id=:requestId AND dayId=:dayId', 1, [
            'requestId' => $requestId, 'dayId' => $dayId, 'oldTime' => trim($this->post->oldTime)
        ]);

        $this->complicatedQuery(['day'], '
                UPDATE `day` SET 
                    `log` = 
                        CASE
                            WHEN log IS NULL THEN ?
                            ELSE CONCAT(log, " \n ", ?)
                        END
                    WHERE id = ?
            ', 1 , [
            $message, $message, $dayId
        ]);
    }

    /**
     * @return array
     * pagination filter(filters requests by page number).
     */
    private function filterPagination () {
        $filterString = "";
        $filterVarList = [];
        $offset = ($this->pagination['currentPage'] - 1)  * $this->pagination['maxReportsOnOnePage'];

        if (isset($this->url[3]) && $offset != 0) {
            $filterString .= " LIMIT ?, ? ";
            $filterVarList = [$offset, $this->pagination['maxReportsOnOnePage']];
        } else {
            $filterString .= " LIMIT ? ";
            $filterVarList = [$this->pagination['maxReportsOnOnePage']];
        }
        //this was a test for me and my good
        // lol
        return [$filterString, $filterVarList];
    }

    /**
     * @return string
     * gets url and testes it, if there is a filter label (/requests/approved, /requests/urgent ...)
     * it will test if this label is in the dayRequestStatus array and if so it will return that status ID.
     */
    private function filterList () {
        $filterString = "";

        if (isset($this->url[2]) && isset($this->dayRequestStatus[$this->url[2]])) {
            $filterString = ' AND request.status LIKE "%' . $this->dayRequestStatus[$this->url[2]] . '%" ';
        } elseif (isset($this->url[2]) && $this->url[2] == 'unread') {
            $filterString = ' AND request.status NOT LIKE "%' . $this->dayRequestStatus['read'] . '%" ';
        }

        if (isset($this->url[1]) && isset($this->requestInboxTypes[$this->url[1]])) {
            $filterString .= $this->requestInboxTypes[$this->url[1]] . $this->session->userId;
        } else {
            $filterString .= $this->requestInboxTypes[$this->defaultPage] . $this->session->userId;
        }
        $filterString .= ' || FIND_IN_SET('.$this->session->userId.', toManagers) ';
        return $filterString;
    }

    /**
     * @param null $Ids
     * @return mixed|null
     * manly used to decode post ids ([2, 3]).
     */
    private function changeJsonToArray ($Ids=null) {
        if (is_null($Ids) || !is_string($Ids)) return null;
        $Ids = json_decode($Ids);
        if (!is_array($Ids) || empty($Ids)) return null;
        return $Ids;
    }

    /**
     * @param null $Id
     * @param null $Status
     * @param null $setStatus
     * @return null|null
     * changes status generally, but it adds `read` status automatically (if you made a decision to change status you have read it).
     */
    private function changeStatus ($Id=null, $Status=null, $setStatus=null) {
        if (is_null($Id) || is_null($Status) || is_null($setStatus)) return null;

        if (strpos($Status, (string)$this->dayRequestStatus['urgent']) !== false){
            $setStatus .= ',' . $this->dayRequestStatus['urgent'];
        }

        $newStatus = $this->dayRequestStatus['read'] . ',' . $setStatus;
        $this->updateRecords('dayrequest', 'status="' . $newStatus . '"', $Id);
    }

    /**
     * @param null $Id
     * @param null $Status
     * @return null|null
     * changes status Id in dayRequest to declined.
     */
    private function denied($Id=null, $Status=null) {
        return $this->changeStatus($Id, $Status, $this->dayRequestStatus['declined']);
    }

    /**
     * @param null $Id
     * @param null $Status
     * @return null|null
     * changes status Id in dayRequest to approved.
     */
    private function approved ($Id=null, $Status=null) {
        return $this->changeStatus($Id, $Status, $this->dayRequestStatus['approved']);
    }

    /**
     * sets all of the requests in a template.
     */
    private function setMessages () {
        $this->getRequestsMessages($this->post->ids);
        $this->requireTemplates($this->templates['Messages']);
    }

    /**
     * @param null $Ids
     * @param null $Message
     * @return null|null
     * saves new messages, it needs tow Ids and a message to correctly identify toUser and the (fromUser can be gotten from $this->session->userId).
     */
    private function saveMessages ($Ids=null, $Message=null, $isLog=null) {
        if (!is_array($Ids)) $Ids = $this->changeJsonToArray($Ids);
        if (is_null($Ids) || is_null($Message)) return null; // when the login is set needs some work.

        $this->complicatedQuery(['requestmessage'],
            'INSERT INTO requestmessage (dayReqId, reqId, message, fromUser, status, toUser) VALUES (?, ?, ?, ?, ?, ?)
                   ', 1,
            [$Ids[0], $Ids[1], $Message, $this->post->fromUserId, $isLog, $this->post->toUserId]
        );
    }

    /**
     * @return $this
     * gets all of requests. (Or Filters it.)
     */
    public function getAllRequests ($count=null) {
        $filterStatus = $this->filterList();
        $filterPagination = $this->filterPagination();

        $select = implode(', ', $this->selectList);
        if (!is_null($count)) {
            $select = ' COUNT(*) AS Count ';
        }
        $this->data['requests'] = $this->complicatedQuery(['dayrequest', 'day', 'users', 'day_times'], '
            SELECT '. $select .', (SELECT max(sentTime) FROM requestmessage) AS sentTime
                FROM {table0} as request
                    LEFT JOIN  {table1}             ON request.dayId = day.id
                    LEFT JOIN  {table2}             ON day.userId    = `users`.id
                    INNER JOIN {table3} as dayTime  ON dayTime.id    = request.dayTimeId
                WHERE day._deleted = 0 AND users._deleted = 0 ' . $filterStatus . ' ORDER BY request.id DESC
        ' . $filterPagination[0], null, $filterPagination[1]);

        return $this;
    }

    /**
     * @param null $Ids
     * @return null|null
     * this function needs request id and day request id to correctly fiend messages.
     */
    public function getRequestsMessages ($Ids=null) {
        $Ids = $this->changeJsonToArray($Ids);
        if (is_null($Ids)) return null;

        $this->data['messages'] = $this->complicatedQuery(['requestmessage', 'users'],
            'SELECT 
                        message.dayReqId, message.message, message.fromUser, message.toUser, message.sentTime, message.status, message.reqId, users.username, users.name
                    FROM {table0} AS message
                    RIGHT JOIN {table1}
                        ON users.id = message.fromUser
                    WHERE message.dayReqId = :dayReqId AND message.reqId = :reqId ORDER BY message.sentTime ASC
                   ', null,
            [
                'dayReqId' => $Ids[0], 'reqId' => $Ids[1]
            ]);
    }

    /**
     * @param null $fileName
     * @return null|null
     * implements templates from $this->templates.
     */
    public function requireTemplates ($fileName=null) {
        if (is_null($fileName)) return null;

        if (!file_exists($fileName)) die ('file doesn\'t exists.');

        require $fileName;
    }

    /**
     * @param null $Ids
     * @param null $lastTimeUpdate
     * @return null|null
     * test if there is update in messages.
     */
    private function isMessageUpdate ($Ids=null, $lastTimeUpdate=null) {
        if (is_null($Ids) || is_null($lastTimeUpdate)) {
            return null;
        }
        $Ids = $this->changeJsonToArray($Ids);
        if (is_null($Ids)) return null;

        $this->data['messageUpdate'] = $this->complicatedQuery(['requestmessage'], '
	        SELECT * FROM {table0} WHERE dayReqId = ? AND reqId = ? AND sentTime > ?
	    ', 1, [
            $Ids[0], $Ids[1], $lastTimeUpdate
        ]);
    }

    /**
     * @return array
     * basic plan for loading css, js and templates (header, requests, footer)
     */
    public function plan () {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css",
                "header/header.css",
                "header/glyphter.css",
                "footer.css",
                "frame.css",
                "request/glyphter.css",
                "requests.css"
            ],
            'js' => [
                "https://cdn.jsdelivr.net/jquery/latest/jquery.min.js",
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js",
                "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js",
                "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
                "https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js",
                "requests.js",
                "basic.js"
            ],
            'plan' => [
                'header',
                'requests',
                'footer'
            ]
        ];

        return $list;
    }
}