<?php if(!defined('__JAMP__')) {header('HTTP/1.0 404 Not Found'); die();}

use DateTime as _date;
use system\jampModel;

class schedule extends system\jampModel{

    /*
     * list of templates
     */
    public $templates = [
        'newScheduleItem'     => 'app/class/includes/schedule-item.php',
        'newWeekItem'         => 'app/class/includes/schedule-new-week.php',
        'calendar'            => 'app/class/includes/calendar/base.php',
        'EmployeesInSchedule' => 'app/class/includes/employeesInSchedule.php',
        'calendarEnd'         => 'app/class/includes/calendar/calendarEnd.php',
        'calendarStart'       => 'app/class/includes/calendar/calendarStart.php'
    ];

    /*
     * saves new schedule id and saves it, so this id can be used in template
     */
    public $newScheduleId = null;

    /*
     * max week id
     */
    public $maxWeekId = null;

    /*
     * _deleted field status Ids
     */
    private $status = [
        '_deleted' => [
            'deleted' => 1
        ]
    ];

    /*
     * shift table rows (for testing)
     */
    private $shiftTable = [
        'clockIn',
        'clockOut',
        'lunchStart',
        'lunchEnd',
        'dayRange',
        'duration',
        'monsDay',
        'scheduleId',
        'status',
        "lunchType",
        "endLunch",
        "startLunch"
    ];

    /*
     * error cods and there names
     */
    private $errorCodes = [
        "weekLimit" => "000"
    ];

    /*
     *  week days
     */
    private $weekDays = [
        "Mon"   => 1,
        "Tues"  => 2,
        "Wed"   => 3,
        "Thurs" => 4,
        "Fri"   => 5,
        "Sat"   => 6,
        "Sun"   => 7
    ];

    /*
     * user schedule update
     */
    private $updateType = [
        "removeFromSchedule" => 2,
        "AddFromSchedule"    => 1
    ];

    /*
     * max number of weeks
     */
    private $weekInScheduleLimit = 4;

    /**
     * @return void|null
     * controls witch page to load
     */
    public function index () {

        if (isset($this->post) && isset($this->post->type)) {
            switch ($this->post->type) {
                case 'addUsersToSchedule' :
                        $this->addUsersToSchedule();
                    break;
                case 'addUsersToNewSchedule' :
                    $this->addUsersToNewSchedule();
                    break;
                case 'changeStatus' :
                    $this->changeShiftStatus();
                    break;
                case 'saveLunch' :
                    $this->saveLunch();
                    break;
                case 'removeLunch' :
                    $this->removeLunch();
                    break;
                case 'scheduleSend' :
                    $this->scheduleSend();
                    break;
                case 'loopSchedule' :
                    $this->loopDays();
                    break;
                case 'addWeek' :
                        $this->addShift();
                        $this->loadTemplate('newWeekItem');
                    break;
                case 'deleteSchedule' :
                        $this->deleteSchedule();
                    break;
                case 'addTimeInputs' :
                        $this->insertWeekDayTime();
                    break;
                case 'getScheduleItem' :
                        $newScedule = $this->addSchedule();
                        $this->newScheduleId = $newScedule['id'];
                        $this->startDate = $newScedule['startDate'];
                        $this->loadTemplate('newScheduleItem');
                    break;
                case 'removeWeek' :
                        $this->deletedWeek($this->post->shiftIds);
                    break;
                case 'showCalendar' :
                    $this->showCalendar();
                    break;
                case 'showEmployeesInSchedule' :
                    $this->showEmployeesInSchedule();
                    break;
            }
            die();
        }

        $this->getSchedule();
        $this->getShifts();

        $this->data['groupsList'] = $this->getGroups();
        $this->data['usersList'] = $this->getUsers();
    }

    public function showEmployeesInSchedule () {
        $this->data['users'] = $this->getUsers();
        $this->data['groupsList'] = $this->getGroups();
        $this->data['ScheduleAndGroups'] = $this->getGroupsAndSchedules();
        $this->getSchedule();

        $this->loadTemplate('EmployeesInSchedule');
    }

    public function showCalendar() {
        $this->data['startYear'] = 2019;
        $this->data['endYear'] = 2021;
        $this->loadTemplate('calendar');
    }

    /**
     * @param null $templateName
     * @return null|null
     * includes template from $this->templates
     */
    public function loadTemplate ($templateName=null) {
        if (is_null($templateName)) return null;

        include $this->templates[$templateName];
    }

    /**
     * save and delete users from the schedule
     */
    public function scheduleSend () {
       // if ((isset($this->post->users) && empty($this->post->users)) || !isset($this->post->users) || isset($this->post->status) || isset($this->post->sendType) || isset($this->post->updateType) || isset($this->post->userOrGroupType))  return null;

        if ($this->post->updateType == 2) {
            if (count($this->post->users) > 1) {
                $this->complicatedQuery(['users_new_schedule'], 'UPDATE users SET scheduleId = ? WHERE id IN (' . implode(',', $this->post->users) . ')', false, [
                    NULL
                ]);
            } else {
                $this->complicatedQuery(['users_new_schedule'], 'UPDATE users SET scheduleId = ? WHERE id = ' . $this->post->users[0], false, [
                    NULL
                ]);
            }
            /*if ($this->userOrGroup['user'] == $this->post->userOrGroupType) {
                $this->complicatedQuery(['users_new_schedule'], 'INSERT INTO users_new_schedule (scheduleId, startDate, user_id, notification) VALUES ( ?, ?, ?, ?)', false, [

                ]);
            } else if ($this->userOrGroup['group'] == $this->post->userOrGroupType) {
                $this->complicatedQuery(['users_new_schedule'], 'INSERT INTO users_new_schedule (scheduleId, startDate, user_id, notification) VALUES ( ?, ?, ?, ?)', false, [

                ]);
            }*/
        } else {
            /*$this->complicatedQuery(['users_new_schedule'], 'UPDATE users SET scheduleId = ? ', false, [
                NULL
            ]);*/
        }
    }

    /**
     * @return null
     */
    public function addUsersToNewSchedule () {
        if (!isset($this->post->schedule) || !isset($this->post->listOfUsers) || !isset($this->post->startDate)) return null;

        $userIds = array_keys($this->post->listOfUsers);

        for ($i=0; $i < count($userIds); $i++) {
            $this->complicatedQuery(['users_new_schedule'], 'INSERT INTO users_new_schedule SET scheduleId = ?, startDate = ?, user_id = ?', false, [
                $this->post->schedule, $this->post->startDate, $userIds[$i]
            ]);
        }
    }

    /**
     * @return null|void
     * addes users to schedule
     */
    public function addUsersToSchedule () {
        if (empty($this->post->listOfUsers) || !isset($this->post->scheduleId)) return null;

        if (count($this->post->listOfUsers) == 1) {
            $this->complicatedQuery(['users'], 'UPDATE users SET scheduleId = ? WHERE id = ?', false, [
                $this->post->scheduleId, array_keys($this->post->listOfUsers)[0]
            ]);
            return;
        }

        $this->complicatedQuery(['users'], 'UPDATE users SET scheduleId = ? WHERE id IN (' . implode(array_keys($this->post->listOfUsers), ',') . ')', false, [
            $this->post->scheduleId
        ]);
    }

    /**
     * @return null
     * loop days from schedule
     */
    public function loopDays () {
        if (!isset($this->post->copyList) || !isset($this->post->loopOver)) return null;

        $i = 0;
        foreach ($this->post->loopOver as $key => $id) {

            $copy = $this->complicatedQuery(['shift'], 'SELECT clockIn, clockOut, lunchStart, lunchEnd, dayRange, duration, workingDuration, lunchType, status FROM shift WHERE id = ?', true, [
                $this->post->copyList[$i]
            ]);

            $this->complicatedQuery(['shift'], '
                UPDATE shift 
                    SET 
                        clockIn = ?, clockOut = ?, lunchStart = ?, lunchEnd = ?, dayRange = ?, duration = ?, workingDuration = ?, lunchType = ?,  status = ?
                WHERE shift.id = ?', true, [
                $copy->clockIn, $copy->clockOut, $copy->lunchStart, $copy->lunchEnd, $copy->dayRange, $copy->duration, $copy->workingDuration, $copy->lunchType, $copy->status, $id
            ]);

            $i++;
            if (!isset($this->post->copyList[$i])) {
                $i = 0;
            }

        }
    }

    /**
     *  sets _deleted Id in shift and schedule to deleted
     */
    public function deleteSchedule () {
        $this->complicatedQuery(['shift'], 'UPDATE shift SET _deleted = ? WHERE scheduleId = ?', true, [
            $this->status['_deleted']['deleted'], $this->post->id
        ]);

        $this->complicatedQuery(['users'], 'UPDATE users SET scheduleId = ? WHERE scheduleId = ?', true, [
            NULL, $this->post->id
        ]);

        $this->complicatedQuery(['schedule'], 'UPDATE schedule SET _deleted = ? WHERE id = ?', true, [
            $this->status['_deleted']['deleted'], $this->post->id
        ]);

        die ();
    }

    /**
     * @return array|bool|mixed|null
     * adds a new schedule (just name and date)
     */
    public function addSchedule () {
        if (empty($this->post) || !isset($this->post->name)) return null;

        $returnData = [];

        $returnData['id'] = $this->complicatedQuery(['schedule'], 'INSERT INTO schedule SET title = ?, StartDate = ?', true, [
            $this->post->name, date("Y-m-d", strtotime($this->post->startDate))
        ], true);


        $this->data['groupsList'] = $this->getGroups();
        $this->data['usersList']  = $this->getUsers();
        $returnData['startDate']  = $this->post->startDate;

        return $returnData;
    }

    /**
     * selects and saves schedules (only title start date, lunch forced and _deleted)
     */
    public function getSchedule () {
        $this->data['schedules'] = $this->complicatedQuery(['schedule'], 'SELECT *, (SELECT count(*) FROM users WHERE active = 1 AND _deleted = 0 AND scheduleId = schedule.id) AS userCount FROM schedule WHERE _deleted != ? ORDER BY id', false,[
            $this->status['_deleted']['deleted']
        ]);
    }

    /**
     * selects and saves groups
     */
    public function getGroups ($scheduleId=null) {
        if (is_null($scheduleId)) {
            return $this->complicatedQuery(['groups'], '
            SELECT *, 
	                (SELECT COUNT(*) FROM users AS Users WHERE FIND_IN_SET(Users.id, Groups.users) AND Users.active = 1 AND Users._deleted = 0) AS freeFromSchedule,
                    (SELECT COUNT(*) FROM users AS Users WHERE FIND_IN_SET(Users.id, Groups.users) AND Users.active = 1 AND Users._deleted = 0 AND Users.scheduleId IS NOT NULL) AS notFreeFromSchedule
    	        FROM groups AS Groups WHERE Groups._deleted != 1', false);
        }

        return $this->complicatedQuery(['groups'], '
            SELECT *, 
	              (SELECT COUNT(*) FROM users AS Users WHERE FIND_IN_SET(Users.id, Groups.users)) AS freeFromSchedule,
                  (SELECT COUNT(*) FROM users AS Users WHERE FIND_IN_SET(Users.id, Groups.users) AND Users.scheduleId IS NOT NULL) AS notFreeFromSchedule
    	        FROM groups AS Groups WHERE Groups._deleted != 1', false);
    }

    /**
     * @param null $scheduleId
     * @return null|null
     * gets shift information by schedules id
     */
    public function getShifts ($scheduleId=null) {
        if (is_null($scheduleId)) {
             $tmp = $this->complicatedQuery(['shift'], 'SELECT * FROM shift WHERE _deleted != ?', false, [ $this->status['_deleted']['deleted'] ]);
             $returnData = [];

             foreach ($tmp as $data) {
                 $returnData['shift-' . $data->scheduleId][] = $data;
             }
             if (!empty($tmp)) {
                 $this->data['shifts'] = $returnData;
             }

             return null;
        }

        $this->data['shift-' . $scheduleId] = $this->complicatedQuery(['shift'], 'SELECT * FROM shift WHERE scheduleId = ? AND _deleted != ?', false, [
            $scheduleId, $this->status['_deleted']['deleted']
        ]);
    }

    /**
     * @param null $shiftsId
     * sets _deleted to 1
     */
    public function deletedShifts ($shiftsId=null) {
        $shifts = $this->complicatedQuery(['shift'], 'SELECT * FROM shift WHERE id = ?', true, [
            $shiftsId
        ]);

        $this->complicatedQuery(['shift'], 'UPDATE shift SET _deleted = ? WHERE id = ?', true, [
            $this->status['_deleted']['deleted'], $shiftsId
        ]);
    }

    /**
     * @param null $scheduleId
     *  sets _deleted to 1
     */
    public function deletedSchedule ($scheduleId=null) {
        $this->complicatedQuery(['schedule'], 'UPDATE schedule SET _deleted = ? WHERE id = ?', true, [
            $this->status['_deleted']['deleted'], $scheduleId
        ]);
    }


    /**
     * @param null $shiftId
     * @return null
     */
    private function deletedWeek ($shiftId=null) {
        if (is_null($shiftId)) return null;

        $shiftIds = substr(substr($shiftId, 0, strlen($shiftId)-1),1);
        $schedule = $this->complicatedQuery(['schedule'], 'SELECT scheduleId, weekId FROM shift WHERE id IN ('.$shiftIds.')', true); //update and fix this

        if ($schedule->weekId != 4) {
            $this->complicatedQuery(['schedule'], 'UPDATE shift SET weekId = weekId-1 WHERE scheduleId = ? AND weekId > ?', true, [
                $schedule->scheduleId, $schedule->weekId
            ]);
        }

        $this->complicatedQuery(['schedule'], 'UPDATE shift SET _deleted = ? WHERE id IN ('.$shiftIds.')', true, [ //update and fix this
            $this->status['_deleted']['deleted']
        ]);
    }

    /**
     * @param null $scheduleId
     * @return null|null
     * returns how match weeks there ara
     */
    public function getMaxWeekId ($scheduleId=null) {
        if (is_null($scheduleId)) return null;

        $maxWeekId = $this->complicatedQuery(['shift'], 'SELECT MAX(weekId) AS weekId FROM shift WHERE scheduleId = ? AND _deleted != ?', true, [
            $scheduleId, $this->status['_deleted']['deleted']
        ]);

        $this->maxWeekId = empty($maxWeekId->weekId) ? 1 : $maxWeekId->weekId+1;
        $this->data['weekDay'] = $this->maxWeekId;
    }

    /**
     * @return null|null
     * add weeks in shift table
     */
    public function addShift () {
        if (!isset($this->post) || !isset($this->post->scheduleId)) return null;

        $this->getMaxWeekId($this->post->scheduleId);

        if ($this->weekInScheduleLimit < $this->maxWeekId) {
            echo $this->errorCodes['weekLimit'];
            die();
        }

        $this->data['weekDayId'] = [];
        $this->data['weekDay'] = $this->maxWeekId;

        foreach ($this->weekDays as $name => $id) {
            $this->data['weekDayId'][$id] = $this->complicatedQuery(['shift'],'INSERT INTO shift SET weekId = ?, weekDay = ?, scheduleId = ?', true, [
                $this->maxWeekId, $id, $this->post->scheduleId
            ], true);
        }
    }

    /**
     * @return array|bool|mixed
     */
    public function  getUsersOutOfGroups () {
        return $this->complicatedQuery(['shift'], 'SELECT * FROM `users` WHERE `_deleted` = 0 AND `active` = 1 AND `scheduleId` IS NULL AND TRIM(`name`) != "" AND TRIM(`last_name`) != "" ');
    }

    /**
     *  saves lunch by type
     */
    public function saveLunch () {
        var_dump($this->post->LunchType);
        if ($this->post->LunchType == "fixed" || is_int($this->post->LunchType) && $this->post->LunchType == 0) {
            var_dump("fixed");
            $this->complicatedQuery(['shift'], 'UPDATE shift SET duration = ?, lunchStart = ?, lunchType = ? WHERE id = ?', true, [
                $this->post->duration, $this->post->timeFrom, 0, $this->post->shiftId
            ]);

        } elseif ($this->post->LunchType == "range" ||  is_int($this->post->LunchType) && $this->post->LunchType == 1) {
            var_dump("range");
            $this->complicatedQuery(['shift'], 'UPDATE shift SET duration = ?, lunchStart = ?, lunchEnd = ?, lunchType = ? WHERE id = ?', true, [
                $this->post->duration, $this->post->timeFrom, $this->post->timeTo, 1, $this->post->shiftId
            ]);

        } elseif ($this->post->LunchType == "flexible" ||  is_int($this->post->LunchType) && $this->post->LunchType == 2) {
            var_dump("flexible");
            $this->complicatedQuery(['shift'], 'UPDATE shift SET duration = ?, lunchType = ? WHERE id = ?', true, [
                $this->post->duration, 2, $this->post->shiftId
            ]);

        }
    }

    /**
     * removes lunch from shift
     */
    public function removeLunch () {
        $this->complicatedQuery(['shift'], 'UPDATE shift SET duration = ?, lunchStart = ?, lunchEnd = ?, lunchType = ? WHERE id = ?', true, [
             NULL, NULL, NULL, NULL, $this->post->shiftId
        ]);
    }

    /**
     * @return array|bool|mixed
     * gets groups and schedule title and Ids
     */
    public function getGroupsAndSchedules () {
        return $this->complicatedQuery(['users'], 'SELECT S.title AS ScheduleTitle, S.id AS ScheduleId,  
    	    (SELECT group_concat(distinct GU.id) FROM users AS U LEFT JOIN groups AS GU ON FIND_IN_SET(U.id, GU.users) WHERE GU._deleted = 0 AND GU._deleted = 0 AND U.scheduleId = S.id) AS GroupsIds
          FROM schedule AS S WHERE S._deleted = 0');
    }

    /**
     *  adds times to a week by type of the type
     */
    public function insertWeekDayTime () {
        if ($this->post->typeAddTimeInputs == 1) {
            $this->complicatedQuery(['shift'], 'UPDATE shift SET clockIn = ?, dayRange = ?, workingDuration = ?, clockOut = ? WHERE id = ? AND status = 1', true, [
                $this->post->clickInTime, $this->post->rangeTime, $this->post->workingDuration, $this->post->clickInTime + $this->post->workingDuration . ":00:00", $this->post->shiftId
            ]);
        } else {
            $this->complicatedQuery(['shift'], 'UPDATE shift SET clockIn = ?, clockOut = ?, clockOutWeekDay = ?, dayRange = null, workingDuration = null WHERE id = ? AND status = 1', true, [
                $this->post->from, $this->post->to, $this->post->clockOutWeekDay, $this->post->shiftId
            ]);
        }

    }

    /**
     * @param null $currentScheduleId
     * @return array|bool|mixed
     * returns user list
     */
    public function getUsers ($currentScheduleId=null) {
        if (is_null($currentScheduleId)) {
            return $this->complicatedQuery(['shift'],'
                SELECT
                	Groups.users, Users.id, Users.scheduleId, Users.name, Groups.id AS groupId, 
                	Users.last_name, Groups.title, (SELECT scheduleId FROM users_new_schedule AS uns WHERE Users.id = uns.user_id LIMIT 1) AS unsScheduleId, Users.is_company AS isCompany
                FROM users as Users 
                	LEFT JOIN groups AS Groups ON FIND_IN_SET(Users.id, Groups.users) WHERE Users.active = 1 AND Users._deleted = 0', false);

        }

        return $this->complicatedQuery(['shift'], '
            SELECT 
                	Groups.users, Users.id, Users.scheduleId, Users.name, Groups.id AS groupId, 
                	Users.last_name, Groups.title, (SELECT scheduleId FROM users_new_schedule AS uns WHERE Users.id = uns.user_id LIMIT 1) AS unsScheduleId 
                FROM users as Users 
                	LEFT JOIN groups AS Groups ON FIND_IN_SET(Users.id, Groups.users) WHERE Users.active = 1 AND Users._deleted = 0 AND Users.scheduleId = ?', true, [
            1, $this->status['_deleted']['deleted'], $currentScheduleId
        ]);
    }

    /**
     * changes shift status
     */
    public function changeShiftStatus () {
        $this->complicatedQuery(['shift'], 'UPDATE shift SET status = ?, clockIn = null, dayRange = null, workingDuration = null, clockOut = null WHERE id = ?', true, [
            $this->post->status, $this->post->shiftId
        ]);
    }

    /**
     * @return array|bool|mixed
     * select all schedules
     */
    public function SelectAll () {
        return $this->complicatedQuery("SELECT s.id AS scheduleId, g.title FROM `schedule`  AS s INNER JOIN users  AS u ON s.id = u.scheduleId INNER JOIN groups AS g ON u.id IN (g.users)");
    }

    /**
     * @return array
     * basic plan for the page
     */
    public function plan() {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
                "https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css",
                "header/header.css",
                "header/glyphter.css",
                "footer.css",
                "frame.css",
                "basic.css",
                "schedule.css"
            ],
            'js' => [
                "https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous",
                "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js", // For Tooltips to work
                "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js",
                "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
                "schedule.js",
                "https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js",
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js",
                "basic.js"
            ],
            'plan' => [
                'header',
                'schedule',
                'footer'
            ]
        ];
        return $list;
    }
}