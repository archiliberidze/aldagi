<?php

if (!defined('__JAMP__')) exit("Direct access not permitted.");

use DateTime as _date;
use system\jampModel;

class quick extends system\jampModel
{
    private $now = null;

    function index()
    {
        if((isset($this->url[1]) && !empty($this->url[1])) || isset($this->post)) {
            if(isset($this->url[1])) {
                $this->setSession('unical_link', $this->url[1]);
                $unical_link = $this->url[1];
            }else{
                $unical_link = $this->session->unical_link;
            }
            $data = $this->complicatedQuery(
                ['users'],
                'SELECT company.name FROM `a-jamp-timepunch`.users
                    JOIN `a-jamp-timepunch`.company ON users.company_id = company.id
                    WHERE users.unical_link = "'.$unical_link.'"
                   ', 1
            );
           if(isset($data->name)){
            $db = 'a-jamp-timepunch_'.$data->name;

                if (isset($this->post)) {
                    $today = date("Y-m-d");
                    $weekDayNum = date('N');
                    if (isset($this->post->pin) && is_numeric($this->post->pin)) {
                        $this->data['users'] = $this->complicatedQuery(
                            ['users', 'positions'],
                            'SELECT users.pin, users.id as userId, users.scheduleId, users.image, users.email, users.name as user_name, users.last_name, positions.name as positionName, positions.id as positionId 
                                    FROM `' . $db . '`.users
                                    JOIN `' . $db . '`.positions ON FIND_IN_SET(users.id, positions.users)
                                   WHERE users._deleted = 0 AND users.pin ="' . $this->post->pin . '"', 1
                        );

                        $this->data['day'] = $this->complicatedQuery(
                            ['day', 'day_times'],
                            'SELECT day.*, day.id as day_unic_id, day_times.* FROM `' . $db . '`.day
                                LEFT JOIN `' . $db . '`.day_times ON day_times.dayId = day.id
                                WHERE day.date ="' . $today . '" && day.userId = "' . $this->data['users']->userId . '" ORDER BY day_times.id DESC', 1
                        );

                        $this->data['shift'] = $this->complicatedQuery(
                            ['shift'],
                            'SELECT * FROM `' . $db . '`.shift WHERE id = "' . $this->data['day']->shiftId . '"', 1
                        );

                        echo json_encode($this->data);
                        die();
                    }
                    if (isset($this->post->action) && is_numeric($this->post->action) && isset($this->post->dayId) && is_numeric($this->post->dayId)) {
                        switch ($this->post->action) {
                            case '1':
                            {
                                $this->checkIn($this->post->dayId, $db);
                                break;
                            }
                            case '2':
                            {
                                $this->checkOut($this->post->dayId, $db);
                                break;
                            }
                        }
                        die();
                    }

                    if (isset($this->post->lunch)) {
                        if ($this->post->lunch == 1) {
                            $this->lunchStart($this->post->dayId, $db);
                        }
                        if ($this->post->lunch == 2) {
                            $this->lunchEnd($this->post->dayId, $db);
                        }
                        die();
                    }
                }
            }else{
                header('location:'.ROOT_URL);
                exit();
            }
        }else{
            header('location:'.ROOT_URL);
            exit();
        }
    }

    function checkIn($dayId, $db)
    {
        $this->data['shift'] = $this->complicatedQuery(
            ['shift'],
            'SELECT * FROM `'.$db.'`.shift WHERE id = "'.$this->post->shiftId.'"', 1
        );

        $this->data['day'] = $this->complicatedQuery(
            ['day', 'day_times'],
            'SELECT day.*, day.id as day_unic_id, day_times.* FROM `'.$db.'`.day
                            LEFT JOIN `'.$db.'`.day_times ON day_times.dayId = day.id
                            WHERE day.id ="' . $this->post->dayId . '"', 1
        );

        $clockInTime = date("Y-m-d h:m:i");
        $timeZone = $this->get_time_zone($dayId, $db);

        print_R(date_default_timezone_get ());
        $this->complicatedQuery(
            ['day_times'],
            'INSERT INTO `'.$db.'`.day_times (clockIn, dayId) VALUES (CONVERT_TZ(NOW(), @@session.time_zone, "'.$timeZone->value.'"), "' . $this->post->dayId . '")'
        );

        if (empty($this->data['shift']->dayRange)) {
            if($clockInTime > $this->data['shift']->clockIn){
                $status = 502;
            }else{
                $status = 503;
            }
        } else {
            $checkIntimeDb = strtotime("+" . $this->data['shift']->dayRange . " minutes", strtotime($clockInTime));
            if ($clockInTime > $checkIntimeDb) {
                $status = 502;
            } else {
                $status = 503;
            }
        }

        $this->complicatedQuery(
            ['day'],
            "UPDATE `$db`.`day` SET `status` = if(`status` is null or `status` = '', $status, concat(concat(status,','),$status))  WHERE `id` = '".$this->post->dayId."'"
        );
        echo 'true';
    }

    function lunchStart($dayId, $db)
    {
        $status = 504;
        $timeZone = $this->get_time_zone($dayId, $db);
        $this->complicatedQuery(
            ['day_times'],
                   "UPDATE `$db`.`day_times` SET clockOut = CONVERT_TZ(NOW(), @@session.time_zone, '$timeZone->value'), start_lunch = 1 WHERE dayId = '".$this->post->dayId."'  ORDER BY id DESC LIMIT 1"
        );
        $this->complicatedQuery(
            ['day'],
            "UPDATE `$db`.`day` SET `status` = if(`status` is null or `status` = '', $status, concat(concat(status,','),$status))  WHERE `id` = '".$this->post->dayId."'"
        );
        echo 'Lunch start success';
    }

    function lunchEnd($dayId, $db)
    {
        $status = 505;
        $timeZone = $this->get_time_zone($dayId, $db);

        $this->complicatedQuery(
            ['day_times'],
            'INSERT INTO `'.$db.'`.day_times (clockIn, end_lunch, dayId) VALUES (CONVERT_TZ(NOW(), @@session.time_zone, "'.$timeZone->value.'"), 1, "' . $this->post->dayId . '")'
        );
        $this->complicatedQuery(
            ['day'],
            "UPDATE `$db`.`day` SET `status` = if(`status` is null or `status` = '', $status, concat(concat(status,','),$status))  WHERE `id` = '".$this->post->dayId."'"
        );

        echo 'Lunch end success';
    }

    function checkOut($dayId, $db)
    {


        $this->data['checkDay'] = $this->complicatedQuery(
            ['day_times'],
            'SELECT * FROM `'.$db.'`.day_times WHERE dayId ="' . $dayId . '" ORDER BY id DESC', 1
        );

        $timeZone = $this->get_time_zone($dayId, $db);
        if($this->data['checkDay']->start_lunch == 1 && $this->data['checkDay']->end_lunch == 0){
            $status = "505,507";
            $this->complicatedQuery(
                ['day_times'],
                'INSERT INTO `'.$db.'`.day_times (clockIn, clockOut, end_lunch, dayId, clockOutStatus) VALUES (CONVERT_TZ(NOW(), @@session.time_zone, "'.$timeZone->value.'"), CONVERT_TZ(NOW(), @@session.time_zone, "'.$timeZone->value.'"), 1, "' . $this->post->dayId . '", "1")'
            );
            $this->complicatedQuery(
                ['day'],
                "UPDATE `$db`.`day` SET `status` = if(`status` is null or `status` = '', $status, concat(concat(status,','),$status))  WHERE `id` = '".$this->post->dayId."'"
            );
        }else{
            $status = 507;
            $this->complicatedQuery(
                ['day'],
                "UPDATE `$db`.`day_times` SET `clockOutStatus` = 1, clockOut = CONVERT_TZ(NOW(), @@session.time_zone, '".$timeZone->value."') WHERE dayId = '".$this->post->dayId."' ORDER BY id DESC LIMIT 1"
            );
            $this->complicatedQuery(
                ['day'],
                "UPDATE `$db`.`day` SET `status` = if(`status` is null or `status` = '', $status, concat(concat(status,','),$status))  WHERE `id` = '".$this->post->dayId."'"
            );
        }
        echo 'Check out success';
    }

    function settingsSelect($dayId, $db)
    {
        $this->data['data'] = $this->complicatedQuery(
            ['`'.$db.'`.users', '`'.$db.'`.schedule', '`'.$db.'`.day'],
            'SELECT a.*, a.id as userId, b.*,  b.id as scheduleId, c.* FROM {table0} a
                        JOIN {table1} b ON FIND_IN_SET(a.id, b.users)
                        JOIN {table2} c ON c.userId = a.id
                        WHERE a._deleted = 0 AND b._deleted = 0 AND c.id = "' . $dayId . '"', 1
        );
        return $this->data['data'];
    }

    function timeRound()
    {
        $this->data['system'] = $this->selectRows('system-settings', 'value', '_deleted = 0', null, 1);
        return $this->data['system'];
    }

    function plan()
    {
        $list = [
            'css' => [
                "animate.css",
                "quick/style.css",
                "quick/circle_ani_1.css",
                "quick/circle_ani_2.css",
                "quick/diagonal_ani_1.css",
                "quick/slide_in_ani_1.css",
                "quick/modal_ani_1.css",
            ],
            'js' => [
                //"https://use.fontawesome.com/releases/v5.0.13/js/solid.js\" integrity=\"sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ crossorigin=\"anonymous",
                //"https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js\" integrity=\"sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY\" crossorigin=\"anonymous",
                "https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous",
                "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js",
                "vue.js",
                "basic.js",
                "quick.js"
            ],
            'plan' => [
                'quick'
            ]
        ];
        return $list;
    }

    public function get_time_zone($dayId, $db){
        $this->data['timezone'] = $this->complicatedQuery(
            [null],
            'SELECT timezones.* FROM `'.$db.'`.day
                    LEFT JOIN `'.$db.'`.users ON users.id = day.userId    
                    LEFT JOIN `'.$db.'`.company ON company.id = users.company_id    
                    LEFT JOIN `'.$db.'`.timezones ON timezones.id = company.timezone_id    
                    WHERE day.id = "'.$dayId.'"', 1
        );

        return $this->data['timezone'];
    }
}