<?php if (!defined('__JAMP__')) exit("Direct access not permitted.");

class home extends system\jampModel
{

    function index()
    {
        $user_id = $this->session->userId;
        $company_id = $this->session->companyId;

        if (isset($this->post) && isset($this->post->action)) {
            switch ($this->post->action) {
                case 'get_current_timepunch':
                    $time_zone = $this->get_time_zone($company_id);
                    $group_id=$this->post->group_id;
                    $check_group = $group_id=='' ? '' : " AND  find_in_set($group_id,user_groups.user_group)";
                    $this->data['result'] = $this->complicatedQuery(null, "SELECT concat(users.name, ' ', users.last_name)                                                as name,
                   users.overtime                                                                         as user_overtime,
                   GROUP_CONCAT(IFNULL(date_format(day_times.clockIn, '%H:%i'), 0), ',',
                                IFNULL(date_format(day_times.clockOut, '%H:%i'), date_format(CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone'), '%H:%i')) ORDER BY day_times.id
                                asc)                                                                       as clockin_clockout,
                   IFNULL(GROUP_CONCAT(TIMESTAMPDIFF(minute, IFNULL(day_times.clockIn, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')), IFNULL(day_times.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone'))),
                                       ',', IFNULL(TIMESTAMPDIFF(minute, IFNULL(day_times.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')), (SELECT dt.clockIn
                                                                                                             FROM day_times as dt
                                                                                                             WHERE dt.id > day_times.id
                                                                                                               and dt.dayId = day.id
                                                                                                             LIMIT 1)), 0)
                                       ORDER BY day_times.id asc), 0)                                      as percent,
                   IFNULL(SUM(TIMESTAMPDIFF(minute, IFNULL(day_times.clockIn, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')), IFNULL(day_times.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))+
                                       IFNULL(TIMESTAMPDIFF(minute, IFNULL(day_times.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')), (SELECT dt.clockIn
                                                                                                             FROM day_times as dt
                                                                                                             WHERE dt.id > day_times.id
                                                                                                               and dt.dayId = day.id
                                                                                                             LIMIT 1)), 0)+IF(TIMESTAMPDIFF(minute, shift.clockOut, IFNULL(
               (SELECT clockOut FROM day_times WHERE day_times.dayId = day.id order by day_times.id desc LIMIT 1),
               CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))<0,0,TIMESTAMPDIFF(minute, shift.clockOut, IFNULL(
               (SELECT clockOut FROM day_times WHERE day_times.dayId = day.id order by day_times.id desc LIMIT 1),
               CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))     ) 
                                       ), 0)                                    as percent_sum,
                    (TIMESTAMPDIFF(minute, TIME ((SELECT IFNULL(  dt.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone'))
                               FROM day_times as dt
                               WHERE dt.id = MAX(day_times.id))), shift.clockOut))                       as must_end_percent,
                   IF((SELECT SUM(IF(day_times.clockOut IS NULL ,1,0)) FROM day_times WHERE dayId=day.id ),0,1)   as check_must_end,
                    IF(TIMESTAMPDIFF(minute, shift.clockOut, IFNULL(
               (SELECT clockOut FROM day_times WHERE day_times.dayId = day.id order by day_times.id desc LIMIT 1),
               CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))<0,0,TIMESTAMPDIFF(minute, shift.clockOut, IFNULL(
               (SELECT clockOut FROM day_times WHERE day_times.dayId = day.id order by day_times.id desc LIMIT 1),
               CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))     )                                   as overtime_percent,
                    concat(IF((SUM(timestampdiff(second, day_times.clockIn,
                                                IFNULL(day_times.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))) -
                              timestampdiff(second, shift.clockIn, shift.clockOut) ) > 0, '', '-'),
                          date_format(sec_to_time(abs(SUM(timestampdiff(second, day_times.clockIn, IFNULL(day_times.clockOut,
                                                       CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')))) -
                                                      timestampdiff(second, shift.clockIn, shift.clockOut) )), '%H:%i'))          as overtime,
                   ABS(IFNULL(TIMESTAMPDIFF(minute, shift.clockIn, min(day_times.clockIn)),0))             as late_percent,
                   GROUP_CONCAT(day_times.end_lunch , ',', day_times.start_lunch ORDER BY day_times.id asc) as start_lunch_end_lunch,
                   date_format(shift.clockIn, '%H:%i')                                                        must_start,
                   date_format(shift.clockOut, '%H:%i')                                                       must_end,
                   IF(time(shift.clockOut) <= time((SELECT IFNULL(day_times.clockOut, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone'))
                                                   FROM day_times
                                                   WHERE day_times.dayId = day.id
                                                   order by day_times.id desc
                                                   LIMIT 1)), 1, 0)                                        as check_overtime,
                   IF(time(shift.clockIn) >= time((SELECT IFNULL(day_times.clockIn, CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone'))
                                                  FROM day_times
                                                  WHERE day_times.dayId = day.id
                                                  order by day_times.id
                                                  LIMIT 1)), 1, 0)                                         as check_start
            
            FROM users
                     LEFT JOIN day ON day.userId = users.id
                     JOIN (SELECT users.id as user_id, group_concat(`groups`.id) as user_group   FROM users
                           LEFT JOIN `groups` ON find_in_set(users.id,`groups`.users)>0
                           group by users.id) as  user_groups ON user_groups.user_id=users.id
                     JOIN shift ON shift.id = day.shiftId
                     LEFT JOIN day_times ON day_times.dayId = day.id
            where day.date = curdate() AND users.active=1  $check_group
            GROUP BY users.id");
                    $count = 0;
                    foreach ($this->data['result'] as $res) {
                        $this->data['result'][$count]->clockin_clockout = explode(',', $this->data['result'][$count]->clockin_clockout);
                        $this->data['result'][$count]->percent = explode(',', $this->data['result'][$count]->percent);
                        $this->data['result'][$count]->start_lunch_end_lunch = explode(',', $this->data['result'][$count]->start_lunch_end_lunch);
                        $count++;
                    }

                    break;
                case 'get_attendance':
                    $time_zone = $this->get_time_zone($company_id);
                          $this->data['result'] = $this->complicatedQuery(null, "SELECT GROUP_CONCAT(each_attandance.attandance_in_week order by each_attandance.date ) as attandance_in_week,
                           IF(SUM(each_attandance.attandance_in_week)>23,CONCAT(SUM(each_attandance.attandance_in_week),SUBSTRING(attandance.total_attandance,3,3)),attandance.total_attandance) as total_attandance,
                           CONCAT(SUM(each_attandance.absence), ' times') as absence,
                           GROUP_CONCAT(each_attandance.absence order by each_attandance.date) as absence_in_week,
                           GROUP_CONCAT(DAYNAME(each_attandance.date) order by each_attandance.date) week_days,
                           DATE_FORMAT(SEC_TO_TIME(SUM(each_attandance.late_time)*60), '%H:%i') as total_late_time,
                           DATE_FORMAT(SEC_TO_TIME(SUM(each_attandance.overtime)*60), '%H:%i') as total_overtime,
                           GROUP_CONCAT(each_attandance.late_time order by each_attandance.date) as late_time_in_week,
                           GROUP_CONCAT(each_attandance.overtime order by each_attandance.date) as overtime_in_week,
                           attandance.user_id
                    
                    FROM ( SELECT users.id as user_id,
                                  DATE_FORMAT(SEC_TO_TIME(SUM(TIMESTAMPDIFF(SECOND ,day_times.clockIn,IFNULL(day_times.clockOut,CONVERT_TZ(NOW(),@@session.time_zone,'$time_zone'))))), '%H:%i' )as total_attandance
                    
                    
                           FROM users
                               LEFT JOIN day ON day.userId=users.id
                               LEFT JOIN day_times ON day_times.dayId=day.id
                               LEFT JOIN ( SELECT shift.id, shift.clockIn,shift.clockOut,shift.duration, shift.dayRange
                                           FROM  shift
                                           ) as shift ON shift.id=day.shiftId
                           WHERE YEARWEEK(day.date,1)=YEARWEEK(date(CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')),1)
                               GROUP BY users.id
                               order by day_times.id
                    
                             ) as attandance
                    JOIN (SELECT users.id as user_id,
                                 day.date,
                                 IFNULL(SUM(TIMESTAMPDIFF(HOUR ,day_times.clockIn,IFNULL(day_times.clockOut,CONVERT_TZ(NOW(),@@session.time_zone,'$time_zone')))),0) as attandance_in_week,
                                 IF(COUNT(day_times.clockIn) >0 ,0,1) as absence,
                                 IF(TIMESTAMPDIFF(MINUTE ,shift.clockIn, TIME (day_times.clockIn))-IFNULL(shift.dayRange,0) > 0, TIMESTAMPDIFF(MINUTE ,shift.clockIn,TIME(day_times.clockIn))-IFNULL(shift.dayRange,0),0) as late_time,
                                 IF( ( SUM(TIMESTAMPDIFF(MINUTE ,day_times.clockIn,IFNULL(day_times.clockOut,CONVERT_TZ(NOW(),@@session.time_zone,'$time_zone')))) -( TIMESTAMPDIFF(MINUTE ,shift.clockIn,shift.clockOut) -IFNULL(shift.duration,0)  ) ) >0, (SUM(TIMESTAMPDIFF(MINUTE ,day_times.clockIn,IFNULL(day_times.clockOut,CONVERT_TZ(NOW(),@@session.time_zone,'$time_zone'))))-( TIMESTAMPDIFF(MINUTE ,shift.clockIn,shift.clockOut) -IFNULL(shift.duration,0)  ) ),0 ) as overtime
                    
                          FROM users
                                   LEFT JOIN day ON day.userId=users.id
                                   LEFT JOIN day_times ON day_times.dayId=day.id
                                   LEFT JOIN ( SELECT shift.id, shift.clockIn,shift.clockOut,shift.duration, shift.dayRange
                                           FROM  shift
                                           ) as shift ON shift.id=day.shiftId
                          WHERE YEARWEEK(day.date,1)=YEARWEEK(date(CONVERT_TZ(NOW(), @@session.time_zone, '$time_zone')),1)
                          GROUP BY users.id,day.date
                          ORDER BY day.date
                    ) as each_attandance ON each_attandance.user_id=attandance.user_id
                    WHERE attandance.user_id='$user_id'
                    GROUP BY attandance.user_id
                    
                    ");
                    $count = 0;
                    foreach ($this->data['result'] as $res) {
                        $this->data['result'][$count]->week_days = explode(',', $this->data['result'][$count]->week_days);
                        $this->data['result'][$count]->absence_in_week = array_map('intval',explode(',', $this->data['result'][$count]->absence_in_week));
                        $this->data['result'][$count]->attandance_in_week = array_map('intval',explode(',', $this->data['result'][$count]->attandance_in_week));
                        $this->data['result'][$count]->late_time_in_week = array_map('intval',explode(',', $this->data['result'][$count]->late_time_in_week));
                        $this->data['result'][$count]->overtime_in_week = array_map('intval',explode(',', $this->data['result'][$count]->overtime_in_week));
                        $count++;
                    }

                    break;
                case 'add_note':
                    $text=$this->post->text;
                    $this->data['result']=$this->insertRecordUni('notes','user_id,text',"'$user_id','$text'",true);

                    break;
                case 'delete_note':
                    $note_id=$this->post->note_id;
                    $this->data['result']=$this->deleteRecords('notes',$note_id);
                    break;
                default:
                    $this->data['error'] = "";
            }
            echo json_encode($this->data);
            die();
        }

        $this->data['groups'] = $this->complicatedQuery(null, "SELECT id,title FROM groups");
        $this->data['employee_schedule_groups_result'] = $this->complicatedQuery(null, "
        SELECT  CONCAT(`groups`.title,'(',SUM( IF( (IFNULL(schedule.title,'_____')!='_____' ),1,0 ) ),')')  user_group,
        `groups`.id as id,
        GROUP_CONCAT(IF( (IFNULL(schedule.title,'_____')!='_____' ), CONCAT(users.name,' ',users.last_name),NULL )) as users,
        GROUP_CONCAT(IF( (IFNULL(schedule.title,'_____')!='_____' ),IFNULL(schedule.title,'_____'),NULL ) ) as schedule
        
        FROM `groups`
        LEFT JOIN users ON FIND_IN_SET(users.id,`groups`.users)>0
        JOIN schedule ON schedule.id=users.scheduleId
        GROUP BY `groups`.id

        ");
        $count = 0;
        foreach ($this->data['employee_schedule_groups_result'] as $res) {
            $this->data['employee_schedule_groups_result'][$count]->users = explode(',', $this->data['employee_schedule_groups_result'][$count]->users);
            $this->data['employee_schedule_groups_result'][$count]->schedule = explode(',', $this->data['employee_schedule_groups_result'][$count]->schedule);
            $count++;
        }


        $this->data['employee_schedule_withot_groups_result'] = $this->complicatedQuery(null, "
        SELECT  CONCAT(`groups`.title,'(',COUNT(users.id),')')  user_group,
        `groups`.id as id,
        GROUP_CONCAT(users.name,' ',users.last_name) as users,
        ''as schedule
        
        FROM `groups`
        LEFT JOIN users ON FIND_IN_SET(users.id,`groups`.users)>0
        WHERE users.scheduleId NOT IN (SELECT id FROM schedule)
        GROUP BY `groups`.id
        ");
        $count = 0;
        foreach ($this->data['employee_schedule_withot_groups_result'] as $res) {
            $this->data['employee_schedule_withot_groups_result'][$count]->users = explode(',', $this->data['employee_schedule_withot_groups_result'][$count]->users);
            $this->data['employee_schedule_withot_groups_result'][$count]->schedule = explode(',', $this->data['employee_schedule_withot_groups_result'][$count]->schedule);
            $count++;
        }

        $this->data['employee_individuals_result'] = $this->complicatedQuery(null, "
        SELECT  users.name as   users,
        `users`.id as id,
        GROUP_CONCAT(schedule.title) as schedule
        
        FROM  users
        JOIN schedule ON schedule.id=users.scheduleId
        WHERE users.id NOT IN (SELECT users.id
        FROM users
        JOIN `groups` ON FIND_IN_SET(users.id,`groups`.users)>0)
        GROUP BY users.id
        ");
        $this->data['employee_individuals_without_result'] = $this->complicatedQuery(null, "
        SELECT  users.name as   users,
        `users`.id as id,
        GROUP_CONCAT(schedule.title) as schedule
        
        FROM  users
        LEFT JOIN schedule ON schedule.id=users.scheduleId
        WHERE users.id NOT IN (SELECT users.id
        FROM users
        JOIN `groups` ON FIND_IN_SET(users.id,`groups`.users)>0)
        AND schedule.title IS NULL 
        GROUP BY users.id
        ");


//        Remove ,schedule.title as schedule  #### Start


//        $this->data['employee_next_schedule_groups_result'] = $this->complicatedQuery(null, "
//        SELECT  CONCAT(`groups`.title,'(',COUNT(users.id),')')  user_group,
//        `groups`.id as id,
//        GROUP_CONCAT(users.name,' ',users.last_name) as users,
//        schedule.title as schedule
//
//        FROM `groups`
//        LEFT JOIN users ON FIND_IN_SET(users.id,`groups`.users)>0
//        JOIN users_new_schedule ON  FIND_IN_SET(`users`.id,users_new_schedule.user_id)
//        JOIN schedule ON schedule.id=users_new_schedule.scheduleId
//        GROUP BY `groups`.id
//
//        ");


        $this->data['employee_next_schedule_groups_result'] = $this->complicatedQuery(null, "
        SELECT  CONCAT(`groups`.title,'(',COUNT(users.id),')')  user_group,
        `groups`.id as id,
        GROUP_CONCAT(users.name,' ',users.last_name) as users        
        
        FROM `groups`
        LEFT JOIN users ON FIND_IN_SET(users.id,`groups`.users)>0
        JOIN users_new_schedule ON  FIND_IN_SET(`users`.id,users_new_schedule.user_id)
        JOIN schedule ON schedule.id=users_new_schedule.scheduleId
        GROUP BY `groups`.id

        ");

//        Remove ,schedule.title as schedule #### End

        $count = 0;
        foreach ($this->data['employee_next_schedule_groups_result'] as $res) {
            $this->data['employee_next_schedule_groups_result'][$count]->users = explode(',', $this->data['employee_next_schedule_groups_result'][$count]->users);
            $this->data['employee_next_schedule_groups_result'][$count]->schedule = explode(',', $this->data['employee_next_schedule_groups_result'][$count]->schedule);
            $count++;
        }
        $this->data['employee_next_schedule_withot_groups_result'] = $this->complicatedQuery(null, "
        SELECT  CONCAT(`groups`.title,'(',COUNT(users.id),')')  user_group,
        `groups`.id as id,
        GROUP_CONCAT(users.name,' ',users.last_name) as users,
        ''as schedule
        
        FROM `groups`
        LEFT JOIN users ON FIND_IN_SET(users.id,`groups`.users)>0
        LEFT JOIN users_new_schedule ON  users.id NOT IN (SELECT user_id from users_new_schedule)
        JOIN schedule ON schedule.id=users_new_schedule.scheduleId
        WHERE users.scheduleId NOT IN (SELECT id FROM schedule)
        GROUP BY `groups`.id
        ");
        $count = 0;
        foreach ($this->data['employee_next_schedule_withot_groups_result'] as $res) {
            $this->data['employee_next_schedule_withot_groups_result'][$count]->users = explode(',', $this->data['employee_next_schedule_withot_groups_result'][$count]->users);
            $this->data['employee_next_schedule_withot_groups_result'][$count]->schedule = explode(',', $this->data['employee_next_schedule_withot_groups_result'][$count]->schedule);
            $count++;
        }

        $this->data['employee_individuals_next_result'] = $this->complicatedQuery(null, "
        SELECT  users.name as   users,
        `users`.id as id,
        GROUP_CONCAT(schedule.title) as schedule
        
        FROM  users
        LEFT JOIN users_new_schedule ON  users.id =users_new_schedule.user_id
        JOIN schedule ON schedule.id=users_new_schedule.scheduleId
        WHERE users.id NOT IN (SELECT users.id
        FROM users
        JOIN `groups` ON FIND_IN_SET(users.id,`groups`.users)>0)
        GROUP BY users.id
        ");
        $this->data['employee_individuals_without_next_result'] = $this->complicatedQuery(null, "
        SELECT  users.name as   users,
        `users`.id as id,
        GROUP_CONCAT(schedule.title) as schedule
        
        FROM  users
        LEFT JOIN users_new_schedule ON  users.id =users_new_schedule.user_id
        LEFT JOIN schedule ON schedule.id=users_new_schedule.scheduleId
        WHERE users.id NOT IN (SELECT users.id
        FROM users
        JOIN `groups` ON FIND_IN_SET(users.id,`groups`.users)>0)
        AND schedule.title IS NULL
        GROUP BY users.id
        ");


        $this->data['notes'] = $this->complicatedQuery(null, "SELECT id, CONCAT('note_',id) as note_id ,text FROM notes");

    }

    function get_time_zone($id)
    {
        $this->data['result'] = $this->complicatedQuery(null, "SELECT  t.value as tz
                                                                            FROM company
                                                                            JOIN timezones t on company.timezone_id = t.id
                                                                            WHERE company.id='$id'");
        return $this->data['result'][0]->tz;
    }

    function plan()
    {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
                "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
                "home.css",
                "basic.css",
                "header/header.css",
                "footer.css"
            ],
            'js' => [
                "https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous",
                "https://code.jquery.com/ui/1.12.1/jquery-ui.js",
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous",
                "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js",
                "https://cdn.jsdelivr.net/npm/sweetalert2@9",
                "https://cdn.jsdelivr.net/npm/apexcharts",
                "home.js",
                "basic.js",
            ],
            'plan' => [
                'header',
                'home',
                'footer'

            ]
        ];
        return $list;
    }


}