<?php
use system\jampModel;
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if(!defined('__JAMP__')){header('HTTP/1.0 404 Not Found');die();}


class login extends system\jampModel {

    /*
     * json response codes (messages witch appears on login page).
     */
    private $responseCodes = [
        'success'                     => ['code' => '000', 'result' => 'Success'],
        'emailIsEmpty'                => ['code' => '001', 'result' => 'Email field is empty'],
        'passwordIsEmpty'             => ['code' => '002', 'result' => 'Password field is empty'],
        'invalidCaptcha'              => ['code' => '003', 'result' => 'Invalid captcha'],
        'emailIsNotAValid'            => ['code' => '004', 'result' => 'Email or password is not valid'],
        'invalidPassword'             => ['code' => '005', 'result' => 'Email or password is not valid'],
        'invalidEmail'                => ['code' => '006', 'result' => 'Wrong email'],
        'recover'                     => ['code' => '007', 'result' => 'Check your email for recovery link'],
        'recoverIncorrect'            => ['code' => '008', 'result' => 'Recover link is incorrect or something went wrong, try again later'],
        'passwordNoUpperCaseSymbol'   => ['code' => '010', 'result' => 'Password does not contains uppercase letters'],
        'passwordNoLowerCaseSymbol'   => ['code' => '011', 'result' => 'Password does not contains lowercase letters'],
        'passwordNoNumbers'           => ['code' => '012', 'result' => 'Password does not contain a number'],
        'passwordNoSymbol'            => ['code' => '013', 'result' => 'Password does not contain symbols'],
        'passwordNoLessThen8'         => ['code' => '014', 'result' => 'Password is less than 8 letters'],
        'passwordSuccessfullyChanged' => ['code' => '015', 'result' => 'Password Successfully Changed'],
        'recoveryLinkSent'            => ['code' => '016', 'result' => 'Recovery link was already sent'],
        'companyNotVerified'          => ['code' => '0017', 'result' => 'Company not verified'],
    ];

    /*
     *  google captcha options
     */
    private $secretOptions =  [
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false
        ]
    ];

    /*
     * redirect urls
     */
    private $urls = [
        'success' => 'requests',
        'recovery' => '/recovery/'
    ];

    /*
     * recovery email contents
     */
    private $emailRecoveryInfo = [ // needs some work when web page will be online.
        'subject' => 'Password Recovery',
        'txt' => '',
        'headers' => [
            'MIME-Version: 1.0' . '\r\n',
            'Content-type:text/html;charset=UTF-8' . '\r\n',
            'Cc: '
        ]
    ];

    /*
     * defines which page will load
     */
    public $currentUserStatus = null;

    /*
     * if in testing error is discovered if will be added to ths array so full json response can be returned.
     */
    private $errorList = [];

    /*
     * active when user login successfully.
     */
    private $success = null;

    /*
     * user info is selected from database using email.
     */
    private $userInfo = false;

    /* FOR TESTING ONLY........................................................................................................*/

        /* this will disable captcha for development (in production remove null and add true so captcha can work properly) */
        public $captcha = null;

    /* END FOR TESTING ONLY................................................................................................... */

    /**
     * @return void|null
     * testing if there is a post request.
     */
    public function index() {
        $this->data['userLoginStatus'] = $this->userLoginStatus;
        $this->getUserStatus();

        if (isset($this->page) && isset($this->url[1]) && $this->url[1] == 'recovery' && isset($this->url[2])) {
            $this->testRecoveryLink();
        }

        if(isset($this->post) && isset($this->post->type)) {
            if ($this->isUserLoggedIn()) {
                $this->setError('success');
                $this->returnResponse();
            }

            switch ($this->post->type) {
                case 'login' :
                        $this->loginPost();
                    break;
                case 'forgot' :
                        $this->forgotPost();
                    break;
                case 'resetPassword' :
                        $this->resetPassword();
                    break;
                case 'register' :
                    $this->register();
                    break;
            }
            die();
        } else {
            if ($this->isUserLoggedIn ()) {
                header("Location: {$this->urls['success']}");
            }
        }
    }

    /*
     * register user
     */
    private function register () {
        if (!isset($this->post->companyName) || !isset($this->post->yourEmail)  || !isset($this->post->Password) || !isset($this->post->RepeatPassword)) return null;

        $unical_link = $this->encording(time());
        $unicDate = date("Ymdhisa");

        $newDb = "a-jamp-timepunch_".$this->post->companyName."_".$unicDate;

        $this->complicatedQuery([NULL],"CREATE DATABASE IF NOT EXISTS `".$newDb."` CHARACTER SET utf8 COLLATE utf8_general_ci");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`company` LIKE `a-jamp-timepunch_template`.`company`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`day` LIKE `a-jamp-timepunch_template`.`day`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`dayrequest` LIKE `a-jamp-timepunch_template`.`dayrequest`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`day_times` LIKE `a-jamp-timepunch_template`.`day_times`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`groups` LIKE `a-jamp-timepunch_template`.`groups`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`notes` LIKE `a-jamp-timepunch_template`.`notes`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`overtime` LIKE `a-jamp-timepunch_template`.`overtime`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`positions` LIKE `a-jamp-timepunch_template`.`positions`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`requestmessage` LIKE `a-jamp-timepunch_template`.`requestmessage`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`schedule` LIKE `a-jamp-timepunch_template`.`schedule`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`shift` LIKE `a-jamp-timepunch_template`.`shift`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`timezones` LIKE `a-jamp-timepunch_template`.`timezones`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`user-status` LIKE `a-jamp-timepunch_template`.`user-status`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`userrecovery` LIKE `a-jamp-timepunch_template`.`userrecovery`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`users` LIKE `a-jamp-timepunch_template`.`users`");
        $this->complicatedQuery([NULL], "CREATE TABLE `".$newDb."`.`users_new_schedule` LIKE `a-jamp-timepunch_template`.`users_new_schedule`");
        $this->complicatedQuery([NULL], " INSERT INTO `".$newDb."`.`timezones` SELECT * FROM `a-jamp-timepunch_template`.`timezones`");


        $newDbCompanyId = $this->complicatedQuery(["company"], "INSERT INTO `".$newDb."`.`company` (`name`, `link`) VALUES (?, ?)", false, [
            $this->post->companyName, $unical_link
        ], true);

        $companyId = $this->complicatedQuery(["company"], "INSERT INTO company (`name`) VALUES (?)", false, [
            $this->post->companyName."_".$unicDate
        ], true);

        $this->complicatedQuery(["users"], "INSERT INTO users (`name`, last_name, email, username, password, company_id, verify, is_company, unical_link)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", false, [
            $this->post->yourName, $this->post->yourLastName, $this->post->yourEmail, $this->post->companyName, $this->encording($this->post->Password), $companyId, $unical_link, "1", $unical_link
        ]);

        $this->complicatedQuery(["users"], "INSERT INTO `".$newDb."`.`users` (`name`, last_name, email, username, password, company_id, is_company)  VALUES (?, ?, ?, ?, ?, ?, ?)", false, [
            $this->post->yourName, $this->post->yourLastName, $this->post->yourEmail, $this->post->companyName, $this->encording($this->post->Password), $newDbCompanyId, "1"
        ]);


        $this->complicatedQuery(["company"], "INSERT INTO `".$newDb."`.`overtime` (`id`, `title`) VALUES (?, ?)", false, [
            '1', 'before'
        ], true);

        $this->complicatedQuery(["company"], "INSERT INTO `".$newDb."`.`overtime` (`id`, `title`) VALUES (?, ?)", false, [
            '2', 'after'
        ], true);

        $subjectText = 'Elva activation link';
        $bodyText = 'for activate your accout please press on this link <a href="'.ROOT_URL.'activate/'.$unical_link.'">Elva timepunch</a>';
        $this->mailer($subjectText, $bodyText, $this->post->yourEmail);

//        $this->setSession('userStatus', $this->userLoginStatus['login']);
//        $this->setSession('userId', $userId);
//        $this->setSession('companyId',  $companyId);
//        $this->setSession('dbName',  $newDb);

        echo 'You have registered successfulslssssy.';
    }


    public function mailer($subjectText, $bodyText, $email){



// Load Composer's autoloader
        require 'PHPMailer/src/Exception.php';
        require 'PHPMailer/src/PHPMailer.php';
        require 'PHPMailer/src/SMTP.php';


// Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->SMTPDebug = 1;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = 'mail.9bit.ge';                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = 'noreply@9bit.ge';                     // SMTP username
            $mail->Password = '8sHJQAFH';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('noreply@9bit.ge', '9bit');

            $mail->addAddress($email);     // Add a recipient

            // Attachments
//    $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subjectText;
            $mail->Body = $bodyText;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

    }


    /**
     * tests recovery link
     */
    private function testRecoveryLink () {
        if (isset($this->url[1]) && $this->url[1] == 'recovery' && isset($this->url[2])) {
            $user = $this->complicatedQuery(['userrecovery'], 'SELECT userid, status FROM userrecovery WHERE recoverytoken = ?', true, [
                $this->url[2]
            ]);

            if (empty($user) || $user == false || ($user !== false && $user->status == 1)) return null;

            if (!empty($user) && $user->userid) {
                $this->setSession('userStatus', $this->userLoginStatus['passwordReset']);
                $this->setSession('userId', $user->userid);
                $this->getUserStatus();
            }
        }
    }

    /**
     * @return null|null
     * if user is not logged in, userStatus will be set as logged out
     */
    private function getUserStatus () {
        $userStatus = $this->getSession('userStatus');

        if (is_null($userStatus) && $userStatus !== $this->userLoginStatus['login']) {
            $this->currentUserStatus = $this->userLoginStatus['loggedOut'];
            $this->data['currentUserStatus'] = $this->userLoginStatus['loggedOut'];
            return null;
        } elseif (!isset($this->url[1]) && $userStatus == $this->userLoginStatus['recovery'] && $this->url[1] != 'recovery') {
            $this->data['currentUserStatus'] = $this->userLoginStatus['passwordReset'];
        }

        $this->currentUserStatus = $userStatus;
        $this->data['currentUserStatus'] = $userStatus;

        return null;
    }

    /**
     * @param null $userId
     * @return bool|null]
     *  test if a recovery token exists and if a new one is needed or user already has a recovery link
     */
    public function checkRecoveryLinkStatus($userId=null) {
        if (is_null($userId)) return null;

        $status = $this->complicatedQuery(['userrecovery'], 'SELECT status FROM userrecovery WHERE userId = ? AND status IS NULL', true, [
            $userId
        ]);

        if ($status == false || empty($status) || (!empty($status) && $status->status == 1)) return true;

        return false;
    }

    /**
     * changes password to user witch id is in the session
     */
    private function resetPassword () {

        $this->testIncomingPassword();

        $this->data['company'] = $this->complicatedQuery([NULL], 'SELECT * FROM `a-jamp-timepunch`.userrecovery as a
            LEFT JOIN `a-jamp-timepunch`.users as b ON a.userid = b.id
            LEFT JOIN `a-jamp-timepunch`.company as c ON b.company_id = c.id
            WHERE a.status = 0 AND  a.recoverytoken = "'.$this->session->resetPassword.'" 
        ');

        $this->complicatedQuery(['users'], 'UPDATE users SET password = ? WHERE id = ?', null, [
            $this->encording($this->post->password), $this->data['company'][0]->userid
        ]);

        $this->complicatedQuery(['userrecovery'], 'UPDATE userrecovery SET status = 1 WHERE userid = ?', null, [
            $this->data['company'][0]->userid
        ]);

        $this->setError('passwordSuccessfullyChanged');
        unset($this->session->resetPassword);
        return $this->returnResponse();
}

    /**
     * @return null|null
     *  saves token in userrecovery table
     */
    private function saveToken ($userId=null, $recoveryToken=null) {
        if (is_null($userId) || is_null($recoveryToken)) return null;

        $this->complicatedQuery(['userrecovery'], 'INSERT INTO `userrecovery`(`recoverytoken`, `userid`) VALUES (?, ?)', null, [
            $recoveryToken, $userId
        ], true);

        $email = $this->userInfo->email;
        if(isset($this->responseCodes)) {
            $subjectText = 'Elve Reset password';
            $bodyText = 'For reset password please go this <a href="' . ROOT_URL . 'reset/' . $recoveryToken . '">Link Elva timepunch</a>';
            $this->mailer($subjectText, $bodyText, $email);
        }
    }

    /**
     * @return string
     * generates random string for recovery token
     */
    private function generateRecoveryToken() {
        return md5(uniqid(rand(), true));
    }

    /**
     * @param null $name
     * @return null|null
     * selects error codes from responseCodes and saves errors in errorList.
     */
    private function setError ($name=null) {
        if (is_null($name)) return null;

        $this->errorList[$name] = $this->responseCodes[$name];

        return true;
    }

    /**
     * password reset, (needs email to find user and sends a recovery link)
     */
    private function forgotPost() {
        if ($this->testIncoming() != true || $this->getUserInfo() != true) $this->returnResponse();

        if (empty($this->userInfo)) $this->getUserInfo();

        $tokenStatus = $this->checkRecoveryLinkStatus($this->userInfo->id);

        if ($this->userInfo == false || empty($this->userInfo)) {
            $this->setError('invalidEmail');
            $this->returnResponse();
        }

        if ($tokenStatus == false) {
            $this->setError('recoveryLinkSent');
            $this->returnResponse();
        }

        $token = $this->generateRecoveryToken();
        $this->saveToken($this->userInfo->id, $token);

        $recoveryLink = $this->page . $this->urls['recovery'] . $token;

        $this->setError('recover');
        $this->sendRecoveryLink($recoveryLink);
        $this->returnResponse();
    }

    /**
     * @param null $recoveryLink
     */
    private function sendRecoveryLink ($recoveryLink=null) {
        $this->sendEmail($this->post->email, $this->emailRecoveryInfo['subject'], $this->emailRecoveryInfo['txt'] . "<a href='{$recoveryLink}'>recovery link</a>");
    }

    /**
     * returns json response
     */
    private function returnResponse () {
        echo json_encode($this->errorList);

        die();
    }

    /**
     * @return bool|null
     * sets all of the user info in session.
     */
    private function setUserInfo () {
        if (!isset($this->userInfo) || $this->userInfo == false) return null; // just in case
        $db = "a-jamp-timepunch_".$this->userInfo->companyName;

        $userId = $this->complicatedQuery(['users'], 'SELECT * FROM `'.$db.'`.`users` WHERE email = "'.$this->post->email.'" && password = "'.$this->encording($this->post->password).'"', 1);


        $this->setSession('userId',     $userId->id);
        $this->setSession('pin',        $userId->pin);
        $this->setSession('companyId',  $userId->company_id);
        $this->setSession('is_company',  $userId->is_company);
        $this->setSession('email',      $userId->email);
        //$this->setSession('username',   $this->userInfo->usename);
        $this->setSession('userStatus', $this->userLoginStatus['login']);
        $this->setSession('dbName',  $db);

        $this->complicatedQuery(['userrecovery'], 'UPDATE userrecovery SET status = 1 WHERE userid = ?', true, [
            $this->userInfo->id
        ]);

        $this->setError('success');
        $this->returnResponse();
    }

    /**
     * @return bool
     * gets user info from table uses using email. (and tests it, if the $this->userInfo is empty the email address is false and the user dos'n exists).
     */
    private function getUserInfo () {
        $this->userInfo = $this->complicatedQuery(['users'], 'SELECT users.*, company.name as companyName FROM users 
        JOIN company ON users.company_id = company.id  
        WHERE users._deleted=0 AND users.email= ?', null, [
            $this->post->email
        ]);

        if ($this->userInfo == false) {
            $this->setError('emailIsNotAValid');
            return false;
        }

        $this->userInfo = $this->userInfo[0];

        return true;
    }

    /**
     * @return void|null
     * testing if password is empty and not only...
     */
    protected function testIncomingPassword () {
        /*
         * 1. password needs to be one lowercase letters
         * 2. one uppercase letter
         * 3. one lowercase letter
         * 4. a symbol
         * 5. needs to have a number
         * 6. it needs to be longer then 8 letters
         */

        $this->post->password = trim($this->post->password);

        if (!preg_match('#[a-z]+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoLowerCaseSymbol');
        }

        if (!preg_match('#[A-Z]+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoUpperCaseSymbol');
        }

        if (!preg_match('#[0-9]+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoNumbers');
        }

        if (!preg_match('#\W+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
//            $this->setError('passwordNoSymbol');
        }

        if (strlen($this->post->password) < 8 && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoLessThen8');
        }

        if ($this->post->password === '' || !isset($this->post->password)) {
            $this->setError('passwordIsEmpty');
        }

        return !empty($this->errorList) ? $this->returnResponse() : null;
    }

    /**
     * @return bool
     * testing user input, if it is valid and not empty.
     */
    private function testIncoming () {
        if (!isset($this->post->email)) {
            $this->setError('emailIsEmpty');
        }

        if ($this->captcha != null) { // for development (this can be remove in production)
            if (!isset($this->post->captcha)) {
                $this->setError('invalidCaptcha');
            }

            $captcha = $this->secret($this->post->captcha);

            if(!isset($captcha->success) || $captcha->success !== true) {
                $this->setError('invalidCaptcha');
            }
        }

        !empty($this->errorList) ? $this->returnResponse() : null;

        if(!filter_var($this->post->email, FILTER_VALIDATE_EMAIL)) {
            $this->setError('emailIsNotAValid');
        }

        return !empty($this->errorList) ? false : true;
    }

    /**
     * collects all the logic in one function, (tests user input, gets user data and tests password).
     */
    private function loginPost () {
        if ($this->testIncoming() != true || $this->getUserInfo() != true) $this->returnResponse();

        $this->testIncomingPassword();

        $password = $this->encording($this->post->password);
        if($password !== $this->userInfo->password) {
            $this->setError('invalidPassword');
            $this->returnResponse();
        }elseif ($this->userInfo->verify !== 'true'){
            $this->setError('companyNotVerified');
            $this->returnResponse();
        } else {
            $this->setUserInfo();
        }
    }

    /**
     * @param null $token
     * @return mixed|null
     * captcha config.
     */
    private function secret($token=null) {
        if (is_null($token)) return null;

        $secretKey = $this->config['secretKey'];

        return json_decode(
            file_get_contents(
                "https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$token}",false,
                stream_context_create($this->secretOptions)
            )
        );
    }

    /**
     * @return array
     * basic plan for loading a page.
     */
    public function plan() {

        $list = [
            'css' => [
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
                'animate.css',
                "request/glyphter.css",
                'login.css'
            ],
            'js' => [
                'vue.js',
                'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
                'https://use.fontawesome.com/releases/v5.0.13/js/solid.js',
                'https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js',
                'https://www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit',
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
                'login.js',
                'basic.js'
            ],
            'plan' => [
                'login'
            ]
        ];
        return $list;
    }
}