<?php

use system\jampModel;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if (!defined('__JAMP__')) {
    header('HTTP/1.0 404 Not Found');
    die();
}
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);


class settings extends system\jampModel
{

    function index()
    {
        /* FOR TESTING ONLY........................................*/
//        if (!$this->getSession('userId')) {
//            $this->setSession('userId', 3);
//            $this->setSession('companyId', 1);
//        }
        /* FOR TESTING ONLY........................................*/

        $user_id = $this->session->userId;
        $company_id = $this->session->companyId;
        if (!empty($_FILES) != null) {
            $this->uploadImage();
        }


        $im = $this->complicatedQuery(['users'],'SELECT `is_company`, `controls-users` as controlUsers, users FROM users
                                                        LEFT JOIN groups ON FIND_IN_SET(users.groups, groups.id)
                                                        WHERE users.id = "'.$this->session->userId.'"', 1);

        if($im->controlUsers != ''){
            $allUsersControl = ($im->users != '')?$im->users.','.$im->controlUsers : $im->controlUsers;
        }else{
            $allUsersControl = $im->users;
        }

        ($allUsersControl != '')? $allUsersControl = $this->session->userId.','.$allUsersControl : $allUsersControl = $this->session->userId;

        ($im->is_company == 1)? $company = '&& is_company = 0' : $company = '&& users.id IN ('.$allUsersControl.')';



        if (isset($this->post->action)) {
            switch ($this->post->action) {
                case 'company_name':
                    $company_name = $this->post->val;
                    $this->data['result'] = $this->complicatedQuery(null, 'UPDATE company SET `name` = "'.$company_name.'"');
                    echo json_encode($this->data);
                    break;
                case 'delete_image':
                    $user_id = isset($this->post->user_id) ? $this->post->user_id : $this->session->userId;
                    $this->updateRecords('users', 'image = ""', $user_id . ' ');

                    break;
                case 'change_password':
                    $this->data['user_password'] = $this->selectRows('users', 'password', "id ='" . $this->session->userId . "'", null);
                    if ($this->encording($this->post->currentpassword) == $this->data['user_password'][0]->password) {
                        if ($this->post->inputpassword == $this->post->repeatpassword) {
                            $password = $this->post->inputpassword;
                            $uppercase = preg_match('@[A-Z]@', $password);
                            $lowercase = preg_match('@[a-z]@', $password);
                            $number = preg_match('@[0-9]@', $password);
//                            $specialChars = preg_match('@[^\w]@', $password);
                            if (!$uppercase || !$lowercase || !$number ||  strlen($password) < 8) {
                                $this->data['error'] = "Password should be at least 8 characters in length and should include at least one upper case letter and one number";
                            } else {
                                $this->data['error'] = '';
                                $this->complicatedQuery(['users'], 'UPDATE `a-jamp-timepunch`.`users` SET password = "'.$this->encording($this->post->inputpassword).'" WHERE email = "'.$this->session->email.'"', 1);
                                $this->updateRecords('users', 'password = "' . $this->encording($this->post->inputpassword) . '"', $this->session->userId . ' ');

                            }
                        } else {
                            $this->data['error'] = 'Password and confirm password does not match';
                        }
                    } else {
                        $this->data['error'] = "current password is incorrect";
                    }

                    echo json_encode($this->data);

                    break;
                case 'change_user_name':
                    if (isset($this->session->is_company) && $this->session->is_company != 0) {
                        $val = $this->post->val;
                        $this->updateRecords('users', "name ='$val'", $this->session->userId . ' ');
                        echo json_encode($val);
                    }
                    break;
                case 'change_user_email':
                    $val = $this->post->val;
                    if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                        $this->data['error'] = "Invalid email format";
                        $this->data['result'] = $this->selectRows('users', 'email', "id ='" . $this->session->userId . "'", null);
                    } else {
                        $oldMail = $this->selectRows('users', 'email', "email ='" . $this->session->email . "'", null, 1);
                        $this->updateRecords('users', "email ='$val'", $this->session->userId . ' ');
                        $this->data['result'] = $this->complicatedQuery(null, 'UPDATE `a-jamp-timepunch`.users SET `email` = "'.$val.'" WHERE email = "'.$oldMail->email.'"');
                        $this->data['error'] = '';
                    }
                    echo json_encode($this->data);

                    break;
                case 'change_user_pin':
                    $val = $this->post->val;
                    $this->updateRecords('users', "pin ='$val'", $this->session->userId . ' ');
                    echo json_encode($val);
                    break;
                case 'change_user_fax':
                    $val = $this->post->val;
                    $this->updateRecords('users', "fax ='$val'", $this->session->userId . ' ');
                    echo json_encode($val);
                    break;
                case 'change_user_address':
                    $val = $this->post->val;
                    $this->updateRecords('users', "address ='$val'", $this->session->userId . ' ');
                    echo json_encode($val);
                    break;
                case 'change_user_phone_number':
                    $val = $this->post->val;
                    $this->updateRecords('users', "phone_number ='$val'", $this->session->userId . ' ');
                    echo json_encode($val);
                    break;
                case 'change_user_zip_code':
                    $val = $this->post->val;
                    $this->updateRecords('users', "zip_code ='$val'", $this->session->userId . ' ');
                    echo json_encode($val);
                    break;
                case 'update_time_zone':
                    $val = $this->post->time_zone;
                    $this->updateRecords('company', "timezone_id ='$val'", $this->session->companyId . ' ');
                    echo json_encode($this->data);
                    break;
                case 'update_company_color':
                    $theme_color = $this->post->theme_color;
                    $text_color = $this->getContrastColor($theme_color);
                    $this->updateRecords('company', "theme_color ='$theme_color', text_color='$text_color'", $this->session->companyId . ' ');
                    echo json_encode($this->data);
                    break;
                case 'update_company_background':
                    $background = $this->post->background;
                    $this->updateRecords('company', "background_image ='$background'", $this->session->companyId . ' ');
                    echo json_encode($this->data);
                    break;
                case 'get_theme_color':
                    $this->data['result'] = $this->selectRows('company', 'theme_color', "id ='" . $this->session->companyId . "'", null);
                    echo json_encode($this->data);
                    break;
                case 'deactivate_user':
                    $deactivate_user = $this->post->deactivate_user;
                    $company_id = $this->session->companyId;
                    $group_id = $this->post->group_id;
                    foreach ($deactivate_user as $val) {
                        $this->updateRecords('users', "active ='0'", $val . ' ');
                    }
                    if(isset($this->post->group_id)){
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            `group`.`group`,
                                                                            (SELECT COUNT(id) FROM users) as users_count,
                                                                            (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                            (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users,
                                                                            (SELECT COUNT(id) FROM `groups`) as count_group,
                                                                            `positions`.name     as `positions`,
                                                                             IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as `group`,
                                                                            GROUP_CONCAT(`groups`.id) as `group_id`
                                                                            FROM users
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                                                                            GROUP BY users.id) as `group` ON `group`.id = users.id
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE users.company_id= '$company_id' AND FIND_IN_SET('$group_id', `group`.group_id) > 0
                                                                            GROUP BY users.id
                                                                            order by users.name");
                    }else{
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            ' ' as `group`,
                                                                            (SELECT COUNT(id) FROM users) as users_count,
                                                                            (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                            (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users,
                                                                            `positions`.name     as `positions`,
                                                                             IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE users.company_id= '$company_id' AND users.id NOT IN (SELECT users.id FROM users 
                                                                                                                                       JOIN `groups` ON FIND_IN_SET(users.id,`groups`.users) )
                                                                            GROUP BY users.id
                                                                            order by users.name ");
                    }

                    $this->data['counts'] = $this->complicatedQuery(null, "SELECT  COUNT(`groups`.id) as count_group,
                                                                                        (SELECT COUNT(id) FROM users) as users_count,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users
                                                                                        FROM `groups`  ");
                    echo json_encode($this->data);
                    break;
                case 'activate_user':
                    $activate_user = $this->post->activate_user;
                    $company_id = $this->session->companyId;
                    $group_id = $this->post->group_id;
                    foreach ($activate_user as $val) {
                        $this->updateRecords('users', "active ='1'", $val . ' ');
                    }

                    if(isset($this->post->group_id)){
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            `group`.`group`,
                                                                            (SELECT COUNT(id) FROM users) as users_count,
                                                                            (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                            (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users,
                                                                            (SELECT COUNT(id) FROM `groups`) as count_group,
                                                                            `positions`.name     as `positions`,
                                                                             IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as `group`,
                                                                            GROUP_CONCAT(`groups`.id) as `group_id`
                                                                            FROM users
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                                                                            GROUP BY users.id) as `group` ON `group`.id = users.id
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE users.company_id= '$company_id'AND FIND_IN_SET('$group_id', `group`.group_id) > 0
                                                                            GROUP BY users.id
                                                                            order by users.name");
                    }else{
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            ' ' as `group`,
                                                                            (SELECT COUNT(id) FROM users) as users_count,
                                                                            (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                            (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users,
                                                                            `positions`.name     as `positions`,
                                                                             IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE users.company_id= '$company_id'AND users.id NOT IN (SELECT users.id FROM users 
                                                                                                                                      JOIN `groups` ON  FIND_IN_SET(users.id, `groups`.users)) > 0
                                                                            GROUP BY users.id
                                                                            order by users.name");
                    }


                    $this->data['counts'] = $this->complicatedQuery(null, "SELECT  COUNT(`groups`.id) as count_group,
                                                                                        (SELECT COUNT(id) FROM users) as users_count,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users
                                                                                        FROM `groups`  ");
                    echo json_encode($this->data);
                    break;
                case 'get_users':
                    $company_id = $this->session->companyId;
                    if(isset($this->post->group_id)){
                        $group_id = $this->post->group_id;
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            `group`.`group`,
                                                                            `positions`.name     as `positions`,
                                                                            IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as `group`,
                                                                            GROUP_CONCAT(`groups`.id) as `group_id`
                                                                            FROM users
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                                                                            GROUP BY users.id) as `group` ON `group`.id = users.id
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE  users.company_id= '$company_id' ".$company." AND FIND_IN_SET('$group_id', `group`.group_id) > 0
                                                                            GROUP BY users.id
                                                                            order by users.name ");
                    }else{
//                        users AND GROUPS
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                                            users.name,
                                                                                            users.last_name,
                                                                                            users.start_date,
                                                                                            users.active,
                                                                                            ' ' as `group`,
                                                                                            `positions`.name     as `positions`,
                                                                                            IF(users.active=0,'red','#7d7d7d') as text_color
                                                                                            FROM users
                                                                                            JOIN (SELECT users.id,
                                                                                            GROUP_CONCAT(positions.name) as name
                                                                                            FROM users
                                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                                            WHERE  users.company_id= '$company_id' ".$company." AND users.id NOT IN (SELECT users.id FROM users
                                                                                                                                      JOIN `groups` ON FIND_IN_SET(users.id,`groups`.users))
                                                                                            GROUP BY users.id
                                                                                            order by users.name ");
                    }


                    $this->data['counts'] = $this->complicatedQuery(null, "SELECT  COUNT(`groups`.id) as count_group,
                                                                                        (SELECT COUNT(id) FROM users) as users_count,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users
                                                                                        FROM `groups`  ");
                    echo json_encode($this->data);
                    break;

                case 'get_groups':
                    if(isset($this->session->is_company) && $this->session->is_company == 1){
                        $sql = "SELECT id, title, role FROM `groups` order by title";
                    }else{
                        $userGroups = $this->complicatedQuery('NULl', 'SELECT groups, `controls-users` as users_control FROM `users` WHERE id = "'.$this->session->userId.'"', 1);

                        $sql = 'SELECT id, title, role FROM `groups` WHERE id IN ("'.$userGroups->groups.'") order by title';
                    }
                    $this->data['result'] = $this->complicatedQuery(null, $sql);
                    echo json_encode($this->data);

                    break;
                case 'get_position':
                    $this->data['result'] = $this->complicatedQuery(null, "SELECT id, name
                                                                                         FROM positions");
                    echo json_encode($this->data);

                    break;
                case 'get_overtime':
                    $this->data['result'] = $this->complicatedQuery(null, "SELECT id, title
                                                                                         FROM overtime");
                    echo json_encode($this->data);

                    break;

                case 'get_all_users':
                    $this->data['result'] = $this->complicatedQuery(null, "SELECT id, image, concat(name,' ',last_name) as name
                                                                                         FROM users
                                                                                         WHERE active=1 AND company_id='$company_id'");
                    echo json_encode($this->data);

                    break;
                case 'get_users_with_group_id':
                    $group_id = $this->post->group_id;
                    $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id, concat(users.name,' ',users.last_name) as name
                                                                                         FROM users
                                                                                         LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) < 1
                                                                                         WHERE users.active=1 AND users.company_id='$company_id' AND `groups`.id='$group_id'");

                    echo json_encode($this->data);
                    break;
                case 'add_users_in_group':
                    $group_id = $this->post->group_id;
                    $users = implode(',', $this->post->users);
                    if (isset($this->post->users)) {
                        $this->updateRecords('groups', "users = if(users='','$users',concat(users,',','$users')) ", $group_id . ' ');
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            `group`.`group`,
                                                                            `positions`.name     as `positions`,
                                                                            IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as `group`,
                                                                            GROUP_CONCAT(`groups`.id) as `group_id`
                                                                            FROM users
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                                                                            GROUP BY users.id) as `group` ON `group`.id = users.id
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE  users.company_id= '$company_id' AND FIND_IN_SET('$group_id', `group`.group_id) > 0
                                                                            GROUP BY users.id
                                                                            order by users.name ");
                    }

                    echo json_encode($this->data);
                    break;
                case 'get_user_group_setting':
                    $user_id = $this->post->user_id;
                    $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id  AS user_id,
                    users.name,
                    users.last_name,
                    users.email,
                    users.pin,
                    users.phone_number,
                    users.fax,
                    users.address,
                    users.zip_code,
                    users.image,
                    users.image_uploaded_date,
                    users.`groups` as manages_group,
                    users.overtime as overtime,
                    DATE(users.active_since) as active_since,
                    group_concat(`groups`.title)        as group_name,
                    group_concat(`groups`.id)           as group_id,
                    schedule.id                         as schedule_id,
                    schedule.range_allowance,
                    schedule.overtime_allowance,
                    manager_users.name                  as manager,
                    positions.id                        as position_id,
                    next_schedule.title                 as next_schedule,
                    next_schedule.start_date            as start_date
                    
                    FROM users
                    LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                    JOIN (SELECT users.id                         as user_id,
                    schedule.id,
                    SEC_TO_TIME(shift.dayRange * 60) as range_allowance,
                    if(users.overtime IS NOT NULL &&  users.overtime !='' &&  users.overtime!='NaN' , 'Yes', 'No') as overtime_allowance
                    FROM users
                    LEFT JOIN schedule ON schedule.id = users.scheduleId
                    LEFT JOIN shift on schedule.id = shift.scheduleId
                    GROUP BY users.id
                    ) AS schedule ON schedule.user_id = users.id
                    JOIN (SELECT users.id                         as user_id,
                    GROUP_CONCAT(manager_users.name) as name
                    FROM users
                    LEFT JOIN users as manager_users ON find_in_set(users.id, manager_users.`controls-users`) > 0
                    GROUP BY users.id) AS manager_users ON manager_users.user_id = users.id
                    JOIN (SELECT users.id as user_id, group_concat(positions.id) as id FROM users LEFT JOIN positions ON find_in_set(users.id,positions.users)>0 GROUP BY users.id) as positions ON positions.user_id=users.id
                    JOIN (SELECT schedule.title,
                                 users.id as user_id,
                                 `users_new_schedule`.startDate as start_date
                          FROM users
                          LEFT JOIN `users_new_schedule` ON FIND_IN_SET(users.id, `users_new_schedule`.user_id)
                          LEFT JOIN  `groups` ON find_in_set(users.id,`groups`.users)
                          LEFT JOIN schedule ON schedule.id = `users_new_schedule`.scheduleId
                          GROUP BY users.id
                          order by `users_new_schedule`.startDate
                          ) as next_schedule ON next_schedule.user_id=users.id
                          WHERE users.id='$user_id'
                    GROUP BY users.id ");

                    $this->data['result_schedule'] = $this->complicatedQuery(null, "SELECT id, title
                                                                                                FROM schedule");

                    echo json_encode($this->data);
                    break;
                case 'edit_group_setting':
                    $user_id = $this->post->user_id;
                    $group_id = $this->post->group_id;
                    $name = $this->post->name;
                    $last_name = $this->post->last_name;
                    $email = $this->post->email;
                    $pin = $this->post->pin;
                    $schedule_id = $this->post->schedule_id=='' ? 0 : $this->post->schedule_id;
                    $phone_number = $this->post->phone_number;
                    $fax = $this->post->fax;
                    $address = $this->post->address;
                    $zip_code = $this->post->zip_code;
                    $manag_group = implode(',', $this->post->manag_group);
                    $overtime = implode(',', $this->post->overtime);
                    $groups = $this->post->groups;
                    $positions = $this->post->positions;

                    $this->updateRecords('positions', "users= REPLACE(REPLACE(REPLACE(users,',$user_id',''),'$user_id,',''),'$user_id','')", '');
                    foreach ($positions as $id) {
                        $this->updateRecords('positions', "users=IF(users='' OR users IS NULL ,'$user_id', CONCAT(users,',$user_id'))", $id . ' ');
                    }
                    $this->updateRecords('groups', "users= REPLACE(REPLACE(REPLACE(users,',$user_id',''),'$user_id,',''),'$user_id','')", '');
                    foreach ($groups as $id) {
                        $this->updateRecords('groups', "users=IF(users='' OR users IS NULL ,'$user_id', CONCAT(users,',$user_id'))", $id . ' ');
                    }
                    $this->updateRecords('users', "name = '$name', last_name='$last_name', email= '$email', pin='$pin', scheduleId='$schedule_id', phone_number='$phone_number', fax='$fax', address='$address', zip_code='$zip_code', groups= '$manag_group', overtime = '$overtime' ", $user_id . ' ');

                    if(isset( $this->post->group_id) AND $group_id!=0 AND $group_id != null){
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            `group`.`group`,
                                                                            `positions`.name     as `positions`,
                                                                            IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as `group`,
                                                                            GROUP_CONCAT(`groups`.id) as `group_id`
                                                                            FROM users
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                                                                            GROUP BY users.id) as `group` ON `group`.id = users.id
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE  users.company_id= '$company_id' AND FIND_IN_SET('$group_id', `group`.group_id) > 0
                                                                            GROUP BY users.id
                                                                            order by users.name ");
                    }else{
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT users.id,
                                                                            users.name,
                                                                            users.last_name,
                                                                            users.start_date,
                                                                            users.active,
                                                                            ' ' as `group`,
                                                                            `positions`.name     as `positions`,
                                                                            IF(users.active=0,'red','#7d7d7d') as text_color
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            WHERE  users.company_id= '$company_id' AND users.id NOT IN ( SELECT users.id FROM users 
                                                                                                                                         JOIN `groups` ON  FIND_IN_SET(users.id, `groups`.users) )
                                                                            GROUP BY users.id
                                                                            order by users.name ");
                    }


                    echo json_encode($this->data);

                    break;
                case 'creat_group':
                    $users = implode(',', $this->post->users);
                    $title = $this->post->title;
                    $find_title = $this->selectRows('groups', 'id', "title ='" . $title . "'", null);
                    if (sizeof($find_title) > 0) {
                        $this->data['error'] = "This Group Name is already in use";
                    } else {
                        $this->data['error'] = "";
                        $this->insertRecordUni('groups', 'title,users', '\'' . $title . '\',\'' . $users . '\'', true);
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT id, title, role
                                                                                             FROM `groups`");
                        $this->data['counts'] = $this->complicatedQuery(null, "SELECT  COUNT(`groups`.id) as count_group,
                                                                                        (SELECT COUNT(id) FROM users) as users_count,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=1) as count_active_users,
                                                                                        (SELECT COUNT(id) FROM users WHERE active=0) as count_deactive_users
                                                                                        FROM `groups`  ");
                    }

                    echo json_encode($this->data);
                    break;
                case 'create_new_employee':
                    $FirstName = $this->post->FirstName;
                    $LastName = $this->post->LastName;
                    $Email = $this->post->Email;
                    $position = $this->post->position;
                    $group = $this->post->group;
                    $PhoneNumber = $this->post->PhoneNumber;
                    $Fax = $this->post->Fax;
                    $Address = $this->post->Address;
                    $Password = $this->post->Password;
                    $Repeatpassword = $this->post->Repeatpassword;
                    $overtime = $this->post->overtime;

                    if (filter_var($Email, FILTER_VALIDATE_EMAIL)) {
                        $count_users = $this->selectRows('users', 'id', "email ='" . $Email . "'", null);
                        if (sizeof($count_users) > 0) {
                            $this->data['error'] = "this email address is already in use";
                        } else {
                            if ($Password == $Repeatpassword) {
                                $uppercase = preg_match('@[A-Z]@', $Password);
                                $lowercase = preg_match('@[a-z]@', $Password);
                                $number = preg_match('@[0-9]@', $Password);
//                                $specialChars = preg_match('@[^\w]@', $Password);
                                if (!$uppercase || !$lowercase || !$number ||  strlen($Password) < 8) {
                                    $this->data['error'] = "Password should be at least 8 characters in length and should include at least one upper case letter, and one number";
                                } else {
                                    if ($FirstName == '' || !isset($FirstName)) {
                                        $this->data['error'] = 'Name is required';
                                    } elseif ($LastName == '' || !isset($LastName)) {
                                        $this->data['error'] = 'Last Name is required';
                                    } elseif ($position == '' || !isset($position)) {
                                        $this->data['error'] = 'Position is required';
                                    } elseif ($PhoneNumber == '' || !isset($PhoneNumber)) {
                                        $this->data['error'] = 'Phone Number is required';
                                    } elseif ($Address == '' || !isset($Address)) {
                                        $this->data['error'] = 'Address is required';
                                    } else {
                                        $this->data['success'] = 'Employeer success added!';

                                        $originCompanyName = substr($this->session->dbName, 17);
                                        $originDb = $this->complicatedQuery(['company'], 'SELECT * FROM `a-jamp-timepunch`.company WHERE name = "'.$originCompanyName.'"', 1);
                                        $this->complicatedQuery(['users'], 'INSERT INTO `a-jamp-timepunch`.users (company_id, email, password, verify, unical_link) VALUE ("'.$originDb->id.'", "'.$Email.'", "'.$this->encording($Password).'", "true", "")' );
                                        $companyId = $this->session->companyId;
                                        $groups = implode(',', $group);
                                        $overtimes = implode(',', $overtime);
                                        $this->insertRecordUni('users', 'company_id, name, last_name, email, password, phone_number, fax, address, groups, overtime ', "'$companyId', '$FirstName', '$LastName', '$Email', '".$this->encording($Password)."', '$PhoneNumber', '$Fax', '$Address', '$groups', '$overtimes'", true);
                                        $last_id_array = $this->selectRows('users', 'max(id) as id', "", null);
                                        $last_id = $last_id_array[0]->id;
                                        foreach ($position as $val) {
                                            $this->updateRecords('positions', "users = if(users='',$last_id,concat(concat(users,','),$last_id)) ", $val . ' ');
                                        }
                                        // send mail
//                                        $this->mailer('You added in jump system', ROOT_URL, $Email);
                                    }
                                }
                            } else {
                                $this->data['error'] = 'Password and confirm password does not match';
                            }
                        }
                    } else {
                        $this->data['error'] = "$Email is not a valid email address";
                    }


                    echo json_encode($this->data);

                    break;
                case 'add_position':
                    $position = $this->post->position;
                    $data_positions = $this->complicatedQuery(['positions'], 'SELECT name FROM positions WHERE name = "'.$position.'"', 1);
                    if($data_positions->name !== $position){
                        $this->insertRecordUni('positions', 'name', '\'' . $position . '\'', true);
                        $this->data['result'] = $this->complicatedQuery(null, "SELECT id, name
                                                                                         FROM positions");
                    }
                    echo json_encode($this->data);

                    break;
                default:
            }
            die();
        }
        $this->data['user'] = $this->selectRows('users', 'image', "id ='" . $this->session->userId . "'", null);
        $this->data['user_info'] = $this->complicatedQuery(null, "SELECT users.name,
                                                                            users.last_name,
                                                                            users.email,
                                                                            users.pin,
                                                                            users.phone_number,
                                                                            users.fax,
                                                                            users.address,
                                                                            users.zip_code,
                                                                            users.groups,
                                                                            users.`controls-users` as controls_user,
                                                                            users.`is_company`,
                                                                            GROUP_CONCAT(overtime.title) as overtime,
                                                                            IFNULL(date(users.active_since),'') as active_since,
                                                                            `group`.`group`,
                                                                            `positions`.name     as `positions`,
                                                                            `manage_group`.name as `manage_group`,
                                                                            GROUP_CONCAT(new_schedule.title) as next_schedule,
                                                                            schedule.title as schedule,
                                                                            CONCAT(GROUP_CONCAT(shift.dayRange),' min') as range_allowance
                                                                            FROM users
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as `group`
                                                                            FROM users
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0
                                                                            GROUP BY users.id) as `group` ON `group`.id = users.id
                                                                            JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(positions.name) as name
                                                                            FROM users
                                                                            LEFT JOIN `positions` ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                            GROUP BY users.id) as `positions` ON `positions`.id = users.id
                                                                            LEFT JOIN (SELECT users.id,
                                                                            GROUP_CONCAT(`groups`.title) as name
                                                                            FROM `users`
                                                                            JOIN groups ON FIND_IN_SET(`groups`.id, users.`groups`) > 0
                                                                            GROUP BY users.id) as `manage_group` ON `manage_group`.id = users.id
                                                                            LEFT JOIN overtime ON FIND_IN_SET(overtime.id,users.overtime)
                                                                            LEFT JOIN users_new_schedule ON users_new_schedule.user_id=users.id
                                                                            LEFT JOIN schedule as new_schedule ON new_schedule.id=users_new_schedule.scheduleId
                                                                            LEFT JOIN schedule  ON schedule.id=users.scheduleId
                                                                            LEFT JOIN day  ON day.userId=users.id AND day.date=CURDATE()
                                                                            LEFT JOIN shift ON shift.id=day.shiftId
                                                                            WHERE users.id='$user_id'
                                                                            GROUP BY users.id");

        $this->data['quick'] = $this->complicatedQuery(null, "SELECT 
                                                                            company.*,
                                                                            timezones.value as timezone
                                                                            FROM users
                                                                            LEFT JOIN company ON company.id=users.company_id 
                                                                            LEFT JOIN `groups` ON FIND_IN_SET(users.id, `groups`.users) > 0 AND `groups`.role=2
                                                                            LEFT JOIN timezones ON company.timezone_id=timezones.id
                                                                            WHERE users.id='$user_id'
                                                                            ");
        $this->data['timezone'] = $this->selectRows('timezones', 'id,label', "", null);

    }

    public function mailer($subjectText, $bodyText, $email){



// Load Composer's autoloader
        require 'PHPMailer/src/Exception.php';
        require 'PHPMailer/src/PHPMailer.php';
        require 'PHPMailer/src/SMTP.php';


// Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->SMTPDebug = 1;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = 'mail.9bit.ge';                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = 'noreply@9bit.ge';                     // SMTP username
            $mail->Password = '8sHJQAFH';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('noreply@9bit.ge', '9bit');

            $mail->addAddress($email);     // Add a recipient

            // Attachments
//    $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subjectText;
            $mail->Body = $bodyText;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

    }

    function uploadImage()
    {
        if ($_FILES['upload_image']) {
            $info = pathinfo($_FILES['upload_image']['name']);
            isset($this->post->user_id) ? $user_id = $this->post->user_id : $user_id = $this->session->userId;
            $i = 0;
            $path = '';
            do {
                $image_name = $info['filename'] . ($i ? "($i)" : "") . "." . $info['extension'];
                $i++;
                $path = 'public/images/users/' . $image_name;
                $this->data['image_name'] = $image_name;
                $this->data['uploaded_date'] = date("Y-m-d");

            } while (file_exists($path));
            if (move_uploaded_file($_FILES['upload_image']['tmp_name'], $path)) {

                $this->updateRecords('users', 'image = "' . $image_name . '",image_uploaded_date = NOW()', $user_id . ' ');

            }
            @unlink($_FILES ['upload_image']);

            echo json_encode($this->data);
            die();
        } elseif ($_FILES['upload_logo_image']) {
            $info = pathinfo($_FILES['upload_logo_image']['name']);
            $i = 0;
            $path = '';
            do {
                $image_name = $info['filename'] . ($i ? "($i)" : "") . "." . $info['extension'];
                $i++;
                $path = 'public/images/quick/' . $image_name;

            } while (file_exists($path));
            if (move_uploaded_file($_FILES['upload_logo_image']['tmp_name'], $path)) {
                $user_id = $this->session->userId;
                $this->data['updatid'] = $this->complicatedQuery(null, " SELECT company.id 
                                                                                      FROM users 
                                                                                      JOIN company ON company.id=users.company_id
                                                                                      WHERE users.id='$user_id'
                                                                            ");
                $updatid = $this->data['updatid'][0]->id;
                $this->updateRecords('company', 'logo = "' . $image_name . '"', $updatid . ' ');

            }
            @unlink($_FILES ['upload_logo_image']);
            $this->data = $image_name;
            echo json_encode($this->data);
            die();
        }

    }

    function getContrastColor($hexColor)
    {

        // hexColor RGB
        $R1 = hexdec(substr($hexColor, 1, 2));
        $G1 = hexdec(substr($hexColor, 3, 2));
        $B1 = hexdec(substr($hexColor, 5, 2));

        // Black RGB
        $blackColor = "#000000";
        $R2BlackColor = hexdec(substr($blackColor, 1, 2));
        $G2BlackColor = hexdec(substr($blackColor, 3, 2));
        $B2BlackColor = hexdec(substr($blackColor, 5, 2));

        // Calc contrast ratio
        $L1 = 0.2126 * pow($R1 / 255, 2.2) +
            0.7152 * pow($G1 / 255, 2.2) +
            0.0722 * pow($B1 / 255, 2.2);

        $L2 = 0.2126 * pow($R2BlackColor / 255, 2.2) +
            0.7152 * pow($G2BlackColor / 255, 2.2) +
            0.0722 * pow($B2BlackColor / 255, 2.2);

        $contrastRatio = 0;
        if ($L1 > $L2) {
            $contrastRatio = (int)(($L1 + 0.05) / ($L2 + 0.05));
        } else {
            $contrastRatio = (int)(($L2 + 0.05) / ($L1 + 0.05));
        }

        // If contrast is more than 5, return black color
        if ($contrastRatio > 5) {
            return '#000000';
        } else {
            // if not, return white color.
            return '#FFFFFF';
        }
    }

    function plan()
    {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
                "https://use.fontawesome.com/releases/v5.6.3/css/all.css",
                "https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css",
                "https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen-sprite.png",
                "header/header.css",
                "footer.css",
                "frame.css",
                "settings.css"
            ],
            'js' => [
                "https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous",
                "https://code.jquery.com/ui/1.12.1/jquery-ui.js\" integrity=\"sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=\" crossorigin=\"anonymous",
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous",
                "https://cdn.jsdelivr.net/npm/sweetalert2@9",
                "https://cdn.jsdelivr.net/npm/@jaames/iro/dist/iro.min.js",
                "https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js",
                "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js",
                "settings.js",
                "basic.js"
            ],
            'plan' => [
                'header',
                'settings',
                'footer'
            ]
        ];
        return $list;
    }


}


?>