<?php

use system\jampModel;

if (!defined('__JAMP__')) {
    header('HTTP/1.0 404 Not Found');
    die();
}

class reset extends system\jampModel
{
    function index() {
        $this->setSession('resetPassword', $this->url[1]);
        if(isset($this->url[1]) && $this->url[1] != ''){
            $this->data['company'] = $this->complicatedQuery([NULL], 'SELECT * FROM `a-jamp-timepunch`.userrecovery as a
                                                    LEFT JOIN `a-jamp-timepunch`.users as b ON a.userid = b.id
                                                    LEFT JOIN `a-jamp-timepunch`.company as c ON b.company_id = c.id
                                                    WHERE a.status =0 AND  a.recoverytoken = "'.$this->url[1].'" 
            ');

            if(empty($this->data['company'])){
                header('location:'.ROOT_URL);
                exit();
            }

        }
    }

    function plan()
    {
        $list = [
            'css' => [
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
                'animate.css',
                "request/glyphter.css",
                'login.css'
            ],
            'js' => [
                'vue.js',
                'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
                'https://use.fontawesome.com/releases/v5.0.13/js/solid.js',
                'https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js',
                'https://www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit',
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
                'login.js',
                'basic.js'
            ],
            'plan' => [
                'reset'
            ]
        ];
        return $list;
    }
}
