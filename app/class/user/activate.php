<?php

use system\jampModel;

if (!defined('__JAMP__')) {
    header('HTTP/1.0 404 Not Found');
    die();
}

class activate extends system\jampModel
{
    function index() {
        if(isset($this->url[1]) && $this->url[1] != ''){
            $this->data['success-code'] = $this->complicatedQuery(
                ['day'],
                'SELECT id FROM `a-jamp-timepunch`.users WHERE verify = "'.$this->url[1].'"', '1'
            );

            if(!empty($this->data['success-code'])){
                $this->complicatedQuery(
                    ['day'],
                    'UPDATE `a-jamp-timepunch`.users SET verify = "true" WHERE id = "'.$this->data['success-code']->id.'"', NULL, NULL, 'true'
                );
            }
        }
    }

    function plan()
    {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "frame.css",
                'login.css',
                'activate.css'
            ],
            'js' => [
                'activate.js'
            ],
            'plan' => [
                'activate',
            ]
        ];
        return $list;
    }
}
