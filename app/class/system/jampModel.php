<?php
namespace system;

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

use \PDO;
use system\jampLanguage as language;

// use const system\core\{_debug, _text, url, config, post, session, cookie};

abstract class jampModel {

	private $db			=   null;
	private $request    =   null;

	public $config      =   null;
	public $url 		=   null;
	public $cUrl        = null;
	public $_text 	    =   null;

    public $tables      =   null;

	public $post 		=   null;
	public $session 	=   null;
	public $cookie 		=   null;

	public $plan        =   null;
    public $data 		=   null;

	public $_location	=   null;
	public $_message    =   null;

    private $_debug     =   true;

    /*
     * user status for session
     */
    public $userLoginStatus = [
        'login'         => 1,
        'recovery'      => 2,
        'loggedOut'     => 3,
        'passwordReset' => 4
    ];

    function __construct($debug, $text, $url, $config, $post, $session, $cookie)
    {
        $currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $this->_debug = $debug;
        $this->_text = $text;
        $this->url = $url;
        $this->config = $config;
        $this->post = $post;
        $this->cookie = $cookie;
        $this->cUrl = $currentUrl;
        $this->session = $session;
        $this->page = !isset($this->url[0]) ? $this->config['defaultPage'] : $this->url[0];

        if (isset($this->url[1]) && $this->url[1] == 'logout') {
            $this->logout();
            $this->redirect('login');
            die();
        }

        if ($this->page == 'quick' || $this->page == 'activate' || $this->page == 'reset') {
            // quick login
        } elseif (!$this->isUserLoggedIn() && $this->page != 'login') {
            $this->redirect('login');
        } elseif ($this->isUserLoggedIn() && $this->page == 'login') {
            $this->redirect($this->config['defaultPage']);

        }
    	#   MySQL Connection
        try {
            $this->db = new PDO(
                'mysql:host='.$this->config["host"].';dbname='.$this->config["name"].';charset=utf8', 
                $this->config["user"], 
                $this->config["pass"],
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                ]
            );
        }
        catch( Exception $ex ) {die($ex->getMessage());}

        $this->plan = $this->plan()['plan'];
        
    	$jampLanguage = new language($this->_debug, $this->config['langList'][0], $this->_text, $this->plan);

        $this->_text = $jampLanguage->index();

        $this->fetchTables();
        if (!isset($this->post) || !isset($this->post->action)) {
            $this->isNewRequest();
        }
        if ($this->isUserLoggedIn()) {
            $this->login_user_info($this->getSession('userId'));
        }
        //  #   index function
        $this->index();
    }

    public function redirect ($url=null) {
        if (is_null($url)) {
            header("Refresh:0");
            die();
        }
        $http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";

        header("Location: {$http}://{$this->config['url']}{$url}");
        die();
    }

    function __destruct() {
        $this->db 			=   null;
        $this->_debug       =   null;
        $this->request 		=   null;
        $this->config 		=   null;
        $this->tables       =   null;
        $this->url 			=   null;
        $this->_text		=   null;
        $this->data			=   null;
    }

    /**
     * @return array
     * basic plan for loading view
     */
    function plan(){
    	$list = [
    		'css' =>[],
    		'js' =>[],
    		'plan' =>[]
    	];
        return $list;
    }

    function index(){
    	return $this->data;
    }

    /**
     * user logout (sets session to loggedOut status => 3)
     */
    private function logout () {
        session_destroy();
    }

    /**
     * @return bool
     * test if the user is login
     */
    public function isUserLoggedIn () {
        $userStatus = $this->getSession('userStatus');

        if (!is_null($userStatus) && $userStatus == $this->userLoginStatus['login']) return true;

        $this->setSession('userStatus', $this->userLoginStatus['loggedOut']);

        return false;
    }

    /**
     * @return array|null
     * returns all of the tables in the database
     */
    function fetchTables(){

        $this->request = $this->db->prepare('SHOW TABLES');

        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . 'SHOW TABLES' . '<br><br>' ;

        $this->request->execute();

        $this->tables = $this->request->fetchAll(PDO::FETCH_COLUMN);
        return $this->tables;
    }

    /**
     * @param null $table
     * @param string $sep
     * @return bool|string
     * sets the query string for getting table info.
     * and adds `example`
     */
    function buildTable($table = null, $sep = '-' ){
        $list = [
            $table,
            $table . $sep . $this->config['langList'][0]
        ];
        
        foreach ($list as $k => $v) {
            if( in_array($v, $this->tables) )return '`'.$v.'`';
        }

        return false;
    }

    /**
     * @param null $tables
     * @param null $query
     * @param null $limit
     * @param null $executeParameters
     * @param bool $lastInsertId
     * @return array|bool|mixed
     * gets tables, builds and combines them to a query, then prepares and executes the code returning one or more items.
     */
    public function complicatedQuery($tables = null, $query = null, $limit = null, $executeParameters=null, $lastInsertId = null){
        if( $tables ){
            foreach ($tables as $key => $value) {
                $tables[$key] = $this->buildTable($value);
            }
            for ($i = 0; $i < count($tables); $i++) {
                $query = str_replace('{table'.$i.'}', $tables[$i], $query);
            }
        }


        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

        $this->request = $this->db->prepare($query);

        if (is_null($executeParameters)) {
            $this->request->execute();
        } else {
            $this->request->execute($executeParameters);
        }

        if (!is_null($lastInsertId)) {
            return $this->db->lastInsertId();
        }

        if ($this->request) {
        	if($limit==true){
                return $this->request->fetch(PDO::FETCH_OBJ);
            }
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    /**
     * @param null $table
     * @param null $fields
     * @param null $condition
     * @param null $order
     * @param null $origLimit
     * @return array|bool|mixed
     * gets all of the information needed for a query. (FOR one table only)
     */
    function selectRows($table = null, $fields = null, $condition = null, $order = null, $origLimit = null){

        $limit = null;
        $table = $this->buildTable($table);

        if( $fields===null ) {
            $fields = '*';
        }
        if( $condition ) {
            $condition = " WHERE " . $condition;
        }
        if( $order ) {
            $order = " ORDER BY " . $order;
        }
        if( $origLimit ) {
            $limit = " LIMIT " . $origLimit;
        }

        $query = sprintf("SELECT %s FROM %s %s %s %s", $fields, $table , $condition, $order, $limit);
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

        $this->request = $this->db->prepare($query);
        $this->request->execute();
         
        if ($this->request) {
            if($origLimit==1){
                return $this->request->fetch(PDO::FETCH_OBJ);
            }
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    /**
     * @param null $table
     * @param null $fields
     * @param null $condition1
     * @param null $condition2
     * @param null $order
     * @param string $limit
     * @return array|bool
     * basically same as the complicatedQuery, only one difference this uses `union` (gets data from tow tables)
     */
    function unionSame($table = null, $fields = null, $condition1 = null, $condition2 = null, $order = null, $limit = '' ){
    	$table = $this->buildTable($table);
    	if( $fields===null ){
    	    $fields = '*';
    	}
    	if( $condition1 ) {
    	    $condition1 = " WHERE ".$condition1;
    	}
    	if( $condition2 ) {
    	    $condition2 = " WHERE ".$condition2;
    	}
    	if( $order ) {
    	    $order = " ORDER BY ".$order;
    	}
    	if( $limit ) {
    	    $limit = " LIMIT ".$limit;
    	}

    	$query = sprintf("(SELECT %s FROM `%s` %s %s %s) UNION (SELECT %s FROM `%s` %s %s %s)", $fields, $table , $condition1, $order, $limit, $fields, $table , $condition2, $order, $limit);

        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

    	$this->request = $this->db->prepare($query);
        $this->request->execute();
                     
    	if ($this->request) {
    		return $this->request->fetchAll(PDO::FETCH_OBJ);
    	}
    	return false;
    }

    /**
     * @param string $table
     * @param string $condition
     * @return bool|mixed
     */
    function countRows($table = '', $condition= '' ){
    	$table = $this->buildTable($table);
    	if( !empty($condition) ){
    		$condition = "WHERE " . $condition;
    	}
        $query = sprintf("SELECT COUNT(*) FROM `%s` %s" , $table , $condition);
        
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

        $this->request = $this->db->prepare($query);
    	$this->request->execute();
        if ($this->request) {
            return $this->request->fetchColumn(0);
        }
        return false;
    }

    public function isNewRequest () {
        $userId = $this->getSession('userId');
        if (is_null($userId)) {
            $this->data['isNewRequest'] = false;
            return null;
        }

        $this->data['isNewRequest'] = $this->complicatedQuery(['dayrequest'], 'SELECT * FROM dayrequest WHERE toUser = ? AND status NOT LIKE "%4%"', 1, [
            $userId
            ]);
    }

    public function login_user_info($user_id){
        $this->data['login_user_info'] = $this->complicatedQuery(null, "SELECT CONCAT(users.name,' ',users.last_name) as name, users.is_company,
                                                                                             GROUP_CONCAT(positions.name) as position 
                                                                                        FROM users
                                                                                        LEFT JOIN positions ON FIND_IN_SET(users.id, `positions`.users) > 0
                                                                                        WHERE users.id='$user_id'
                                                                                        GROUP BY users.id");
    }

    function updateRecords($table = '', $set = '', $id = '') {
        if( !empty($table) && !empty($set) && !empty($id) ){
            $table = $this->buildTable($table);
            $query = sprintf("UPDATE %s SET %s WHERE `id` = %d;", $table, $set, $id);

            //  #   Debug
            if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

            $this->request = $this->db->prepare($query);
            $this->request->execute();

            if ($this->request) {
                return true;
            }
            return false;
        }
//        without where for  update all row
       elseif( !empty($table) && !empty($set)){
            $table = $this->buildTable($table);
            $query = sprintf("UPDATE %s SET %s ", $table, $set);

            //  #   Debug
            if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

            $this->request = $this->db->prepare($query);
            $this->request->execute();

            if ($this->request) {
                return true;
            }
            return false;
        }
    }

    function deleteRecords($table = '',  $id = '') {
        if( !empty($table)  && !empty($id) ){
            $table = $this->buildTable($table);
            $query = sprintf("DELETE FROM  %s  WHERE `id` = %d;", $table, $id);

            //  #   Debug
            if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

            $this->request = $this->db->prepare($query);
            $this->request->execute();

            if ($this->request) {
                return true;
            }
            return false;
        }
    }

    function insertRecordUni($table='', $fields='', $values='', $lastInsertId = false) {
        if( !empty($table) && !empty($fields) && !empty($values) ){
            $table = $this->buildTable($table);
            $query = sprintf("INSERT INTO %s (%s) VALUES (%s)", $table, $fields, $values);
            //  #   Debug
            if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

            $this->request = $this->db->prepare($query);
    		$this->request->execute();

            if( $lastInsertId ){
                return $this->db->lastInsertId();
            }

            if ($this->request) {
                return true;
            }

            return false;
        }
    }

    function groupBy( $table = null, $fields = null, $condition = null, $groupBy = null, $order = null ){
        $table = $this->buildTable($table);

        if( $fields===null ){ $fields = '*'; }
        if( $condition ) { $condition = " WHERE ".$condition; }
        if( $order ) { $order = " ORDER BY ".$order; }
        $groupBy =" GROUP BY ".$groupBy;

        $query = sprintf("SELECT %s FROM `%s` %s %s %s" , $fields, $table, $condition, $groupBy, $order);
        
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

        $this->request = $this->db->prepare($query);
        $this->request->execute();
        if ($this->request) {
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    function describeTable($table = null){
        $table = $this->buildTable($table);
        $query = sprintf( "DESCRIBE %s" , $table );
        
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query, PHP_EOL;

        $this->request = $this->db->prepare($query);
        $this->request->execute();
        if ($this->request) {
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    function customQuery( $query= '', $OBJ = true){
        if( !empty($query) ){
            $this->request = $this->db->prepare($query);
            $this->request->execute();
        }

        if ($this->request) {
            if ( $OBJ ){
                if ($OBJ == 2) {
                    return $this->request->fetchAll(PDO::FETCH_COLUMN, 0);
                }
                return $this->request->fetchAll(PDO::FETCH_OBJ);
            } else {
                return $this->request->fetchAll();
            }
        }
        return false;
    }
    
    function notify( $location = null, $message = null ){

        $this->_location = $this->config["defaultPage"];
        $this->_message     =   $message;

        if($location)$this->_location    =   $location;
    	
        return;
    }

    function setSession($name = null, $value = null){
    	session_regenerate_id();
    	$_SESSION[$name] = $value;
    }


    public function getSession ($name=null) {
        if (is_null($name)) return null;

        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    function encording($value = null){
        if( $value ){
            $value = crypt($value, '$'.$this->config["algo"].'$rounds='.$this->config["roun"].'$'.$this->config["solt"].'');
            $value = base64_encode( $value );
            $value = md5($value);
            return $value;
        }
        return false;
    }

    public function sendEmail ($email=null, $subject=null, $txt=null, $headers=null) {
        if (is_null($email) || is_null($subject) || is_null($txt) || is_null($headers)) return null;

        email($email, $subject, $txt, $headers);
    }

    public function selectUserCompanyTimeZone ($userId=null) {
        if (!isset($userId) || is_null($userId)) return null;

        return $this->complicatedQuery(['users'], '
            SELECT `value` FROM timezones WHERE id = (SELECT timezone_id FROM company WHERE id = (SELECT company_id FROM users WHERE id = ?))
        ', false, [
            $userId
        ]);
    }

}