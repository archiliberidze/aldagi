<?php 
namespace system;

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

class jampLanguage {

	private $_debug		=   null;
	private $_lang		=   null;
	private $_text		=   null;
	private $_plan		=   null;
	

	function __construct($debug, $lang, $text, $plan){
		$this->_lang	=	$lang;
		$this->_text	=	$text;
		$this->_plan	=	$plan;
		$this->_debug	=	$debug;
		
		$this->load();
	}

	function __destruct(){
		$this->_debug	=	null;
		$this->_lang 	=	null;
		$this->_text 	=	null;
		$this->_plan 	=	null;
	}

	function index(){
		return $this->_text;
	}

	// 	#	Load page translation file
	function load( $s = '_', $e = '.lang' ) {
		foreach ( $this->_plan as $k => $v ){
    	    if ( !file_exists ( __JAMP__['lang'] . $v . $s . $this->_lang . $e ) ){
	            break;
	        }

	        //  #   Debug
        	if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugLangFile'] . __JAMP__['lang'] . $v . $s . $this->_lang . $e, PHP_EOL;

        	$fileArray = preg_split("/\\r\\n|\\r|\\n/", file_get_contents( __JAMP__['lang'] . $v . $s . $this->_lang . $e ));
	        foreach ($fileArray as $key => $value) {
	            if ( strpos($value, '###') !== 0 && $value!=='' ) {
	                $langArray = array_filter( explode("===", $value) );
	                if( isset($langArray[0]) && isset($langArray[1]) ){
	                    $this->_text[$langArray[0]] = $langArray[1];
	                }
	            }
	        }
    	}
    }


}
?>