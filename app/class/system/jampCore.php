<?php 
namespace system; 

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

use system\jampNotification as notification;
use system\jampDebug as debug;
use system\jampRoute as route;

class jampCore{

	private $config         =   null;
	private $page       	=   null;

	private $_debug 		=   null;
	private $_error 		=   null;
	
	public $url             =   null;

	public $post        	=   null;
	public $session     	=   null;
	public $cookie      	=   null;

	public $root            =   null;
	public $plan            =   null;
	public $_text        	=   null;
	public $data            =   null;

	function __construct( $config, $debug ) {
	    $this->config = $config;
	    $this->_debug = $debug;

	    $this->files = $this->config['files'];
	    $this->page = $this->config['defaultPage'];

	    //  #   Debug
        if( $this->_debug ){
        	$debug 			= new debug;
        	$this->_text 	= $debug->index();
        }
	    
	    //  #   _POST
	    $this->processPost();
	    //  #   $_COOKIE
	    $this->processCookie();
	    //  #   $_SESSION
	    $this->processSession();
	    // 	#	Route
	    $this->route();
	    //  # 	Page
	    $this->setPage();
	    $this->getData();
	    // 	#	Unset
	    $this->unsetVariables();
	}

	function __destruct() {
	    $this->config       =   null;
	    $this->page 		=   null;
	    $this->url          =   null;
	    $this->root         =   null;
	}

	function processPost(){
	    if( isset($_POST) && !empty($_POST) ){
	    	//  #   Debug
	        if( $this->_debug ){
	            echo $this->_text['_debug'] . $this->_text['_debugPostList'] . '<pre>' ; 
	            print_r( $_POST );
	            echo '</pre>';
	        }
	        foreach ($_POST as $key => $value) {
	            if( strpos($key, "-") !== false ){
	                $newKey = null;
	                foreach ( explode("-", $key) as $key1 => $value1) {
	                    $newKey.=($key1!=0? ucfirst($value1) : $value1);
	                }
	                $key = $newKey;
	            }
	            if( empty($value) && $value!=='0' ){
	                $this->post[$key] = null;
	            }
	            elseif( is_array($value) ){
	                foreach ($value as $key1 => $value1) {
	                    $this->post[$key][$key1] = filter_var($value1, FILTER_SANITIZE_STRING);
	                }
	            }
	            else{
	                $this->post[$key] = filter_var($value, FILTER_SANITIZE_STRING);
	            }
	        }
	        $this->post = (object)$this->post;
	        unset($_POST);
	    }
	}

	function processSession(){
	    if( isset($_SESSION) && !empty($_SESSION) ){
	    	//  #   Debug
	        if( $this->_debug ){ 
	            echo $this->_text['_debug'] . $this->_text['_debugSessionList'] . '<pre>' ; 
	            print_r( $_SESSION );
	            echo '</pre>';
	        }
	        $this->session = (object)$_SESSION;
	    }
	}

	function processCookie(){
		//  #   Debug
	    if( $this->_debug ){ 
	        echo $this->_text['_debug'] . $this->_text['_debugCookieList'] . '<pre>' ; 
	        print_r( $_COOKIE );
	        echo '</pre>';
	    }
	    $this->cookie = (object)$_COOKIE;
	}

	function translate($key = null){
	    if( isset($this->_text[$key]) ){
	        return $this->_text[$key];
	    }else{
	        return 'No Translation: '.$key;
	    } 
	}

	function route(){
		$route = new route($this->_debug,$this->_text, $this->page);
		$this->root = $route->_root;
		$this->url = $route->_url;
	}

	function compiler( $pagePath ) {
		$this->render( $pagePath );
	}

	//  #   Define page
	function setPage(){
	    if( isset($this->url[0]) ){
	        if( strpos($this->url[0], "-") !== false ){
	            $this->page = null;
	            foreach ( explode("-", $this->url[0]) as $key => $value) {
	                $this->page.=($key!=0? ucfirst($value) : $value);
	            }
	        }
	        else{
	            $this->page = $this->url[0];
	        }

	        if( is_numeric(substr($this->page, 0, 1)) ){
	            $this->page = lcfirst(substr($this->page, 1));
	        }
	    }
	}


    /**
     * loads 404 html page from config
     */
    public function load404 () {
        include_once($this->config['404Page']);
        die();
	}

	//  #   build Page
	function getData(){
        if (!file_exists(__JAMP__['user'].$this->page.__JAMP__['ext'])) $this->load404();

	    $class = new $this->page($this->_debug, $this->_text, $this->url, $this->config, $this->post, $this->session, $this->cookie);
	    $this->_text = $class->_text;
		//  #   Notifications
	    if( $class->_location || $class->_message ){

//        	$error 			= 	new notification;
//        	$error->_text 	= 	$this->_text;
//
//	        $error -> index( $class->_location, $class->_message );
	    }

	    $this->plan = $class->plan();
	    $this->data = $class->data;
	}

	// 	#	unset variables
	function unsetVariables(){
		$this->config = null;
	}

	// Compiler
	function render($page=null) {
        if (is_null($page)) $this->load404();

        if(file_exists($page)) {
			ob_start();
            include($page);
            $output = ob_get_clean();
            $output = $this->files($output);

            echo $output;
		}
	}

	function files($buffer){
		return preg_replace_callback('~@file\{[\W](.*?)[\W]\}~s', [$this, 'checkFiles'], $buffer);
	}

	function checkFiles($matches){
		$matches = array_pop($matches);
		$result = $this->scanFiles(__JAMP__['images'], $matches);

		return $result;
	}

	function scanFiles($directory = null, $matches = null){
		$matches = explode(',', $matches);

		$newItems = [];
		foreach($matches as $items) {
			$items = explode('=', $items);

			$keys = trim(reset($items));
			$values = trim(end($items));
			$newItems[$keys] = $values;
		}

		$filePath = reset($newItems);
		$fileDirectory = $directory.$filePath;

		if(file_exists($fileDirectory)) {
			$fileExtension = pathinfo($fileDirectory, PATHINFO_EXTENSION);
			if( in_array($fileExtension, $this->files) ) {
				$newItems['src'] = $directory.$newItems['src'];
				
				$result = null;
				foreach($newItems as $key => $value) {
					$result .= $key.'="'.$value.'" ';
				}
				
				return '<img '.$result.'/>';
			}
		}
		else {
			return 'File not found';
		}
	}
}