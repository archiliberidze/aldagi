<?php 
namespace system;

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

class jampRoute {
	private $_debug				=   null;
	private $_text				=   null;

	private $_check 			=	['.php', 'script'];
	private $_page				=   null;

	public $_url 				=	null;
	public $_root				=   null;

	function __construct($debug, $text, $page){
		$this->_debug 	= 	$debug;
		$this->_text	= 	$text;
		$this->_page 	= 	$page;
		$this->_url 		=	$_SERVER["REQUEST_URI"];

		$this->setUrlRooting();
		$this->setServerRoot();

		$this->index();
	}

	function __destruct(){
		$this->_debug 			= null;
		$this->_text 			= null;
		$this->_page 			= null;
	}

	function index(){
	}

	function setUrlRooting(){

		//  #   Check url
	    foreach ($this->_check as $k => $v) {
	        if( strpos( strtolower( $this->_url ), $v ) !== false ){
	            // If match found locate to default url
	            header( "Location: " . $this->_page );
	            die();
	        }
	    }
		//  #   Rooting
	    $this->_url = array_values(
	                        array_filter(
	                            array_diff(explode("/", $this->_url), explode('\\', getcwd()))
	                        )
	                    );
	    //  #   Debug
	    if( $this->_debug ){ 
	        echo $this->_text['_debug'] . $this->_text['_debugUrl'] . '<pre>' ; 
	        print_r( $this->_url );
	        echo '</pre>';
	    }
	    if(empty($this->_url))$this->_url[0] = $this->_page;

	}

	function setServerRoot() {
	    $protocol = 'http';
	    if ( $_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ) {
	        $protocol .= 's';
	    }
	    $host = $protocol ."://" . $_SERVER['HTTP_HOST'];
	    $host = str_replace("www.www", "www", $host);
	    // $dir = str_replace( $_SERVER['PHP_SELF'], '', preg_replace( '/([\w.]+)$/', '', $_SERVER['SCRIPT_NAME'] ) );
	    $dir = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
	    $this->_root = $host . $dir;
	    //  #   Debug
	    if( $this->_debug ){ 
	        echo $this->_text['_debug'] . $this->_text['_debugRoot'] . '<pre>' ; 
	        echo '"' . $protocol . '", "' . $host . '", "' . $dir .'"';
	        echo '</pre>';
	    }
	}
}

?>