<?php 
namespace system;

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

class jampDebug {
	private $debug				=   null;
	private $debugFileName		=	__JAMP__['lang'] . '_debug.lang';

	private $_text				=	null; //	Text information

	function __construct(){
		$this->load();
	}

	function __destruct(){
		$this->debug 			= null;
		$this->debugFileName 	= null;
		$this->_text 			= null;
	}

	function index(){
		return $this->_text;
	}

	// 	# 	Load Debug Language File
	function load(){
        $prepare = preg_split("/\\r\\n|\\r|\\n/", file_get_contents( $this->debugFileName ));

        foreach ($prepare as $key => $value) {

            if ( strpos($value, '###') !== 0 && $value!=='' ) {
                $langArray = array_filter( explode("===", $value) );

                if( isset($langArray[0]) && isset($langArray[1]) ){
                    $this->_text[$langArray[0]] = $langArray[1];
                }

            }

        }
    }
}


?>