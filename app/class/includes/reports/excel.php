<?php

//echo '<pre>';
//print_r($this->data['excel']);
//echo '</pre>';
//die();
ob_end_clean();
ob_start();

require_once('app/excel/Classes/PHPExcel.php');
$phpexcel = new PHPExcel();

$dayCounts = count($this->data['excel']);
$i = 2;

$page = $phpexcel->setActiveSheetIndex(0);
$page->setCellValue("A1", $this->data['excel'][0]->name.' '.$this->data['excel'][0]->last_name);
$page->setCellValue("B1", "Date");
$page->setCellValue("C1", "Clock In");
$page->setCellValue("D1", "Lunch Start");
$page->setCellValue("E1",  "Lunch End");
$page->setCellValue("F1", "Clock Out");
$page->setCellValue("G1", "Position");

foreach($this->data['excel'] as $item){


    $clockInEx = explode(',', $item->day_time_clockIn);
    $clockOutEx = explode(',', $item->day_times_clockOut);
    $lunchStEx = explode(',', $item->day_times_startLunch);
    $lunchEndEx = explode(',', $item->day_times_endLunch);
    $clockOutStatusEx = explode(',', $item->clockOutStatus);
    $countClockOutEx = count($clockInEx) - 1;

    if(isset($clockInEx) && $lunchEndEx[0] != 1){ $clockIn = substr($clockInEx[0], 11); }else{ $clockIn = '—'; }
    if(isset($lunchStEx) && isset($clockInEx) && $lunchStEx[0] == 1){$lunchStart = substr($clockInEx[1], 11);}else{$lunchStart = '—';}
    if(isset($lunchEndEx[1]) && isset($clockInEx) && $lunchEndEx[1] == 1){$lunchEnd = substr($clockOutEx[1], 11);}else{$lunchEnd = '—';}
    if(isset($clockOutEx) && $lunchStEx[$countClockOutEx] != 1){$clockOut = substr($clockOutEx[$countClockOutEx], 11);}else{$clockOut = '—';}

    $page->setCellValue("A".$i, "");
    $page->setCellValue("B".$i, $item->date);
    $page->setCellValue("C".$i, $clockIn);
    $page->setCellValue("D".$i, $lunchStart);
    $page->setCellValue("E".$i, $lunchEnd);
    $page->setCellValue("F".$i, $clockOut);
    $page->setCellValue("G".$i, $item->position_name);
    $i++;
}
$page->setTitle("excel_times"); // Ставим заголовок "Test" на странице

/* Начинаем готовиться к записи информации в xlsx-файл */
$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
/* Записываем в файл */
$objWriter->save("excel_times.xlsx");
header('Content-Type: application/vnd.ms-excel');