<div class="accordion groyp_by_employees" id="accordionExample">
    <?php
    $last_user_id = '';
    $countDay = count($this->data['day']);
    $dayCount = 1;
    $dayCount = 1;
    $nextUser = 1;
    $total_overtimes = 0;
    $noExtraWork = 0;
    $latness = 0;
    $totalLatness = 0;
    $totalLunch = 0;
    foreach ($this->data['day'] as $item) {
        $clockInEx = explode(',', $item->day_time_clockIn);
        $clockOutEx = explode(',', $item->day_times_clockOut);
        $lunchStEx = explode(',', $item->day_times_startLunch);
        $lunchEndEx = explode(',', $item->day_times_endLunch);
        $clockOutStatusEx = explode(',', $item->clockOutStatus);
        $day_time_idEx = explode(',', $item->day_time_id);
        $countEx = count($clockInEx) - 1;
        $stayMinutesOnWork = (strtotime($item->clockOut) - strtotime($item->clockIn) - strtotime($item->duration)) / 60;

        // Calculate Before Extra Time Start
        $beforeExtraTime = (strtotime($item->clockIn) - strtotime(substr($clockInEx[0], 11))) / 60;
        if ($beforeExtraTime <= 0) {
            $beforeExtraTime = 0;
        }
        // Calculate Before Extra Time End

        // Calculate After Extra Time Start
        $personWorkingTime = 0;
        for ($i = 0; $i <= $countEx; $i++) {
            $personWorkingTime += floor((strtotime(substr($clockOutEx[$i], 11)) - strtotime(substr($clockInEx[$i], 11))) / 60);
            $workWithoiutExtra = $personWorkingTime;
        }
        $personWorkingTime = $personWorkingTime - $beforeExtraTime;
        $afterExtratime = $personWorkingTime - $stayMinutesOnWork;
        if ($afterExtratime <= 0) {
            $afterExtratime = 0;
        }
        // Calculate After Extra Time End
        $workWithoiutExtra = $workWithoiutExtra - $beforeExtraTime - $afterExtratime;
        ?>

        <div>
            <?php
            if ($item->user_id != $last_user_id) {
                ?>
                <div class="row selection_color">
                    <div class="col-sm-3 clolor_info border-right bg-gray">
                        <?php echo (isset($item->last_name) ? $item->last_name : '').' '.(isset($item->name) ? $item->name : ''); ?>
                    </div>
                    <div class="col-sm-7 color-dark-gray border-right bg-gray">
                        <?php echo(isset($item->position_name) ? $item->position_name : ''); ?>
                    </div>
                    <div class="col-sm-2 bg-gray center">
                        <span class="icon-download download-btn"></span>
                        <span class="icon-print print-btn"></span>
                        <span class="icon-icon_exel exel-btn cursor-pointer" id="excelUuserId_<?= $item->user_id; ?>"></span>
                    </div>

                    <div class="padding-10-10 col-sm-2 b-t-1-ccc b-l-1-ccc">
                        <span class="p-l-15">Date</span>
                    </div>
                    <div class="padding-10-10 col-sm-6 b-t-1-ccc">
                        <div class="row">
                            <div class="col-sm-3">Clock In</div>
                            <div class="col-sm-3">Start Lunch</div>
                            <div class="col-sm-3">End Lunch</div>
                            <div class="col-sm-3">Clock Out</div>
                        </div>
                    </div>
                    <div class="padding-10-0-0 col-sm-2 center b-t-1-ccc">
                        <p class="m-b-0">Extratime</p>
                        <span class="sm">Before</span>
                        <span class="sm">After</span>
                    </div>
                    <div class="padding-10-10 col-sm-2 right p-r-10 b-t-1-ccc b-r-1-ccc">Log | Edit</div>
                </div>
                <?php
            }
            $last_user_id = $item->user_id;
            ?>
            <div class="card border-none">
                <div id="heading<?= $day_time_idEx[0]; ?>">
                    <div class="row b-r-1-ccc b-l-1-ccc   <?php echo (!isset($this->data['day'][$nextUser]->user_id) || $item->user_id != $this->data['day'][$nextUser]->user_id) ? 'b-b-1-ccc' : ''; ?>">
                        <div class="padding-10-10 col-sm-2 <?php echo (isset($clockInEx[1])) ? 'p-l-0' : ''; ?> <?php echo (isset($clockInEx[1])) ? 'cursor-pointer' : ''; ?>"
                             data-toggle="collapse" <?php echo (isset($clockInEx[1])) ? 'data-target="#collapse' . $day_time_idEx[0] . '" aria-expanded="false" aria-controls="collapse' . $day_time_idEx[0] : ''; ?>
                        ">
                        <?php echo (isset($clockOutStatusEx[1]) && $clockOutStatusEx[1] == 0 ) ? '<span id="triangle-right"></span>' : ''; ?>
                        <span class="p-l-15"><?php echo(isset($item->date) ? $item->date : ''); ?></span>
                    </div>
                    <div class="padding-10-10 col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo(isset($clockInEx) && $lunchEndEx != 1 ? substr($clockInEx[0], 11) : ''); ?></div>
                            <div class="col-sm-3"><?php echo(isset($lunchStEx) && isset($clockOutEx) && $lunchStEx[0] == 1 ? substr($clockOutEx[0], 11) : '—'); ?></div>
                            <div class="col-sm-3">
                                <?php
                                if(isset($lunchEndEx[0]) && ($lunchEndEx[0] == 1)){
                                    echo substr($clockInEx[0], 11);
                                }elseif(isset($lunchEndEx[1]) && ($lunchEndEx[1] == 1)){
                                    echo substr($clockInEx[1], 11);
                                }else{
                                    echo '—';
                                }
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <?php
                                if(isset($lunchStEx[0]) &&$lunchStEx[0] == 1 && isset($lunchEndEx[1]) && $lunchEndEx[1] == 1){
                                    $totalLunch += date("i",strtotime($clockInEx[1]) - strtotime( $clockOutEx[0]));
                                }

                                if(isset($clockOutStatusEx[0]) && $clockOutStatusEx[0] == 1){
                                    echo substr($clockOutEx[0], 11);
                                    $i = 1;
                                }elseif(isset($clockOutStatusEx[1]) && $clockOutStatusEx[1] == 1){
                                    echo substr($clockOutEx[1], 11);
                                    $i =2;
                                }else{
                                    echo '—';
                                    $i = 1;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="padding-10-10 col-sm-2 center">
                        <div class="extratime extratime-right-line"><?php echo round($beforeExtraTime); ?> min
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="hidden" value="<?php echo round($beforeExtraTime); ?>"
                                       class="before-extratime-<?= $item->day_id; ?>"/>
                                <input type="checkbox" class="custom-control-input dayInputBefore"
                                       name="dayIds[]" <?php if (!empty($item->before_extratime)) echo 'checked'; ?>
                                       value="<?= $item->day_id; ?>"
                                       id="before-defaultInline1<?= $day_time_idEx[0]; ?>">
                                <label class="custom-control-label my-checkbox"
                                       for="before-defaultInline1<?= $day_time_idEx[0]; ?>"></label>
                            </div>
                        </div>


                        <div class="extratime"><?php echo round($afterExtratime); ?> min
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="hidden" value="<?php echo round($afterExtratime); ?>"
                                       class="after-extratime-<?= $item->day_id; ?>"/>
                                <input type="checkbox" class="custom-control-input dayInputAfter"
                                       name="dayIds[]" <?php if (!empty($item->after_extratime)) echo 'checked'; ?>
                                       value="<?= $item->day_id; ?>"
                                       id="defaultInline1<?= $day_time_idEx[0]; ?>">
                                <label class="custom-control-label my-checkbox"
                                       for="defaultInline1<?= $day_time_idEx[0]; ?>"></label>
                            </div>
                        </div>
                    </div>
                    <div class="padding-10-10 col-sm-2 right p-r-20">
                        <a <?php if (!empty($item->request_day_id)) {
                            echo 'href';
                        } ?> class="icon-icon_log pencil-a p-r-10 <?php if (empty($item->request_day_id)) {
                            echo 'disabled-log';
                        } else {
                            echo ' log-day';
                        } ?>" <?php if (!empty($item->request_day_id)) {
                            echo 'data-toggle="modal"
                                   id="log-' . $item->day_id . '-' . $item->user_id . '" data-target="#exampleModalCenter-2"';
                        } ?>></a>
                        <a href class="icon-pencil pencil-a update-day" data-toggle="modal"
                           id="<?= $day_time_idEx[0]; ?>" data-target="#exampleModalCenter"></a>
                        <input type="hidden" class="day_id" value="<?php echo $item->day_id; ?>"/>
                    </div>
                    <div id="collapse<?= $day_time_idEx[0]; ?>" class="collapse col-12 selection-day-times"
                         aria-labelledby="heading<?= $day_time_idEx[0]; ?>" data-parent="#accordionExample">
                        <div class="card-body">
                            <?php
                            for ($i = 1; $i <= $countEx; $i++) {
                                ?>
                                <div class="row">
                                    <div class="padding-10-10 col-sm-2"></div>
                                    <div class="padding-10-10 col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-3"><?php echo(isset($clockInEx) && $lunchEndEx[$i] != 1 ? substr($clockInEx[$i], 11) : '—'); ?></div>
                                            <div class="col-sm-3"><?php echo(isset($lunchStEx) && isset($clockInEx) && $lunchStEx[$i] == 1 ? substr($clockInEx[$i], 11) : '—'); ?></div>
                                            <div class="col-sm-3"><?php echo(isset($lunchEndEx) && isset($clockInEx) && $lunchStEx[$i] == 1 ? substr($clockOutEx[$i], 11) : '—'); ?></div>
                                            <div class="col-sm-3"><?php echo(isset($clockOutEx) && $lunchStEx[$i] != 1 ? substr($clockOutEx[$i], 11) : '—'); ?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-2 center">
                                        <a href class="icon-pencil pencil-a update-day" data-toggle="modal"
                                           id="<?= $day_time_idEx[$i]; ?>"
                                           data-target="#exampleModalCenter"></a>
                                        <input type="hidden" class="day_id"
                                               value="<?php echo $item->day_id; ?>"/>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php $total_overtimes += floor($afterExtratime) + floor($beforeExtraTime);
                    $hours = floor($total_overtimes / 60);
                    $minutes = $total_overtimes % 60;

                    ($workWithoiutExtra >= 1) ? $noExtraWork += $workWithoiutExtra: $noExtraWork += 0;
                    $totalLatness += $latness;

                    if(!isset($this->data['day'][$nextUser]->user_id) || $item->user_id != $this->data['day'][$nextUser]->user_id){ ?>
                        <div class="calulate-times b-t-1-ccc col-12">
                            <div class="row">
                                <div class="col-2">Absence <span class="clolor_info">1 day</span></div>
                                <div class="col-2">Lateness <span class="clolor_info"><?php printf('%02d:%02d', floor($totalLatness / 60), $totalLatness % 60); ?> hr</span></div>
                                <div class="col-3">Total Lunch <span class="clolor_info"><?php printf('%02d:%02d', floor( $totalLunch / 60), $totalLunch % 60); ?> hr</span></div>
                                <!--                                <div class="col-2">Lunch delay <span class="clolor_info">4 hr</span></div>-->
                                <div class="col-3">Work hours <span class="clolor_info"><?php printf('%02d:%02d', floor($noExtraWork / 60), $noExtraWork % 60); ?> hr</span></div>
                                <div class="col-2">Overtimes <span class="clolor_info"><?php printf('%02d:%02d', $hours, $minutes); ?> hr</span></div>
                            </div>
                        </div>
                        <?php $total_overtimes = 0; $noExtraWork = 0; $latness = 0; $totalLatness = 0; $totalLunch = 0; } $nextUser += 1; ?>
                </div>
            </div>
        </div>
        <?php
        $dayCount++;
    }
    ?>
</div>