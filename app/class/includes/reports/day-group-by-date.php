<?php
//    echo '<pre>';
//    print_r($this->data['day']);
//    echo '</pre>';
?>

<div class="groyp_by_date">
    <?php
    $num_day = '';
    foreach ($this->data['day'] as $item) {
        $clockInEx = explode(',', $item->day_time_clockIn);
        $clockOutEx = explode(',', $item->day_times_clockOut);
        $lunchStEx = explode(',', $item->day_times_startLunch);
        $lunchEndEx = explode(',', $item->day_times_endLunch);
        $day_time_idEx = explode(',', $item->day_time_id);
        $countEx = count($clockInEx) - 1;
        $stayMinutesOnWork = (strtotime($item->clockOut) - strtotime($item->clockIn) - strtotime($item->duration)) / 60;

        // Calculate Before Extra Time Start
        $beforeExtraTime = (strtotime($item->clockIn) - strtotime(substr($clockInEx[0], 11))) / 60;
        if ($beforeExtraTime <= 0) {
            $beforeExtraTime = 0;
        }
        // Calculate Before Extra Time End

        // Calculate After Extra Time Start
        $personWorkingTime = 0;
        for ($i = 0; $i <= $countEx; $i++) {
            $personWorkingTime += floor((strtotime(substr($clockOutEx[$i], 11)) - strtotime(substr($clockInEx[$i], 11))) / 60);
        }
        $personWorkingTime = $personWorkingTime - $beforeExtraTime;
        $afterExtratime = $personWorkingTime - $stayMinutesOnWork;
        if ($afterExtratime <= 0) {
            $afterExtratime = 0;
        }
        // Calculate After Extra Time End
        ?>
        <div class="row selection_color">
            <?php if ($item->date != $num_day) { ?>
            <div class="col-sm-10 m-t-10">
                <?php echo (isset($item->date) ? $item->date : ''); ?>
            </div>
            <div class="col-sm-2 center m-t-10">
                <span class="icon-download download-btn disabled text-secondary"></span>
                <span class="icon-print print-btn disabled text-secondary"></span>
                <span class="icon-icon_exel exel-btn cursor-pointer" id="excelUuserId_<?= $item->user_id; ?>"</span>
            </div>

                <div class="padding-10-10 col-sm-2 b-t-1-ccc b-l-1-ccc">
                    <span class="p-l-15">Name</span>
                </div>
            <div class="padding-10-10 col-sm-6 b-t-1-ccc">
                <div class="row">
                    <div class="col-sm-3">Clock In</div>
                    <div class="col-sm-3">Start Lunch</div>
                    <div class="col-sm-3">End Lunch</div>
                    <div class="col-sm-3">Clock Out</div>
                </div>
            </div>
            <div class="padding-10-0-0 col-sm-2 center b-t-1-ccc">
                <p class="m-b-0">Extratime</p>
                <span class="sm">Before</span>
                <span class="sm">After</span>
            </div>
            <div class="padding-10-10 col-sm-2 right p-r-10 b-t-1-ccc b-r-1-ccc">Log | Edit</div>
            <?php } $num_day = $item->date; ?>

            <div class="padding-10-10 col-sm-2 b-l-1-ccc">
                <span class="p-l-15"><?php echo (isset($item->last_name) ? $item->last_name : '').' '.(isset($item->name) ? $item->name : ''); ?></span>
            </div>
            <div class="padding-10-10 col-sm-6">
                <div class="row">
                    <?php
//                        echo '<pre>';
//                        print_r($item);
//                        echo '</pre>';
//                        die();
                    ?>
                    <div class="col-sm-3"><?php echo(isset($clockInEx) ? substr($clockInEx[0], 11) : ''); ?></div>
                    <div class="col-sm-3"><?php echo(isset($lunchStEx) && isset($clockInEx) && $lunchStEx[0] == 1 ? substr($clockOutEx[0], 11) : '—'); ?></div>
                    <div class="col-sm-3"><?php echo(isset($lunchEndEx) && isset($clockInEx) && $lunchStEx[0] == 1 ? substr($clockInEx[1], 11) : '—'); ?></div>
                    <div class="col-sm-3"><?php echo(isset($clockOutEx) && $lunchStEx[0] == 1 ? substr($clockOutEx[1], 11) : substr($clockOutEx[0], 11)); ?></div>
                </div>
        </div>
            <div class="padding-10-10 col-sm-2 center">
                <div class="extratime extratime-right-line"><?php echo round($beforeExtraTime); ?> min
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="hidden" value="<?php echo round($beforeExtraTime); ?>"
                               class="before-extratime-<?= $item->day_id; ?>"/>
                        <input type="checkbox" class="custom-control-input dayInputBefore"
                               name="dayIds[]" <?php if (!empty($item->before_extratime)) echo 'checked'; ?>
                               value="<?= $item->day_id; ?>"
                               id="before-defaultInline1<?= $day_time_idEx[0]; ?>">
                        <label class="custom-control-label my-checkbox"
                               for="before-defaultInline1<?= $day_time_idEx[0]; ?>"></label>
                    </div>
                </div>

                <div class="extratime"><?php echo round($afterExtratime); ?> min
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="hidden" value="<?php echo round($afterExtratime); ?>"
                               class="after-extratime-<?= $item->day_id; ?>"/>
                        <input type="checkbox" class="custom-control-input dayInputAfter"
                               name="dayIds[]" <?php if (!empty($item->after_extratime)) echo 'checked'; ?>
                               value="<?= $item->day_id; ?>"
                               id="defaultInline1<?= $day_time_idEx[0]; ?>">
                        <label class="custom-control-label my-checkbox"
                               for="defaultInline1<?= $day_time_idEx[0]; ?>"></label>
                    </div>
                </div>
            </div>
            <div class="padding-10-10 col-sm-2 right p-r-20 b-r-1-ccc">
                <a <?php if (!empty($item->request_day_id)) {
                    echo 'href';
                } ?> class="icon-icon_log pencil-a p-r-10 <?php if (empty($item->request_day_id)) {
                    echo 'disabled-log';
                } else {
                    echo ' log-day';
                } ?>" <?php if (!empty($item->request_day_id)) {
                    echo 'data-toggle="modal"
                                   id="log-' . $item->day_id . '-' . $item->user_id . '" data-target="#exampleModalCenter-2"';
                } ?>></a>
                <a href class="icon-pencil pencil-a update-day" data-toggle="modal"
                   id="<?= $day_time_idEx[0]; ?>" data-target="#exampleModalCenter"></a>
                <input type="hidden" class="day_id" value="<?php echo $item->day_id; ?>"/>
            </div>
        </div>
        <?php
    }
    ?>
</div>