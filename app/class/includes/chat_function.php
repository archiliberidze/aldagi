<?php
    if (!isset($this->data['messages']) || empty($this->data['messages'])) return null;
    foreach ($this->data['messages'] as $message) {
    ?>
        <li class="<?php
            if($message->status == 1 || $message->status == 3) {
                echo 'approve';
            } else if ($message->status == 2) {
                echo 'denied';
            } ?>">
            <span>
                <?php
                    if ($message->fromUser != $this->session->userId) {
                        echo isset(explode(' ', $message->name)[0]) ? explode(' ', $message->name)[0] . ' ' : 'no name';
                        echo isset(explode(' ', $message->name)[1]) && isset(explode(' ', $message->name)[1][0]) ? explode(' ', $message->name)[1][0] . '.'  : '' ;
                    } elseif ($message->status > 0) {
                        echo 'System';
                    } else {
                        echo 'You';
                    }
                ?> :
            </span>
            <span <?php echo $message->fromUser == $this->session->userId ? 'class="your-comment"' : '' ; ?>><?php echo $message->message ?></span>
            <span><?php echo date('H:i. m/d', strtotime($message->sentTime)); ?></span>
        </li>

    <?php
} ?>
<script>
    var li<?php echo $this->data['messages'][count($this->data['messages'])-1]->reqId; ?>Height = 0;

    $('li').each(function() {
        li<?php echo $this->data['messages'][count($this->data['messages'])-1]->reqId; ?>Height += $(this).height();
    });
    window.lastUpdate<?php echo $this->data['messages'][count($this->data['messages'])-1]->reqId; ?> = '<?php echo $message->sentTime; ?>';
    window.scrollOfset<?php echo $this->data['messages'][count($this->data['messages'])-1]->reqId; ?> = li<?php echo $this->data['messages'][count($this->data['messages'])-1]->reqId; ?>Height;
</script>
