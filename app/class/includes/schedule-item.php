<div id="<?php echo $this->newScheduleId; ?>"  data-startDate="<?php echo $this->startDate; ?>"  class="sch-item row my-3 position-relative">
    <div class="sch-item-first col-3">
        <span class="align-middle"><?php echo !empty($this->post) && isset($this->post->name) ? $this->post->name : ""; ?></span>
        <div></div>
    </div>
    <div class="sch-item-days col">
        <div class="row">
            <div class="col mx-1 py-1 disabled"></div>
            <div class="col mx-1 py-1 disabled"></div>
            <div class="col mx-1 py-1 disabled"></div>
            <div class="col mx-1 py-1 disabled"></div>
            <div class="col mx-1 py-1 disabled"></div>
            <div class="col mx-1 py-1 disabled"></div>
            <div class="col mx-1 py-1 disabled"></div>
        </div>
    </div>
    <div class="sch-item-actions col-1">
        <div class="row">
            <div class="col icon-users">
                 <span>0</span>
            </div>
            <div class="col icon-trashcan align-middle remove-schedule" data-schedule-id="<?php echo $this->newScheduleId; ?>"></div>
        </div>
    </div>
    <div class="w-100 p-3 sch-inner-item position-relative" id="sch-inner-item-<?php echo $this->newScheduleId; ?>">
        <div class="row switch-schedule-to-group">
            <div class="col-2">
                <div class="remove-send-schedule d-none">
                    <span class="scheduleGroupRemove">
                        <i class="schedule-icon disabled-icon schedule-group-remove-disabled-icon scheduleGroupRemove" data-startdate="<?php echo $schedule->StartDate; ?>" data-scheduleId="<?php echo $schedule->id; ?>"></i>
                    </span>
                    <span>Remove</span>
                    <span class="scheduleSend">
                        <i class="disabled-icon schedule-send-disabled-icon scheduleSend" data-startdate="<?php echo $schedule->StartDate; ?>" data-scheduleId="<?php echo $schedule->id; ?>"></i>
                    </span>
                    <span>Send Schedule</span>
                </div>
            </div>
            <div class="col-4 text-right active cursor-pointer change-to-groups" data-type="schedule">
                Schedule
            </div>
            <div class="col-6">
                <div class="col-6 text-left cursor-pointer change-to-groups in-this-schedule-switch" data-type="in-this-schedule" data-startDate="<?php echo $this->startDate ?>" data-schedule="<?php echo $this->newScheduleId; ?>">
                    In this schedule
                </div>
                <div class="col-6 help-container-icon">
                    <button class="helpButtonTow help-icon float-left"></button>
                </div>
            </div>
            <div class="w-100 p-3 schedule-week-list">
                <div class="row">
                    <div class="col">
                        <div>
                            <div class="row my-2 px-3 week-days">
                                <div class="col mx-2 py-1">
                                    Mon.
                                </div>
                                <div class="col mx-2 py-1">
                                    Tues.
                                </div>
                                <div class="col mx-2 py-1">
                                    Wed.
                                </div>
                                <div class="col mx-2 py-1">
                                    Thurs.
                                </div>
                                <div class="col mx-2 py-1">
                                    Fri.
                                </div>
                                <div class="col mx-2 py-1">
                                    Sat.
                                </div>
                                <div class="col mx-2 py-1">
                                    Sun.
                                </div>
                                <div class="col mx-2 py-1">
                                </div>
                            </div>
                        </div>
                        <div class="sch-new-week-<?php echo $this->newScheduleId; ?>"></div>
                        <button data-schedule-id="<?php echo $this->newScheduleId; ?>" class="sch-new-week icon-plussign float-left"></button>
                    </div>
                    <div class="col-3 sch-drag-items">
                        <div class="row my-1 p-1">
                            <div class="col-2 icon-lunch"></div>
                            <div class="col text-left">Drag and drop to set lunch</div>
                        </div>
                        <div class="row my-1 p-1">
                            <div class="col-1 schedule-check-icon"></div>
                            <div class="col text-left">Drag and drop to set a workday</div>
                        </div>
                        <div class="row my-1 p-1">
                            <div class="col-2 icon-xsign"></div>
                            <div class="col text-left">Drag and drop to set a day off</div>
                        </div>
                        <div class="row my-1 p-1">
                            <div class="col-1">
                                <div class="icon-leftbracket"></div>
                            </div>
                            <div class="col-1">
                                <div class="icon-rightbracket"></div>
                            </div>
                            <div class="col text-left">
                                Loop Handles
                            </div>
                        </div>
                        <div class="row my-1 p-1">
                            <div class="col text-left">
                                Loop handles let you decide which days would you like to be repeated in a sequence.
                                You can include as many days as you wish in a loop and they will be repeated continuously.
                                If you include day off in a loop, they will be repeated as well. If you put a day off outside the loop,
                                the loop sequence will skip those days and continue from the next workday.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 in-this-schedule d-none">
                <div class="col-md-12">
                    <div class="col-md-6 select-groups">
                        <div class="col-md-12">
                            <div class="col-md-6 select-search">
                                <span>Groups:</span>
                                <div class="form-group float-left">
                                    <input type="text" class="search-select-groups" placeholder="Search">
                                </div>
                                <div class="line"></div>
                            </div>
                            <div class="col-md-6">
                                <p class="select-all-groups">Select All</p>
                            </div>
                        </div>
                        <div class="d-none copy-item-group">
                            <div class="col-md-12 schedule-group-list-itam">
                                <div class="col-md-3 title"></div>
                                <div class="col-md-3 startDateTitle">Start date:</div>
                                <div class="col-md-3 startDate">08.02.2019</div>
                                <div class="col-md-3 select">
                                    <label class="container groups-list-container-checkbox">
                                        <input type="checkbox">
                                        <span class="checkmark" data-user-list="[]" data-scheduleid="<?php echo $this->newScheduleId; ?>"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <?php

                        $blackList = [];
                        $groupList = [];
                        foreach ($this->data['usersList'] as $User) {
                            if ($User->scheduleId == $this->newScheduleId) {
                                $groupList[$User->groupId][] = $User->id;
                            }
                        }

                        foreach ($this->data['usersList'] as $User) {
                            if ($User->scheduleId == $this->newScheduleId) {
                                if (!in_array($User->groupId, $blackList) && !is_null($User->title)) {
                                    $blackList[] = $User->groupId;
                                    ?>
                                    <div class="col-md-12 schedule-group-list-itam group-item-<?php echo $User->groupId; ?>">
                                        <div class="col-md-3 title"><?php echo $User->title; ?></div>
                                        <div class="col-md-3 startDateTitle">Start date:</div>
                                        <div class="col-md-3 startDate"><?php echo $this->StartDate; ?></div>
                                        <div class="col-md-3 select">
                                            <label class="container groups-list-container-checkbox">
                                                <input type="checkbox">
                                                <span class="checkmark" data-scheduleid="<?php echo $User->scheduleId; ?>" data-user-list="<?php echo json_encode($groupList[$User->groupId]); ?>"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <span id="groups-list-<?php echo $this->newScheduleId; ?>"></span>
                        <div class="col-md-12">
                            <div class="schedule-add" data-toggle="modal" data-startDate="<?php echo $this->startDate; ?>" data-target="#addToScheduleGroup" data-schedule-id="<?php echo $this->newScheduleId; ?>">
                                <i class="schedule-add-icon"></i>
                                <span>Add new group</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 select-individuals">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <span class="float-left">Individuals:</span>
                                <div class="form-group">
                                    <input type="text" class="float-left search-select-individuals" placeholder="Search">
                                </div>
                                <div class="line"></div>
                            </div>
                            <div class="col-md-6">
                                <p class="select-all">Select All</p>
                            </div>
                            <div class="col-md-12 schedule-individuals-list d-none">
                                <div class="col-md-12 schedule-individuals-list-item">
                                    <div class="col-md-3 title"></div>
                                    <div class="col-md-3 startDateTitle">Start date:</div>
                                    <div class="col-md-3 startDate"><?php echo $this->startDate; ?></div>
                                    <div class="col-md-3 select">
                                        <label class="container individuals-list-container-checkbox">
                                            <input type="checkbox">
                                            <span class="checkmark" data-scheduleid="<?php echo $User->scheduleId; ?>"></span>
                                        </label>
                                    </div>
                                </div>
                                <?php
                                $blackList = [];
                                $groupList = [];
                                foreach ($this->data['usersList'] as $User) {
                                    if ($User->scheduleId == $this->newScheduleId) {
                                        $groupList[$User->groupId][] = $User->groupId;
                                        if (!in_array($User->groupId, $blackList)) {
                                            $blackList[] = $User->groupId;

                                            ?>
                                            <div class="col-md-12 schedule-group-list-itam">
                                                <div class="col-md-3 title"><?php echo $User->title; ?></div>
                                                <div class="col-md-3 startDateTitle">Starst date:</div>
                                                <div class="col-md-3 startDate"><?php echo $schedule->StartDate; ?></div>
                                                <div class="col-md-3 select">
                                                    <label class="container individuals-list-container-checkbox">
                                                        <input type="checkbox">
                                                        <span class="checkmark" data-scheduleid="<?php echo $User->scheduleId; ?>" data-user-list="<?php echo json_encode($groupList[$User->groupId]); ?>"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="schedule-add schedule-add-individual" data-toggle="modal" data-target="#addToScheduleIndividuals" data-startDate="<?php echo $this->startDate; ?>" data-schedule-id="<?php echo $this->newScheduleId; ?>">
                                <i class="schedule-add-icon"></i>
                                <span>Add new user</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 sch-trig icon-arrow1"></div>
</div>

<script>
    $(function(){
        $(schedule.tags.changeToGroups).unbind();
        $(schedule.tags.helpButtonTow).unbind();
        $(schedule.tags.helpButtonOne).unbind();
        schedule.addToSchedule.opensUserListGroups();
        schedule.addToSchedule.saveGroupsInSchedule();
        schedule.notifications.helpInit();
        schedule.addToSchedule.groupSelect();
        schedule.unbind();
        schedule.openSchedule();
        schedule.changeToGroups();
        schedule.addWeek();
        schedule.removeSchedule();
        schedule.dragAndDrops.autoLoad();
        schedule.addToSchedule.autoLoad();
    });
</script>