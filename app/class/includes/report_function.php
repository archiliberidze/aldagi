<?php
//echo '<pre>';
//print_r($this->post);
//echo '</pre>';
$absenceDays = 0;
if(count($this->data['shift']) >= count($this->data['day'])){
    $absenceDays = count($this->data['shift']) - count($this->data['day']);
}
?>
<div class="selection-full">
    <div class="rep-st container p-70-35 ">
        <span clas="group_by">Group by:</span><span class="employees_style employees group_by_active">Employees</span>
        <span class="group_by_style employees_date cursor-pointer">Date</span>

        <div class="row sorting_line m-t-20 m-b-10">
            <div class="col-md-2 right-border">
                <span class="sort_by">Sort by:</span>
            </div>
            <div class="col-md-2 right-border">Attendance
                <div class="arrows">
                    <span class="icon-arrow active-arrow arrow-align-top"></span>
                    <span class="icon-arrow arrow-down arrow-align-bottom"></span>
                </div>
            </div>
            <div class="col-md-2 right-border">Absence
                <div class="arrows">
                    <span class="icon-arrow arrow-align-top"></span>
                    <span class="icon-arrow arrow-down arrow-align-bottom"></span>
                </div>
            </div>
            <div class="col-md-2 right-border">Lateness
                <div class="arrows">
                    <span class="icon-arrow arrow-align-top"></span>
                    <span class="icon-arrow arrow-down arrow-align-bottom"></span>
                </div>
            </div>
            <div class="col-md-2 right-border">Lunch delay
                <div class="arrows">
                    <span class="icon-arrow arrow-align-top"></span>
                    <span class="icon-arrow arrow-down arrow-align-bottom"></span>
                </div>
            </div>
            <div class="col-md-2">Overtimes
                <div class="arrows">
                    <span class="icon-arrow arrow-align-top"></span>
                    <span class="icon-arrow arrow-down arrow-align-bottom"></span>
                </div>
            </div>
        </div>

        <div class="full-selection">
            <div class="accordion groyp_by_employees" id="accordionExample">
                <?php
                $last_user_id = '';
                $countDay = count($this->data['day']);
                $dayCount = 1;
                $nextUser = 1;
                $total_overtimes = 0;
                $noExtraWork = 0;

                $totalLatness = 0;
                $totalLunch = 0;
                foreach ($this->data['day'] as $item) {
                    $latness = 0;
                    $clockInEx = explode(',', $item->day_time_clockIn);
                    $clockOutEx = explode(',', $item->day_times_clockOut);
                    $lunchStEx = explode(',', $item->day_times_startLunch);
                    $lunchEndEx = explode(',', $item->day_times_endLunch);
                    $clockOutStatusEx = explode(',', $item->clockOutStatus);


                if(strtotime(substr($clockInEx[0], 11)) > strtotime("+".$item->dayRange.' minutes', strtotime($item->clockIn))){
                    $latness = (strtotime(substr($clockInEx[0], 11)) - strtotime("+".$item->dayRange.' minutes', strtotime($item->clockIn))) / 60;
                }
                $day_time_idEx = explode(',', $item->day_time_id);
                $countEx = count($clockInEx) - 1;
                $stayMinutesOnWork = (strtotime($item->clockOut) - strtotime($item->clockIn) - strtotime($item->duration)) / 60;

                // Calculate Before Extra Time Start
                $beforeExtraTime = (strtotime($item->clockIn) - strtotime(substr($clockInEx[0], 11))) / 60;
                if ($beforeExtraTime <= 0) {
                    $beforeExtraTime = 0;
                }
                // Calculate Before Extra Time End

                // Calculate After Extra Time Start
                $personWorkingTime = 0;
                for ($i = 0; $i <= $countEx; $i++) {
                    $personWorkingTime += floor((strtotime(substr($clockOutEx[$i], 11)) - strtotime(substr($clockInEx[$i], 11))) / 60);
                    $workWithoiutExtra = $personWorkingTime;
                }
                $personWorkingTime = $personWorkingTime - $beforeExtraTime;
                $afterExtratime = $personWorkingTime - $stayMinutesOnWork;
                if ($afterExtratime <= 0) {
                    $afterExtratime = 0;
                }
                // Calculate After Extra Time End
                $workWithoiutExtra = $workWithoiutExtra - $beforeExtraTime - $afterExtratime;
                ?>
                <div>
                    <?php
                    if ($item->user_id != $last_user_id) {
                        ?>
                        <div class="row selection_color">
                            <div class="col-sm-3 clolor_info border-right bg-gray">
                                <?php echo (isset($item->last_name) ? $item->last_name : '').' '.(isset($item->name) ? $item->name : '') ; ?>
                            </div>
                            <div class="col-sm-7 color-dark-gray border-right bg-gray">
                                <?php echo(isset($item->position_name) ? $item->position_name : ''); ?>
                            </div>
                            <div class="col-sm-2 bg-gray center">
                                <span class="icon-download download-btn disabled text-secondary"></span>
                                <span class="icon-print print-btn disabled text-secondary"></span>
                                <span class="icon-icon_exel exel-btn cursor-pointer" id="excelUuserId_<?= $item->user_id; ?>"></span>
                            </div>

                            <div class="padding-10-10 col-sm-2 b-t-1-ccc b-l-1-ccc">
                                <span class="p-l-15">Date</span>
                            </div>
                            <div class="padding-10-10 col-sm-6 b-t-1-ccc">
                                <div class="row">
                                    <div class="col-sm-3">Clock In</div>
                                    <div class="col-sm-3">Start Lunch</div>
                                    <div class="col-sm-3">End Lunch</div>
                                    <div class="col-sm-3">Clock Out</div>
                                </div>
                            </div>
                            <div class="padding-10-0-0 col-sm-2 center b-t-1-ccc">
                                <p class="m-b-0">Extratime</p>
                                <span class="sm">Before</span>
                                <span class="sm">After</span>
                            </div>
                            <div class="padding-10-10 col-sm-2 right p-r-10 b-t-1-ccc b-r-1-ccc">Log | Edit</div>
                        </div>
                        <?php
                    }
                    $last_user_id = $item->user_id;
                    ?>
                    <div class="card border-none">
                        <div id="heading<?= $day_time_idEx[0]; ?>">
                            <div class="row b-r-1-ccc b-l-1-ccc  <?php echo (!isset($this->data['day'][$nextUser]->user_id) || $item->user_id != $this->data['day'][$nextUser]->user_id) ? 'b-b-1-ccc' : ''; ?>">
                                <div class="padding-10-10 col-sm-2 <?php echo (isset($clockInEx[1])) ? 'p-l-0' : ''; ?> <?php echo (isset($clockInEx[1])) ? 'cursor-pointer' : ''; ?>"
                                     data-toggle="collapse" <?php echo (isset($clockInEx[1])) ? 'data-target="#collapse' . $day_time_idEx[0] . '" aria-expanded="false" aria-controls="collapse' . $day_time_idEx[0] : ''; ?>
                                ">
                                <?php echo (isset($clockOutStatusEx[1]) && $clockOutStatusEx[1] == 0 ) ? '<span id="triangle-right"></span>' : ''; ?>
                                <span class="p-l-15"><?php echo(isset($item->date) ? $item->date : ''); ?></span>
                            </div>
                            <div class="padding-10-10 col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3"><?php echo(isset($clockInEx) && $lunchEndEx != 1 ? substr($clockInEx[0], 11) : ''); ?></div>
                                    <div class="col-sm-3"><?php echo(isset($lunchStEx) && isset($clockOutEx) && $lunchStEx[0] == 1 ? substr($clockOutEx[0], 11) : '—'); ?></div>
                                    <div class="col-sm-3">
                                        <?php
                                            if(isset($lunchEndEx[0]) && ($lunchEndEx[0] == 1)){
                                                echo substr($clockInEx[0], 11);
                                            }elseif(isset($lunchEndEx[1]) && ($lunchEndEx[1] == 1)){
                                                echo substr($clockInEx[1], 11);
                                            }else{
                                                echo '—';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php
                                            if(isset($lunchStEx[0]) &&$lunchStEx[0] == 1 && isset($lunchEndEx[1]) && $lunchEndEx[1] == 1){
                                                $totalLunch += date("i",strtotime($clockInEx[1]) - strtotime( $clockOutEx[0]));
                                            }


                                            if(isset($clockOutStatusEx[0]) && $clockOutStatusEx[0] == 1){
                                                echo substr($clockOutEx[0], 11);
                                                $i = 1;
                                            }elseif(isset($clockOutStatusEx[1]) && $clockOutStatusEx[1] == 1){
                                                echo substr($clockOutEx[1], 11);
                                                $i =2;
                                            }else{
                                                echo '—';
                                                $i = 1;
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-10-0 col-sm-2 center">

                                <div class="extratime extratime-right-line"><?php echo floor($beforeExtraTime); ?> min
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="hidden" value="<?php echo floor($beforeExtraTime); ?>"
                                               class="before-extratime-<?= $item->day_id; ?>"/>
                                        <input type="checkbox" class="custom-control-input dayInputBefore"
                                               name="dayIds[]" <?php if (!empty($item->before_extratime)) echo 'checked'; ?>
                                               value="<?= $item->day_id; ?>"
                                               id="before-defaultInline1<?= $day_time_idEx[0]; ?>">
                                        <label class="custom-control-label my-checkbox"
                                               for="before-defaultInline1<?= $day_time_idEx[0]; ?>"></label>
                                    </div>
                                </div>

                                <div class="extratime"><?php echo floor($afterExtratime); ?> min
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="hidden" value="<?php echo floor($afterExtratime); ?>"
                                               class="after-extratime-<?= $item->day_id; ?>"/>
                                        <input type="checkbox" class="custom-control-input dayInputAfter"
                                               name="dayIds[]" <?php if (!empty($item->after_extratime)) echo 'checked'; ?>
                                               value="<?= $item->day_id; ?>"
                                               id="defaultInline1<?= $day_time_idEx[0]; ?>">
                                        <label class="custom-control-label my-checkbox"
                                               for="defaultInline1<?= $day_time_idEx[0]; ?>"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-10-10 col-sm-2 right p-r-20">
                                <a <?php if (!empty($item->request_day_id)) {
                                    echo 'href';
                                } ?> class="icon-icon_log pencil-a p-r-10 <?php if (empty($item->request_day_id)) {
                                    echo 'disabled-log';
                                } else {
                                    echo ' log-day';
                                } ?>" <?php if (!empty($item->request_day_id)) {
                                    echo 'data-toggle="modal"
                                   id="log-' . $item->day_id . '-' . $item->user_id . '" data-target="#exampleModalCenter-2"';
                                } ?>></a>
                                <a href class="icon-pencil pencil-a update-day" data-toggle="modal"
                                   id="<?= $day_time_idEx[0]; ?>" data-target="#exampleModalCenter"></a>
                                <input type="hidden" class="day_id" value="<?php echo $item->day_id; ?>"/>
                            </div>
                            <div id="collapse<?= $day_time_idEx[0]; ?>" class="collapse col-12 selection-day-times"
                                 aria-labelledby="heading<?= $day_time_idEx[0]; ?>" data-parent="#accordionExample">
                                <div class="card-body">
                                    <?php
                                    for ($i; $i <= $countEx; $i++) {
                                        if($lunchStEx[$i] == 1 && $lunchEndEx[$i+1] == 1){
                                            $totalLunch += strtotime($clockInEx[$i+1]) - strtotime($clockOutEx[$i]);
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="padding-10-10 col-sm-2"></div>
                                            <div class="padding-10-10 col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-3"><?php echo(isset($clockInEx) && $lunchEndEx[$i] != 1 ? substr($clockInEx[$i], 11) : '—'); ?></div>
                                                    <div class="col-sm-3"><?php echo(isset($lunchStEx) && isset($clockInEx) && $lunchStEx[$i] == 1 ? substr($clockInEx[$i], 11) : '—'); ?></div>
                                                    <div class="col-sm-3"><?php echo(isset($lunchEndEx) && isset($clockInEx) && $lunchStEx[$i] == 1 ? substr($clockOutEx[$i], 11) : '—'); ?></div>
                                                    <div class="col-sm-3"><?php echo(isset($clockOutEx) && $lunchStEx[$i] != 1 ? substr($clockOutEx[$i], 11) : '—'); ?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-2 center">
                                                <a href class="icon-pencil pencil-a update-day" data-toggle="modal"
                                                   id="<?= $day_time_idEx[$i]; ?>"
                                                   data-target="#exampleModalCenter"></a>
                                                <input type="hidden" class="day_id"
                                                       value="<?php echo $item->day_id; ?>"/>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php $total_overtimes += floor($afterExtratime) + floor($beforeExtraTime);
                            $hours = floor($total_overtimes / 60);
                            $minutes = $total_overtimes % 60;

                            ($workWithoiutExtra >= 1) ? $noExtraWork += $workWithoiutExtra: $noExtraWork += 0;
                            $totalLatness += $latness;
                            if(!isset($this->data['day'][$nextUser]->user_id) || $item->user_id != $this->data['day'][$nextUser]->user_id){ ?>
                            <div class="calulate-times b-t-1-ccc col-12">
                                <div class="row">
                                    <div class="col-2">Absence <span class="clolor_info"><?= $absenceDays; ?> day</span></div>
                                    <div class="col-2">Lateness <span class="clolor_info"><?php printf('%02d:%02d', floor($totalLatness / 60), $totalLatness % 60); ?> hr</span></div>
                                    <div class="col-3">Total Lunch <span class="clolor_info"><?php printf('%02d:%02d', floor( $totalLunch / 60), $totalLunch % 60); ?> hr</span></div>
<!--                                    <div class="col-2">Lunch delay <span class="clolor_info">4 hr</span></div>-->
                                    <div class="col-3">Work hours <span class="clolor_info"><?php printf('%02d:%02d', floor($noExtraWork / 60), $noExtraWork % 60); ?> hr</span></div>
                                    <div class="col-2">Overtimes <span class="clolor_info"><?php printf('%02d:%02d', $hours, $minutes); ?> hr</span></div>
                                </div>
                            </div>
                            <?php $total_overtimes = 0; $noExtraWork = 0; $latness = 0; $totalLatness = 0; $totalLunch = 0; } $nextUser += 1; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $dayCount++;
            }
            ?>
        </div>
    </div>
</div>
</div>
<div class="rep-st m-t-55 container center">
    <div class="report-btn btn-report">Selection</div>
</div>

<!-- Modal-1 -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Change Time</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="close-modal">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type">Choose type</label>
                            <select name="type" id="type" class="form-control" required>
                                <option>Choose type</option>
                                <option name="checkIn">Clock In</option>
                                <option name="lunchStart">Lunch Start</option>
                                <option name="lunchEnd">Lunch End</option>
                                <option name="checkOut">Clock Out</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="updateTime">Set Time</label>
                            <input type="time" name="updateTime" id="updateTime" class="form-control"
                                   placeholder="hh:mm:ss" required/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea name="message" rows="5" id="message" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-check form-check-inline float-right">
                            <input class="form-check-input mt-1" type="checkbox" name="urgent" id="urgent">
                            <label class="form-check-label" for="urgent">
                                Select as urgent
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveUpdateDay" data-dismiss="modal">Save changes
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal-1 --
<div class="modal fade" id="exampleModalCenter-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="close-modal">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>