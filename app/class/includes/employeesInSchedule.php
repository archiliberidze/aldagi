<div class="col-md-12" id="emptyScheduleTitle">
    <span>Employee Schedule:</span>
</div>
<?php /*echo '<pre>'; print_r($this->data['users']); echo '</pre>';*/ ?>
<div class="col-md-12 employee-module">
    <div class="row">
        <div class="col-md-4">
            <span id="employee-schedule-title">Employee Schedule</span>
        </div>
        <div class="col-md-8">
            <span class="employee-x-icon"></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 employeeScheduleUserListSearch">
            <span>Search: </span>
            <input type="text">
        </div>
        <div class="col-md-8">
            <i class="employee-schedule-left-arrow"></i>
            <span class="employee-schedule-left-arrow-title">Click Here to check next schedule</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 employee-schedule">
            <div class="col-md-12">
                <spna class="employee-schedule-list-title-one col-md-6">Groups: </spna>
                <spna class="employee-schedule-list-title-tow col-md-6">Current schedule: </spna>
                <ul class="employeeScheduleListUl">
                    <?php
                        foreach ($this->data['ScheduleAndGroups'] AS $scheduleAndGroup) {
                            foreach ($this->data['groupsList'] as $groupsList) {
                                if (!is_null($scheduleAndGroup->GroupsIds) && in_array($groupsList->id, explode($scheduleAndGroup->GroupsIds, ','))) {
                                ?>
                                    <li class="employeeGroupNames">
                                        <span class="employeeGroupName col-md-6" data-schedule-id="<?php echo $scheduleAndGroup->scheduleId; ?>"><?php echo $scheduleAndGroup->GroupTitle; ?> (<span><?php //echo $scheduleAndGroup->users; ?></span>)</span>
                                        <span class="employeeCurrentScheduleName col-md-6" data-schedule-id="<?php echo $scheduleAndGroup->scheduleId; ?>"><?php echo $scheduleAndGroup->ScheduleTitle; ?></span>
                                    </li>
                                    <li class="userListSchedule-" style="display: none;">
                                        <ul class="employeeScheduleUserListUl">
                                            <?php
                                                foreach ($this->data['users'] AS $user) {
                                                    if ($user->name == "" || !($user->groupId != $scheduleAndGroup->GroupId && $user->scheduleId != $scheduleAndGroup->ScheduleId)) continue;
                                                    ?>
                                                        <li class="employeeScheduleUserList"><?php echo $user->name . $user->last_name; ?></li>
                                                    <?php
                                                }
                                            ?>
                                        </ul>
                                    </li>
                                <?php
                                }
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-md-6 employee-schedule">
            <div class="col-md-12">
                <spna class="employee-schedule-list-title-one col-md-6">Individuals: </spna>
                <spna class="employee-schedule-list-title-tow col-md-6">Current schedule: </spna>
                <ul class="employeeScheduleListUl">
                    <li class="employeeGroupNames">
                        <span class="employeeGroupName col-md-6" data-schedule-id="1">Rati avaliani</span>
                        <span class="employeeCurrentScheduleName col-md-6" data-schedule-id="1">Fulltime shift</span>
                    </li>
                    <li class="employeeGroupNames">
                        <span class="employeeGroupName col-md-6" data-schedule-id="1">Tevdore</span>
                        <span class="employeeCurrentScheduleName col-md-6" data-schedule-id="1">Fulltime shift</span>
                    </li>
                    <li class="employeeGroupNames">
                        <span class="employeeGroupName col-md-6" data-schedule-id="1">Gio Ioseliani</span>
                        <span class="employeeCurrentScheduleName col-md-6" data-schedule-id="1">Fulltime shift</span>
                    </li>
                    <li class="employeeGroupNames">
                        <span class="employeeGroupName col-md-6" data-schedule-id="1">Keti</span>
                        <span class="employeeCurrentScheduleName col-md-6" data-schedule-id="1">Fulltime shift</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>