<?php
if ($this->data['pagination']['allPages'] == 1) return;

$url = "#";
$baseFilter = array_keys($this->data['menu'])[0];

if (isset($this->url[1]) && isset($this->url[2])) {
    $url = "{$this->url[0]}/{$this->url[1]}/{$this->url[2]}/";
} elseif (isset($this->url[1]) && !isset($this->url[2])) {
    $url = "{$this->url[0]}/{$this->url[1]}/{$baseFilter}/";
} else {
    $url = "{$this->url[0]}/{$this->data['defaultPage']}/{$baseFilter}/";
}

if ((int)$this->data['pagination']['allPages'] >= 2) {

    if ($this->data['pagination']['currentPage'] > 2){ ?>
        <li>
            <a href="<?php echo $url, $this->data['pagination']['allPages'] - ($this->data['pagination']['allPages'] -1); ?>">
                <?php echo $this->data['pagination']['allPages'] - ($this->data['pagination']['allPages'] -1); ?>
            </a>
        </li>
        <?php if($this->data['pagination']['currentPage'] < 4 && $this->data['pagination']['currentPage'] > 3) { ?>
            <li>...</li>
        <?php
        }
    }

    if ($this->data['pagination']['currentPage'] > 3){ ?>
        <li>
            <a href="<?php echo $url, $this->data['pagination']['allPages'] - ($this->data['pagination']['allPages'] -2); ?>">
                <?php echo $this->data['pagination']['allPages'] - ($this->data['pagination']['allPages'] -2); ?>
            </a>
        </li>
        <?php if($this->data['pagination']['currentPage'] > 4) { ?>
                <li>...</li>
            <?php
        }
    }

    if (($this->data['pagination']['allPages']-1) - $this->data['pagination']['allPages'] > 0){ ?>
        <li>
            <a href="<?php echo $url, $this->data['pagination']['currentPage'] -1; ?>">
                <?php echo $this->data['pagination']['currentPage'] -1; ?>
            </a>
        </li>
        <?php
    }

    if ($this->data['pagination']['currentPage'] -1 > 0 ){ ?>
        <li>
            <a href="<?php echo $url, $this->data['pagination']['currentPage'] -1; ?>">
                <?php echo $this->data['pagination']['currentPage'] -1; ?>
            </a>
        </li>
        <?php
    }
    ?>

    <li class="active">
        <?php echo $this->data['pagination']['currentPage']; ?>
    </li>

    <?php
        if ($this->data['pagination']['currentPage']+1 < $this->data['pagination']['allPages']){ ?>
            <li>
                <a href="<?php echo $url, $this->data['pagination']['currentPage'] + 1; ?>">
                    <?php echo $this->data['pagination']['currentPage']+1; ?>
                </a>
            </li>
    <?php
        }

        if ($this->data['pagination']['allPages'] <= 3 && $this->data['pagination']['currentPage'] != 3 && $this->data['pagination']['currentPage'] != 2){ ?>
            <li>
                <a href="<?php echo $url, $this->data['pagination']['allPages']; ?>">
                    <?php echo $this->data['pagination']['allPages']; ?>
                </a>
            </li>
            <?php
        }

        if ($this->data['pagination']['allPages']-1 > 0 && $this->data['pagination']['allPages']-2 != $this->data['pagination']['currentPage'] && $this->data['pagination']['allPages']-1 != $this->data['pagination']['currentPage'] && $this->data['pagination']['allPages'] > 3) { ?>
            <?php
                if ($this->data['pagination']['allPages']-3 != $this->data['pagination']['currentPage']) {
                    ?>
                    <li>...</li>
            <?php
                }
                ?>
            <li>
                <a href="<?php echo $url, $this->data['pagination']['allPages']-1; ?>">
                    <?php echo $this->data['pagination']['allPages']-1; ?>
                </a>
            </li>
    <?php
        }
    }
?>
