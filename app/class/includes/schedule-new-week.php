<?php
$scheduleId = $this->post->scheduleId; // Get ID fo the schedule
$i = $this->post->dayCount;            // Get number of the last day in the schedule week days. It may be 0, 7, 14 or 21
$currentWeekID = 'week-'.$i.'-'.$scheduleId;
?>
<div class="row my-2 px-3 weeks-list" id="<?php echo $currentWeekID; ?>">
    <div class="col-1 mx-2 sch-week-title"><?php echo $this->data['weekDay']; ?></div>

    <?php
        foreach ($this->data['weekDayId'] as $key => $id) {
        ?>
            <div class="col changeable noselect mx-2 py-1 add-time-to-day"
                 data-scheduleId="<?php echo $scheduleId; ?>"
                 data-clockIn=""
                 data-clockOut=""
                 data-workingDuration=""
                 data-range=""
                 data-weekday-start="<?php echo ($i % 7) + 1; ?>"
                 data-weekday-end="<?php echo ($i % 7) + 1; ?>"
                 id="shift-<?php echo $id; ?>"
                 data-shiftId="<?php echo $id; ?>">
                <span class="icon-r d-none"></span>
                <span class="icon-small-lunch d-none"></span>
                <span class="clockInClockOut">
                     --:-- - --:--
                </span>
            </div>
        <?php
            $i++;
        }

        $this->data['weekDayId'] = array_map(function($value) {
            return intval($value);
        }, $this->data['weekDayId'])
    ?>
    <div class="col noselect mx-2 py-1 schedule-week-remove pointer" data-shc-id="<?php echo $scheduleId; ?>" data-week-id="<?php echo $currentWeekID; ?>" data-shiftId='<?php echo json_encode(array_values($this->data['weekDayId'])); ?>'><i class="schedule-remove-icon"></i></div>
</div>
<script>
    $(function(){
        schedule.unbind();
        schedule.removeWeek();
        schedule.addWeek();
        schedule.addDayTime();
        schedule.openSchedule();
        schedule.dragAndDrops.autoLoad();
    });
</script>