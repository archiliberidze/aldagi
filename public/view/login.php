<div class="modal fade"  id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                You have registered successfully.
            </div>
        </div>
    </div>
</div>
<div id="app">
    <div class="header_div">
    	<img src="<?php echo __JAMP__["images"]; ?>/login/jamps_logo_1.svg" class="jamps_logo_1" alt="Jamps Logo" />
    </div>

    <div class="position_centered form_wrap">
        <div class="form_title_div">
            <img src="<?php echo __JAMP__["images"]; ?>/login/elva_logo_form_title.svg" class="form_title_logo" alt="Login Title Logo" />
        </div>
        <div class="form_content">
            <div class="fields_wrap">
                <?php if ($this->data['currentUserStatus'] == $this->data['userLoginStatus']['passwordReset']) { ?>
                    <div class="recovery">
                        <div>
                            <input type="password" name="new_password" class="inp_field" id="new_password">
                            <span class="field_title_txt password">new password</span>
                        </div>
                        <div>
                            <input type="password" name="old_password" class="inp_field" id="repeat_password">
                            <span class="field_title_txt password">repeat new password</span>
                        </div>
                    </div>
                <?php } else { ?>
                    <input type="email" name="email" class="login-email inp_field" autocomplete="off" />
                    <input type="email" name="email" class="forgot-email inp_field" autocomplete="off" /><br>
                    <span class="field_title_txt">Email</span><br>
                    <input type="password" name="password" class="login-password inp_field" /><br>
                    <span class="field_title_txt password">Password</span>

                    <sapn><?php echo isset($this->data['recoverIncorrect']) ? $this->data['recoverIncorrect']['result'] : '';?></sapn>
                    <span class="forgot-notification"><i class="icon icon-success d-none"></i>The recovery link will be sent to your email address.</span><br><br>
                <?php } ?>
            </div>
            <div class="register d-none">
                <div>
                    <input type="text" name="registerCompanyName" class="inp_field registerInput registerCompanyName" placeholder="Company Name">
                    <span class='errorMessage'></span>
                </div>
                <div>
                    <input type="text" name="registerYourEmail" class="inp_field registerInput registerYourEmail" placeholder="Your Email">
                    <span class='errorMessage'></span>
                </div>
                <div>
                    <input type="text" name="registerName" class="inp_field registerInput registerYourName" placeholder="Name">
                    <span class='errorMessage'></span>
                </div>
                <div>
                    <input type="text" name="registerLastName" class="inp_field registerInput registerYourLastName" placeholder="Last Name">
                    <span class='errorMessage'></span>
                </div>
                <div>
                    <input type="password" name="registerPassword" class="inp_field registerInput registerPassword" placeholder="Password">
                    <span class='errorMessage'></span>
                </div>
                <div>
                    <input type="password" name="registerRepeatPassword" class="inp_field registerInput registerRepeatPassword" placeholder="Repeat Password">
                    <span class='errorMessage'></span>
                </div>
            </div>
            <div class="butts_wrap">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="180" valign="top" class="inline-block">
                            <div style="height: 45px; width: 180px;">
                            	<div id="login-gcaptcha"></div>
                            </div>
                        </td>
                        <td align="right">
                            <div class="fields_wrap_buttons">
                                <?php if ($this->data['currentUserStatus'] == $this->data['userLoginStatus']['passwordReset']) { ?>
                                    <div class="container">
                                        <a class="change-password" data-animation="ripple" style="color: #FFF;" action="">Change</a>
                                    </div>
                                <?php } else { ?>
                                    <div class="container">
                                        <a class="sign-in" data-animation="ripple" style="color: #FFF;" action="">Sign In</a>
                                        <a class="forgot-confirm" data-animation="ripple" style="color: #FFF; display: none;" action="">Confirm</a>
                                    </div>
                                    <span class="forgot-password-label pointer">Forgot Password?</span>
                                    <span class="sign-in-label d-none">Sign In</span>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <div class="container register-container d-none">
                            <a class="singUp" data-animation="ripple" style="color: #FFF;" action="">Sign Up</a>
                        </div>
                    </tr>
                    <tr class="createAccount">
                        <td>Not a member yet? <span>Create an account</span></td>
                    </tr>
                    <tr class="loginHere d-none">
                        <td>Have e already an account? <span>Login Here</span></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="register-message d-none">
            <h3 class="register-successfully">You have registered successfully<br/> Please verify your E-mail</h3>
            <span class="register-login">
                Auto Redirect:
                <span class="register-count"> 5 </span>
            </span>
        </div>

		<div class="login-notification"></div>
    </div>



    <div class="footer_div">
        @file{ src=file }
        @file{ src=login/upplan_logo_footer.png, class=footer_logos logos_disabled, alt=alt-name }
        @file{ src=login/brunva_logo_footer.png, class=footer_logos logos_disabled, alt=alt-name }
        @file{ src=login/elva_logo_footer.png, class=footer_logos, alt=alt-name }
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            window.pageStatus = login.userLoginStatus[<?php echo isset($this->data['currentUserStatus']) ? $this->data['currentUserStatus'] : 'null';?>];
        });
    </script>
</div>