<?php

if (!defined('__JAMP__')) exit("Direct access not permitted.");

?>



<div class="modal fade"  id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                You have registered successfully.
            </div>
        </div>
    </div>
</div>
<div id="app">
    <div class="header_div">
        <img src="<?php echo __JAMP__["images"]; ?>/login/jamps_logo_1.svg" class="jamps_logo_1" alt="Jamps Logo" />
    </div>

    <div class="position_centered form_wrap">
        <div class="form_title_div">
            <img src="<?php echo __JAMP__["images"]; ?>/login/elva_logo_form_title.svg" class="form_title_logo" alt="Login Title Logo" />
        </div>
        <div class="form_content">
            <div class="fields_wrap">
                    <div class="recovery">
                        <div>
                            <input type="password" name="new_password" class="inp_field" id="new_password">
                            <span class="field_title_txt password">new password</span>
                        </div>
                        <div>
                            <input type="password" name="old_password" class="inp_field" id="repeat_password">
                            <span class="field_title_txt password">repeat new password</span>
                        </div>
                    </div>
            </div>

            <div class="butts_wrap">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="180" valign="top" class="inline-block">
                            <div style="height: 45px; width: 180px;">
                                <div id="login-gcaptcha"></div>
                            </div>
                        </td>
                        <td align="right">
                            <div class="fields_wrap_buttons">
                                <div class="container">
                                    <a class="change-password" data-animation="ripple" style="color: #FFF;" action="">Change</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <div class="container register-container d-none">
                            <a class="singUp" data-animation="ripple" style="color: #FFF;" action="">Sign Up</a>
                        </div>
                    </tr>
                    <tr class="loginHere d-none">
                        <td>Have e already an account? <span>Login Here</span></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="register-message d-none">
            <h3 class="register-successfully">You have registered successfully<br/> Please verify your E-mail</h3>
            <span class="register-login">
                Auto Redirect:
                <span class="register-count"> 5 </span>
            </span>
        </div>

        <div class="login-notification"></div>
    </div>



    <div class="footer_div">
        @file{ src=file }
        @file{ src=login/upplan_logo_footer.png, class=footer_logos logos_disabled, alt=alt-name }
        @file{ src=login/brunva_logo_footer.png, class=footer_logos logos_disabled, alt=alt-name }
        @file{ src=login/elva_logo_footer.png, class=footer_logos, alt=alt-name }
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            window.pageStatus = login.userLoginStatus[<?php echo isset($this->data['currentUserStatus']) ? $this->data['currentUserStatus'] : 'null';?>];
        });
    </script>
</div>