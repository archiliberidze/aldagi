<?php
//echo '<pre>';
//print_r($this->session);
//echo '</pre>';
?>

<div class="settings container">
    <div class="col-lg-12">
        <div class="nav">
            <ul class="list-group nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                <?php if($this->data['user_info'][0]->is_company == 1){ ?>
                <li><a class="nav-item nav-link active" data-toggle="tab" href="#quick-timepunch" role="tab"
                       aria-controls="quick-timepunch" aria-selected="true">Quick TimePunch</a></li>
                <?php } ?>
                <?php $activeAdmin = (($this->data['user_info'][0]->groups != '' || $this->data['user_info'][0]->controls_user != '') && $this->data['user_info'][0]->is_company == 0)? 'active' : ''; ?>
                <?php if($this->data['user_info'][0]->is_company == 1 OR $this->data['user_info'][0]->groups != '' OR $this->data['user_info'][0]->controls_user != ''){ ?>
                <li><a class="nav-item nav-link <?= $activeAdmin; ?>" data-toggle="tab" href="#users-groups" role="tab"
                       aria-controls="users-groups" aria-selected="false">Users & Groups</a></li>
                <?php } ?>
                <?php $active = ($this->data['user_info'][0]->groups == '' && $this->data['user_info'][0]->controls_user == '' && $this->data['user_info'][0]->is_company == 0)? 'active' : ''; ?>
                <li class="border-none"><a class="nav-item nav-link <?= $active; ?>" data-toggle="tab" href="#personal" role="tab"
                       aria-controls="personal" aria-selected="false">Personal</a></li>
            </ul>
        </div>

        <!--Quick TimePunch-->
        <?php if($this->data['user_info'][0]->is_company == 1){ ?>
        <div class="tab-pane fade show active" id="quick-timepunch" role="tabpanel"
             aria-labelledby="quick-timepunch-tab">
            <div class="row">
                <div class="col-12">Company</div>
                <div class="col-12 row">
                    <div class="col-2" id="logo_image_content">
                        <?php
                        echo $this->data['quick'][0]->logo != "" && $this->data['quick'][0]->logo != null
                            ? '<div class="settings_logo" id="logo_image" style="background-image: url(' . "'" . 'public/images/quick/' . $this->data['quick'][0]->logo . "'" . '")">
                                <span id="">Change</span>
                                <input id="upload_logo_image" type="file" accept="image/*"/>
                               </div>'
                            :
                            '<div class="settings_logo" id="logo_image">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                              <input id="upload_logo_image" type="file" accept="image/*"/>
                             </div>'
                        ?>


                    </div>
                    <div class="col-5">
                        <div class="card"><input type="text" ng-model="company_name" class="form-control" id="company_name" name="company_name" value="<?php echo $this->data['quick'][0]->name ?>" /></div>
                    </div>
                    <div class="col-5 row">

                        <div class="col-sm-12">
                            <label for="timezone-offset" class="col-form-label">Time Zone: </label>
                            <select name="timezone_offset" id="timezone-offset">
                                <?php foreach ($this->data['timezone'] as $timezone)
                                    echo $timezone->id == $this->data['quick'][0]->timezone_id
                                        ? '<option selected="selected" value="' . $timezone->id . '">' . $timezone->label . '</option>' :
                                        '<option  value="' . $timezone->id . '">' . $timezone->label . '</option>'
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 row">
                        <div class="col-7 row">
                            <div class="col-4"><span class="ml-20">Logo</span></div>
                            <div class="col-6">Comapany name</div>
                            <div class="col-12">Your unique ELVA Timepunch link:</div>
                            <div class="col-12 row">
                                <div class="col-lg-6">
                                    <input type="text" id="jamp_url" name="jamp_url"
                                           value="<?php echo ROOT_URL.'quick/'.$this->data['quick'][0]->link ?>">
                                </div>
                                <div class="col-lg-2 col-6 unic_link cursor_pointer" onclick="copy_link()">
                                    <i class="fa fa-copy"></i>
                                    <span>Copy</span>
                                </div>
                                <a href="<?php echo ROOT_URL.'quick/'.$this->data['quick'][0]->link ?>" class="col-lg-4 col-6 unic_link" target="_blank">
                                    <i class="fa fa-window-restore"></i>
                                    <span>Open in new tab</span>
                                </a>

                            </div>
                            <div class="col-12">
                                <span>We recommend not sharing this link to employees, as it can be accessed from any place with internet.
                                 To avoid misunderstandings, the link will not be fully displayed in the address bar, so it can’t be copied.
                                </span>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="wrap">
                                <div class="half readout row">
                                    <div> Choose theme color:</div>
                                    <div id="color_values" data-myval=""></div>
                                </div>
                                <div class="half">
                                    <div class="colorPicker"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-12 row">
                        <div class="col-12">Choose Timepunch mode:</div>
                        <div class="row text-center text-lg-left col-12">
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail" src="public/images/quick/mode/Group 1790.jpg"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail" src="public/images/quick/mode/Group 1792.jpg"
                                         alt="">
                                </a>
                            </div>
                        </div>
                        <div class="row col-12 text-lg-left">
                            <div class="col-lg-4 col-md-4 col-6">
                                <label class="form-check-label">
                                    <input type="radio" <?php echo $this->data['quick'][0]->theme_color == '#FFFFFF' ? 'checked' : '' ?>
                                           class="form-check-input" name="them" value="#FFFFFF">Light mode
                                </label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <label class="form-check-label">
                                    <input type="radio" <?php echo $this->data['quick'][0]->theme_color == '#000000' ? 'checked' : '' ?>
                                           class="form-check-input" name="them" value="#000000">Dark mode
                                </label>
                            </div>
                        </div>
                        <div class="col-12">Choose Timepunch Background:</div>
                        <div class="row text-center text-lg-left">
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail"
                                         src="public/images/quick/mini/art-background-blank-1020317.jpg" alt="">
                                    <label class="form-check-label">
                                        <input type="radio" <?php echo $this->data['quick'][0]->background_image == 'art-background-blank-1020317.jpg' ? 'checked' : '' ?>
                                               class="form-check-input" name="background"
                                               value="art-background-blank-1020317.jpg">
                                    </label>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail"
                                         src="public/images/quick/mini/astro-astronomy-background-956999.jpg" alt="">
                                    <label class="form-check-label">
                                        <input type="radio"
                                               class="form-check-input" <?php echo $this->data['quick'][0]->background_image == 'astro-astronomy-background-956999.jpg' ? 'checked' : '' ?>
                                               name="background" value="astro-astronomy-background-956999.jpg">
                                    </label>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail"
                                         src="public/images/quick/mini/background-beautiful-blue-1435752.jpg" alt="">
                                    <label class="form-check-label">
                                        <input type="radio"
                                               class="form-check-input" <?php echo $this->data['quick'][0]->background_image == 'background-beautiful-blue-1435752.jpg' ? 'checked' : '' ?>
                                               name="background" value="background-beautiful-blue-1435752.jpg">
                                    </label>
                                </a>
                            </div>
                        </div>
                        <div class="row text-center text-lg-left">
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail"
                                         src="public/images/quick/mini/background-black-colors-952670.jpg" alt="">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input"
                                               name="background" <?php echo $this->data['quick'][0]->background_image == 'background-black-colors-952670.jpg' ? 'checked' : '' ?>
                                               value="background-black-colors-952670.jpg">
                                    </label>
                                </a>

                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <a class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail"
                                         src="public/images/quick/mini/background-design-material-850796.jpg" alt="">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input"
                                               name="background" <?php echo $this->data['quick'][0]->background_image == 'background-design-material-850796.jpg' ? 'checked' : '' ?>
                                               value="background-design-material-850796.jpg">
                                    </label>
                                </a>

                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <a target="_self" class="d-block mb-4 h-100">
                                    <img class="img-fluid img-thumbnail"
                                         src="public/images/quick/mini/backgrounds-blank-blue-953214.jpg" alt="">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input"
                                               name="background" <?php echo $this->data['quick'][0]->background_image == 'backgrounds-blank-blue-953214.jpg' ? 'checked' : '' ?>
                                               value="backgrounds-blank-blue-953214.jpg">
                                    </label>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <!--Users & Groups -->

        <div ng-app="myApp" ng-controller="myCtrl" class="tab-pane fade card <?= ($activeAdmin == 'active')? 'show active':''; ?>" id="users-groups" role="tabpanel"
             aria-labelledby="users-groups-tab">
            <div class="row">
                <div class="col-3">
                    <h3 class="user_groups_individuals active" ng-click="call_users()">Individuals</h3>
                    <h3 class="group_h3">Groups:</h3>
                    <div class="menu">
                        <a ng-repeat="x in groups" class="hvr-grow" ng-click="call_users(x.id)">{{x.title}}</a>
                        <a class="icon-plussign creat_group" data-toggle="modal" data-target="#CreateGroupModal"
                           id="creat_group"> Create Group</a>
                    </div>

                </div>
                <div class="col-9 row">
                    <div class="text-center col-4 icon-usercreat"><span data-toggle="modal" data-target="#CreateNewEmployeeModal"
                                                            id="CreateNewEmployee"> Create New Employee</span></div>
                    <div class="text-center col-4 icon-plussign invisible"><span data-toggle="modal" data-target="#AdduserModal"
                                                           ng-click="get_users_for_add()">Add user</span></div>
                    <div class="text-right col-4 icon-deactivate"><span id="deactivate_user"> Deactivate user</span></div>
                    <div class="text-right col-4 icon-activate" style="display: none"><span id="activate_user"> Activate user</span>
                    </div>
                    <div class="col-12 table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Group</th>
                                <th scope="col">Position</th>
                                <th scope="col">Start Date</th>
                                <th scope="col" id="select_all" class="active">Select All<input type="checkbox"
                                                                                                id="select_all_input">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="x in users" style="color: {{x.text_color}}">
                                <td ng-click="get_user_group_setting(x.id)">{{x.last_name}} {{x.name}}</td>
                                <td ng-click="get_user_group_setting(x.id)">{{x.group}}</td>
                                <td ng-click="get_user_group_setting(x.id)">{{x.positions}}</td>
                                <td ng-click="get_user_group_setting(x.id)">{{x.start_date}}</td>
                                <th scope="row"><input type="checkbox" ng-click="changebutton(x.active)"
                                                       class="checkSingle" value="{{x.id}}"></th>
                            </tr>
                            </tbody>
                            <tfoot>
                            <th scope="col" colspan="2">Groups: <span>{{counts[0].count_group}}</span> <br> Active
                                users: <span>{{counts[0].count_active_users}}</span></th>
                            <th scope="col" colspan="3">All users: <span>{{counts[0].users_count}}</span> <br>
                                Deactivated users: <span>{{counts[0].count_deactive_users}}</span></th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <!-- creat new employee modal  -->
            <div class="modal" id="CreateNewEmployeeModal">
                <div class="modal-dialog  modal-xl">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title icon-usercreat1"> Create New Employee</h4>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="col-12 row">
                                <div class="form-group col-4">
                                    <label for="FirstName" class="col-form-label">First Name:</label>
                                    <input ng-model="FirstName" type="text" class="form-control" id="FirstName"
                                           placeholder="">FirstName
                                </div>
                                <div class="form-group col-4">
                                    <label for="LastName" class="col-form-label">Last Name:</label>
                                    <input ng-model="LastName" type="text" class="form-control" id="LastName"
                                           placeholder="">
                                </div>
                                <div class="form-group col-4">
                                    <label for="Email" class="col-form-label">Email:</label>
                                    <input ng-model="Email" type="text" class="form-control" id="Email" placeholder="">
                                </div>
                                <div class="form-group col-8">
                                    <label for="Email" class="col-form-label">Job Position</label>
                                    <select chosen multiple
                                            options="position"
                                            ng-model="position"
                                            ng-options="position.id as position.name  for position in positions"
                                            class="form-control" id="JobPosition">
                                    </select>
                                </div>
                                <div class="form-group col-4">


                                    <label for="ManagesGroup" class="col-form-label">Manages Group</label>
                                    <select chosen multiple
                                            options="group"
                                            ng-model="group"
                                            ng-options="group.id as group.title  for group in groups"
                                            class="form-control" id="ManagesGroup">
                                    </select>

                                </div>
                                <div class="form-group col-4">
                                    <label for="PhoneNumber" class="col-form-label">Phone Number</label>
                                    <input ng-model="PhoneNumber" type="text" class="form-control" id="PhoneNumber"
                                           placeholder="">
                                </div>
                                <div class="form-group col-4">
                                    <label for="Fax" class="col-form-label">Fax</label>
                                    <input ng-model="Fax" type="text" class="form-control" id="Fax" placeholder="">
                                </div>
                                <div class="form-group col-4">
                                    <label for="Address" class="col-form-label">Address</label>
                                    <input ng-model="Address" type="text" class="form-control" id="Address"
                                           placeholder="">
                                </div>
                                <div class="form-group col-4">
                                    <label for="Password" class="col-form-label">Password</label>
                                    <input ng-model="Password" type="password" class="form-control" id="Password"
                                           placeholder="">
                                </div>
                                <div class="form-group col-4">
                                    <label for="Repeatpassword" class="col-form-label">Repeat password</label>
                                    <input ng-model="Repeatpassword" type="password" class="form-control"
                                           id="Repeatpassword" placeholder="">
                                </div>
                                <div class="form-group col-4">
                                    <label for="Overtimeallowance" class="col-form-label">Overtime allowance</label>
                                    <select chosen multiple
                                            options="overtime"
                                            ng-model="overtime"
                                            ng-options="overtime.id as overtime.title  for overtime in overtimes"
                                            class="form-control" id="Overtimeallowance">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-save" ng-click="SaveEmployee()">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>

            <!--Group Setting-->

            <div class="modal" id="GroupSettingsModal">
                <div class="modal-dialog  modal-xl">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h5 class="modal-title"> Settings: </h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="card">
                                <div class="col-4">
                                    <div class="col-12 row">
                                        <div>Group name:</div>
                                        <span>{{group_setting.group_name}}</span>
                                    </div>
                                    <div class="col-12 row">
                                        <div>Schedule:</div>
                                        <span>{{group_setting.schedule}}</span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="col-12 row">
                                        <div>Overtime allowance:</div>
                                        <span>{{group_setting.overtime_allowance}}</span>
                                    </div>
                                    <div class="col-12 row">
                                        <div>Range allowance:</div>
                                        <span>{{group_setting.range_allowance}}</span>
                                        <span> hrs</span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="col-12 row">
                                        <div> Manager:</div>
                                        <span> {{group_setting.manager}} </span>
                                    </div>
                                    <div class="col-12 row">
                                        <div>Creation date:</div>
                                        <span>{{group_setting.active_since}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12"> <h5>User details:</h5></div>

                            <div class="card">
                                <div class="row">
                                    <div class="col-6 row group_setting_image" ng-if="group_setting.image==''|| !group_setting.image ">
                                        <div class="card" id="image_content_group_setting">
                                            <div id="image_group_setting"> + Upload an image </div>
                                        </div>
                                    </div>

                                    <div class="col-6 row group_setting_image"  ng-if="group_settings.image!='' && group_setting.image ">
                                        <div class="card col-7">

                                            <img src="public/images/users/{{group_setting.image}}"
                                                 class="card-img" alt="...">

                                        </div>
                                        <div class="col-5">
                                            <a id="image_group_setting">Change Photo</a>
                                            <br>
                                            <hr>
                                            <a id="delete_image_group_setting"> Delete Photo</a>
                                            <hr>
                                            <div>
                                                Current photo Uploaded
                                            </div>
                                            <div>
                                                {{group_setting.image_uploaded_date}}
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-6 row ">
                                        <input id="upload_image_group_setting" type="file" accept="image/*"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6 col-md-12 col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Name:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Last Name:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_last_name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Email:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Pin:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_pin">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Group:</label>
                                            <div class="col-sm-8">
                                                <select chosen multiple
                                                        options="group"
                                                        ng-model="group_setting_group_id"
                                                        ng-options="group.id as group.title  for group in groups"
                                                        class="form-control" id="ManagesGroup">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Manages Group:</label>
                                            <div class="col-sm-8">
                                                <select chosen multiple
                                                        options="group"
                                                        ng-model="group_setting_manag_group"
                                                        ng-options="group.id as group.title  for group in groups"
                                                        class="form-control" id="ManagesGroup">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Position:</label>
                                            <div class="col-sm-8">
                                                <select chosen multiple
                                                        options="position"
                                                        ng-model="group_setting_position"
                                                        ng-options="position.id as position.name  for position in positions"
                                                        class="form-control" id="JobPosition">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Schedule:</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control" ng-model="group_setting_schedule_id" ng-options="x.id as x.title for x in schedule_group_setting"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6 col-md-12 col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Next Schedule:</label>
                                            <div class="col-sm-8">
                                                <input type="text" disabled="disabled" class="form-control" ng-model="group_setting_next_schedule">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"> Start Date:</label>
                                            <div class="col-sm-8">
                                                <input type="text" disabled="disabled" class="form-control" ng-model="group_setting_start_date">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"> Active since:</label>
                                            <div class="col-sm-8">
                                                <input type="text" disabled="disabled" class="form-control" ng-model="group_setting_active_since">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"> Phone number:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_phone_number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"> Fax:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_fax">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"> Address:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_address">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"> Zip code:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" ng-model="group_setting_zip_code">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Overtime allowance:</label>
                                            <div class="col-sm-8">
                                                <select chosen multiple
                                                        options="overtime"
                                                        ng-model="group_setting_overtime"
                                                        ng-options="overtime.id as overtime.title  for overtime in overtimes"
                                                        class="form-control" id="ManagesGroup">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-save" ng-click="SaveGroupSetting()">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>

            <!--    Creat  Group Modal        -->

            <div class="modal" id="CreateGroupModal">
                <div class="modal-dialog  modal-md">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title"><i class="fa fa-users"></i> Create Group</h4>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="col-12">
                                <span>Group Name:</span>
                                <input ng-model="GroupName" type="text" class="form-control form-control-sm"
                                       id="GroupName"
                                       placeholder="">

                                <form class="form-inline" id="live-search" class="styled" method="post">
                                    <fieldset>
                                        <i class="fas fa-search" aria-hidden="true"></i>
                                        <input class="form-control form-control-sm w-90 filter" id="filter" value=""
                                               type="search" placeholder="Search for people to add "
                                               aria-label="Search">
                                    </fieldset>

                                </form>

                                <nav>
                                    <ul>
                                        <li ng-repeat="x in all_users" remove_id="{{x.id}}">
                                        <span><img ng-src="public/images/users/{{x.image}}"
                                                   onerror="this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAA1BMVEUAgACc+aWRAAAASElEQVR4nO3BgQAAAADDoPlTX+AIVQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwDcaiAAFXD1ujAAAAAElFTkSuQmCC'"
                                                   alt="" width="50" height="50">   <span>{{x.name}}</span> </span>
                                            <button ng-click="AddUser(x.id)" class="btn btn-primary btn-sm"
                                                    type="button"><i
                                                        class="fa fa-plus"></i> Add
                                            </button>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-save" ng-click="SaveGroup()">Create</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Add User Modal-->

            <div class="modal" id="AdduserModal">
                <div class="modal-dialog  modal-lg">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title icon-plussign"> Add User</h4>
                            <input class="form-control-sm filter" value="" type="search" placeholder="Search"
                                   aria-label="Search">
                            <div id="Select_All_Users">Select All</div>
                            <input type="checkbox" id="select_all_input_users">
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <nav>
                                <ul>
                                    <li ng-repeat="x in all_users_for_add_users" class="row">
                                        <div>{{x.name}}</div>
                                        <input type="checkbox" class="checkSingleUsersAdd" value="{{x.id}}">
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-save" ng-click="add_user_in_group()"> Add</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <!--Personal-->

        <div class="tab-pane fade <?= ($active == 'active')? 'show active':''; ?>" id="personal" role="tabpanel" aria-labelledby="personal">
            <div class="row">
                <div class="col-lg-6">
                    Photo:
                </div>
                <div class="col-lg-6 d-lg-block d-md-none d-ms-none">
                    Personal Info:
                </div>

            </div>
            <div class="row">
                <div class="col-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="card">
                        <div>
                            <?php if (isset($this->data['user']) && !empty($this->data['user'][0]->image)) {
                                ?>
                                <div class="card col-7">

                                    <img src="public/images/users/<?php echo $this->data['user'][0]->image ?>"
                                         class="card-img" alt="...">

                                </div>
                                <div class="col-5">
                                    <a id="image">Change Photo</a>
                                    <br>
                                    <hr>
                                    <a id="delete_image"> Delete Photo</a>
                                    <hr>
                                    <div>If you are unable to change your photo, you do not have permission to do so.
                                        Please
                                        contact your manager
                                    </div>

                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="card" id="image_content">
                                    <div id="image"> + Upload an image</div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <input id="upload_image" type="file" accept="image/*"/>
                        <p>Change password:</p>
                        <div class="card">
                            <div class="form-group row">
                                <label for="currentpassword" class="col-sm-6 col-form-label">Current Password:</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="currentpassword" placeholder="">
                                </div>
                                <label for="inputpassword" class="col-sm-6 col-form-label">New password:</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="inputpassword" placeholder="">
                                </div>
                                <label for="repeatpassword" class="col-sm-6 col-form-label">Repeat password:</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="repeatpassword" placeholder="">
                                </div>
                                <div class="col-sm-12">
                                    <input type="submit" class="btn" id="pswsubmit" value="OK">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-lg-none d-sm-block">Personal Info:</div>
                <div class="col-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="form-group row">
                            <label for="name" class="col-sm-5 col-form-label">Name:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="name" placeholder="" <?= (isset($this->session->is_company) && $this->session->is_company == 0)?'disabled="disabled"':''; ?>
                                       value="<?php echo $this->data['user_info'][0]->name ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->name != '' && $this->data['user_info'][0]->name != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="email" class="col-sm-5 col-form-label">Email:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="email" placeholder=""
                                       value="<?php echo $this->data['user_info'][0]->email ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->email != '' && $this->data['user_info'][0]->email != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="pin" class="col-sm-5 col-form-label">Pin:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="pin" placeholder=""
                                       value="<?php echo $this->data['user_info'][0]->pin ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->pin != '' && $this->data['user_info'][0]->pin != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="group" class="col-sm-5 col-form-label">Group:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="group" placeholder="" disabled="disabled"
                                       value="<?php echo $this->data['user_info'][0]->group ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->group != '' && $this->data['user_info'][0]->group != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="manages_group" class="col-sm-5 col-form-label">Manages group:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="manages_group" placeholder=""
                                       disabled="disabled"
                                       value="<?php echo ($this->data['user_info'][0]->is_company != 0)?'Manages all groups':$this->data['user_info'][0]->manage_group; ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->manage_group != '' && $this->data['user_info'][0]->manage_group != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="position" class="col-sm-5 col-form-label">Position:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="position" placeholder="" disabled="disabled"
                                       value="<?php echo $this->data['user_info'][0]->positions ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->positions != '' && $this->data['user_info'][0]->positions != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="schedule" class="col-sm-5 col-form-label">Schedule:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="schedule" placeholder="" disabled="disabled"
                                       value="<?php echo $this->data['user_info'][0]->schedule ?>" >
                            </div>
                            <?php echo $this->data['user_info'][0]->schedule != '' && $this->data['user_info'][0]->schedule != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="next_schedule" class="col-sm-5 col-form-label">Next schedule:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="next_schedule" placeholder="" disabled="disabled"
                                       value="<?php echo $this->data['user_info'][0]->next_schedule ?>" >
                            </div>
                            <?php echo $this->data['user_info'][0]->next_schedule != '' && $this->data['user_info'][0]->next_schedule != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="active_since" class="col-sm-5 col-form-label">Active since:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="active_since" placeholder=""
                                       disabled="disabled"
                                       value="<?php echo $this->data['user_info'][0]->active_since ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->active_since != '' && $this->data['user_info'][0]->active_since != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="phone_number" class="col-sm-5 col-form-label">Phone number:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="phone_number" placeholder=""
                                       value="<?php echo $this->data['user_info'][0]->phone_number ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->phone_number != '' && $this->data['user_info'][0]->phone_number != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="fax" class="col-sm-5 col-form-label">Fax:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="fax" placeholder=""
                                       value="<?php echo $this->data['user_info'][0]->fax ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->fax != '' && $this->data['user_info'][0]->fax != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="address" class="col-sm-5 col-form-label">Address:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="address" placeholder=""
                                       value="<?php echo $this->data['user_info'][0]->address ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->address != '' && $this->data['user_info'][0]->address != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="zip_code" class="col-sm-5 col-form-label">Zip code:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="zip_code" placeholder=""
                                       value="<?php echo $this->data['user_info'][0]->zip_code ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->zip_code != '' && $this->data['user_info'][0]->zip_code != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="range_allowance" class="col-sm-5 col-form-label">Range allowance: </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="range_allowance" placeholder=""
                                       disabled="disabled" value="<?php echo $this->data['user_info'][0]->range_allowance ?>">
                            </div>
                            <?php echo $this->data['user_info'][0]->range_allowance != '' && $this->data['user_info'][0]->range_allowance != null ? '<i class="fa fa-check"></i>' : '' ?>
                            <label for="overtime_allowance" class="col-sm-5 col-form-label">Overtime allowance:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="overtime_allowance" placeholder=""
                                       disabled="disabled" value="<?php echo $this->data['user_info'][0]->overtime ?>" >
                            </div>
                            <?php echo $this->data['user_info'][0]->overtime != '' && $this->data['user_info'][0]->overtime != null  && $this->data['user_info'][0]->overtime != 'NaN' ? '<i class="fa fa-check"></i>' : '' ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>

