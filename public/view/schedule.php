<div class="modal fade" id="dayControllBox" tabindex="-1" role="dialog" aria-labelledby="dayControllBox" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center p-4">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn sch-cancel mx-auto" data-dismiss="modal"><?=$this->translate("cancel")?></button>
                <button type="button" class="btn sch-accept mx-auto"><?=$this->translate("save")?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="messageBox" tabindex="-1" role="dialog" aria-labelledby="messageBox" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center p-4">
            	<p>Error</p>
            	<p>Not Posibru!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn sch-cancel mx-auto" data-dismiss="modal"><?=$this->translate("ok")?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="loopBox" tabindex="-1" role="dialog" aria-labelledby="loopBox" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center p-4">
            	<p>If ya loop me the schedule will be overwritten accordingly to the loop!</p>
            	<p>The action is irreversible!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn sch-cancel mx-auto" data-dismiss="modal"><?=$this->translate("cancel")?></button>
                <button type="button" class="btn sch-accept mx-auto"><?=$this->translate("loop")?></button>
            </div>
        </div>
    </div>
</div>
<!----------------------------------------------------------------------------------------------->

<div class="modal fade" id="addToScheduleGroup" tabindex="-1" role="dialog" aria-labelledby="loopBox" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="searchAndSelectBox">
                    <div class="col-md-10">
                        <div class="form-group row">
                            <label for="groups" class="col-sm-1 col-form-label">Groups:</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="groupsSearch" placeholder="search">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <span id="select-all-groups-modal">Select All</span>
                    </div>
                </div>
                <table class="table table-striped table-groups table-groups-live-search">
                    <tbody>
                    <?php
                    foreach ($this->data['groupsList'] as $group) {
                        $userGroup  = array_filter(explode(',', $group->users), function($x) { return !empty($x); });
                        $userBlackList = [];
                        ?>
                        <tr class="user-group-list" id="user-group-list-<?php echo $group->id; ?>" data-group-id="<?php echo $group->id; ?>">
                            <td><i class="add-group-user-icon" data-groups-chckbox-id="<?php echo $group->id; ?>"></i></td>
                            <td><?php echo $group->title; ?></td>
                            <td>(<span class="freeInThisSchedule"><?php echo $group->freeFromSchedule; ?></span>)</td>
                            <td>(<span class="notFreeInThisSchedule"><?php echo $group->notFreeFromSchedule == 0 ? '0' : '-' . $group->notFreeFromSchedule; ?></span>)</td>
                            <td>
                                <label class="container select-all-groups-modal-checkbox">
                                    <input type="checkbox" data-groups-chckbox-id="<?php echo $group->id; ?>">
                                    <span class="checkmark groups" data-user-list="group-users-list-<?php echo $group->id; ?>"></span>
                                </label>
                            </td>
                            <?php
                                foreach ($this->data['usersList'] as $user) {
                                    if (!empty($userGroup) && in_array($user->id, $userGroup) && !is_null($user->name) && is_null($user->isCompany) && !isset($userBlackList[$user->id])) {
                                        $userBlackList[$user->id] = $user->id;
                                        ?>
                                        <tr colspan="10" class="d-none group-names userListItem-<?php echo $user->id; ?>-<?php echo $group->id; ?> userListItemId-<?php echo $user->id; ?> group-users-list-<?php echo $group->id; ?> schedule-id-<?php echo $user->scheduleId; ?>" data-group-id="<?php echo $group->id; ?>">
                                            <td class="user-name <?php echo is_null($user->scheduleId) ? 'in-schedule-user' : 'not-schedule-user'; ?>"><?php echo $user->name . ' ' . $user->last_name; ?></td>
                                            <td class="user-checkbox" colspan="4">
                                                <label class="container select-all-groups-modal-checkbox">
                                                    <input type="checkbox" <?php echo is_null($user->scheduleId) ? '' : 'disabled'; ?> data-groups-chckbox-id="<?php echo $group->id; ?>">
                                                    <span class="checkmark <?php echo is_null($user->scheduleId) ? 'checkmark-in-schedule-user' : 'checkmark-not-schedule-user'; ?>" data-group-title="<?php echo $group->title; ?>" data-user-id="<?php echo $user->id; ?>" data-groups-chckbox-id="<?php echo $group->id; ?>"></span>
                                                </label>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                }
                            ?>
                        </tr>
                <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn sch-accept col-md-1" id="saveGroupsInSchedule">Add</button>
                <button type="button" class="btn sch-cancel col-md-1" id="cancelGroupsInSchedule" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addToScheduleIndividuals" tabindex="-1" role="dialog" aria-labelledby="loopBox" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="searchAndSelectBox">
                    <div class="col-md-10">
                        <div class="form-group row">
                            <label for="groups" class="col-sm-2 col-form-label">Individuals:</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="individualsSearchPop" placeholder="search">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <span>Select All</span>
                    </div>
                </div>
                <table class="table table-striped table-users table-individuals-live-search">
                    <tbody>
                        <?php
                            foreach ($this->data['usersList'] as $user) {
                                if (!is_null($user->name) && is_null($user->title)) {
                                    ?>
                                        <tr data-scheduleId="<?php echo $user->scheduleId; ?>" class="scheduleUserList userListItem-<?php echo $user->id; ?> userListItemId-<?php echo $user->id; ?> scheduleUsers-<?php echo $user->scheduleId; ?>"  date-userId-id="<?php echo $user->id; ?>">
                                            <td class="scheduleUserListUsername <?php echo is_null($user->scheduleId) ? '' : 'not-schedule-user'; ?>"><?php echo "$user->name $user->last_name"; ?></td>
                                            <td>
                                                <label class="container select-all-individuals-modal-checkbox">
                                                    <input type="checkbox" class="select-all-individuals-modal-checkbox-input" <?php echo is_null($user->scheduleId) ? '' : 'disabled'; ?> data-individuals-chckbox-id="<?php echo $user->id; ?>">
                                                    <span class="checkmark" data-user-id="<?php echo $user->id; ?>" data-new-schedule-id="<?php echo $user->scheduleId; ?>"></span>
                                                </label>
                                            </td>
                                        </tr>
                                    <?php
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn sch-accept col-md-1" id="saveIndividualsInSchedule">Add</button>
                <button type="button" class="btn sch-cancel col-md-1" id="cancelIndividualsInSchedule" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="loopBox" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header help-modal-header">
                <h5>How it works?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center p-4">
                <div id="helpOne">help one</div>
                <div id="helpTow">help tow</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="addScheduleName d-none">
                    <h2>Add Schedule</h2>
                    <p class="errorField"></p>
                    <div class="col-md-12" id="addScheduleInpGroup">
                        <div class="form-group col-4 float-right">
                            <span>Schedule Name</span>
                            <input class="form-control newScheduleName" type="text" placeholder="">
                        </div>
                        <div class="form-group col-4 float-right">
                            <span>Schedule End Date</span>
                            <input id="newScheduleEndDate" class="form-control newScheduleDate" type="text">
                        </div>
                        <div class="form-group col-4 float-right">
                            <span>Schedule Start Date</span>
                            <input id="newScheduleStartDate" class="form-control newScheduleDate" type="text">
                        </div>
                    </div>
                </div>
                <div class="removeQuestion d-none">
                    <h2>Are you sure you want to continue ?</h2>
                </div>
                <div class="error-messages d-none">
                    <h2></h2>
                </div>

                <div class="add-time-to-day-modal d-none">
                    <div class="switch-button">
                        <p class="switch-slider-button">Set clock in time range</p>
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round switch-slider-button-click"></span>
                        </label>
                    </div>
                    <div id="add-time-to-day-one" class="d-none">
                        <p class="errorField"></p>
                        <div>
                            <ul>
                                <li>Set Clock In day and time</li>
                                <li>From</li>
                                <li>
                                    <select class="form-control" id="clockInWeekDayTimeRange">
                                        <option value="1" >Monday</option>
                                        <option value="2" >Tuesday</option>
                                        <option value="3" >Wednesday</option>
                                        <option value="4" >Thursday</option>
                                        <option value="5" >Friday</option>
                                        <option value="6" >Saturday</option>
                                        <option value="7" >Sunday</option>
                                    </select>
                                    &nbsp;
                                    <input type="time" class="form-control" id="clockInTimeRange">
                                </li>
                            </ul>
                            <div class="line-range"></div>
                            <ul>
                                <li>Set Clock Out day and time</li>
                                <li>To</li>
                                <li>
                                    <select class="form-control" id="clockOutWeekDayTimeRange">
                                        <option value="1" >Monday</option>
                                        <option value="2" >Tuesday</option>
                                        <option value="3" >Wednesday</option>
                                        <option value="4" >Thursday</option>
                                        <option value="5" >Friday</option>
                                        <option value="6" >Saturday</option>
                                        <option value="7" >Sunday</option>
                                    </select>
                                    &nbsp;
                                    <input type="time" class="form-control" id="clockOutTimeRange">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="add-time-to-day-two" class="d-none">
                        <p class="errorField"></p>
                        <div>
                            <ul>
                                <li>Clock In time</li>
                                <li>From</li>
                                <li>
                                    <select class="form-control" id="weekDay">
                                        <option value="1" >Monday</option>
                                        <option value="2" >Tuesday</option>
                                        <option value="3" >Wednesday</option>
                                        <option value="4" >Thursday</option>
                                        <option value="5" >Friday</option>
                                        <option value="6" >Saturday</option>
                                        <option value="7" >Sunday</option>
                                    </select>
                                    &nbsp;
                                    <input id="clickInTimeFrom" class="form-control" type="time">
                                </li>
                            </ul>
                            <div class="line-range"></div>
                            <ul class="clickInTimeWorkingDuration">
                                <li>Range time</li>
                                <li>In minutes</li>
                                <li><input id="rangeTimeTo" min="0" max="999" type="number"></li>
                            </ul>
                            <div class="line-range"></div>
                            <ul class="clickInTimeWorkingDuration">
                                <li>Working duration</li>
                                <li>In hours</li>
                                <li><input id="workingDuration" min="0" max="99" type="number"></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="notifications">
                    <div class="notifications-title"></div>
                </div>
                <div class="save-cancel-button">
                    <button type="button" class="btn sch-cancel" id="modelCancel">Cancel</button>
                    <button type="button" class="btn sch-accept"  id="modelSave" >Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="lunchCont">
    <div class="sch-lunch">
        <div class="sch-lunch-type">
            <div class="m-0 row">
                <div data-cls="fixed" class="col p-0 py-2 fixed text-center lunch-type">Fixed</div>
            </div>
            <div class="m-0 row">
                <div data-cls="range" class="col p-0 py-2 range text-center lunch-type">Range</div>
            </div>
            <div class="m-0 row">
                <div data-cls="flexible" class="col p-0 py-2 flexible text-center lunch-type">Flexible</div>
            </div>
        </div>
        <div class="sch-lunch-inner">
            <div class="row">
                <div class="col sch-lunch-inner-title text-center py-1"></div>
            </div>
            <div class="row">
                <div class="col sch-li-dif">
                    <p class="mb-1">Time</p>
                    <input class="sch-timepicker schTimeFrom text-center" type="time" value="01:00" placeholder="00:00">
                    <div class="time-schTimeTo">
                        <p class="m-0 sch-li-df">-</p>
                        <input class="sch-timepicker schTimeTo text-center" min="01:00" max="12:00" type="time" value="01:00" placeholder="00:00">
                    </div>
                </div>
                <div class="col pb-2">
                    <p class="mb-1">Duration</p>
                    <input class="text-center filter-int schDuration" type="text">
                </div>
                <div class="sch-lunch-inner-actions">
                    <div class="icon-trashcan"></div>
                    <div class="icon-checkmark"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
	<div class="row">
        <div class="col-md-12">
            <ul id="sch-menu">
                <li data-menu-type="mangeSchedule" class="active">Manage Schedule</li>
                <li data-menu-type="employeesInSchedule">Employee Schedule</li>
                <li data-menu-type="employeesCalendar">My Personal Schedule</li>
            </ul>
        </div>
        <div class="col-md-12 schedulePageToggle d-none" id="employeesCalendar">
            <div class="myCurrentSchedule">
                <div class="myCurrentScheduleMonths col-md-12">
                    <span class="employeesCalendarPreviousMonth" data-calendar-name=".calendarCurrentInsert" data-start-date="2019-5" data-end-date="2021-11"></span>
                    <span class="employeesCalendarMonthTitle" data-current-date="<?php echo date("Y-n"); ?>"><?php echo date("Y-n"); ?></span>
                    <span class="employeesCalendarNextMonth" data-calendar-name=".calendarCurrentInsert" data-start-date="2019-5" data-end-date="2021-11"></span>
                </div>
                <span class="myCurrentScheduleTitle">My Current Schedule:</span>
                <div class="calendarInsert calendarCurrentInsert"><img src="public/images/gifs/loading-gif.gif" alt=""></div>
            </div>
            <div class="myNewSchedule">
                <div class="myNewScheduleMonths col-md-12">
                    <span class="employeesCalendarPreviousMonth" data-calendar-name=".calendarNewInsert" data-start-date="2019-5" data-end-date="2021-11"></span>
                    <span class="employeesCalendarMonthTitle" data-current-date="<?php echo date("Y-n"); ?>"><?php echo date("Y-n"); ?></span>
                    <span class="employeesCalendarNextMonth" data-calendar-name=".calendarNewInsert" data-start-date="2019-5" data-end-date="2021-11"></span>
                </div>
                <span class="myNewScheduleTitle">My Next Schedule:</span>
                <div class="calendarInsert calendarNewInsert"><img src="public/images/gifs/loading-gif.gif" alt=""></div>
            </div>
        </div>
        <div class="col-md-12 schedulePageToggle d-none" id="employeesInSchedule">
            <img src="public/images/gifs/loading-gif.gif" alt="">
        </div>
		<div class="sch-main-left text-center col schedulePageToggle" id="mangeSchedule">
			<div class="sch-head row pt-4 pr-3 pl-3 pb-1">
				<div class="sch-first col-3"><?=$this->translate("schname")?></div>
				<div class="sch-days col">
					<div class="row">
                        <?php
                        	for ($i=0; $i < 7; $i++) {
                        		echo '<div class="col">'. $this->translate("day".$i) .'</div>';
                        	}
                        ?>
					</div>
				</div>
				<div class="col-1"></div>
                <button id="helpButtonOne" class="question-blue-icon float-left"></button>
			</div>
			<div class="sch-item-list">
                <button id="addNewSchedule" class="sch-new-schedule icon-plussign float-left"></button>
                <div class="sch-items">
                <?php
                	if($this->data['schedules']){
                		foreach ($this->data['schedules'] as $schedule) {
                            $times = [];
                		    if (isset($this->data['shifts']['shift-' . $schedule->id])) {
                                $times = $this->data['shifts']['shift-' . $schedule->id];
                            }
                		    ?>
                				<div id="<?php echo $schedule->id; ?>" class="sch-item row py-3 my-3 position-relative">
                					<div class="sch-item-first col-3">
                						<span class="align-middle"><?php echo $schedule->title; ?></span>
                						<div></div>
                					</div>
                					<div class="sch-item-days col">
                						<div class="row">
                                            <?php
                                                for ($i=0; $i < 7; $i++) {
                                                    ?>
                                                    <div class="col mx-1 py-1 <?php
                                                        echo (@is_null($times[$i]->clockIn) && @is_null($times[$i]->workingDuration) && @$times[$i]->status == 2) || @is_null($times[$i]) ? "disabled" : ""
                                                    ?>">
                                                        <?php
                                                        if (@!is_null($times[$i])) {
                                                            if ($times[$i]->status == 2) {
                                                                ?>
                                                                    Day Off
                                                                <?php
                                                            } else {
                                                                if (is_null($times[$i]->workingDuration)) {
                                                                    echo is_null($times[$i]->clockIn) || $times[$i]->status == 2 ? "--:-- - --:--" : date("H:i", strtotime($times[$i]->clockIn)) . ' - ' . date("H:i", strtotime($times[$i]->clockOut));
                                                                } else {
                                                                    echo is_null($times[$i]->workingDuration) || $times[$i]->status == 2 ? "--:-- - --:--" : date("H:i", strtotime($times[$i]->clockIn)) . ' - ' . $times[$i]->dayRange . ' - ' . $times[$i]->workingDuration;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                            ?>
                						</div>
                					</div>
                					<div class="sch-item-actions col-1">
                						<div class="row">
                							<div class="col icon-users">
                								<span>
                                                    <?php echo $schedule->userCount; ?>
                								</span>
                							</div>
                							<div class="col icon-trashcan align-middle remove-schedule" data-schedule-id="<?php echo $schedule->id; ?>"></div>
                						</div>
                					</div>
                                    <div class="w-100 p-3 sch-inner-item position-relative" id="sch-inner-item-<?php echo $schedule->id; ?>" data-startDate="<?php echo $schedule->StartDate; ?>">
                                        <div class="row switch-schedule-to-group">
                                            <div class="col-2">
                                                <div class="remove-send-schedule d-none">
                                                    <span class="scheduleGroupRemove">
                                                        <i class="schedule-icon disabled-icon schedule-group-remove-disabled-icon scheduleGroupRemove" data-startdate="<?php echo $schedule->StartDate; ?>" data-scheduleId="<?php echo $schedule->id; ?>"></i>
                                                    </span>
                                                    <span>Remove</span>
                                                    <span class="scheduleSend">
                                                        <i class="disabled-icon schedule-send-disabled-icon scheduleSend" data-startdate="<?php echo $schedule->StartDate; ?>" data-scheduleId="<?php echo $schedule->id; ?>"></i>
                                                    </span>
                                                    <span>Send Schedule</span>
                                                </div>
                                            </div>
                                            <div class="col-4 text-right active cursor-pointer change-to-groups" data-type="schedule">
                                                Schedule
                                            </div>
                                            <div class="col-6">
                                                <div class="col-6 text-left cursor-pointer change-to-groups in-this-schedule-switch" data-type="in-this-schedule" data-startDate="<?php echo $schedule->StartDate ?>" data-schedule="<?php echo $schedule->id; ?>">
                                                    In this schedule
                                                </div>
                                                <div class="col-6 help-container-icon">
                                                    <button class="helpButtonTow help-icon float-left"></button>
                                                </div>
                                            </div>
                                            <div class="w-100 p-3 schedule-week-list">
                                                <div class="row">
                                                    <div class="col">
                                                        <div>
                                                            <div class="row my-2 px-3 week-days">
                                                                <div class="col mx-2 py-1">
                                                                    Mon.
                                                                </div>
                                                                <div class="col mx-2 py-1">
                                                                    Tues.
                                                                </div>
                                                                <div class="col mx-2 py-1">
                                                                    Wed.
                                                                </div>
                                                                <div class="col mx-2 py-1">
                                                                    Thurs.
                                                                </div>
                                                                <div class="col mx-2 py-1">
                                                                    Fri.
                                                                </div>
                                                                <div class="col mx-2 py-1">
                                                                    Sat.
                                                                </div>
                                                                <div class="col mx-2 py-1">
                                                                    Sun.
                                                                </div>
                                                                <div class="col mx-2 py-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="sch-new-week-<?php echo $schedule->id; ?>">
                                                            <?php
                                                            $Ids = array();
                                                                for ($i=0; $i < count($times); $i++) {

                                                                    $Ids[] = $times[$i]->id;
                                                                    if ($i % 7 == 0 || $i == 0) {
                                                                        if ($i != 0) {
                                                            ?>
                                                                        </div>
                                                                    <?php
                                                                        }
                                                                        ?>
                                                                    <?php
                                                                    $currentWeekID = 'week-'.$i.'-'.$schedule->id; // ID of the currently created week
                                                                    ?>
                                                                    <div class="row my-2 px-3 weeks-list" id="<?php echo $currentWeekID; ?>">
                                                                        <div class="col-1 mx-2 sch-week-title"><?php echo $times[$i]->weekId; ?></div>
                                                            <?php } ?>
                                                                        <div class="col changeable noselect mx-2 py-1 add-time-to-day <?php echo $times[$i]->status == 2 ? 'disabled' : '';  ?>"
                                                                             data-scheduleId="<?php echo $schedule->id; ?>"
                                                                             data-clockIn="<?php echo $times[$i]->clockIn == "" ? "" : date("H:i", strtotime($times[$i]->clockIn)); ?>"
                                                                             data-clockOut="<?php echo $times[$i]->clockOut == "" ? "" : date("H:i", strtotime($times[$i]->clockOut)); ?>"
                                                                             data-workingDuration="<?php echo $times[$i]->workingDuration; ?>"
                                                                             data-range="<?php echo $times[$i]->dayRange; ?>"
                                                                             data-weekday-start="<?php echo ($i % 7) + 1; ?>"
                                                                             data-weekday-end="<?php echo $times[$i]->clockOutWeekDay != 0 ? $times[$i]->clockOutWeekDay : ($i % 7) + 1; ?>"
                                                                             id="shift-<?php echo $times[$i]->id; ?>"
                                                                             data-shiftId="<?php echo $times[$i]->id; ?>">
                                                                            <?php
                                                                                if (!is_null($times[$i]->lunchType)) {
                                                                                    ?>
                                                                                        <div class="sch-lunch">
                                                                                            <div class="sch-lunch-inner <?php
                                                                                            if ($times[$i]->lunchType == 0) {
                                                                                                ?>fixedBorderColor<?php
                                                                                            } elseif ($times[$i]->lunchType == 1) {
                                                                                                ?>rangeBorderColor<?php
                                                                                            } elseif ($times[$i]->lunchType == 2) {
                                                                                                ?>flexibleBorderColor<?php
                                                                                            }
                                                                                            ?>" style="display: block;">
                                                                                                <div class="row">
                                                                                                    <div class="col sch-lunch-inner-title text-center py-1">
                                                                                                        <?php
                                                                                                            if ($times[$i]->lunchType == 0) {
                                                                                                                ?>
                                                                                                                    Fixed
                                                                                                                <?php
                                                                                                            } elseif ($times[$i]->lunchType == 1) {
                                                                                                                ?>
                                                                                                                    Range
                                                                                                                <?php
                                                                                                            } elseif ($times[$i]->lunchType == 2) {
                                                                                                                ?>
                                                                                                                    Flexible
                                                                                                                <?php
                                                                                                            }
                                                                                                        ?>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col sch-li-dif">
                                                                                                        <p class="mb-1 sch-time-title">Time</p>
                                                                                                        <?php
                                                                                                            if ($times[$i]->lunchType == 2) {
                                                                                                                ?>
                                                                                                                    <input class="sch-timepicker text-center schTimeFrom" type="input" value="Anytime" disabled="disabled" placeholder="00:00">
                                                                                                                <?php
                                                                                                            } else {
                                                                                                                ?>
                                                                                                                    <input class="sch-timepicker text-center schTimeFrom" min="01:00" max="12:00" type="time" value="<?php echo $times[$i]->lunchStart ?>" placeholder="00:00">
                                                                                                                <?php
                                                                                                            }

                                                                                                            if ($times[$i]->lunchType == 1) {
                                                                                                               ?>
                                                                                                               <div class="time-schTimeTo">
                                                                                                                   <p class="m-0 sch-li-df">-</p>
                                                                                                                   <input class="sch-timepicker text-center schTimeTo" min="01:00" max="12:00" type="time" value="<?php echo $times[$i]->lunchEnd; ?>" placeholder="00:00">
                                                                                                               </div>
                                                                                                               <?php
                                                                                                            }
                                                                                                        ?>
                                                                                                    </div>
                                                                                                    <div class="col pb-2">
                                                                                                        <p class="mb-1 sch-duration-title">Duration</p>
                                                                                                        <input class="text-center filter-int schDuration" value="<?php echo $times[$i]->duration; ?>" type="text">
                                                                                                    </div>
                                                                                                    <div class="sch-lunch-inner-actions <?php
                                                                                                            if ($times[$i]->lunchType == 0) {
                                                                                                                ?>fixedColor<?php
                                                                                                            } elseif ($times[$i]->lunchType == 1) {
                                                                                                                ?>rangeColor<?php
                                                                                                            } elseif ($times[$i]->lunchType == 2) {
                                                                                                                ?>flexibleColor<?php
                                                                                                            }
                                                                                                    ?>">
                                                                                                        <div class="icon-trashcan"></div>
                                                                                                        <div class="icon-checkmark"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <span class="icon-small-lunch" data-toggle="tooltip" data-placement="top" title="Click to edit lunch time for this day"
                                                                                            date-set-val     = "<?php echo $times[$i]->lunchStart ? "true" : "false"; ?>"
                                                                                            data-schTimeFrom = "<?php echo $times[$i]->lunchStart ?>"
                                                                                            data-chDuration  = "<?php echo $times[$i]->duration; ?>"
                                                                                            data-schTimeTo   = "<?php echo $times[$i]->lunchEnd; ?>"
                                                                                            data-type        = "<?php echo $times[$i]->lunchType; ?>"
                                                                                            data-shiftId     = "<?php echo $times[$i]->id; ?>">
                                                                                        </span>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                        <span class="icon-small-lunch d-none" data-shiftId="<?php echo $times[$i]->id; ?>"></span>
                                                                                    <?php
                                                                                }
                                                                            ?>

                                                                            <span class="icon-r <?php echo is_null($times[$i]->workingDuration) ? 'd-none' : ''; ?>"></span>
                                                                            <span class="clockInClockOut">
                                                                                <?php
                                                                                if ($times[$i]->status == 2) {
                                                                                    ?>
                                                                                        Day Off
                                                                                    <?php
                                                                                } else {
                                                                                    if (is_null($times[$i]->workingDuration)) {
                                                                                        echo is_null($times[$i]->clockIn) || $times[$i]->status == 2 ? "--:-- - --:--" : date("H:i", strtotime($times[$i]->clockIn)) . ' - ' . date("H:i", strtotime($times[$i]->clockOut));
                                                                                    } else {
                                                                                        echo is_null($times[$i]->workingDuration) || $times[$i]->status == 2 ? "--:-- - --:--" : date("H:i", strtotime($times[$i]->clockIn)) . ' - ' . $times[$i]->dayRange . ' - ' . $times[$i]->workingDuration;
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </span>
                                                                        </div>
                                                                    <?php if ($i % 7 == 6) {  ?>
                                                                        <div class="col noselect mx-2 py-1"><i class="schedule-remove-icon schedule-week-remove pointer" data-shc-id="<?php echo $schedule->id; ?>" data-week-id="<?php echo $currentWeekID; ?>" data-shiftId="<?php echo json_encode($Ids); ?>"></i></div>
                                                                    <?php
                                                                            $Ids = array();
                                                                        }
                                                                        if ($i == count($times)-1) {
                                                                        ?>

                                                                    </div>
                                                        <?php } ?>
                                                            <?php } ?>

                                                    </div>
                                                    <span id="addWeekButt-<?php echo $schedule->id; ?>" data-schedule-id="<?php echo $schedule->id; ?>" data-current-dayCount="<?php echo $i; ?>" class="sch-new-week sch-new-week-icon float-left pointer"></span>
                                                    <span id="addWeekButtLoader-<?php echo $schedule->id; ?>" class="sch-new-week-loader">
                                                        <img src='public/images/gifs/loading-gif.gif' class="sch-new-week-icon-loading"> Adding week...
                                                    </span>
                                                    </div>
                                                    <div class="col-3 sch-drag-items">
                                                        <div class="row my-1 p-1">
                                                            <div class="col-2 icon-lunch"></div>
                                                            <div class="col text-left">Drag and drop to set lunch</div>
                                                        </div>
                                                        <div class="row my-1 p-1">
                                                            <div class="col-1 schedule-check-icon"></div>
                                                            <div class="col text-left">Drag and drop to set a workday</div>
                                                        </div>
                                                        <div class="row my-1 p-1">
                                                            <div class="col-2 icon-xsign"></div>
                                                            <div class="col text-left">Drag and drop to set a day off</div>
                                                        </div>
                                                        <div class="row my-1 p-1">
                                                            <div class="col-1">
                                                                <div class="icon-leftbracket"></div>
                                                            </div>
                                                            <div class="col-1">
                                                                <div class="icon-rightbracket"></div>
                                                            </div>
                                                            <div class="col text-left">
                                                                Loop Handles
                                                            </div>
                                                        </div>
                                                        <div class="row my-1 p-1">
                                                            <div class="col text-left">
                                                                Loop handles let you decide which days would you like to be repeated in a sequence.
                                                                You can include as many days as you wish in a loop and they will be repeated continuously.
                                                                If you include day off in a loop, they will be repeated as well. If you put a day off outside the loop,
                                                                the loop sequence will skip those days and continue from the next workday.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-100 in-this-schedule d-none">
                                                <div class="col-md-12">
                                                    <div class="col-md-6 select-groups">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6 select-search">
                                                                <span>Groups:</span>
                                                                <div class="form-group float-left">
                                                                    <input type="text" class="search-select-groups" placeholder="Search">
                                                                </div>
                                                                <div class="line"></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p class="select-all-groups">Select All</p>
                                                            </div>
                                                        </div>
                                                        <div class="d-none copy-item-group">
                                                            <div class="col-md-12 schedule-group-list-itam">
                                                                <div class="col-md-3 title"></div>
                                                                <div class="col-md-3 startDateTitle">Start date:</div>
                                                                <div class="col-md-3 startDate">08.02.2019</div>
                                                                <div class="col-md-3 select">
                                                                    <label class="container groups-list-container-checkbox">
                                                                        <input type="checkbox">
                                                                        <span class="checkmark" data-user-list="[]" data-scheduleid="<?php echo $schedule->id; ?>"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                            $blackList = [];
                                                            $groupList = [];
                                                            foreach ($this->data['usersList'] as $User) {
                                                                if ($User->scheduleId == $schedule->id) {
                                                                    $groupList[$User->groupId][] = $User->id;
                                                                }
                                                            }

                                                            foreach ($this->data['usersList'] as $User) {
                                                                if ($User->scheduleId == $schedule->id) {
                                                                    if (!in_array($User->groupId, $blackList) && !is_null($User->title)) {
                                                                            $blackList[] = $User->groupId;
                                                                        ?>
                                                                            <div class="col-md-12 schedule-group-list-itam group-item-<?php echo $User->groupId; ?>">
                                                                                <div class="col-md-3 title"><?php echo $User->title; ?></div>
                                                                                <div class="col-md-3 startDateTitle">Start date:</div>
                                                                                <div class="col-md-3 startDate"><?php echo $schedule->StartDate; ?></div>
                                                                                <div class="col-md-3 select">
                                                                                    <label class="container groups-list-container-checkbox">
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark" data-scheduleid="<?php echo $User->scheduleId; ?>" data-user-list="<?php echo json_encode($groupList[$User->groupId]); ?>"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                        <span id="groups-list-<?php echo $schedule->id; ?>"></span>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <div class="schedule-add" data-toggle="modal" data-startDate="<?php echo $schedule->StartDate; ?>" data-target="#addToScheduleGroup" data-schedule-id="<?php echo $schedule->id; ?>">
                                                                    <i class="schedule-add-icon"></i>
                                                                    <span>Add new group</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="schedule-add-next" data-toggle="modal" data-startDate="<?php echo $schedule->StartDate; ?>" data-target="#addToScheduleGroup" data-schedule-id="<?php echo $schedule->id; ?>">
                                                                    <i class="schedule-add-icon"></i>
                                                                    <span>Add to next schedule</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 select-individuals">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <span class="float-left search-select-individuals-title">Individuals:</span>
                                                                <div class="form-group">
                                                                    <input type="text" class="float-left search-select-individuals" placeholder="Search">
                                                                </div>
                                                                <div class="line"></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p class="select-individuals-select-all select-all">Select All</p>
                                                            </div>
                                                            <div class="col-md-12 schedule-individuals-list">
                                                                <div class="col-md-12 schedule-individuals-list-item d-none">
                                                                    <div class="col-md-3 title"></div>
                                                                    <div class="col-md-3 startDateTitle">Start date:</div>
                                                                    <div class="col-md-3 startDate"><?php echo $schedule->StartDate; ?></div>
                                                                    <div class="col-md-3 select">
                                                                        <label class="container individuals-list-container-checkbox">
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    foreach ($this->data['usersList'] as $user) {
                                                                        if (is_null($user->title) && $user->scheduleId == $schedule->id && is_null($user->title)) {
                                                                            ?>
                                                                            <div class="col-md-12 schedule-individuals-list-item">
                                                                                <div class="col-md-3 title"><?php echo $user->name . ' ' . $user->last_name; ?></div>
                                                                                <div class="col-md-3 startDateTitle">Start date:</div>
                                                                                <div class="col-md-3 startDate"><?php echo $schedule->StartDate ?></div>
                                                                                <div class="col-md-3 select">
                                                                                    <label class="container individuals-list-container-checkbox" data-userId="<?php echo $user->id; ?>">
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark" data-scheduleId="<?php echo $schedule->id; ?>"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <div class="schedule-add schedule-add-individual" data-toggle="modal" data-target="#addToScheduleIndividuals" data-schedule-id="<?php echo $schedule->id; ?>">
                                                                    <i class="schedule-add-icon"></i>
                                                                    <span>Add new user</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="schedule-add schedule-add-to-new-individual" data-toggle="modal" data-target="#addToScheduleIndividuals" data-schedule-id="<?php echo $schedule->id; ?>">
                                                                    <i class="schedule-add-icon"></i>
                                                                    <span>Add to next schedule</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 sch-trig icon-arrow1"></div>
                                </div>
                			    <?php
                		    }
                	    }
                    ?>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<div class="sch-main-right col-2">
			<div class="row">
				<div class="col"></div>
			</div>
		</div>
	</div>
</div>