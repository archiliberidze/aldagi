<?php

if (!defined('__JAMP__')) exit("Direct access not permitted.");

use DateTime as _date;

//echo '<pre>';
//print_r($this->data);
//print_r($this->data['users']);
//echo '</pre>';
//die();
?>
<!--<div class="preloader">-->
<!--    <div class="preloader__row">-->
<!--        <div class="preloader__item"></div>-->
<!--        <div class="preloader__item"></div>-->
<!--    </div>-->
<!--</div>-->


<div class="wrap">
    <div class="report-full">
        <div class="rep-st container">
            <div class="search-report-div">
                <span class="word-search">Search:</span> <input type="text" id="liveSearch" name="search"
                                                                class="searchInput"/>

                <div class="menu-type-div">
                    <input type="text" name="daterange" value="" id="datepicker"/><img class="calendar-img"
                                                                                       src="<?php echo __JAMP__["images"]; ?>/reports/calendar-01.svg"/>
                    <div class="pay-period">
                        or
                        <select class="inline-block input-height" id="day_period">
                            <option value="">This pay period</option>
                            <option value="7">1 Last Week</option>
                            <option value="14">2 Last Week</option>
                            <option value="28">4 Last Week</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 menu">
                    <a class="group-name active" id="group-">All</a>
                    <?php
                    foreach ($this->data['groups'] as $group) {
                        ?>
                        <a class="group-name" id="group-<?php echo $group->id; ?>"><?= $group->title ?></a>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-10 usersGroup table-view">
                    <div id="user-list">
                        <table class="table m-t-20 table-borderless table-hover table-users-all">
                            <thead>
                            <tr>
                                <th>
                                    Name
                                    <div class="arrows">
                                        <span class="icon-arrow active-arrow"></span>
                                        <span class="icon-arrow arrow-down"></span>
                                    </div>
                                </th>
                                <th>
                                    Group
                                    <div class="arrows">
                                        <span class="icon-arrow"></span>
                                        <span class="icon-arrow arrow-down"></span>
                                    </div>
                                </th>
                                <th>
                                    Position
                                    <div class="arrows">
                                        <span class="icon-arrow"></span>
                                        <span class="icon-arrow arrow-down"></span>
                                    </div>
                                </th>
                                <th>
                                    Start Date
                                    <div class="arrows">
                                        <span class="icon-arrow"></span>
                                        <span class="icon-arrow arrow-down"></span>
                                    </div>
                                </th>
                                <th class="none-bold user-check-all center cursor-pointer">Select All</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($this->data['users']) && !empty($this->data['users'])) { ?>
                                <?php foreach ($this->data['users'] as $item) { ?>
                                    <tr>
                                        <td class="username-group" data-id="<?= $item->table_user_id; ?>"
                                            data-toggle="modal" data-target="#exampleModalCenter-3">
                                            <?php echo(isset($item->last_name) ? $item->last_name : ''); ?><?php echo(isset($item->user_name) ? $item->user_name : ''); ?>
                                        </td>
                                        <td><?php echo(isset($item->group_title) ? $item->group_title : ''); ?></td>
                                        <td><?php echo(isset($item->positionName) ? $item->positionName : ''); ?></td>
                                        <td><?php echo(isset($item->active_since) ? substr($item->active_since, 0, 10) : ''); ?></td>
                                        <td class="center">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input user_reports_ids"
                                                       name="user_ids[]"
                                                       value="<?= $item->user_id; ?>"
                                                       id="defaultInline1<?= $item->user_id; ?>">
                                                <label class="custom-control-label my-checkbox"
                                                       for="defaultInline1<?= $item->user_id; ?>"></label>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-10 box-view">
                    <div class="row usersGroup box-view-row">
                        <?php if (isset($this->data['data']) && !empty($this->data['data'])) { ?>
                            <?php foreach ($this->data['data'] as $item) { ?>
                                <div class="col-md-2 person-info">
                                    <img src="public/images/reports/man.jpg" class="person-image"/>
                                    <p class="username"><?php if (isset($item->username)) {
                                            echo $item->username;
                                        } ?></p>
                                    <span class="title-user"><?php if (isset($item->title)) {
                                            echo $item->title;
                                        } ?></span>
                                    <span class="from-user">From <?php if (isset($item->date)) {
                                            echo $item->date;
                                        } ?></span>
                                    <span class="position-user"><?php if (isset($item->position)) {
                                            echo $item->position;
                                        } ?></span>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="m-t-r-l-20 float-right menu-type-div">
                        Select All <span class="icon-thumbnails menu-type-active"></span> <span
                                class="icon-table icon-list"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="center">
            <div class="report-btn btn-report" id="report_id">REPORT</div>
        </div>
    </div>
    <div id="selection"></div>
</div>

<!-- Modal user info- -->
<div class="modal fade" id="exampleModalCenter-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="user-info-modal modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body" id="user-info">

            </div>
        </div>
    </div>
</div>

<script>
    $('.username-group').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "reports",
            // dataType: 'json',
            data: {
                username_user_id: $(this).attr('data-id'),
            },
            success: function (data) {
                if (!data) {
                    // vue.shakeAtWrong();
                } else {
                    document.getElementById('user-info').innerHTML = data;
                }
            }
        });
    });
</script>