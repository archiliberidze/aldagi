<?php if (!defined('__JAMP__')) exit("Direct access not permitted."); ?>
<div class="wrap">

    <!-- asking for if user is certain (bootstrap modal) -->
    <div class="modal fade" id="certain-modal" tabindex="-1" role="dialog" aria-labelledby="certainModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body certainModalBody">
                    <p>Are you sure you want to <span></span> ?</p>
                    <div class="changeTime d-none">
                        <div class="form-group mb-2 mx-sm-3">
                            <label for="editTime">Change Time</label>
                            <p class="editTime-errors"></p>
                            <input type="time" class="form-control" class="editTime" id="editTime" placeholder="">
                        </div>
                    </div>
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-yes">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end asking for if user is certain  (bootstrap modal) -->

    <div class="requests">
        <div class="req-st container-build">
            <div class="row">
                <div class="col-md-2 col-xs-12 menu">
                    <div class="menu-list">
                        <div class="in-sent">
                            <a href="requests/inbox" <?php echo isset($this->url[1]) && $this->url[1] == 'inbox' || !isset($this->url[1]) ? 'class="active"' : ''; ?>>Inbox</a>
                            <a href="requests/sent" <?php echo isset($this->url[1]) && $this->url[1] == 'sent' ? 'class="active"' : ''; ?>>Sent</a>
                        </div>
                        <?php
                        foreach ($this->data['menu'] as $link => $item) {
                            ?>
                            <a
                                <?php
                                echo isset($this->url[2]) && strtolower($item) == $this->url[2] ? 'class="active"' : "";
                                ?>
                                    href="<?php echo isset($this->url[1]) ? "{$this->url[0]}/{$this->url[1]}/{$link}" : "{$this->url[0]}/{$this->data['defaultPage']}/{$link}"; ?>">
                                <?php
                                echo $item;
                                if ($item == 'Urgent' && !empty($this->data['urgent'])) {
                                    ?>
                                    <i class="icon icon-danger-trangle"></i>
                                    <?php
                                }
                                ?>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-10 col-xs-12 table-view">
                    <div class="pagination">
                        <ul class="pagination-list">
                            <?php require $this->data['pagination']['template']; ?>
                        </ul>
                    </div>
                    <div class="messenger-list">
                        <?php foreach ($this->data['requests'] as $request) { ?>
                            <ul data-ids="[<?php echo $request->requestsDayId, ',', $request->requestId ?>]"
                                class="message-row  <?php

                                if (strpos($request->status, '1') !== false) {
                                    echo "message-agree";
                                } elseif (strpos($request->status, '2') !== false) {
                                    echo "message-rejected";
                                } elseif (strpos($request->status, '0') !== false) {
                                    echo "message-active";
                                }

                                if ($request->requestLunchStart) {
                                    $request->lunchStart = $request->startLunchId == 1 && isset($request->checkOut) ? $request->checkOut : NULL;
                                    $times = ['needsCorrection' => $request->lunchStart, 'correct' => $request->requestLunchStart, 'type' => 'Lunch Start'];
                                } elseif ($request->requestLunchEnd) {
                                    $request->lunchEnd = $request->endLunchId == 1 ? $request->checkIn : NULL;
                                    $times = ['needsCorrection' => $request->lunchEnd, 'correct' => $request->requestLunchEnd, 'type' => 'Lunch End'];
                                } elseif ($request->requestCheckIn) {
                                    $times = ['needsCorrection' => $request->checkIn, 'correct' => $request->requestCheckIn, 'type' => 'Check In'];
                                } elseif ($request->requestCheckOut) {
                                    $times = ['needsCorrection' => $request->clockOut, 'correct' => $request->requestCheckOut, 'type' => 'Check Out'];
                                }
                                ?>">
                                <li>
                                    <span>
                                        <?php
                                        if (in_array($_SESSION['userId'], explode(',', $request->toManagers))) {
                                            echo is_null($request->username) ? $request->name : $request->username;
                                        } else {
                                            echo is_null($request->username) ? 'You | '. $request->name : 'You | '. $request->username;
                                        }
                                        ?>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Change
                                        <span class="timeChangeType-<?php echo $request->requestId; ?>">
                                            <?php echo $times['type']; ?>
                                        </span>
                                        Time:
                                        <span>
                                            <?php echo is_null($request->oldTime) ? date('H:i', strtotime($times['needsCorrection'])) : $request->oldTime; ?>
                                        </span>
                                    </span>
                                    <span class="icon icon-arrow float-none inline-block"></span>
                                    <span class="changeTimeTo-<?php echo $request->requestId; ?>">
                                        <?php echo date('H:i', strtotime($times['correct'])); ?>
                                    </span>
                                </li>
                                <li><span><?php echo date('d/m', strtotime($request->dayDate)); ?></span></li>
                                <li>
                                    <span data-requestId="<?php echo $request->requestId ?>"
                                          data-update-to-time="<?php echo date('H:i', strtotime($times['correct'])); ?>"
                                          data-day-timeId="<?php echo $request->dayTimeId; ?>"
                                          data-old-time="<?php echo is_null($request->oldTime) ? date('H:i', strtotime($times['needsCorrection'])) : $request->oldTime; ?>"
                                          data-feild="<?php echo is_null($request->requestCheckIn) ? "clockOut" : "clockIn"; ?>"
                                          data-time="<?php echo is_null($request->oldTime) ? date('G:i', strtotime($times['needsCorrection'])) : $request->oldTime;
                                          echo ' to ' . $times['type'] . ' : ' . date('G:i', strtotime($times['correct'])); ?>"
                                          data-dayId="<?php echo $request->dayID ?>"
                                          data-status="<?php echo $request->status ?>"
                                          data-request-time="<?php echo $request->status ?>"
                                          data-from-user-id="<?php echo $request->toUser; ?>"
                                          data-to-user-id="<?php echo $request->fromUser; ?>"
                                          data-timeAndDate="<?php echo is_null($request->checkIn) ? date('Y-m-d', strtotime($request->clockOut)) : date('Y-m-d', strtotime($request->checkIn)); ?>"
                                          id="info-<?php echo $request->requestId; ?>">
                                        <?php if (in_array($this->session->userId, explode(',', $request->toManagers))) { ?>
                                            <i class="icon icon-success icon-disabled"></i>
                                            <i class="icon icon-cancel icon-disabled"></i>
                                            <i class="icon icon-edit icon-disabled"></i>
                                        <?php } ?>
                                        <i class="icon icon-message icon-disabled cursor-pointer"></i>
                                    </span>
                                </li>
                                <?php if (strpos($request->status, '3') !== false) { ?>
                                    <li><i class="icon icon-danger-trangle icon-disabled"></i></li>
                                <?php } ?>
                            </ul>
                            <ul data-ids="[<?php echo $request->requestsDayId, ',', $request->requestId ?>]"
                                data-update-date="<?php echo $request->sentTime; ?>" class="message-chat d-none">
                                <!-- <li class="message-user-list">
                                     <ul>
                                         <li>To: </li>
                                         <li>Macy Grey,</li>
                                         <li>Harold Burbank,</li>
                                         <li>Larry brockovich,</li>
                                         <li>Stanley Colowan,</li>
                                         <li>brandon Biggs,</li>
                                         <li>Lee Yuni,</li>
                                         <li>Carl Starwin.</li>
                                     </ul>
                                 </li>-->
                                <li class="message-chat-comments-<?php echo $request->requestId; ?> message-chat-comments"></li>
                                <li class="message-chat-messenger">
                                    <div class="message-chat-textarea"><textarea
                                                id="message-textarea-<?php echo $request->requestId; ?>"
                                                rows="3"></textarea></div>
                                    <div class="message-chat-send"
                                         data-id="[<?php echo $request->requestsDayId, ',', $request->requestId ?>]"
                                         data-from-user-id="<?php echo $_SESSION['userId']; ?>"
                                         data-to-user-id="<?php echo $request->fromUser == $_SESSION['userId'] ? $request->toUser : $request->fromUser; ?>"
                                    ><i class="icon-send"></i></div>
                                </li>
                            </ul>
                        <?php } ?>
                        <?php if (empty($this->data['requests'])) { ?>
                            <p class="empty-page">There are no requests.</p>
                        <?php } ?>
                    </div>
                    <div class="pagination mt-0">
                        <ul class="pagination-list">
                            <?php require $this->data['pagination']['template']; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>