<?php
use DateTime as _date;

//echo '<pre>';
//print_r($class);
//echo '</pre>';
//?>
<!--<div class="row header">-->
<!--    <div class="d-none d-sm-none  d-lg-block col-lg-3 text-left">-->
<!--        <a href="#"><img src="public\images\logos/elva_logo.png" class="header_logo" alt="Elva_logo"></a>-->
<!--    </div>-->
<!--</div>-->



<div id="app">
    <table class="user_info" cellspacing="0" cellpadding="0" border="0" summary="User's Info Table">
        <tr>
            <td>
                <div id="user_info_ani" class="hide">
                    <div id="user_info_txt_ani">
                        <span id="users_name" class="users_name">Name Surname</span><br>
                        <span id="users_position" class="users_position">Position</span>
                    </div>
                </div>
            </td>
            <td id="user_info_pos" class="user_info_pos"></td>
        </tr>
        <tr>
            <td></td>
            <td align="center">
                <div id="sign_out_txt" @click="resetAnimations()" class="animated sign_out_txt pointer hide" style="margin-top: 70px;">Sign Out</div>
            </td>
        </tr>
    </table>
    <img src="public/images/users/face_1.jpg" id="user_pic" class="user_pic position_centered hide" alt="Employee's picture">

    <!-- SVG Double circle -->
    <svg id="ani_circle_SVG" class="ani_circle_SVG position_centered hide">
        <defs>
            <filter id="f2" x="-0.5" y="-0.5" width="125" height="125">
                <feOffset result="offOut" in="SourceGraphic" dx="0" dy="0" />
                <feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" />
                <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
            </filter>
        </defs>
        <circle id="ani_circle" class="ani_circle hide" cx="50%" cy="50%" r="40%" fill="none" filter="url(#f2)" /> <!-- the "r" attribute is for Mozilla. For Opera and Chrome "r" is controlled via javascript -->
    </svg>

    <svg id="static_circle_SVG" class="static_circle_SVG position_centered hide">
        <defs>
            <filter id="f3" x="-0.5" y="-0.5" width="125" height="125">
                <feOffset result="offOut" in="SourceGraphic" dx="0" dy="0" />
                <feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" />
                <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
            </filter>
        </defs>
        <circle id="static_circle" class="static_circle" cx="50%" cy="50%" r="40%" fill="none" filter="url(#f3)" /> <!-- the "r" attribute is for Mozilla. For Opera and Chrome "r" is controlled via javascript -->
    </svg>
    <!-- ----------------- -->

    <!-- Action buttons -->
    <div id="action_butt_div" class="action_butt position_centered pointer" @mousedown="pressEffect('action_butt_div')" @mouseup="depressEffect('action_butt_div')" @mouseover="setHoverBg('action_butt_div')" @mouseout="setHoutBg('action_butt_div')">
        <span id="action_butt_txt" class="animated position_centered action_butt_txt pointer hide">Clock&nbsp;In</span>
        <input type="hidden" id="clockAction" value="1">
        <input type="hidden" id="clockDayId" value="">
        <input type="hidden" id="shiftId" value="">
    </div>
    <div id="action_butt_div_2" class="action_butt position_centered pointer" @mousedown="pressEffect('action_butt_div_2')" @mouseup="depressEffect('action_butt_div_2')" @mouseover="setHoverBg('action_butt_div_2')" @mouseout="setHoutBg('action_butt_div_2')">
        <span id="action_butt_txt_2" class="animated position_centered action_butt_txt pointer hide">Lunch&nbsp;Start</span>
        <input type="hidden" id="lunchAction" value="1">
    </div>
    <!-- -------------- -->

    <!-- Modal message -->
    <div id="modal_div" class="modal_div hide">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>&nbsp;</td>
                <td width="15" class="white_bg">&nbsp;</td>
                <td width="3"></td>
                <td align="center" valign="middle" class="modal_content">
                    <i id="icon_succ" class="fas fa-check" style="color: #006999; font-size: 30px;"></i> <span id="modal_txt" class="modal_txt">Message!</span>
                </td>
                <td width="3"></td>
                <td width="15" class="white_bg">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <!-- ------------- -->

    <!-- Begin: Numpad -->
    <div class="numpad_container">
        <div id="numpad_wrapper">
            <div id="pin_title_div" class="pin_title">ENTER PIN</div>
            <div class="pin_inp_div">
                <input id="input_1" class="animated pin_inp_fld" type="text" placeholder="" :value="txt_to_show" disabled>
                <input id="pin_filed_to_send" type="hidden" :value="pincode">
            </div>

            <div class="num_butt_div"><img id="num_key_1" src="public/images/quick/numpad/1.svg" class="numpad_btn" @click="writeContent('1')" @mousedown="btn_pressed('1')" @mouseup="btn_un_pressed('1')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_2" src="public/images/quick/numpad/2.svg" class="numpad_btn" @click="writeContent('2')" @mousedown="btn_pressed('2')" @mouseup="btn_un_pressed('2')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_3" src="public/images/quick/numpad/3.svg" class="numpad_btn" @click="writeContent('3')" @mousedown="btn_pressed('3')" @mouseup="btn_un_pressed('3')" alt=""></div>

            <div class="spacer_2"></div>

            <div class="num_butt_div"><img id="num_key_4" src="public/images/quick/numpad/4.svg" class="numpad_btn" @click="writeContent('4')" @mousedown="btn_pressed('4')" @mouseup="btn_un_pressed('4')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_5" src="public/images/quick/numpad/5.svg" class="numpad_btn" @click="writeContent('5')" @mousedown="btn_pressed('5')" @mouseup="btn_un_pressed('5')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_6" src="public/images/quick/numpad/6.svg" class="numpad_btn" @click="writeContent('6')" @mousedown="btn_pressed('6')" @mouseup="btn_un_pressed('6')" alt=""></div>

            <div class="spacer_2"></div>

            <div class="num_butt_div"><img id="num_key_7" src="public/images/quick/numpad/7.svg" class="numpad_btn" @click="writeContent('7')" @mousedown="btn_pressed('7')" @mouseup="btn_un_pressed('7')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_8" src="public/images/quick/numpad/8.svg" class="numpad_btn" @click="writeContent('8')" @mousedown="btn_pressed('8')" @mouseup="btn_un_pressed('8')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_9" src="public/images/quick/numpad/9.svg" class="numpad_btn" @click="writeContent('9')" @mousedown="btn_pressed('9')" @mouseup="btn_un_pressed('9')" alt=""></div>

            <div class="num_butt_ok_div"><img id="num_key_ok" src="public/images/quick/numpad/okay.svg" class="numpad_btn_ok" @mousedown="btn_pressed('ok')" @mouseup="btn_un_pressed('ok')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_0" src="public/images/quick/numpad/zero-0.svg" class="numpad_btn" @click="writeContent('0')" @mousedown="btn_pressed('0')" @mouseup="btn_un_pressed('0')" alt=""></div>
            <div class="spacer_1"></div>
            <div class="num_butt_div"><img id="num_key_arr" src="public/images/quick/numpad/arrow.svg" class="numpad_btn" @click="correctField()" @mousedown="btn_pressed('arr')" @mouseup="btn_un_pressed('arr')" alt=""></div>
        </div>
    </div>
    <!-- End: Numpad -->
</div>

<!--<footer>-->
<!--    <div class="width-50">-->
<!--        <img src="--><?php //echo __JAMP__["images"]; ?><!--/login/jamps_logo_1.svg" class="jamps_logo_1" alt="Jamps Logo" />-->
<!--    </div>-->
<!--    <div class="width-50 f-right">-->
<!--        <img src="--><?php //echo __JAMP__["images"]; ?><!--/login/jamps_logo_1.svg" class="jamps_logo_1" alt="Jamps Logo" />-->
<!--    </div>-->
<!--</footer>-->