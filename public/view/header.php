<div class="container-fluid m-b-100">
    <div class="modal fade" id="confirmationBox" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center p-4">
                    Are you sure you want to something?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn cb-cancel mx-auto" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn cb-accept mx-auto">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row header">
        <div class="d-none d-sm-none d-md-none d-lg-block col-lg-3 text-left">
            <a href="#"><img src="public\images\logos/elva_logo.png" class="header_logo" alt="Elva_logo"></a>
        </div>
        <div class="d-none d-sm-none d-md-block col-md-2 d-lg-none text-left">
            <a href="#"><img src="public\images\logos/elva_logo.png" class="header_logo_small"
                             alt="Elva_logo_small"></a>
        </div>
        <div class="col" style="padding-top: 9px;">
            <nav class="navbar navbar-expand-md navbar-light" style="max-width: 850px;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#HeaderMenu"
                        aria-controls="HeaderMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="HeaderMenu">
                    <div class="navbar-nav center">
                        <a class="nav-item nav-link <?PHP echo $this->page == $this->translate("menu1") ? 'active' : '' ?> "
                           href="home"><?= $this->translate("menu1"); ?></a>
                    </div>
                    <div class="navbar-nav center">
                        <a class="nav-item nav-link <?PHP echo $this->page == $this->translate("menu2") ? 'active' : '' ?> "
                           href="reports"><?= $this->translate("menu2"); ?></a>
                    </div>
                    <div class="navbar-nav center">
                        <a class="nav-item nav-link <?PHP echo $this->page == $this->translate("menu3") ? 'active' : '' ?> "
                           href="requests"><?= $this->translate("menu3"); ?>
                            <?php if (isset($this->data['isNewRequest']) && $this->data['isNewRequest'] !== false) { ?>
                                <span class="hea-notification"></span>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="navbar-nav center">
                        <a class="nav-item nav-link <?PHP echo $this->page == $this->translate("menu4") ? 'active' : '' ?> "
                           href="schedule"><?= $this->translate("menu4"); ?></a>
                    </div>
                    <div class="navbar-nav center">
                        <a class="nav-item nav-link <?PHP echo $this->page == $this->translate("menu5") ? 'active' : '' ?> "
                           href="settings"><?= $this->translate("menu5"); ?></a>
                    </div>
                </div>
            </nav>
        </div>

        <div class="d-none d-sm-none d-md-none d-lg-block col-lg-3 text-right user_info_cell">
            <span class="user-logout d-flex"><?php echo $this->data['login_user_info'][0]->name ?> <i
                        class="icon_log_out_icon d-block"></i></span>
            <div class="logout-box d-none">
                <i class="arrow-up"></i>
                <p><?php if($this->data['login_user_info'][0]->is_company == 1){ echo 'Super Admin';}elseif($this->data['login_user_info'][0]->position == 'Employee'){echo '';}else{echo $this->data['login_user_info'][0]->position.'';} ?></p>
                <a href="login/logout">log out</a>
            </div>
        </div>
        <div class="d-none d-sm-block col-sm-2 d-md-block col-md-2 d-lg-none text-right user_info_cell">

          <span class="user-logout d-flex"> <i
                      class="icon_log_out_icon d-block"></i></span>
            <div class="logout-box logout-box-small d-none">
                <i class="arrow-up"></i>
                <p><?php if($this->data['login_user_info'][0]->is_company == 1){ echo 'Super Admin';}elseif($this->data['login_user_info'][0]->position == 'Employee'){echo '';}else{echo $this->data['login_user_info'][0]->position.'';} ?></p>
                <a href="login/logout">log out</a>
            </div>

        </div>
    </div>