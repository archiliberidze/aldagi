<?php

if (!defined('__JAMP__')) exit("Direct access not permitted.");

if(isset($this->data['success-code']) && $this->data['success-code'] != '') {
    ?>
    <div id="app">
        <div class="register-message ">
            <h3 class="register-successfully">You have successfully activate<br/> your E-mail</h3>
            <span class="register-login">
        Auto Redirect:
        <span class="register-count"> 5 </span>
    </span>
        </div>
    </div>
    <?php
}else{
?>
    <div id="app">
        <div class="register-message ">
            <h3 class="register-error">Invalid activation Link</h3>
            <span class="register-login">
        Auto Redirect:
        <span class="register-count"> 5 </span>
    </span>
        </div>
    </div>
<?php
}
?>