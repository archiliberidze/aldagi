    <footer class="row fixed-bottom">
        <div class="col-2">
            <span class="foo-time">
                <?=date("d.m.Y")?><br>
                <?=date("H:m A")?>
            </span>
        </div>
        <div class="col-8">
        </div>
        <div class="col-2 foo-border">
            @file{ src=logos/jamps_logo_1.svg, class=align-middle foo-logo }
        </div>

    </footer>



<!--     <div class="footer_div" style="z-index: 99;">

        <table border="0" class="standart_table">
            <tr>
                <td width="240" height="96" align="center" valign="middle">
                    <span class="footer_time">19.06.2019<br>
                    06:46 AM</span>
                </td>
                <td>&nbsp;</td>
                <td width="2"valign="middle">
                    <div style="background-color: #CCC; height: 68px;">
                        @file{ src=logos/pixel.gif }
                    </div>
                </td>
                <td width="240" align="center" valign="middle">

                </td>
            </tr>
        </table>

    </div> -->

</div>