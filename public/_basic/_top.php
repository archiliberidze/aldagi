<?php if(!defined('__JAMP__')) exit( "Direct access not permitted." );?>
<!DOCTYPE html>
<html>
<head>
    <base href="<?=$class->root;?>" target="_self" />
    <link rel="shortcut icon" href="<?=__JAMP__['images'];?>favicon.png">
    <title>JAMP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Copyright" content="Developed by JUMP team | <?=date("Y");?>" />
<?php
	PHP_EOL;
	foreach ($class->plan['css'] as $key => $value) {
		if( strpos($value, "https://") !== false || strpos($value, "http://") !== false){
			echo '	<link rel="stylesheet" type="text/css" href="'. $value .'" />'. PHP_EOL;
		}
		else{
			echo '	<link rel="stylesheet" type="text/css" href="'. __JAMP__['css'] . $value .'" />'. PHP_EOL;
		}
	}
	PHP_EOL;
?>
</head>
<body>
<? PHP_EOL; ?>