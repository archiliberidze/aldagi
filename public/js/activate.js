(function(d){
    var display = d.querySelector('#app .register-count')
    var timeLeft = parseInt(display.innerHTML)

    var timer = setInterval(function(){
        if (--timeLeft >= 0) {
            display.innerHTML = timeLeft
        } else {
            window.location.href = "";
        }
    }, 1000)
})(document)