var requests = {
    "tags" : {
        "iconMessage"            : ".icon-message",
        "iconEdit"               : ".icon-edit",
        "iconCancel"             : ".icon-cancel",
        "iconSuccess"            : ".icon-success",
        "message"                : ".message-row",
        "chatId"                 : "data-ids",
        "dataRequestId"          : "data-requestId",
        "actionIcons"            : "li:nth-child(4) span i, li:nth-child(5) i",
        "modal"                  : "#certain-modal",
        "modalBody"              : ".certainModalBody",
        "modalTextField"         : " > p > span",
        "modalYesButton"         : ".certainModalBody .btn.btn-primary",
        "messageChat"            : ".message-chat",
        "messageSend"            : ".message-chat-send",
        "messageTextarea"        : ".message-chat-textarea > #message-textarea",
        "messageChatComments"    : ".message-chat-comments-",
        "messageChatCommentsAll" : ".message-chat-comments",
        "btnYes"                 : ".btn-yes",
        "statusId"               : "data-status",
        "time"                   : "data-time",
        "dayId"                  : "data-dayid",
        "changeTime"             : ".changeTime",
        "changeTimeTo"           : ".changeTimeTo-",
        "editTime"               : "#editTime",
        "timeChangeType"         : ".timeChangeType-",
        "lastUpdateTime"         : "lastUpdate",
        "scrollOfset"            : "scrollOfset",
        "messageAgree"           : ".message-agree",
        "messageRejected"        : ".message-rejected",
        "messageActive"          : ".message-active",
        "editTimeErrors"         : ".editTime-errors",
        "btnClose"               : ".btn-close",
        "dataUpdateDate"         : "data-update-date",
        "dataDayTimeId"          : "data-day-timeid",
        "dataFromUserId"         : "data-from-user-id",
        "dataToUserId"           : "data-to-user-id",
        "dataField"              : "data-feild",
        "dataTimeAndDate"        : "data-timeAndDate",
        "dataOldTime"            : "data-old-time",
        "currentInfo"            : "#info-",
        "dataUpdateToTime"       : "data-update-to-time"
    },
    "urls" : {
        "getChat"        : "requests",
        "setStatus"      : "requests",
        "editTime"       : "requests",
        "updateMessages" : "requests"
    },
    "types" : {
        "messages"       : "messages",
        "send"           : "save",
        "approved"       : "approved",
        "denied"         : "denied",
        "edit"           : "edit",
        "updateMessages" : "update"

    },
    "config" : {
        "messageUpdateTime" : 6000
    },
    "post" : function (url, dataPass, callBack) {
        $.ajax({
            "type"  : "POST",
            "url"   : url,
            data    : dataPass,
            success : callBack
        });
    },
    "errors" : {
        "errorMessages" : {
            "edit" : "Time Was not changed"
        },
        "addInline": function (type, _this) {
            $(_this).text(requests.errors.errorMessages[type]);

            $(requests.tags.btnClose).click(function () {
                requests.errors.removeInline(_this);
            });
        },
        "removeInline" : function (_this) {
            $(_this).text('');
        },
        "checkStatus"  : function (_this) {
            let current = $(_this).parent();
            let status = current.attr('data-status');

            if (status.includes("1")) {
                current.find(requests.tags.iconSuccess).toggleClass("icon-disabled cursor-pointer");
            } else if (status.includes("2")) {
                current.find(requests.tags.iconCancel).toggleClass("icon-disabled cursor-pointer");
            }
        }
    },
    "request" : {
        "id"           : null,
        "statusIds"    : null,
        "dayId"        : null,
        "requestId"    : null,
        "timeLog"      : null,
        "dayTimeId"    : null,
        "field"        : null,

        "changeStatus" : function (type, postObject) {
            if (postObject == undefined || postObject == null || postObject == false ) {
                let oldTime = $(requests.tags.currentInfo + requests.request.id).attr(requests.tags.dataOldTime);
                var postObject = {
                    "id"         : requests.request.id,
                    'type'       : type,
                    'status'     : requests.request.statusIds,
                    "oldTime"    : oldTime,
                    "dayId"      : requests.request.dayId,
                    "dayTimeId"  : requests.request.dayTimeId,
                    "timeLog"    : requests.request.timeLog,
                    "fromUserId" : requests.request.fromUserId,
                    "toUserId"   : requests.request.toUserId
                };
            };

            requests.post(requests.urls.setStatus, postObject,
                function (data){
                    $(requests.tags.btnYes).off();
                    location.reload();
                });
        },
        "setRequestId" : function (_this) {
            let parent = $(_this).parent();

            requests.request.id          = parent.attr(requests.tags.dataRequestId);
            requests.request.statusIds   = parent.attr(requests.tags.statusId);
            requests.request.dayId       = parent.attr(requests.tags.dayId);
            requests.request.timeLog     = parent.attr(requests.tags.time);

            requests.request.dayTimeId   = parent.attr(requests.tags.dataDayTimeId);
            requests.request.timeLog     = parent.attr(requests.tags.time);
            requests.request.field       = parent.attr(requests.tags.dataField);
            requests.request.fromUserId  = parent.attr(requests.tags.dataFromUserId);
            requests.request.toUserId    = parent.attr(requests.tags.dataToUserId);
            requests.request.timeAndDate = parent.attr(requests.tags.dataTimeAndDate);

            return parent;
        },
        "edit" : function () {
            $(requests.tags.iconEdit).click(function () {
                requests.request.editTime(this, true);
            });
        },
        "editTime" : function (_this, editInput) {
            if ($(_this).hasClass('icon-disabled')) return null;
            requests.request.setRequestId(_this);

            if (editInput != undefined) {

                $(requests.tags.modalBody + ' > p ').addClass('d-none');
                $(requests.tags.changeTime).removeClass('d-none');
                $(requests.tags.modal).modal('toggle');

                $(requests.tags.btnYes).text('Save');

            }

            let editTimeInput = $(requests.tags.editTime);
            let oldTime = $(requests.tags.currentInfo + requests.request.id).attr(requests.tags.dataOldTime);

            let updateToTime = $(requests.tags.currentInfo + requests.request.id).attr(requests.tags.dataUpdateToTime);

            editTimeInput.val(updateToTime.trim());

            let row = $(requests.tags.timeChangeType + requests.request.id).html();

            requests.request.save(function () {
                let newTime = editTimeInput.val();

                if (newTime.trim() == '') {
                    return;
                }

                if (newTime.toString().trim() == oldTime.toString().trim() && editInput) {
                    requests.errors.addInline('edit', requests.tags.editTimeErrors);
                    return;
                }

                if (editInput == undefined) {
                    newTime = newTime.toString().trim();
                }

                requests.request.changeStatus(requests.request.types, {
                    "newTime"     : newTime,
                    "oldTime"     : oldTime,
                    "type"        : requests.types.edit,
                    "dayTimeId"   : requests.request.dayTimeId,
                    "id"          : requests.request.id,
                    "dayId"       : requests.request.dayId,
                    "row"         : row,
                    "field"       : requests.request.field,
                    "fromUserId"  : requests.request.fromUserId,
                    "toUserId"    : requests.request.toUserId,
                    "timeAndDate" : requests.request.timeAndDate
                });
                requests.errors.removeInline(requests.tags.editTimeErrors);
                $(requests.tags.modal).modal('toggle');
            });

        },
        "selectNewStatus" : function (clickOn, Text, Status) {
            $(clickOn).click(function () {
                if ($(this).hasClass('icon-disabled')) return null;

                $(requests.tags.modalBody + requests.tags.modalTextField).text(Text);
                $(requests.tags.modalBody + ' > p ').removeClass('d-none');
                $(requests.tags.changeTime).addClass('d-none');
                $(requests.tags.modal).modal('toggle');
                $(requests.tags.btnYes).text('Yes');

                requests.request.setRequestId(this);

                requests.request.save(function () {
                    requests.request.changeStatus(requests.types[Status]);
                    if (Status == 'approved') {
                        requests.request.editTime(this);
                    }
                    $(requests.tags.modal).modal('toggle');
                });

            });
        },
        "approve" : function () {
            requests.request.selectNewStatus(requests.tags.iconSuccess, 'Approve', 'approved');
        },
        "denied" : function () {
            requests.request.selectNewStatus(requests.tags.iconCancel, 'Cancel', 'denied')
        },
        "save" : function (callBack) {
            $(requests.tags.btnYes).click(callBack);
        },
        "autoLoad" : function () {
            this.edit();
            this.approve();
            this.denied();
        }
    },
    "message" : {
        "id" : null,
        "updateInterval" : null,
        "lastUpdateDataAndTime" : null,
        "messageTextareaTest" : function (text) {
            if (text.trim() !== "") {
                return true;
            }

            return false;
        },
        "isUpdate" : function (callBack) {
            this.updateInterval = setInterval(callBack, requests.config.messageUpdateTime);
        },
        "stopUpdates" : function () {
            clearInterval(this.updateInterval);
        },
        "setChat" : function (html, id) {
            $(requests.tags.messageChatComments + JSON.parse(id)[1]).html(html);
        },
        "activateChat" : function ()  {
            $(requests.tags.messageChat + "[" + requests.tags.chatId + "='" + requests.message.id + "']").toggleClass('d-none');
        },
        "onEnter" : function () {
            $(requests.tags.messageChat).on('keypress',function(e) {
                if(e.which == 13) {
                    $(this).find(requests.tags.messageSend).click();
                }
            });
        },
        "openChat" : function () {
            $(requests.tags.iconMessage).click(function () {
                requests.message.id = $(this).parent().parent().parent().attr(requests.tags.chatId);
                let _this = $(this).parent().parent().parent();
                let icons = _this.find(requests.tags.actionIcons);

                requests.errors.checkStatus(this);

                icons.toggleClass("icon-disabled cursor-pointer");
                $(this).addClass('cursor-pointer');
                _this.toggleClass("m-0");

                if (!_this.hasClass('m-0')) {
                    requests.message.stopUpdates();
                } else {
                    requests.message.isUpdate(function () {
                        requests.message.lastUpdateDataAndTime = window[requests.tags.lastUpdateTime + JSON.parse(requests.message.id)[1]];
                        if (requests.message.lastUpdateDataAndTime == undefined) {
                            requests.message.lastUpdateDataAndTime = $(requests.tags.messageChat).attr(requests.tags.dataUpdateDate);
                        }

                        requests.post(requests.urls.updateMessages, {
                            "ids"                   : requests.message.id,
                            "lastUpdateDataAndTime" : requests.message.lastUpdateDataAndTime,
                            "type"                  : requests.types.updateMessages
                        },function (data){
                            try {
                                var info = JSON.parse(data);
                                return;
                            } catch (e) {
                                requests.message.setChat(data, requests.message.id);

                                $(requests.tags.messageChatCommentsAll).animate({
                                    scrollTop: window[requests.tags.scrollOfset + JSON.parse(requests.message.id)[1]]+'0'
                                }, 2000);
                            }
                        });
                    });
                }

                if (!_this.hasClass('icon-disabled')) {
                    requests.message.activateChat();
                    requests.post(requests.urls.getChat, {"ids" : requests.message.id, "type" : requests.types.messages}, function (data){
                        requests.message.setChat(data, requests.message.id);
                        $(requests.tags.messageChatCommentsAll).animate({
                            scrollTop: window[requests.tags.scrollOfset + JSON.parse(requests.message.id)[1]]+'0'
                        }, 2000);
                    });
                }
            });
        },
        "send" : function () {
            $(requests.tags.messageSend).click(function () {

                var ids = $(this).parent().parent().attr(requests.tags.chatId);
                var jsonIds = JSON.parse(ids);

                var messageTextarea = $(requests.tags.messageTextarea + '-' + jsonIds[1]);
                var fromUserId = $(this).attr(requests.tags.dataFromUserId);
                var toUserId = $(this).attr(requests.tags.dataToUserId);

                // error on sending
                if (requests.message.messageTextareaTest(messageTextarea.val())) {
                    requests.post(requests.urls.getChat, {
                        "ids"     : ids,
                        "fromUserId" : fromUserId,
                        "toUserId" : toUserId,
                        "type"    : requests.types.send,
                        "message" : messageTextarea.val()
                    }, function (data){
                        messageTextarea.val('');

                        requests.message.setChat(data, ids);
                        $(requests.tags.messageChatCommentsAll).animate({
                            scrollTop: window[requests.tags.scrollOfset + JSON.parse(requests.message.id)[1]]+'0'
                        }, 2000);

                    });
                }
            });
        },
        "autoLoad" : function () {
            this.openChat();
            this.send();
            this.onEnter();
        }
    },
    "autoLoad" : function () {
        this.message.autoLoad();
        this.request.autoLoad();
    }
};

$(document).ready(function () {
    requests.autoLoad();
});