var schedule = {
    "tags": {
        "addNewSchedule": "#addNewSchedule",
        "scheduleItemList": ".sch-items",
        "addScheduleName": ".addScheduleName",
        "generalModal": "#modal",
        "modalSaveButton": "#modelSave",
        "modelCancel": "#modelCancel",
        "newScheduleName": ".newScheduleName",
        "addScheduleNameTitle": ".addScheduleName > h2",
        "errorField": ".errorField",
        "addWeekButton": ".sch-new-week",
        "addWeekButID": "#addWeekButt",
        "addWeekButLoaderID": "#addWeekButtLoader",
        "scheduleId": "data-scheduleid",
        "scheduleRemoveIcon": ".schedule-week-remove",
        "weekId": "data-weekid",
        "dataDelWeekSchID": "data-shc-id",
        "weekDay": "data-weekday",
        "weekDayRangeSched": "#weekDay",
        "addTimeToDayModal": ".add-time-to-day-modal",
        "addTimeToDayOne": "#add-time-to-day-one",
        "addTimeToDayTwo": "#add-time-to-day-two",
        "daySchedule": "#day-schedule",
        "workRangeFrom": ".work-range-from",
        "workRangeTill": ".work-range-till",
        "dayStart": "#dayStart",
        "dayEnd": "#dayEnd",
        "switchSliderButtonClick": ".switch-slider-button-click",
        "switch": ".switch",
        "clickInTime": "clickInTime",
        "rangeTime": "rangeTime",
        "workingDurationClass": "workingDuration",
        "changeToGroups": ".change-to-groups",
        "schedule": "schedule",
        "inThisSchedule": ".in-this-schedule",
        "pageType": "data-type",
        "active": "active",
        "helpButtonOne": "#helpButtonOne",
        "helpModal": "#helpModal",
        "modalContent": ".modal-content",
        "modalDialog": ".modal-dialog",
        "modalBody": ".modal-body",
        "uiResizableHandle": ".ui-resizable-handle",
        "helpModalHeader": ".help-modal-header",
        "switchButton": ".switch-button",
        "helpButtonTow": ".helpButtonTow",
        "helpOne": "#helpOne",
        "helpTow": "#helpTow",
        "newScheduleStartDate": "#newScheduleStartDate",
        "schTrig": ".sch-trig",
        "scheduleOpen": "schedule-open",
        "schInnerItem": "#sch-inner-item-",
        "iconArrow1": "icon-arrow1",
        "iconArrow2": "icon-arrow2",
        "schTrigActive": "sch-trig-active",
        "schItem": ".sch-item",
        "removeSchedule": ".remove-schedule",
        "removeQuestion": ".removeQuestion",
        "positionRelative": ".position-relative",
        "iconLunch": ".icon-lunch",
        "iconXSign": ".icon-xsign",
        "iconLeftBracket": ".icon-leftbracket",
        "iconRightBracket": ".icon-rightbracket",
        "changeable": ".changeable",
        "dataShiftId": "data-shiftId",
        "clockInTimeRange": "#clockInTimeRange",
        "clockOutTimeRange": "#clockOutTimeRange",
        "clickInTimeFrom": "#clickInTimeFrom",
        "rangeTimeTo": "#rangeTimeTo",
        "workingDuration": "#workingDuration",
        "shift": "#shift-",
        "dataClockIn": "data-clockin",
        "dataClockOut": "data-clockout",
        "dataclockOutWeekDay": "data-weekday-end",
        "dataWorkingduration": "data-workingduration",
        "dataWeekdayStart": "data-weekday-start",
        "clockInWeekDay": "#clockInWeekDayTimeRange",
        "clockOutWeekDay": "#clockOutWeekDayTimeRange",
        "dataRange": "data-range",
        "scheduleWeekList": ".schedule-week-list",
        "schWeekID": "data-week-id",
        "removeSendSchedule": ".remove-send-schedule",
        "dataScheduleId": "data-schedule-id",
        "dayCount": "data-current-daycount",
        "errorMessages": ".error-messages",
        "dataGeneraModalType": "data-genera-modal-type",
        "addTimeToDay": ".add-time-to-day",
        "lunchCont": "#lunchCont",
        "scheduleCheckIcon": ".schedule-check-icon",
        "iconSmallLunch": ".icon-small-lunch",
        "schLunch": ".sch-lunch",
        "schLunchType": ".sch-lunch-type",
        "lunchType": ".lunch-type",
        "dataCls": "data-cls",
        "schLunchInner": ".sch-lunch-inner",
        "lunchSaveButton": ".icon-checkmark",
        "lunchRemoveButton": ".icon-trashcan",
        "groupsSearch": "#groupsSearch",
        "tableGroupsLiveSearch": ".table-groups-live-search",
        "tableIndividualsLiveSearch": ".table-individuals-live-search",
        "searchSelectGroups": ".search-select-groups",
        "searchSelectIndividuals": ".search-select-individuals",
        "scheduleGroupListItam": ".schedule-group-list-itam",
        "selectAllGroups": ".select-all-groups",
        "groupsListContainerCheckbox": ".groups-list-container-checkbox",
        "selectAllGroupsModalCheckbox": ".select-all-groups-modal-checkbox",
        "selectAllGroupsModal": "#select-all-groups-modal",
        "saveGroupsInSchedule": "#saveGroupsInSchedule",
        "dataGroupsChckboxId": "data-groups-chckbox-id",
        "schLunchInnerTitle": ".sch-lunch-inner-title",
        "timeSchTimeTo": ".time-schTimeTo",
        "schTimeFrom": ".schTimeFrom",
        "schDuration": ".schDuration",
        "schTimeTo": ".schTimeTo",
        "iconCheckmark": ".icon-checkmark",
        "dataSchTimeFrom": "dataSchTimeFrom",
        "dataChDuration": "dataChDuration",
        "dataSchTimeTo": "dataSchTimeTo",
        "clockInClockOut": ".clockInClockOut",
        "dataInfoType": "data-type",
        "schLunchInnerActions": ".sch-lunch-inner-actions",
        "weeksList": ".weeks-list",
        "inputBracketLeft": ".input-bracket-left",
        "inputBracketRight": ".input-bracket-right",
        "individualsSearchPop": "#individualsSearchPop",
        "selectAllIndividualsModalCheckbox": ".select-all-individuals-modal-checkbox",
        "selectAllIndividualsModalCheckboxInput": ".select-all-individuals-modal-checkbox-input",
        "dataIndividualsChckboxId": "data-individuals-chckbox-id",
        "scheduleAddIndividual": ".schedule-add-individual",
        "checkmark": ".checkmark",
        "saveIndividualsInSchedule": "#saveIndividualsInSchedule",
        "scheduleUsers": ".scheduleUsers-",
        "scheduleUserList": ".scheduleUserList",
        "scheduleUserListItem": ".userListItem-",
        "dataStartDate": "data-startdate",
        "scheduleIndividualsListItem": ".schedule-individuals-list-item",
        "inThisScheduleSwitch": ".in-this-schedule-switch",
        "scheduleIndividualsList": ".schedule-individuals-list",
        "addToScheduleIndividuals": "#addToScheduleIndividuals",
        "scheduleUserListUsername": ".scheduleUserListUsername",
        "dataUserId": "data-userId",
        "tableUsers": ".table-users",
        "individualsListContainerCheckbox": ".individuals-list-container-checkbox",
        "scheduleGroupRemoveDisabledIcon": "schedule-group-remove-disabled-icon",
        "scheduleSendDisabledIcon": "schedule-send-disabled-icon",
        "scheduleGroupRemoveIcon": "schedule-group-remove-icon",
        "scheduleSendIcon": "schedule-send-icon",
        "scheduleIcon": ".schedule-icon",
        "disabledIcon": ".disabled-icon",
        "scheduleSend": ".scheduleSend",
        "scheduleGroupRemove": ".scheduleGroupRemove",
        "StartDate": ".StartDate",
        "dateUserId": "date-userId-id",
        "addGroupUserIcon": ".add-group-user-icon",
        "groupNames": ".group-names",
        "groupUsersList": ".group-users-list-",
        "addGroupUserIonOpen": "add-group-user-icon-open",
        "userGroupList": ".user-group-list",
        "freeInThisSchedule": ".freeInThisSchedule",
        "notFreeInThisSchedule": ".notFreeInThisSchedule",
        "dataGroupId": "data-group-id",
        "checkmark ": ".checkmark",
        "checkmarkInScheduleUser": "checkmark-in-schedule-user",
        "inScheduleUser": ".in-schedule-user",
        "checkmarkNotScheduleUser": "checkmark-not-schedule-user",
        "dataUserList": "data-user-list",
        "groups": "groups",
        "addToScheduleGroup": "#addToScheduleGroup",
        "scheduleAdd": ".schedule-add",
        "copyItemGroup": ".copy-item-group",
        "dataGroupTitle": "data-group-title",
        "dateStartDate": "data-startdate",
        "groupsList": "#groups-list-",
        "selectIndividualsSelectAll": ".select-individuals-select-all",
        "groupItem": ".group-item-",
        "userGroupListItem": "#user-group-list-",
        "notScheduleUser": ".not-schedule-user",
        "schTimeTitle": ".sch-time-title",
        "schDurationTitle": ".sch-duration-title",
        "lunchSelectError": "lunch-select-error"
    },
    "urls": {
        "getItem": "schedule"
    },
    "types": {
        "add"                     : "addSchedule",
        "update"                  : "updateSchedule",
        "loop"                    : "loopSchedule",
        "addWeek"                 : "addWeek",
        "delete"                  : "deleteSchedule",
        "unsetDay"                : "unsetDay",
        "getScheduleItem"         : "getScheduleItem",
        "removeWeek"              : "removeWeek",
        "addTimeInputs"           : "addTimeInputs",
        "addTimeSlider"           : "addTimeInputs",
        "changeStatus"            : "changeStatus",
        "removeLunch"             : "removeLunch",
        "saveLunch"               : "saveLunch",
        "addUsersToSchedule"      : "addUsersToSchedule",
        "scheduleGroupRemove"     : "scheduleGroupRemove",
        "scheduleSend"            : "scheduleSend",
        "addUsersToNewSchedule"   : "addUsersToNewSchedule",
        "showCalendar"            : "showCalendar",
        "showEmployeesInSchedule" : "showEmployeesInSchedule"
    },
    "errorMessages": {
        "emptyInput": "Input field is empty",
        "empty": "Fill in the blanks",
        "emptyClickInTime": "Click in time fill is empty",
        "emptyClickOutTime": "Clock out time fill is empty",
        "emptyRangeTime": "Range time fill is empty",
        "emptyWorkingDuration": "Working duration fill is empty",
        "date": "Date Incorrect",
        "dateEmpty": "Schedule Start Date Empty",
        "nameEmpty": "Schedule Name Empty",
        "codes": {
            "000": "You can not add more then 4 weeks."
        }
    },
    "errorStatus": 0,
    "showGeneralModal": function (type) {
        $(type).removeClass('d-none');
        $(schedule.tags.modalSaveButton).attr(schedule.tags.dataGeneraModalType, type);
        $(schedule.tags.modelCancel).attr(schedule.tags.dataGeneraModalType, type);
        $(schedule.tags.generalModal).modal('toggle');
        schedule.removeError();
    },
    "hindGeneralModal": function (type) {
        $(schedule.tags.modalSaveButton).attr(schedule.tags.dataGeneraModalType, '');
        $(schedule.tags.modelCancel).attr(schedule.tags.dataGeneraModalType, '');
        $(schedule.tags.generalModal).modal('toggle');
        $(type).addClass('d-none');
        schedule.removeError();
    },
    "displayError": function (appendTo, errorMassage) {
        schedule.errorStatus = 1;
        $(schedule.tags.errorField).text(schedule.errorMessages[errorMassage]);
    },
    "removeError": function () {
        schedule.errorStatus = 0;
        $(schedule.tags.errorField).text('');
    },
    "liveSearch": function (filterInput, filterDiv, callBack) {
        $(filterInput).keyup(function () {

            let input = $(filterInput).val();
            let count = 0;
            $(filterDiv).each(function () {

                if ($(this).text().search(new RegExp(input, "i")) < 0) {
                    $(this).parent().fadeOut();
                } else {
                    $(this).parent().show();
                    count++;
                }
            });
        });
    },
    "post": function (url, dataPass, callBack) {
        $.ajax({
            "type": "POST",
            "url": url,
            data: dataPass,
            success: callBack
        });
    },
    "modalAction": function (saveCallBack, cancelCallBack) {
        let type = $(schedule.tags.modelCancel).attr(schedule.tags.dataGeneraModalType);
        if (type.trim() == '') schedule.hindGeneralModal(type);
        $(schedule.tags.modalSaveButton).unbind();
        $(schedule.tags.modelCancel).unbind();
        $(schedule.tags.modalSaveButton).click(function () {
            saveCallBack(function () {
                if (schedule.errorStatus == 1) return;
                schedule.hindGeneralModal(type);
            });
        });

        $(schedule.tags.modelCancel).click(function () {
            if (cancelCallBack == undefined) {
                schedule.hindGeneralModal(type);
                return;
            }
            cancelCallBack(function () {
                schedule.hindGeneralModal(type);
            });
        });
    },
    "modalSaveAction": function (callBack) {
        $(schedule.tags.modalSaveButton).click(callBack);
    },
    "setNewScheduleItem": function (data) {
        $(schedule.tags.scheduleItemList).append(data);
        schedule.hindGeneralModal(schedule.tags.addScheduleName);
    },
    "getNewScheduleItem": function () {
        $(schedule.tags.addNewSchedule).click(function () {

            schedule.showGeneralModal(schedule.tags.addScheduleName);
            schedule.modalAction(function (e) {
                let newScheduleInfo = window.schedule.addScheduleInputTest();
                if (newScheduleInfo == null) return;

                schedule.removeError();
                e();
                $(schedule.tags.newScheduleStartDate).val('');
                $(schedule.tags.newScheduleName).val('');

                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.getScheduleItem,
                    "name": newScheduleInfo.name,
                    "startDate": newScheduleInfo.date
                }, schedule.setNewScheduleItem);
            });

        });
    },
    "addWeek": function () {
        $(schedule.tags.addWeekButton).click(function () {
            let id = $(this).attr(schedule.tags.dataScheduleId);
            let dayCount = $(this).attr(schedule.tags.dayCount);

            /* Add week loading indicator. Test Code */
            $(schedule.tags.addWeekButID + '-' + id).css("display", "none");
            $(schedule.tags.addWeekButLoaderID + '-' + id).css("display", "block");
            /* ------------------------------------- */

            schedule.post(schedule.urls.getItem, {
                "type": schedule.types.addWeek,
                "scheduleId": id,
                "dayCount" : dayCount
            }, function (data) {
                if (data == "000") {

                    schedule.showGeneralModal(schedule.tags.errorMessages);

                    $(schedule.tags.errorMessages).find('h2').text(schedule.errorMessages.codes["000"]);
                    $(schedule.tags.generalModal).show().addClass('show');
                    $(schedule.tags.modalSaveButton).addClass("d-none");

                    $(schedule.tags.modelCancel).click(function () {
                        $(schedule.tags.errorMessages).addClass("d-none");
                        $(schedule.tags.modalSaveButton).removeClass("d-none");
                        $(schedule.tags.generalModal).modal('hide').removeClass('show').css({"display": "none"});
                        $(schedule.tags.removeQuestion).addClass('d-none');
                        $(this).removeClass('m-0');
                    }).addClass('m-0');
                } else {
                    schedule.setNewWeek(data, id);
                    $(schedule.tags.addWeekButton).attr(schedule.tags.dayCount, parseInt(dayCount)+7); // Update day number on add week button to know the correct value of a week when adding another one
                }

                /* Add week loading indicator. Test Code */
                $(schedule.tags.addWeekButID + '-' + id).css("display", "block");
                $(schedule.tags.addWeekButLoaderID + '-' + id).css("display", "none");
                /* ------------------------------------- */

            });
        });
    },
    "setNewWeek": function (data, id) {
        $(schedule.tags.addWeekButton + '-' + id).append(data);
    },
    "removeSchedule": function () {
        $(schedule.tags.removeSchedule).click(function () {
            window.cId = $(this).attr(schedule.tags.dataScheduleId);

            $(schedule.tags.modalSaveButton).text("Continue");

            schedule.showGeneralModal(schedule.tags.removeQuestion);
            schedule.modalAction(function (e) {
                if (window.cId == undefined) return null;

                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.delete,
                    "id": window.cId
                }, function () {
                    $("#" + window.cId).remove();
                });
                e();
            }, function (e) {
                e();
                $(schedule.tags.modalSaveButton).text("Save");
            });

        });
    },
    "notifications": {
        "helpInit": function () {
            $(schedule.tags.helpButtonTow).click(function () {
                schedule.notifications.showHelp();
                $(schedule.tags.helpModalHeader).addClass('helpPageTow');
                $(schedule.notifications.uiResizableHandle).addClass('d-none');
                $(schedule.tags.helpOne).addClass('d-none');
                $(schedule.tags.helpTow).removeClass('d-none');

                $(schedule.tags.helpModalHeader + ' button ').click(function () {
                    $(schedule.tags.helpModal).hide().removeClass('show');
                });
            });

            $(schedule.tags.helpButtonOne).click(function () {
                schedule.notifications.showHelp();
                $(schedule.notifications.uiResizableHandle).addClass('d-none');
                $(schedule.tags.helpOne).removeClass('d-none');
                $(schedule.tags.helpTow).addClass('d-none');

                $(schedule.tags.helpModalHeader).removeClass('helpPageTow');
                $(schedule.tags.helpModalHeader + ' button ').click(function () {
                    $(schedule.tags.helpModal).hide().removeClass('show');
                });
            });
        },
        "showHelp": function () {
            $(schedule.tags.modalContent).resizable({
                minHeight: 300,
                minWidth: 300
            });
            $(schedule.tags.modalDialog).draggable();

            $(schedule.tags.helpModal).on('show.bs.modal', function () {
                $(this).find(schedule.tags.modalBody).css({
                    'max-height': '100%'
                });
            });

            $(schedule.tags.helpModal).show().addClass('show');
        },
        "autoLoad": function () {
            schedule.notifications.helpInit();
        }
    },
    "removeWeek": function () {
        $(schedule.tags.scheduleRemoveIcon).click(function () {
            let Id = $(this).attr(schedule.tags.dataShiftId);
            let scheduleClass = $(this).parent().parent().parent().attr('class');
            let _this = $(this);
            let __this = _this.parent().parent();
            let scheduleId = __this.attr('dataScheduleId');
            let weekId = __this.attr('');
            let weekToDeleteID = '#' + $(this).attr(schedule.tags.schWeekID);
            let addWeekButtID = schedule.tags.addWeekButID + '-' + $(this).attr(schedule.tags.dataDelWeekSchID);
            let dayCount = $(addWeekButtID).attr(schedule.tags.dayCount);

            schedule.showGeneralModal(schedule.tags.removeQuestion);
            schedule.modalAction(function (e) {
                let __this = $(_this).parent();
                /*console.log(__this.parent().attr('id'));*/
                //$('#' + __this.parent().attr('id')).remove();
                $('.' + scheduleClass).find('.weeks-list').each(function (index) {
                    $(this).find('.sch-week-title').text(index + 1);
                });

                /* Loading indicator on week deletion. Test Code */
                $(weekToDeleteID).html("<span style='margin: 0 auto; margin-top: -15px;'><img src='public/images/gifs/loading-gif.gif' style='height: 30px; display: inline-block;'> Removing week...</span>");
                /* --------------------------------------------- */

                $(schedule.tags.generalModal).removeClass('scheduleRemoveIcon');

                schedule.post(schedule.urls.getItem, {
                    "scheduleId": scheduleId,
                    "weekId": weekId,
                    "type": schedule.types.removeWeek,
                    "shiftIds": Id
                }, function (data) {
                    $(weekToDeleteID).remove();
                    $(addWeekButtID).attr(schedule.tags.dayCount, parseInt(dayCount)-7); // Update day number on add week button to know the correct value of a week when adding another one
                    // _this.parent().parent().remove(); // This line causes to disappear all other weeks if page was not reloaded after adding a week.
                });

                e();
            }, function (e) {
                $(schedule.tags.modalSaveButton).text("save");
                e();
            });
        });
    },
    "addExistingClockInOutInfo": function (data) {
        schedule.resetAddTimeToDate();
        if (data.workingduration.trim() != '') {
            $(schedule.tags.addTimeToDayOne).addClass('d-none');
            $(schedule.tags.addTimeToDayTwo).removeClass('d-none');
            $(schedule.tags.switch).addClass('inputs');
            $(schedule.tags.switch).find('input').prop("checked", true);

            if (data.clickIn) {
                $(schedule.tags.clickInTimeFrom).val(data.clickIn);
            }
            if (data.workingduration) {
                $(schedule.tags.rangeTimeTo).val(data.Range);
            }
            if (data.Range) {
                $(schedule.tags.workingDuration).val(data.workingduration);
            }
            if (data.weekDayStart) {
                $(schedule.tags.weekDayRangeSched).children("[value!=" + data.weekDayStart + "]").removeAttr("selected");
                $(schedule.tags.weekDayRangeSched).children("[value=" + data.weekDayStart + "]").attr("selected", "selected");
            }
        } else {
            $(schedule.tags.addTimeToDayOne).removeClass('d-none');
            $(schedule.tags.addTimeToDayTwo).addClass('d-none');
            $(schedule.tags.switch).removeClass('inputs');
            $(schedule.tags.switch).find('input').prop("checked", false);

            if (data.clickIn) {
                $(schedule.tags.clockInTimeRange).val(data.clickIn);
            }
            if (data.clockOut) {
                $(schedule.tags.clockOutTimeRange).val(data.clockOut);
            }
            if (data.weekDayStart) {
                $(schedule.tags.clockInWeekDay).children("[value!=" + data.weekDayStart + "]").removeAttr("selected");
                $(schedule.tags.clockInWeekDay).children("[value=" + data.weekDayStart + "]").attr("selected", "selected");

                $(schedule.tags.clockOutWeekDay).children("[value!=" + data.weekDayEnd + "]").removeAttr("selected");
                $(schedule.tags.clockOutWeekDay).children("[value=" + data.weekDayEnd + "]").attr("selected", "selected");

                $(schedule.tags.weekDayRangeSched).children("[value!=" + data.weekDayStart + "]").removeAttr("selected");      /* This is for second tab, ranged day */
                $(schedule.tags.weekDayRangeSched).children("[value=" + data.weekDayStart + "]").attr("selected", "selected"); /* This is for second tab, ranged day */
                // alert(data.weekDayStart + ' - ' + data.weekDayEnd);
            }
        }
    },
    "setClockInTimeRange": function (shiftId, data) {
        let clickInTimeFrom = $(schedule.tags.clickInTimeFrom).val();
        let rangeTimeTo = $(schedule.tags.rangeTimeTo).val();
        let workingDuration = $(schedule.tags.workingDuration).val();

        if (clickInTimeFrom.trim() == "") {
            schedule.displayError('addScheduleNameTitle', 'emptyClickInTime');
            return null;
        }

        if (rangeTimeTo == undefined || rangeTimeTo.trim() == "") {
            schedule.displayError('addScheduleNameTitle', 'emptyRangeTime');
            return null;
        }

        if (workingDuration == undefined || workingDuration.trim() == "") {
            schedule.displayError('addScheduleNameTitle', 'emptyWorkingDuration');
            return null;
        }

        schedule.errorStatus = 0;

        if (rangeTimeTo > 999) rangeTimeTo = 999;
        if (workingDuration > 99) workingDuration = 99;

        $(schedule.tags.errorField).text('');

        schedule.post(schedule.urls.getItem, {
            "shiftId": shiftId,
            "clickInTime": clickInTimeFrom,
            "rangeTime": rangeTimeTo,
            "workingDuration": workingDuration,
            "typeAddTimeInputs": 1,
            "type": schedule.types.addTimeInputs
        }, function (data) {
            $(schedule.tags.shift + shiftId).find('.clockInClockOut').html(clickInTimeFrom + " - " + rangeTimeTo + ' - ' + workingDuration);
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataRange, rangeTimeTo);
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataWorkingduration, workingDuration);
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataClockIn, clickInTimeFrom);

            $(schedule.tags.shift + shiftId).find('.icon-r').removeClass('d-none');
            schedule.resetAddTimeToDate();

            $(schedule.tags.addTimeToDayOne).removeClass('d-none');
            $(schedule.tags.addTimeToDayTwo).addClass('d-none');
            $(schedule.tags.switch).removeClass('inputs');
            $(schedule.tags.switch).find('input').prop("checked", false);
        });
    },
    "resetAddTimeToDate": function () {
        $(schedule.tags.clockInTimeRange).val('');
        $(schedule.tags.clockOutTimeRange).val('');
        $(schedule.tags.clickInTimeFrom).val('');
        $(schedule.tags.rangeTimeTo).val('');
        $(schedule.tags.workingDuration).val('');
    },
    "setClockInTime": function (shiftId, data) {

        let workRangeFrom = $(schedule.tags.clockInTimeRange).val();
        let workRangeTill = $(schedule.tags.clockOutTimeRange).val();
        let clockOutWeekDay = $(schedule.tags.clockOutWeekDay).val();

        if (workRangeFrom.trim() == "") {
            schedule.displayError('addScheduleNameTitle', 'emptyClickInTime');
            return null;
        }

        if (workRangeTill.trim() == "") {
            schedule.displayError('addScheduleNameTitle', 'emptyClickOutTime');
            return null;
        }

        schedule.errorStatus = 0;

        $(schedule.tags.errorField).text('');

        schedule.post(schedule.urls.getItem, {
            "shiftId": shiftId,
            "from": workRangeFrom,
            "to": workRangeTill,
            "clockOutWeekDay": clockOutWeekDay,
            "typeAddTimeInputs": 0,
            "type": schedule.types.addTimeSlider
        }, function (data) {
            $(schedule.tags.shift + shiftId).find('.clockInClockOut').text(workRangeFrom + " - " + workRangeTill);
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataClockIn, workRangeFrom);
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataClockOut, workRangeTill);
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataRange, '');
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataWorkingduration, '');
            $(schedule.tags.shift + shiftId).find('.icon-r').addClass('d-none');
            $(schedule.tags.shift + shiftId).attr(schedule.tags.dataclockOutWeekDay, clockOutWeekDay);

            schedule.resetAddTimeToDate();
        });
    },
    "saveWeekTime": function (shiftId, data) {
        if ($(schedule.tags.switch).hasClass('inputs')) {
            schedule.setClockInTimeRange(shiftId, data);
        } else {
            schedule.setClockInTime(shiftId, data);
        }
    },
    "addScheduleInputTest": function () {
        let date = $(schedule.tags.newScheduleStartDate).val();
        let name = $(schedule.tags.newScheduleName).val();

        if (date == undefined || date.trim() == '') {
            schedule.displayError('addScheduleName', 'dateEmpty');
            return null;
        }

        if (!date instanceof Date) {
            schedule.displayError('addScheduleName', 'date');
            return null;
        }

        if (name == undefined || name.trim() == '') {
            schedule.displayError('addScheduleName', 'nameEmpty');
            return null;
        }

        schedule.removeError();

        return {
            "date": date,
            "name": name
        };
    },
    "loadDatepicker": function () {
        //$(schedule.tags.newScheduleStartDate).datepicker();
    },
    "addDayTimePop": function (_this) {
        if (_this.attr('class').includes('disabled')) {
            return;
        }

        window.cId = _this.attr(schedule.tags.scheduleId);

        let SheduleId = _this.attr(schedule.tags.dataShiftId);

        let data = {
            "clickIn": _this.attr(schedule.tags.dataClockIn),
            "clockOut": _this.attr(schedule.tags.dataClockOut),
            "workingduration": _this.attr(schedule.tags.dataWorkingduration),
            "weekDayStart": _this.attr(schedule.tags.dataWeekdayStart),
            "weekDayEnd": _this.attr(schedule.tags.dataclockOutWeekDay),
            "Range": _this.attr(schedule.tags.dataRange),
        };

        schedule.addExistingClockInOutInfo(data);

        schedule.showGeneralModal(schedule.tags.addTimeToDayModal);
        schedule.modalAction(function (e) {
            if (window.cId == undefined) return null;

            schedule.saveWeekTime(SheduleId, data);
            e();
        }, function (e) {
            e();
        });
    },
    "clockInClockOut": function () {
        $(schedule.tags.clockInClockOut).click(function () {
            schedule.addDayTimePop($(this).parent());
        });
    },
    "addDayTime": function () {
        $(schedule.tags.addTimeToDay).click(function () {
            let _this = $(this);

            if ($(this).find(schedule.tags.iconSmallLunch).hasClass('d-none')) {
                schedule.addDayTimePop(_this);
            } else {
                schedule.clockInClockOut();
            }
        });
    },
    "switch": function () {
        $(schedule.tags.switchSliderButtonClick).click(function () {
            if ($(schedule.tags.switch).hasClass('inputs')) {
                $(schedule.tags.addTimeToDayOne).removeClass('d-none');
                $(schedule.tags.addTimeToDayTwo).addClass('d-none');
                $(schedule.tags.switch).removeClass('inputs');
                $(schedule.tags.switch).find('input').prop("checked", true);
            } else {
                $(schedule.tags.addTimeToDayOne).addClass('d-none');
                $(schedule.tags.addTimeToDayTwo).removeClass('d-none');
                $(schedule.tags.switch).addClass('inputs');
                $(schedule.tags.switch).find('input').prop("checked", false);
            }
        });

        $(schedule.tags.modelCancel).click(function () {
            $(schedule.tags.addTimeToDayTwo).addClass('d-none');
            $(schedule.tags.switch).find('input').prop("checked", false);
        });
    },
    "changeToGroups": function () {
        $(schedule.tags.changeToGroups).click(function () {
            let pageType = $(this).attr(schedule.tags.pageType);
            let startDate = $(this).attr(schedule.tags.dataStartDate);

            $(this).parent().parent().find(schedule.tags.changeToGroups).removeClass(schedule.tags.active);
            $(this).addClass(schedule.tags.active);

            if (pageType == "schedule") {
                $(this).parent().parent().find(schedule.tags.inThisSchedule).addClass('d-none');
                $(this).parent().parent().find(schedule.tags.scheduleWeekList).removeClass('d-none');
            } else {
                $(this).parent().parent().find(schedule.tags.inThisSchedule).removeClass('d-none');
                $(this).parent().parent().find(schedule.tags.scheduleWeekList).addClass('d-none');
            }

            schedule.setStartDate($(this));

            schedule.showRemoveSendButtons($(this));
        });
    },
    "setStartDate": function (_this) {
        if (_this.parent().find(schedule.tags.changeToGroups).hasClass(schedule.tags.inThisScheduleSwitch)) return null;

        let startDate = _this.parent().find(schedule.tags.changeToGroups).attr(schedule.tags.dataStartDate);
        _this.parent().find(schedule.tags.startDate).text(startDate);
    },
    "showRemoveSendButtons": function (_this) {
        if (_this.parent().parent().find(schedule.tags.scheduleWeekList).hasClass('d-none')) {
            _this.parent().parent().find(schedule.tags.removeSendSchedule).removeClass('d-none');
        } else {
            _this.parent().parent().find(schedule.tags.removeSendSchedule).addClass('d-none');
        }
    },
    "openSchedule": function () {
        $(schedule.tags.schTrig).click(function () {

            let cId = $(this).parent().attr('id');

            $(this).parent().toggleClass('open');

            if ($(schedule.tags.schItem).hasClass('open')) {
                $(schedule.tags.scheduleItemList).addClass(schedule.tags.scheduleOpen);
            } else {
                $(schedule.tags.scheduleItemList).removeClass(schedule.tags.scheduleOpen);
            }

            $(this).siblings(schedule.tags.schInnerItem + cId).slideToggle(500);
            $(this).toggleClass(schedule.tags.iconArrow1).toggleClass(schedule.tags.iconArrow2);
            $(this).parent().find(schedule.tags.schTrig).toggleClass(schedule.tags.schTrigActive);
        });
    },
    "selectAll": function (clickOn, list) {
        $(clickOn).click(function () {
            $(list).find(schedule.tags.checkmark).each(function () {
                if (!$(this).hasClass('activeCheckbox')) {
                    $(this).click();
                }
            });
        });
    },
    "dragAndDrops": {
        "icons": [
            "iconLunch",
            "iconXSign",
            "iconLeftBracket",
            "iconRightBracket",
            "scheduleCheckIcon"
        ],
        "iconsList": {
            "iconLunch": "icon-lunch",
            "iconXSign": "icon-xsign",
            "iconLeftBracket": "icon-leftbracket",
            "iconRightBracket": "icon-rightbracket",
            "scheduleCheckIcon": "schedule-check-icon"
        },
        "types": {
            "Fixed": "",
            "Range": "",
            "Flexible": ""
        },
        "errorMessages": {
            "emptyTime": "Time input Is empty",
            "emptyDuration": "Duration field empty",
            "emptyTimeTo": "Time To input Is empty"
        },
        "loop": {
            "startId": 0,
            "endId": 0,
            "loopStatus": null,
            "overLoopStatus": null,
            "loopIdList": [],
            "loopOver": []
        },
        "setLoopDataTagList": [
            "dataScheduleId",
            "dataClockIn",
            "dataClockOut",
            "dataWorkingDuration",
            "dataRange"
        ],
        "onSave": false,
        "errorCase": false,
        "lunchStatus": null,
        "init": function () {
            for (let i = 0; i < schedule.dragAndDrops.icons.length; i++) {
                $(schedule.tags[schedule.dragAndDrops.icons[i]]).draggable({
                    revert: true,
                    revertDuration: 0
                });
            }
        },
        "changeable": function () {
            $(schedule.tags.changeable).droppable({
                "accept": schedule.tags.iconLunch + ", " + schedule.tags.iconXSign + ", " + schedule.tags.iconLeftBracket + ", " + schedule.tags.iconRightBracket + ", " + schedule.tags.iconRightBracket + ", " + schedule.tags.scheduleCheckIcon,
                "drop": function (event, ui) {
                    if ($(ui.draggable).hasClass(schedule.dragAndDrops.iconsList.iconLunch)) {
                        schedule.dragAndDrops.loadLunch(event.target);
                    } else if ($(ui.draggable).hasClass(schedule.dragAndDrops.iconsList.iconXSign)) {
                        schedule.dragAndDrops.loadDayOff(event.target);
                    } else if ($(ui.draggable).hasClass(schedule.dragAndDrops.iconsList.iconLeftBracket)) {
                        schedule.dragAndDrops.loadLoopHandleLeft(event.target, 1);
                    } else if ($(ui.draggable).hasClass(schedule.dragAndDrops.iconsList.iconRightBracket)) {
                        schedule.dragAndDrops.loadLoopHandleRight(event.target, $(ui.draggable));
                    } else if ($(ui.draggable).hasClass(schedule.dragAndDrops.iconsList.scheduleCheckIcon)) {
                        schedule.dragAndDrops.loadScheduleCheckIcon(event.target);
                    }
                }
            });
        },
        "showError": function (message) {
            schedule.dragAndDrops.errorCase = true;
            return null;
        },
        "addError": function (_this, message) {
            _this.text(schedule.dragAndDrops.errorMessages[message]).addClass(schedule.tags.lunchSelectError);
        },
        "removeError": function (_this, message) {
            _this.text(message).removeClass(schedule.tags.lunchSelectError);
        },
        "getLunchValues": function (_this) {
            let schTimeFrom = $(_this).find(schedule.tags.schTimeFrom).val();
            let schDuration = $(_this).find(schedule.tags.schDuration).val();
            let schTimeTo = $(_this).find(schedule.tags.schTimeTo).val();

            /* if (schTimeTo != undefined) {
                 if (schTimeTo.trim() == "" || schTimeFrom != 'Anytime') {
                     schedule.dragAndDrops.addError($(_this).find(schedule.tags.schDurationTitle), 'emptyTime');
                     return false;
                 }
             }

             if (schTimeFrom.trim() == "" && schTimeFrom != 'Anytime') {
                 schedule.dragAndDrops.addError( $(_this).find(schedule.tags.schTimeTitle), 'emptyTimeTo');
                 return false;
             }*/

            if (schDuration.trim() == "") {
                schedule.dragAndDrops.addError($(_this).find(schedule.tags.schDurationTitle), 'emptyDuration');
                return false;
            }

            schedule.dragAndDrops.removeError($(_this).find(schedule.tags.schDurationTitle), 'Duration');

            return {
                "schTimeFrom": schTimeFrom,
                "schDuration": schDuration,
                "schTimeTo": schTimeTo
            };

        },
        "removeLunch": function (_this, Id) {
            $(_this).find(schedule.tags.lunchRemoveButton).click(function () {
                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.removeLunch,
                    "shiftId": Id
                }, function () {
                    schedule.dragAndDrops.resetLunch();
                    schedule.dragAndDrops.unsetLunchPopup(_this);
                    $(_this).find(schedule.tags.iconSmallLunch).addClass('d-none');
                });
            });
        },
        "saveLunch": function (_this, Id, lunchType) {
            $(_this).find(schedule.tags.lunchSaveButton).click(function () {

                let inputs = schedule.dragAndDrops.getLunchValues(_this);

                if (!inputs) return;

                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.saveLunch,
                    "timeFrom": inputs.schTimeFrom,
                    "timeTo": inputs.schTimeTo,
                    "duration": inputs.schDuration,
                    "shiftId": Id,
                    "LunchType": lunchType
                }, function () {
                    schedule.dragAndDrops.unsetLunchPopup(_this);

                    $(_this).find(schedule.tags.iconSmallLunch).attr(schedule.tags.dataSchTimeFrom, inputs.schTimeFrom);
                    $(_this).find(schedule.tags.iconSmallLunch).attr(schedule.tags.dataChDuration, inputs.schDuration);
                    $(_this).find(schedule.tags.iconSmallLunch).attr(schedule.tags.dataSchTimeTo, inputs.schTimeTo);
                    $(_this).find(schedule.tags.iconSmallLunch).attr(schedule.tags.dataInfoType, lunchType);
                    $(_this).find(schedule.tags.iconSmallLunch).attr(schedule.tags.dataShiftId, Id);

                    $(_this).find(schedule.tags.iconSmallLunch).removeClass('d-none');
                    schedule.dragAndDrops.onSave = true;
                });
            });
        },
        "selectLunchType": function (_this) {
            _this.find(schedule.tags.lunchType).click(function () {
                let shiftId = $(_this).attr(schedule.tags.dataShiftId);
                let type = $(this).attr(schedule.tags.dataCls);

                $(this).parent().parent().parent().find(schedule.tags.schLunchType).css({"display": "none"});

                let __this = $(this).parent().parent().parent();
                __this.find(schedule.tags.schLunchInner).css({'display': 'block'});

                schedule.dragAndDrops.resetLunch();

                if (type == "fixed") {
                    __this.find(schedule.tags.schLunchInnerActions).removeClass('rangeColor').removeClass('flexibleColor').addClass('fixedColor');
                    __this.find(schedule.tags.schLunchInner).removeClass('rangeBorderColor').removeClass('flexibleBorderColor').addClass('fixedBorderColor');
                    __this.find(schedule.tags.schLunchInnerTitle).text('Fixed');
                    __this.find(schedule.tags.timeSchTimeTo).addClass('d-none');
                } else if (type == "range") {
                    __this.find(schedule.tags.schLunchInnerActions).removeClass('fixedColor').removeClass('flexibleColor').addClass('rangeColor');
                    __this.find(schedule.tags.schLunchInner).removeClass('fixedBorderColor').removeClass('flexibleBorderColor').addClass('rangeBorderColor');
                    __this.find(schedule.tags.schLunchInnerTitle).text('Range');
                    __this.find(schedule.tags.timeSchTimeTo).removeClass('d-none').find('input').val();
                } else if (type == "flexible") {
                    __this.find(schedule.tags.schLunchInnerActions).removeClass('fixedColor').removeClass('rangeColor').addClass('flexibleColor');
                    __this.find(schedule.tags.schLunchInner).removeClass('fixedBorderColor').removeClass('rangeBorderColor').addClass('flexibleBorderColor');
                    __this.find(schedule.tags.schLunchInnerTitle).text('Flexible');
                    __this.find('input[type="time"]').attr('type', 'text').attr('disabled', 'disabled').val('Anytime');
                    __this.find(schedule.tags.timeSchTimeTo).addClass('d-none');
                }

                schedule.dragAndDrops.saveLunch(_this, shiftId, type);
                schedule.dragAndDrops.removeLunch(_this, shiftId);
            });
        },
        "resetLunch": function () {
            schedule.dragAndDrops.onSave = false;
            $(schedule.tags.lunchCont).find('input').val('');
        },
        "loadScheduleCheckIcon": function (_this) {
            schedule.dragAndDrops.changeStatus(_this, 1);
        },
        "loadLunch": function (_this) {
            var lunch = $(_this);

            if (lunch.hasClass('disabled')) {
                return;
            }

            lunch.find(schedule.tags.iconSmallLunch).removeClass('d-none');

            let lunchCont = $(schedule.tags.lunchCont).html();

            if (lunch.find(schedule.tags.schLunch).length == 0) {
                lunch.append(lunchCont);
            }

            lunch.find(schedule.tags.schLunch).delay(500).show("slide", {direction: "left"}, 500);
            lunch.find(schedule.tags.schLunch).unbind();

            schedule.dragAndDrops.unsetLunchPopupMouseup(lunch);
            schedule.dragAndDrops.selectLunchType(lunch);
        },
        "loadDayOff": function (_this) {
            $(_this).find('.icon-small-lunch').addClass('d-none');
            $(_this).find('.icon-r').addClass('d-none');
            schedule.dragAndDrops.changeStatus(_this, 2);
        },
        "loadLoopHandleLeft": function (_this) {
            $(_this).append('<span class="icon-leftbracket input-bracket-left"></span>');
            let shiftId = $(_this).attr(schedule.tags.dataShiftId);
            schedule.dragAndDrops.loop.startId = shiftId;
            schedule.dragAndDrops.loadLoop($(_this));
        },
        "loadLoopHandleRight": function (_this, __this) {
            $(_this).append('<span class="icon-rightbracket input-bracket-right"></span>');
            let shiftId = $(_this).attr(schedule.tags.dataShiftId);
            schedule.dragAndDrops.loop.endId = shiftId;
            schedule.dragAndDrops.loadLoop($(_this));
        },
        "resetLoopHandle": function () {
            schedule.dragAndDrops.setLoop();
            schedule.dragAndDrops.loop.startId = 0;
            schedule.dragAndDrops.loop.endId = 0;
            schedule.dragAndDrops.loop.loopIdList = [];
            schedule.dragAndDrops.loop.loopOver = [];
            $(schedule.tags.inputBracketLeft).remove();
            $(schedule.tags.inputBracketRight).remove();
            schedule.dragAndDrops.loop.overLoopStatus = null;
            schedule.dragAndDrops.loop.loopStatus = null;
        },
        "loadLoop": function (_this) {
            if (schedule.dragAndDrops.loop.startId == 0 || schedule.dragAndDrops.loop.endId == 0) return;

            _this.parent().parent().find(schedule.tags.weeksList).each(function () {
                $(this).find(schedule.tags.addTimeToDay).each(function () {
                    let cId = $(this).attr(schedule.tags.dataShiftId);

                    if (schedule.dragAndDrops.loop.overLoopStatus == true) {
                        schedule.dragAndDrops.loop.loopOver.push(cId);
                    }

                    if (schedule.dragAndDrops.loop.startId == cId) {
                        schedule.dragAndDrops.loop.loopStatus = true;
                    }
                    if (schedule.dragAndDrops.loop.loopStatus) {
                        schedule.dragAndDrops.loop.loopIdList.push(cId);
                    }
                    if (schedule.dragAndDrops.loop.endId == cId) {
                        schedule.dragAndDrops.loop.loopStatus = null;
                        schedule.dragAndDrops.loop.overLoopStatus = true;
                    }
                });
            });

            if (schedule.dragAndDrops.loop.loopIdList.length > 0) {
                schedule.dragAndDrops.sendLoop();
            }
        },
        "setLoop": function () {
            let b = 0;
            for (let i = 0; i < schedule.dragAndDrops.loop.loopOver.length; i++) {

                for (let c = 0; c < schedule.dragAndDrops.setLoopDataTagList.length; c++) {
                    if ($(schedule.tags.shift + schedule.dragAndDrops.loop.loopIdList[b]).is("[" + schedule.tags[schedule.dragAndDrops.setLoopDataTagList[c]] + "]") == true) {
                        $(schedule.tags.shift + schedule.dragAndDrops.loop.loopOver[i]).attr(
                            schedule.tags[schedule.dragAndDrops.setLoopDataTagList[c]],

                            $(schedule.tags.shift + schedule.dragAndDrops.loop.loopIdList[b]).attr(
                                schedule.tags[schedule.dragAndDrops.setLoopDataTagList[c]]
                            )
                        );
                    }

                    if ($(schedule.tags.shift + schedule.dragAndDrops.loop.loopOver[i]).hasClass('disabled')) {
                        $(schedule.tags.shift + schedule.dragAndDrops.loop.loopOver[i]).removeClass('disabled');
                    }

                    if ($(schedule.tags.shift + schedule.dragAndDrops.loop.loopIdList[b]).hasClass('disabled')) {
                        $(schedule.tags.shift + schedule.dragAndDrops.loop.loopOver[i]).addClass('disabled');
                    }
                }

                let copy = $(schedule.tags.shift + schedule.dragAndDrops.loop.loopIdList[b]).html();
                $(schedule.tags.shift + schedule.dragAndDrops.loop.loopOver[i]).html(copy);

                b++;
                if (schedule.dragAndDrops.loop.loopIdList[b] == undefined) {
                    b = 0;
                }
            }
        },
        "sendLoop": function () {
            schedule.post(schedule.urls.getItem, {
                "type": schedule.types.loop,
                "copyList": schedule.dragAndDrops.loop.loopIdList,
                "loopOver": schedule.dragAndDrops.loop.loopOver
            }, schedule.dragAndDrops.resetLoopHandle());
        },
        "autoLoad": function () {
            schedule.dragAndDrops.init();
            schedule.dragAndDrops.changeable();
            schedule.dragAndDrops.iconSmallLunchPop();
        },
        "unsetLunchPopup": function (lunch, type) {
            let _this = lunch.find(schedule.tags.schLunch);

            _this.css({'display': 'none'});
            _this.find(schedule.tags.schLunchInner).css({'display': 'none'});
            _this.find(schedule.tags.schLunchType).css({'display': 'block'});

            let schTimeFrom = lunch.find(schedule.tags.schTimeFrom).val();
            let schDuration = lunch.find(schedule.tags.schDuration).val();
            let schTimeTo = lunch.find(schedule.tags.schTimeTo).val();
            //let lunchType   = lunch.find(schedule.tags.dataType);

            /*console.log(lunch.find(schedule.tags.schTimeTo));*/

            if (schTimeFrom.trim() == "" || schDuration.trim() == "" || schTimeTo.trim() == "") {
                lunch.find(schedule.tags.iconSmallLunch).addClass('d-none');
            }

            /*console.log(lunchType);
            if (lunch.is("[data-type]") == true) {
                lunchType = lunch.attr(schedule.tags.dataInfoType);
            }*/
            //$(schedule.tags.schLunchInner).removeClass('d-block');

            /*lunch.find(schedule.tags.schLunchType).delay(500).show("slide", { direction: "left" }, 400);*/
            /*lunch.find(schedule.tags.schLunchInner).delay(100).hide("slide", {direction: "left" }, 100)
            lunch.find(schedule.tags.schLunch).delay(100).hide("slide", {direction: "left" }, 100).attr('style', '');*/
        },
        "unsetLunchPopupMouseup": function (lunch, type) {
            let _this = lunch;

            $(document).mouseup(function (e) {
                if (!_this.is(e.target) && _this.has(e.target).length === 0) {
                    schedule.dragAndDrops.unsetLunchPopup(lunch);
                }
            });
        },
        "changeStatus": function (_this, status) {
            let Id = $(_this).attr(schedule.tags.dataShiftId);
            schedule.resetAddTimeToDate();

            schedule.post(schedule.urls.getItem, {
                "type": schedule.types.changeStatus,
                "status": status,
                "shiftId": Id,
            }, function (data) {
                if (status == 1) {
                    if ($(_this).text().trim() == "Day Off") {
                        $(_this).find('.clockInClockOut').text("--:-- - --:--");
                    }
                    $(_this).removeClass('disabled');
                } else if (status == 2) {
                    $(_this).attr(schedule.tags.dataClockIn, '').attr(schedule.tags.dataClockOut, '').attr(schedule.tags.dataWorkingduration, '').attr(schedule.tags.dataRange, '').addClass('disabled').find('.clockInClockOut').text('Day Off');
                }
            });
        },
        "iconSmallLunchPop": function () {
            $(schedule.tags.iconSmallLunch).click(function () {

                let shiftId = $(this).attr(schedule.tags.dataShiftId);
                let type = $(this).attr(schedule.tags.dataInfoType);

                $(this).parent().find(schedule.tags.schLunchType).addClass('d-none');
                $(this).parent().find(schedule.tags.schLunch).delay(400).show("slide", {direction: "left"}, 400);
                $(this).parent().find(schedule.tags.schLunchInner).css({'display': "block"});

                schedule.dragAndDrops.saveLunch($(this).parent(), shiftId, type);
                schedule.dragAndDrops.removeLunch($(this).parent(), shiftId);

                schedule.dragAndDrops.unsetLunchPopupMouseup($(this).parent().parent(), true);
            });
        }
    },
    "addToSchedule": {
        "listOfUsers": {},
        "listOfChangeUsers": {},
        "currentShedule": 0,
        "type": {
            "user": {
                "add": "saveIndividualsInSchedule",
                "cancel": "cancelIndividualsInSchedule",
                "checkBoxInput": "selectAllIndividualsModalCheckbox"
            },
            "group": {
                "add": "saveGroupsInSchedule",
                "cancel": "cancelIndividualsInSchedule",
                "checkBoxInput": "select-all-groups-modal-checkbox"
            }
        },
        "updateType": {
            "removeFromSchedule": 2,
            "AddFromSchedule": 1
        },
        "userOrGroup": {
            "user": 1,
            "group": 2
        },
        "init": function () {

        },
        "userSelect": function () {
            $(schedule.tags.selectAllIndividualsModalCheckbox).find(schedule.tags.checkmark).click(function () {
                let currentId = $(this).parent().find('input').attr(schedule.tags.dataIndividualsChckboxId);
                let userName = $(this).parent().parent().parent().find(schedule.tags.scheduleUserListUsername);

                if (schedule.addToSchedule.listOfUsers[currentId] !== undefined) {
                    delete schedule.addToSchedule.listOfUsers[currentId];
                } else {
                    schedule.addToSchedule.listOfUsers[currentId] = userName.html();
                }
            });
        },
        "setUsers": function (userName, userId, __this) {
            let newUserHtml = $(schedule.tags.scheduleIndividualsListItem).parent().parent().find('.d-none');

            newUserHtml.find(schedule.tags.individualsListContainerCheckbox).attr(schedule.tags.dataUserId, userId);
            newUserHtml.find('.title').html(userName);
            __this.find(schedule.tags.scheduleIndividualsList).append('<div class="col-md-12 schedule-individuals-list-item">' + newUserHtml.html() + '</div>');

            $('.individuals-list-container-checkbox[data-userid="' + userId + '"]').find('.checkmark').click(function () {
                if ($(this).hasClass('activeCheckbox') == false) {
                    $(this).addClass('activeCheckbox');
                } else {
                    $(this).removeClass('activeCheckbox');
                }
            });

            schedule.addToSchedule.setRemoveSendActions(__this);

            $(schedule.tags.addToScheduleIndividuals).modal('hide');
        },
        "addIndividual": function () {
            $(schedule.tags.scheduleAddIndividual).click(function () {
                schedule.addToSchedule.filterUsers($(this));

                $(schedule.tags.saveIndividualsInSchedule).attr(schedule.tags.dataScheduleId, $(this).attr(schedule.tags.dataScheduleId))
                schedule.addToSchedule.currentShedule = $(this).attr(schedule.tags.dataScheduleId);
            });
        },
        "filterUsers": function (_this) {
            $(schedule.tags.scheduleUserList).each(function () {
                let dataScheduleId = $(this).attr(schedule.tags.scheduleId);
                let userId = $(this).attr(schedule.tags.dateUserId);

                /*if (_this.attr(schedule.tags.dataScheduleId) == dataScheduleId) {
                    $(this).addClass('d-none');
                }*/
            });
        },
        "selectIndividualsInSchedule": function () {
            $(schedule.tags.individualsListContainerCheckbox).find(schedule.tags.checkmark).click(function () {
                let userId = $(this).parent().attr(schedule.tags.dataUserId);

                if (schedule.addToSchedule.listOfChangeUsers[userId] != undefined) {
                    delete schedule.addToSchedule.listOfChangeUsers[userId];
                } else {
                    schedule.addToSchedule.listOfChangeUsers[userId] = $(this);
                }
                schedule.addToSchedule.setRemoveSendActions($(this));
            });
        },
        "setRemoveSendActions": function (_this) {

            let scheduleId = _this.attr(schedule.tags.scheduleId);
            _this = $(schedule.tags.schInnerItem + scheduleId);

            if (Object.entries(schedule.addToSchedule.listOfChangeUsers).length != 0) {
                _this.find('.' + schedule.tags.scheduleGroupRemoveDisabledIcon).attr('class', schedule.tags.scheduleGroupRemoveIcon);
                _this.find('.' + schedule.tags.scheduleSendDisabledIcon).attr('class', schedule.tags.scheduleSendIcon);
            } else {
                _this.find('.' + schedule.tags.scheduleGroupRemoveIcon).attr('class', schedule.tags.scheduleGroupRemoveDisabledIcon);
                _this.find('.' + schedule.tags.scheduleSendIcon).attr('class', schedule.tags.scheduleSendDisabledIcon);
            }
        },
        "getUserIds": function () {
            let keys = [];

            for (var key in schedule.addToSchedule.listOfChangeUsers) {
                if (schedule.addToSchedule.listOfChangeUsers.hasOwnProperty(key)) {
                    if (key != "undefined") {
                        keys.push(key);
                    }
                }
            }
            return keys;
        },
        "scheduleSend": function () {
            $(schedule.tags.scheduleSend).click(function () {
                let users = schedule.addToSchedule.getUserIds();
                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.scheduleSend,
                    "users": users,
                    "updateType": schedule.addToSchedule.updateType.AddFromSchedule,
                    "userOrGroupType": schedule.addToSchedule.userOrGroup.group,
                    "startDate": $(this).attr(schedule.tags.dataStartDate),
                    "scheduleId": $(this).attr(schedule.tags.scheduleId)
                }, function () {

                });
            });
        },
        "scheduleGroupRemove": function () {
            $(schedule.tags.scheduleGroupRemove).click(function () {
                let userList = schedule.addToSchedule.getUserIds();
                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.scheduleSend,
                    "users": userList,
                    "updateType": schedule.addToSchedule.updateType.removeFromSchedule,
                    "userOrGroupType": schedule.addToSchedule.userOrGroup.user,
                    "startDate": $(this).attr(schedule.tags.dataStartDate),
                    "scheduleId": $(this).attr(schedule.tags.scheduleId)
                }, function () {
                    for (let i = 0; i < userList.length; i++) {
                        let notFreeInThisSchedule = $('.userListItemId-' + userList[i]).parent().find(schedule.tags.notFreeInThisSchedule);

                        if (parseInt(notFreeInThisSchedule.text()) + 1 >= 1) {
                            notFreeInThisSchedule.text(0);
                        } else {
                            notFreeInThisSchedule.text(parseInt(notFreeInThisSchedule.text()) + 1);
                        }

                        let _this = $(schedule.tags.schInnerItem + schedule.addToSchedule.listOfChangeUsers[userList[i]].attr(schedule.tags.scheduleId));

                        _this.find('.' + schedule.tags.scheduleGroupRemoveIcon).attr('class', schedule.tags.scheduleGroupRemoveDisabledIcon);
                        _this.find('.' + schedule.tags.scheduleSendIcon).attr('class', schedule.tags.scheduleSendDisabledIcon);

                        $('.userListItemId-' + userList[i]).find(schedule.tags.scheduleUserListUsername).removeClass('not-schedule-user');
                        $('.userListItemId-' + userList[i]).find(schedule.tags.inScheduleUser).removeClass('not-schedule-user').removeClass('activeCheckbox');
                        $('.userListItemId-' + userList[i]).find('input').removeAttr('disabled');
                        $('.userListItemId-' + userList[i]).find('.checkmark-in-schedule-user').removeClass('checkmark-not-schedule-user');
                        schedule.addToSchedule.listOfChangeUsers[userList[i]].parent().parent().parent().remove();
                    }
                });
            });
        },
        "resetUserList": function () {
            schedule.addToSchedule.listOfUsers = {};
        },
        "modelIndividualsAddButton": function () {
            $(schedule.tags.saveIndividualsInSchedule).click(function () {
                schedule.addToSchedule.currentShedule = $(this).attr(schedule.tags.dataScheduleId);

                schedule.post(schedule.urls.getItem, {
                        "type": schedule.types.addUsersToSchedule,
                        "scheduleId": $(this).attr(schedule.tags.dataScheduleId),
                        "listOfUsers": schedule.addToSchedule.listOfUsers,
                        "startDate": $(schedule.tags.schInnerItem + schedule.addToSchedule.currentShedule).attr(schedule.tags.dataStartDate)
                    },
                    function () {
                        var size = 0, key;
                        for (key in schedule.addToSchedule.listOfUsers) {

                            if (schedule.addToSchedule.listOfUsers.hasOwnProperty(key)) size++;

                            $(schedule.tags.scheduleUserListItem + key).find(schedule.tags.selectAllIndividualsModalCheckbox).click();
                            $(schedule.tags.scheduleUserListItem + key).find(schedule.tags.scheduleUserListUsername).addClass('not-schedule-user');
                            $(schedule.tags.scheduleUserListItem + key).find(schedule.tags.selectAllIndividualsModalCheckboxInput).attr('disabled', 'disabled');

                            schedule.addToSchedule.setUsers(schedule.addToSchedule.listOfUsers[key], key, $(schedule.tags.schInnerItem + schedule.addToSchedule.currentShedule));


                        }
                        schedule.addToSchedule.resetUserList();
                    });
            });
        },
        "removeUserFromList": function (id) {

        },
        "resetGroupsInSchedule": function () {
            $(schedule.tags.addToScheduleGroup).find('.activeCheckbox').click();
            $(schedule.tags.copyItemGroup).find('.title').text('');
            $(schedule.tags.copyItemGroup).find('.startDate').text('');
            $(schedule.tags.copyItemGroup).find('.checkmark').attr('');
        },
        "saveGroupsInSchedule": function () {
            var scheduleGroupTitle = {};
            $(schedule.tags.saveGroupsInSchedule).click(function () {

                let saveGroupsInSchedule = $(this).parent().parent();
                let tableGroupsLive = saveGroupsInSchedule.find(schedule.tags.tableGroupsLiveSearch).find(schedule.tags.selectAllGroupsModalCheckbox);
                let groupIdObject = {};
                let ScheduleId = $(this).attr(schedule.tags.dataScheduleId);
                let startDate = $(this).attr(schedule.tags.dateStartDate);
                var addedList = {};

                tableGroupsLive.each(function () {
                    let _this = $(this).find('span');
                    let currentGroupTitle = _this.attr(schedule.tags.dataGroupTitle);
                    var addGroupTitle = true;
                    let Id = _this.attr('data-user-id');

                    if (!_this.hasClass(schedule.tags.groups) && _this.hasClass('activeCheckbox')) {
                        $(schedule.tags.scheduleGroupListItam).find('.title').each(function () {
                            if ($(this).text().trim() == currentGroupTitle.trim()) {
                                addGroupTitle = false;
                                return;
                            } else {
                                addGroupTitle = true;
                            }
                        });

                        if (addGroupTitle && addedList[currentGroupTitle] == undefined) {
                            addedList[currentGroupTitle] = {
                                "currentGroupTitle": currentGroupTitle,
                                "startDate": startDate,
                                "groupId": null,
                                "users": []
                            };
                        }

                        if (Id !== undefined) {
                            let groupList = $(this).parent().parent();
                            let groupId = groupList.attr(schedule.tags.dataGroupId);


                            addedList[currentGroupTitle]['users'].push(Id);
                            addedList[currentGroupTitle]['groupId'] = groupId;
                            groupIdObject[Id] = _this.attr(schedule.tags.dataGroupTitle);

                            $(schedule.tags.userGroupListItem + groupId).find(schedule.tags.addGroupUserIcon).click();

                            let outOfScheduleNumber = $(schedule.tags.userGroupListItem + groupId).find(schedule.tags.notFreeInThisSchedule);

                            if (parseInt(outOfScheduleNumber.text()) - 1 <= 0) {
                                outOfScheduleNumber.text(parseInt(outOfScheduleNumber.text()) - 1);
                            } else {
                                outOfScheduleNumber.text(0);
                            }

                            $(schedule.tags.scheduleUserListItem + Id + '-' + groupId).find(schedule.tags.selectAllGroupsModalCheckbox + ' > input[type="checkbox"]').click();
                            $(schedule.tags.scheduleUserListItem + Id + '-' + groupId).find(schedule.tags.inScheduleUser).addClass('not-schedule-user');
                            $(schedule.tags.scheduleUserListItem + Id + '-' + groupId).find(schedule.tags.selectAllGroupsModalCheckbox + ' > input[type="checkbox"]').attr('disabled', 'disabled');
                            $(schedule.tags.scheduleUserListItem + Id + '-' + groupId).find(schedule.tags.selectAllGroupsModalCheckbox + ' span').addClass('checkmark-not-schedule-user').removeClass('activeCheckbox');
                        }
                    }
                });

                for (var key in addedList) {
                    if (addedList.hasOwnProperty(key)) {
                        let group = $(schedule.tags.groupsList + ScheduleId).find('.group-item-' + addedList[key]['groupId']);

                        if (group.length >= 1) {
                            let userList = group.find('.checkmark').attr(schedule.tags.dataUserList);

                            userList = JSON.parse(userList);
                            let newUserId = parseInt(addedList[key]['users']);
                            userList.push(newUserId);

                            group.find('.checkmark').attr(schedule.tags.dataUserList, JSON.stringify(userList));
                            group.find('.checkmark').click(function () {
                                if ($(this).hasClass('activeCheckbox')) {
                                    $(this).removeClass('activeCheckbox');
                                } else {
                                    $(this).addClass('activeCheckbox');
                                    schedule.addToSchedule.setRemoveSendActions($(this));
                                }
                            });
                        } else {
                            $(schedule.tags.copyItemGroup).find('.title').text(addedList[key]['currentGroupTitle']);
                            $(schedule.tags.copyItemGroup).find('.startDate').text(addedList[key]['startDate']);
                            $(schedule.tags.copyItemGroup).find('.groups-list-container-checkbox span').attr(schedule.tags.scheduleId, ScheduleId);

                            $(schedule.tags.copyItemGroup).find('span').attr(schedule.tags.dataUserList, '[' + addedList[key]['users'].toString() + ']');
                            $(schedule.tags.copyItemGroup).find(schedule.tags.scheduleGroupListItam).addClass('group-item-' + addedList[key]['groupId']);

                            let groupHtml = $(schedule.tags.copyItemGroup).html();
                            $(schedule.tags.groupsList + ScheduleId).append(groupHtml);

                            console.log($(schedule.tags.groupsList + ScheduleId).html());

                            $(schedule.tags.copyItemGroup).find('.title').text('');
                            $(schedule.tags.copyItemGroup).find('.startDate').text('');
                            $(schedule.tags.copyItemGroup).find('.groups-list-container-checkbox span').attr(schedule.tags.scheduleId, '');
                            $(schedule.tags.copyItemGroup).find('span').attr(schedule.tags.dataUserList, '');
                            $(schedule.tags.copyItemGroup).find(schedule.tags.scheduleGroupListItam).removeClass('group-item-' + addedList[key]['groupId']);
                        }
                    }
                }

                schedule.post(schedule.urls.getItem, {
                    "type": schedule.types.addUsersToSchedule,
                    "scheduleId": ScheduleId,
                    "listOfUsers": groupIdObject,
                    "startDate": $(schedule.tags.schInnerItem + ScheduleId).attr(schedule.tags.dataStartDate)
                }, function () {
                    $(schedule.tags.addToScheduleGroup).modal('hide');
                    schedule.addToSchedule.resetGroupsInSchedule();
                    addedList = {};
                    groupIdObject = {};

                    $(schedule.tags.saveGroupsInSchedule).unbind();
                    schedule.addToSchedule.saveGroupsInSchedule();
                });

                schedule.addToSchedule.listContainerCheckbox();
            });
        },
        "opensUserListGroups": function () {
            $(schedule.tags.addGroupUserIcon).click(function () {
                let ChckboxId = $(this).attr(schedule.tags.dataGroupsChckboxId);

                $(schedule.tags.groupUsersList + ChckboxId).toggleClass('d-none');
                $(this).toggleClass(schedule.tags.addGroupUserIonOpen);
            });
        },
        "groupSelect": function () {
            $(schedule.tags.checkmark).click(function () {
                if ($(this).hasClass(schedule.tags.groups)) {
                    let id = $(this).attr(schedule.tags.dataUserList);
                    $('.' + id).find(schedule.tags.checkmark).click();
                }

                if ($(this).parent().find('input').attr('disabled') == undefined) {
                    $(this).toggleClass('activeCheckbox');
                }
            });
        },
        "scheduleAddButton": function () {
            $(schedule.tags.scheduleAdd).click(function () {
                $(schedule.tags.saveGroupsInSchedule).attr(schedule.tags.dataScheduleId, $(this).attr(schedule.tags.dataScheduleId));
                $(schedule.tags.saveGroupsInSchedule).attr(schedule.tags.dateStartDate, $(this).attr(schedule.tags.dateStartDate));
            });
        },
        "listContainerCheckbox": function () { //activeCheckbox
            schedule.addToSchedule.listOfChangeUsers = {};
            $(schedule.tags.groupsListContainerCheckbox).find(schedule.tags.checkmark).click(function () {

                let userIds = $(this).attr(schedule.tags.dataUserList);

                userIds = JSON.parse(userIds);

                for (i = 0; i < userIds.length; i++) {
                    if (schedule.addToSchedule.listOfChangeUsers[userIds[i]] != undefined) {
                        delete schedule.addToSchedule.listOfChangeUsers[userIds[i]];
                    } else {
                        schedule.addToSchedule.listOfChangeUsers[userIds[i]] = $(this);
                    }
                }

                schedule.addToSchedule.setRemoveSendActions($(this));
            });
        },
        "autoLoad": function () {
            schedule.addToSchedule.listContainerCheckbox();
            schedule.addToSchedule.scheduleAddButton();
            schedule.addToSchedule.saveGroupsInSchedule();
            schedule.addToSchedule.groupSelect();
            schedule.addToSchedule.selectIndividualsInSchedule();
            schedule.addToSchedule.modelIndividualsAddButton();
            schedule.addToSchedule.addIndividual();
            schedule.addToSchedule.userSelect();

            schedule.addToSchedule.scheduleGroupRemove();
            schedule.addToSchedule.scheduleSend();
            schedule.addToSchedule.opensUserListGroups();
        }
    },
    "employee": {
        "tags" : {
            "schMenu"                             : "#sch-menu",
            "menuType"                            : "data-menu-type",
            "active"                              : "active",
            "employeeGroupName"                   : ".employeeGroupName",
            "employeeCurrentScheduleName"         : ".employeeCurrentScheduleName",
            "userListSchedule"                    : ".userListSchedule-",
            "dataScheduleId"                      : "data-schedule-id",
            "employeeGroupNames"                  : ".employeeGroupNames",
            "activeEmployeeGroupList"             : "activeEmployeeGroupList",
            "employeesCalendarNextMonth"          : ".employeesCalendarNextMonth",
            "employeesCalendarPreviousMonth"      : ".employeesCalendarPreviousMonth",
            "employeesCalendarMonthTitle"         : ".employeesCalendarMonthTitle",
            "employeesCalendarMonthArrowDisabled" : "employeesCalendarMonthArrowDisabled",
            "dataStartDate"                       : "data-start-date",
            "dataEndDate"                         : "data-end-date",
            "dataCurrentDate"                     : "data-current-date",
            "schedulePageToggle"                  : ".schedulePageToggle",
            "calendarDaysNames"                   : ".calendarDaysNames",
            "calendarMonth"                       : ".calendarMonth-",
            "calendarInsert"                      : ".calendarInsert",
            "dataCalendarName"                    : "data-calendar-name",
            "employeesInSchedule"                 : "#employeesInSchedule"
        },
        "changeMonthDays" : function () {
            $(schedule.employee.tags.employeesCalendarNextMonth).click(function () {
                let currentMonth = $(this).parent().find(schedule.employee.tags.employeesCalendarMonthTitle).attr(schedule.employee.tags.dataCurrentDate);
                let currentDate = new Date(currentMonth);
                let EndDate = new Date($(this).attr(schedule.employee.tags.dataEndDate));

                $(this).parent().find(schedule.employee.tags.employeesCalendarPreviousMonth).removeClass(schedule.employee.tags.employeesCalendarMonthArrowDisabled);

                let NewMM = currentDate.getMonth() + 2;
                let NewYY = currentDate.getFullYear();

                let EndDateMM = EndDate.getMonth() + 2;
                let EndDateYY = EndDate.getFullYear();

                if (NewMM == EndDateMM && NewYY == EndDateYY) {
                    $(this).addClass(schedule.employee.tags.employeesCalendarMonthArrowDisabled);
                    return;
                }

                if (NewMM == 13) {
                    NewMM = 1;
                    NewYY = NewYY+1;
                }

                let date = NewYY + '-' + NewMM;

                $($(this).attr(schedule.employee.tags.dataCalendarName)).find(schedule.employee.tags.calendarDaysNames).addClass('d-none');
                $($(this).attr(schedule.employee.tags.dataCalendarName)).find(schedule.employee.tags.calendarMonth + NewMM + '-' + NewYY).removeClass('d-none')

                $(this).parent().find(schedule.employee.tags.employeesCalendarMonthTitle).attr(schedule.employee.tags.dataCurrentDate, date).text(date);
            });
            $(schedule.employee.tags.employeesCalendarPreviousMonth).click(function () {
                let currentMonth = $(this).parent().find(schedule.employee.tags.employeesCalendarMonthTitle).attr(schedule.employee.tags.dataCurrentDate);
                let currentDate = new Date(currentMonth);
                let StartDate = new Date($(this).attr(schedule.employee.tags.dataStartDate));

                $(this).parent().find(schedule.employee.tags.employeesCalendarNextMonth).removeClass(schedule.employee.tags.employeesCalendarMonthArrowDisabled);

                let StartDateMM = StartDate.getMonth();
                let StartDateYY = StartDate.getFullYear();

                let NewMM = currentDate.getMonth();
                let NewYY = currentDate.getFullYear();

                if (NewMM == StartDateMM && NewYY == StartDateYY) {
                    $(this).addClass(schedule.employee.tags.employeesCalendarMonthArrowDisabled);
                    return;
                }

                if (NewMM == 0) {
                    NewMM = 12;
                    NewYY = NewYY-1;
                }

                let date = NewYY + '-' + NewMM;

                $($(this).attr(schedule.employee.tags.dataCalendarName)).find(schedule.employee.tags.calendarDaysNames).addClass('d-none');
                $($(this).attr(schedule.employee.tags.dataCalendarName)).find(schedule.employee.tags.calendarMonth + NewMM + '-' + NewYY).removeClass('d-none')

                $(this).parent().find(schedule.employee.tags.employeesCalendarMonthTitle).attr(schedule.employee.tags.dataCurrentDate, date).text(date);
            });
        },
        "selectPage" : function () {
            $(schedule.employee.tags.schMenu).find('li').click(function () {
                $(schedule.employee.tags.schMenu).find('li').removeClass(schedule.employee.tags.active);
                $(this).addClass(schedule.employee.tags.active);
                let menuType = $(this).attr(schedule.employee.tags.menuType);

                $(schedule.employee.tags.schedulePageToggle).addClass('d-none');
                $("#" + menuType).removeClass('d-none');
            });
        },
        "activatePage" : function (type) {
            $(schedule.employee.tags.employeeGroupName + ', ' + schedule.employee.tags.employeeCurrentScheduleName).click(function () {
                $(this).parent().toggleClass(schedule.employee.tags.activeEmployeeGroupList);
                let Id = $(this).attr(schedule.employee.tags.dataScheduleId);
                let currentList = $(schedule.employee.tags.userListSchedule + Id);

                currentList.slideToggle(500, function () {});

            });
        },
        "showCalendar" : function () {
            schedule.post(schedule.urls.getItem, {
                    "type"       : schedule.types.showCalendar
                }, function (data) {
                    $(schedule.employee.tags.calendarInsert).html(data);
                }
            );
        },
        "showEmployeesInSchedule" : function () {
            schedule.post(schedule.urls.getItem, {
                    "type"       : schedule.types.showEmployeesInSchedule
                }, function (data) {
                    $(schedule.employee.tags.employeesInSchedule).html(data);
                    schedule.employee.activatePage();
                }
            );
        },
        "autoLoad" : function () {
            schedule.employee.showEmployeesInSchedule();
            schedule.employee.changeMonthDays();
            schedule.employee.selectPage();
            schedule.employee.showCalendar();
        }
    },
    "unbind" : function () {
        $(schedule.tags.schTrig).unbind();
        $(schedule.tags.schItem).unbind();
        $(schedule.tags.checkmark).unbind();
        $(schedule.tags.saveGroupsInSchedule).unbind();
        $(schedule.tags.addWeekButton).unbind();
        $(schedule.tags.removeSchedule).unbind();
        $(schedule.tags.addGroupUserIcon).unbind();
    },
    "autoLoad" : function () {
        $('.newScheduleDate').datepicker({
            uiLibrary: 'bootstrap'
        });
        schedule.employee.autoLoad();
        schedule.dragAndDrops.autoLoad();
        schedule.notifications.autoLoad();
        schedule.addToSchedule.autoLoad();
        schedule.getNewScheduleItem();
        schedule.removeWeek();
        schedule.addWeek();
        schedule.addDayTime();
        schedule.switch();
        schedule.changeToGroups();
        schedule.loadDatepicker();
        schedule.openSchedule();
        schedule.removeSchedule();
        schedule.liveSearch(schedule.tags.groupsSearch, schedule.tags.tableGroupsLiveSearch + ' td:nth-child(2)');

        schedule.liveSearch(schedule.tags.individualsSearchPop, schedule.tags.tableIndividualsLiveSearch + ' td:nth-child(1)');
        schedule.liveSearch(schedule.tags.searchSelectIndividuals, schedule.tags.scheduleIndividualsList + ' .title');

        schedule.liveSearch(schedule.tags.searchSelectGroups, schedule.tags.scheduleGroupListItam + ' .title');

        schedule.selectAll(schedule.tags.selectAllGroups, schedule.tags.groupsListContainerCheckbox);
        schedule.selectAll(schedule.tags.selectAllGroupsModal, schedule.tags.selectAllGroupsModalCheckbox);

        schedule.selectAll(schedule.tags.selectIndividualsSelectAll, schedule.tags.individualsListContainerCheckbox);
        schedule.selectAll(schedule.tags.selectAllGroupsModal, schedule.tags.selectAllGroupsModalCheckbox);
    }
};

$(function(){
    $('[data-toggle="tooltip"]').tooltip(); // For tooltips to work
    schedule.autoLoad();
});

/*

function formatTimeChar(e, self){
    // Input Value var
    var inputValue = self.val();

    // Make sure keypress value is a Number
    if( (e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 95 && e.keyCode < 106) || e.keyCode == 8){

        // Make sure first value is not greater than 2
        if(inputValue.length == 0){
            if(e.keyCode > 49 && e.keyCode < 58 || e.keyCode > 97 && e.keyCode < 106){
                e.preventDefault();
                self.val(2);
            }
        }

        // Make sure second value is not greater than 4
        else if(inputValue.length == 1 && e.keyCode != 8){
            e.preventDefault();
            if( e.keyCode > 50 && e.keyCode < 58 || e.keyCode > 98 && e.keyCode < 106 ){
                self.val(inputValue + '3:');
            }
            else{
                self.val(inputValue + String.fromCharCode(e.keyCode) + ':');
            }
        }

        else if(inputValue.length == 2 && e.keyCode != 8){
            e.preventDefault();
            if( e.keyCode > 52  && e.keyCode < 58 || e.keyCode > 100 && e.keyCode < 106 ){
                self.val(inputValue + ':5');
            }
            else{
                self.val(inputValue + ':' + String.fromCharCode(e.keyCode));
            }
        }

        // Make sure that third value is not greater than 5
        else if(inputValue.length == 3 && e.keyCode != 8){
            if( e.keyCode > 52  && e.keyCode < 58 || e.keyCode > 100 && e.keyCode < 106 ){
                e.preventDefault();
                self.val( inputValue + '5' );
            }
        }

        // Make sure only 5 Characters can be input
        else if(inputValue.length > 4 && e.keyCode != 8){
            e.preventDefault();
            return false;
        }
    }

    // Prevent Alpha and Special Character inputs
    else{
        e.preventDefault();
        return false;
    }
}

*/

