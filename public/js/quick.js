//quick animations
var vue = new Vue({
    el:'#app',
    data: {
        pin: [],
        pincode: '',
        txt_to_show: '- - - -',
    },
    methods: {
        // Numpad operation
        writeContent(key) {
            if(this.pin.length <=3 ) {
                this.pin.push(key);
                this.pincode = this.pin.join('');
                this.maskField(this.pin);
            }
        },
        clearField() {
            this.pincode = '';
            this.txt_to_show = '- - - -';
            this.pin = [];
        },
        correctField() {
            if(this.pin.length >0 ) {
                this.pin.pop();
                this.pincode = this.pin.join('');
                this.maskField(this.pin);
            }
        },
        maskField(pin) {
            if(pin.length == 0) { this.txt_to_show = '- - - -' }
            if(pin.length == 1) { this.txt_to_show = '• - - -' }
            if(pin.length == 2) { this.txt_to_show = '• • - -' }
            if(pin.length == 3) { this.txt_to_show = '• • • -' }
            if(pin.length == 4) { this.txt_to_show = '• • • •' }
        },

        btn_pressed(key) {
            document.getElementById('num_key_' + key).style.opacity = '0.75';
        },
        btn_un_pressed(key) {
            document.getElementById('num_key_' + key).style.opacity = '0.80';
        },

        // Some mouse Press/Click/Hover effects
        pressEffect(elid) {
            document.getElementById(elid).style.background = 'rgba(255, 255, 255, 0.25)';
        },
        depressEffect(elid) {
            document.getElementById(elid).style.background = 'rgba(255, 255, 255, 0.15)';
        },
        setHoverBg(elid) {
            document.getElementById(elid).style.background = 'rgba(255, 255, 255, 0.15)';
        },
        setHoutBg(elid) {
            document.getElementById(elid).style.background = 'none';
        },

        // Animations
        shakeAtWrong() { // Numpad input field's text shake
            this.clearField(); // To show dashes instead of dots
            inp_field = document.getElementById('input_1');
            inp_field.className = ''; // Reset classes to relunch animation
            inp_field.offsetWidth = inp_field.offsetWidth; // Redraw object for class reset to take effect
            inp_field.className = 'animated shake faster pin_inp_fld'; // Run the animation
        },
        fadeAtSuccess() { // Fade up numpad on success

            setTimeout(this.hideNumpad, 1);
            setTimeout(this.showCircleAni, 1010);
            setTimeout(this.fadeInUserPic, 1400);
            // setTimeout(this.showDoubleCircles, 2700); // Call this function in case of two buttons
            setTimeout(this.diagonalSlide, 2700);
            setTimeout(this.showInfoText, 3700);
            setTimeout(this.showLogoutText, 4200);
            setTimeout(this.showActionButt, 4200);
        },
        fadeDoubleCircle(){
            setTimeout(this.showDoubleCircles, 2700);
        },
        fadeInUserPic() {
            document.getElementById('user_pic').className = 'animated fadeIn user_pic position_centered';
        },
        hideNumpad() {
            document.getElementById('numpad_wrapper').className = 'animated fadeOutUp';
        },
        showCircleAni() {
            document.getElementById('action_butt_txt').className = 'position_centered action_butt_txt pointer hide';
            document.getElementById('action_butt_txt_2').className = 'position_centered action_butt_txt pointer hide';
            circle_SVG = document.getElementById('ani_circle_SVG');
            circle_el = document.getElementById('ani_circle');
            circle_SVG.classList.remove('hide');
            circle_SVG.offsetWidth = circle_SVG.offsetWidth;
            circle_SVG.classList.add('show');
            circle_el.classList.remove('hide', 'ani_circle_fill', 'ani_circle_unfill');
            circle_el.offsetWidth = circle_el.offsetWidth;
            circle_el.classList.add('show', 'ani_circle_fill');
        },
        diagonalSlide() {
            document.getElementById('user_pic').classList.add('start_diagonal_ani');
        },
        showDoubleCircles() {
            // Get vars
            static_circle_SVG = document.getElementById('static_circle_SVG');
            static_circle = document.getElementById('static_circle');
            ani_circle_SVG = document.getElementById('ani_circle_SVG');
            action_butt_div = document.getElementById('action_butt_div');
            action_butt_div_2 = document.getElementById('action_butt_div_2');
            // --------

            static_circle_SVG.classList.remove('hide');
            static_circle.classList.remove('hide', 'ani_circle_unfill');
            static_circle_SVG.offsetWidth = static_circle_SVG.offsetWidth;
            static_circle.offsetWidth = static_circle.offsetWidth;
            static_circle_SVG.classList.add('show',);
            static_circle.classList.add('show');

            // Run doubling animation
            ani_circle_SVG.classList.remove('ani_circle_1_right');
            static_circle_SVG.classList.remove('ani_circle_2_left');
            action_butt_div.classList.remove('hide', 'ani_circle_1_right');
            action_butt_div_2.classList.remove('hide', 'ani_circle_2_left');
            ani_circle_SVG.offsetWidth = ani_circle_SVG.offsetWidth;
            static_circle_SVG.offsetWidth = static_circle_SVG.offsetWidth;
            action_butt_div.offsetWidth = action_butt_div.offsetWidth;
            action_butt_div_2.offsetWidth = action_butt_div_2.offsetWidth;
            ani_circle_SVG.classList.add('ani_circle_1_left');
            static_circle_SVG.classList.add('ani_circle_2_right');

            action_butt_div.classList.add('show', 'ani_circle_1_left');
            action_butt_div_2.classList.add('show', 'ani_circle_2_right', 'show_on_top');
            // ----------------------

            setTimeout(this.showSecondButt, 1500); // Sow actual button and it's text
        },
        showInfoText() {
            user_info_ani = document.getElementById('user_info_ani');
            user_info_ani.classList.remove('start_slidein_ani', 'start_slideout_ani');
            user_info_ani.offsetWidth = user_info_ani.offsetWidth;
            user_info_ani.classList.add('start_slidein_ani');
            document.getElementById('user_info_txt_ani').className = 'animated fadeIn users_position';
        },
        showLogoutText() {
            document.getElementById('sign_out_txt').className = 'animated fadeIn sign_out_txt pointer';
        },
        showActionButt() {
            document.getElementById('action_butt_txt').className = 'animated fadeIn position_centered action_butt_txt pointer';
            document.getElementById('action_butt_div').classList.add('show_on_top');
        },
        showSecondButt() {
            document.getElementById('action_butt_txt_2').className = 'animated fadeIn position_centered action_butt_txt pointer';
        },
        showModal() {
            modal_div =document.getElementById('modal_div');
            modal_div.classList.remove('show_modal_ani', 'hide_modal_ani');
            modal_div.offsetWidth = modal_div.offsetWidth;
            modal_div.classList.add('show_modal_ani');
        },

        resetAnimations (){
            setTimeout(this.hideModal, 2400);
            setTimeout(this.hideInfoText, 800);
            setTimeout(this.hideLogoutText, 800);
            setTimeout(this.hideUserPic, 1400);
            setTimeout(this.hideActionButt, 1400);
            // setTimeout(this.hideDoubleCircles, 1400); // Call this function in case of two buttons
            setTimeout(this.hideCircle, 2400);
            setTimeout(this.showNumpad, 3400);
        },
        hideDoubleCircle(){
            setTimeout(this.hideDoubleCircles, 1400);
        },
        hideModal() {
            document.getElementById('modal_div').classList.add('hide_modal_ani');
        },
        hideInfoText() {
            document.getElementById('user_info_ani').classList.add('start_slideout_ani');
            document.getElementById('user_info_txt_ani').className = 'animated fadeOut users_position';
        },
        hideLogoutText() {
            document.getElementById('sign_out_txt').className = 'animated fadeOut sign_out_txt pointer';
        },
        hideUserPic() {
            document.getElementById('user_pic').className = 'animated fadeOut user_pic corner_pos';
        },
        hideCircle() {
            circle_el = document.getElementById('ani_circle');
            circle_el.offsetWidth = circle_el.offsetWidth;
            circle_el.classList.add('ani_circle_unfill');

            static_circle = document.getElementById('static_circle');
            static_circle.offsetWidth = static_circle.offsetWidth;
            static_circle.classList.add('ani_circle_unfill');
        },
        hideDoubleCircles() {
            // Run UnDoubling animation
            document.getElementById('ani_circle_SVG').classList.add('ani_circle_1_right');
            document.getElementById('static_circle_SVG').classList.add('ani_circle_2_left');
            document.getElementById('action_butt_div').classList.remove('show', 'ani_circle_1_left', 'show_on_top');
            document.getElementById('action_butt_div_2').classList.remove('show', 'ani_circle_2_right', 'show_on_top');
            document.getElementById('action_butt_div').classList.add('ani_circle_1_right');
            document.getElementById('action_butt_div_2').classList.add('ani_circle_2_left');
            // ------------------------

            this.hideSecondButt(); // Hide actual button and it's text
        },
        hideActionButt() {
            action_butt_txt = document.getElementById('action_butt_txt');
            action_butt_div = document.getElementById('action_butt_div');
            action_butt_txt.className = '';
            action_butt_txt.offsetWidth = action_butt_txt.offsetWidth;
            action_butt_txt.className = 'position_centered animated fadeOut action_butt_txt pointer';
            action_butt_div.classList.remove('show_on_top');
            action_butt_div.offsetWidth = action_butt_div.offsetWidth;
        },
        hideSecondButt() {
            action_butt_txt_2 = document.getElementById('action_butt_txt_2');
            action_butt_txt_2.className = '';
            action_butt_txt_2.offsetWidth = action_butt_txt_2.offsetWidth;
            action_butt_txt_2.className = 'position_centered animated fadeOut action_butt_txt pointer';
        },
        showNumpad() {
            numpad_container = document.getElementById('numpad_wrapper');
            numpad_container.classList.remove('animated', 'fadeOut');
            numpad_container.offsetWidth = numpad_container.offsetWidth;
            numpad_container.classList.add('numpad_fadein');

            // document.getElementById('numpad_wrapper').style.display = 'block';
            // document.getElementById('numpad_wrapper').className = 'animated fadeIn';

            // document.getElementById('numpad_wrapper').classList.remove('animated', 'fadeOut');
            // document.getElementById('numpad_wrapper').style.display = 'block';
            // document.getElementById('numpad_wrapper').classList.add('numpad_fadein');

            // document.getElementById('numpad_wrapper').className = '';
            // document.getElementById('numpad_wrapper').style.display = 'block';
            // document.getElementById('numpad_wrapper').className = 'numpad_fadein';

            // Prevent input field text from "shake" animation when numpad is shown again
            this.clearField(); // To show dashes instead of dots
            document.getElementById('input_1').className = 'faster pin_inp_fld'; // Apply classes without animation
        }
        // ----------
    }
});

// On Window Resize events
window.addEventListener("resize", setFontSize);
window.onload = setFontSize();

function setFontSize() {
    if(window.innerWidth <= 1288) {
        document.getElementById('input_1').style.fontSize = '39px';
        document.getElementById('pin_title_div').style.fontSize = '39px';
        document.getElementById('users_name').style.fontSize = '26px';
        document.getElementById('users_position').style.fontSize = '17px';
        document.getElementById('sign_out_txt').style.fontSize = '21px';
        document.getElementById('action_butt_txt').style.fontSize = '17px';
        document.getElementById('action_butt_txt_2').style.fontSize = '17px'; <!-- Hide in case of only one button -->
        document.getElementById('icon_succ').style.fontSize = '30px';
        document.getElementById('modal_txt').style.fontSize = '30px';
        document.getElementById('ani_circle').style.r = '51px'; // For Chrome and Opera
        document.getElementById('ani_circle').style.strokeDasharray = '330px 1000px';
    } else {
        document.getElementById('input_1').style.fontSize = '3vw';
        document.getElementById('pin_title_div').style.fontSize = '3vw';
        document.getElementById('users_name').style.fontSize = '2vw';
        document.getElementById('users_position').style.fontSize = '1.3vw';
        document.getElementById('sign_out_txt').style.fontSize = '1.7vw';
        document.getElementById('action_butt_txt').style.fontSize = '1.3vw';
        document.getElementById('action_butt_txt_2').style.fontSize = '1.3vw'; <!-- Hide in case of only one button -->
        document.getElementById('icon_succ').style.fontSize = '2.4vw';
        document.getElementById('modal_txt').style.fontSize = '2.4vw';
        document.getElementById('ani_circle').style.r = '4vw'; // For Chrome and Opera
        document.getElementById('ani_circle').style.strokeDasharray = '35vw 1000px';
    }
};

// On Window Resize events
window.addEventListener("resize", setFontSize);
window.onload = setFontSize();

function setFontSize() {
    if(window.innerWidth <= 1288) {
        document.getElementById('input_1').style.fontSize = '39px';
        document.getElementById('pin_title_div').style.fontSize = '39px';
        document.getElementById('users_name').style.fontSize = '26px';
        document.getElementById('users_position').style.fontSize = '17px';
        document.getElementById('sign_out_txt').style.fontSize = '21px';
        document.getElementById('action_butt_txt').style.fontSize = '17px';
        document.getElementById('icon_succ').style.fontSize = '30px';
        document.getElementById('modal_txt').style.fontSize = '30px';
        document.getElementById('ani_circle').style.r = '51px'; // For Chrome and Opera
    } else {
        document.getElementById('input_1').style.fontSize = '3vw';
        document.getElementById('pin_title_div').style.fontSize = '3vw';
        document.getElementById('users_name').style.fontSize = '2vw';
        document.getElementById('users_position').style.fontSize = '1.3vw';
        document.getElementById('sign_out_txt').style.fontSize = '1.7vw';
        document.getElementById('action_butt_txt').style.fontSize = '1.3vw';
        document.getElementById('icon_succ').style.fontSize = '2.4vw';
        document.getElementById('modal_txt').style.fontSize = '2.4vw';
        document.getElementById('ani_circle').style.r = '4vw'; // For Chrome and Opera
    }
}


// Click ok and send pin code

$(document).ready(function() {
    $("#num_key_ok").click(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "quick",
            dataType: 'json',
            data: {
                pin: $("#pin_filed_to_send").val()
            },
            success: function(data) {
                if(!data){
                    console.log(error);
                }
                else{

                    console.log(data);
                    deployDataLogin(data);
                    vue.fadeAtSuccess();

                }
                console.log(data);
            },
            error: function(data) {
                // if not found pin vibration wrong
                vue.shakeAtWrong();
                console.log(data)
                console.log('error')
            }
        });
    });
});
//
$('#action_butt_div').click(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "quick",
        // dataType: 'json',
        data: {
            action: $('#clockAction').val(),
            dayId: $('#clockDayId').val(),
            shiftId: $('#shiftId').val()
        },
        success: function(data) {
            if(!data){
                // vue.shakeAtWrong();
            }
            else{
                console.log(data);
                vue.showModal();
                vue.resetAnimations();
                vue.hideDoubleCircle();
            }
        },
        error: function(data) {
            console.log(data +'errorasd');
        }
    });
});
//
$('#action_butt_div_2').click(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "quick",
        // dataType: 'json',
        data: {
            lunch: $('#lunchAction').val(),
            dayId: $('#clockDayId').val()
        },
        success: function(data) {
            if(!data){
                // vue.shakeAtWrong();
            }
            else{
                console.log(data);
                // deployDataLogin(data);
                // vue.fadeAtSuccess();
                vue.showModal();
                vue.resetAnimations();
                vue.hideDoubleCircle();
            }
        },
        error: function(data) {
            console.log(data +'error123');
        }
    });
});

$('#sign_out_txt').click(function(e) {
    vue.hideDoubleCircle();
});

function deployDataLogin(data){

    console.log(data);

    $('#users_name').html(data.users.user_name+' '+data.users.last_name);
    $('#users_position').html(data.users.positionName);
    $('#clockDayId').val(data.day.day_unic_id);
    $('#shiftId').val(data.day.shiftId);


    var dt = new Date();
    var time = dt.getHours() + ":" + dt.getMinutes();

    if(data.day.clockIn == null || (data.day.clockOut != null && data.day.start_lunch == 0)){
        $('#clockAction').val(1);
        $('#action_butt_txt').html('Clock&nbsp;In');
    }else{
        $('#clockAction').val(2);
        $('#action_butt_txt').html('Clock&nbsp;Out');
    }

    if (data.shift.duration != null) {
        if(data.shift.lunchStart <= time && data.shift.lunchStart != null) {
            if (data.day.end_lunch >= 0 && data.day.start_lunch == 1) {
                $('#lunchAction').val(2);
                $('#action_butt_txt_2').html('Lunch&nbsp;End');
                vue.fadeDoubleCircle();
            } else if(data.day.clockIn && data.day.clockOut == null){
                $('#lunchAction').val(1);
                $('#action_butt_txt_2').html('Lunch&nbsp;Start');
                vue.fadeDoubleCircle();
            }
        }else if(data.shift.lunchStart == null){
            $('#lunchAction').val(2);
            $('#action_butt_txt_2').html('Lunch&nbsp;End');
            vue.fadeDoubleCircle();
        }
    }

    if(data.users.image == null){
        $('#user_pic').attr("src",'public/images/users/face_1.jpg');
    }else{
        $('#user_pic').attr("src",'public/images/users/'+data.users.image);
    }
}

// $(window).on('load', function () {
//     $('body').addClass('loaded_hiding');
//     window.setTimeout(function () {
//         $('body').addClass('loaded');
//         $('body').removeClass('loaded_hiding');
//     }, 500);
// }