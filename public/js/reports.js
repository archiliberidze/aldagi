function filterByInput (filterInput, filterDiv) {
    $(filterInput).keyup(function(){

        let input = $(filterInput).val();
        let count = 0;
        $(filterDiv).each(function(){

            if ($(this).text().search(new RegExp(input, "i")) < 0) {
                $(this).parent().fadeOut();
            } else {
                $(this).parent().show();
                count++;
            }
        });
    });
}

function sortTable() {
    let switching = true;
    let shouldSwitch = null;
    let rows = null;

    let table = document.getElementsByClassName(".table-users-all");


    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;

        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (let i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;

            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            let x = rows[i].getElementsByTagName("th")[0];
            let y = rows[i + 1].getElementsByTagName("th")[0];

            // Check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }

        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }

    }
}

function sortUl (sortFrom, sortTo) {
    var items = $(sortFrom).get();

    items.sort(function(a,b){
        var keyA = $(a).text();
        var keyB = $(b).text();

        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        return 0;
    });

    var ul = $(sortFrom);
    $.each(items, function(i, li){
        ul.append(li);
    });
}
function sortDown () {

}
function sortUp () {

}
var tags  = {
    "itemClick" : ".sort",
    "sortItem" : ".sortBy",
    "arrows" : ".arrows",
    "arrowUp" : ".arrow-up",
    "arrowDown" : ".arrow-down",
    "sortName"  : "data-name"
};
(function sortClick () {
    let itemClick = $(tags.itemClick);

    itemClick.find(tags.arrowUp).click(function () {
        let name = $(this).parent().parent();
        console.log(name.attr(tags.sortName));
    });

    itemClick.find(tags.arrowDown).click(function () {
        let name = $(this).parent().parent();
        console.log(name.attr(tags.sortName));
    });
})();
$(document).ready(function () {

    filterByInput('#liveSearch', '.username-group');
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap'
    });

    $('.icon-thumbnails').click(function () {
        $('.table-view').css("display", "none");
        $('.box-view').css("display", "block");
    });

    $('.icon-table').click(function () {
        $('.table-view').css("display", "block");
        $('.box-view').css("display", "none");
    });
});

// select user for show times

function changeReports (callBack) {
    $('.update-day').click(function () {
        var dayId = this.id;
        $('#saveUpdateDay').click(function () {
            //e.preventDefault();
            var xhr = $.ajax({
                type: "POST",
                url: "reports",
                // dataType: 'json',
                data: {
                    updateDay: dayId,
                    type: $('#type option:selected').attr("name"),
                    time: $('#updateTime').val(),
                    message: $('#message').val(),
                    day_id: $('#heading'+dayId).find('.day_id').val(),
                    urgent: $('#urgent').is(":checked")
                },
                success: function (data) {
                    Swal.fire({
                        icon: 'success',
                        title: '',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });
        });
    });
}

$('#report_id').click(function (e) {
    var selected_ids = [];
    var datepicker = $('#datepicker').val();
    var day_period = $('#day_period').val();
    $('.custom-checkbox input:checked').each(function () {
        selected_ids.push($(this).val());
    });

    if((datepicker === '' &&  day_period === '') || selected_ids === ''){
        console.log('asd');
    }

    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "reports",
        // dataType: 'json',
        data: {
            ids: selected_ids,
            datepicker: datepicker,
            day_period: day_period,
        },
        success: function (data) {
            if (data) {
                // update times
                $('.selection-full').css('display', 'block');
                document.getElementById('selection').innerHTML = data;

                $('.dayInputBefore').click(function () {
                    var day_id = $(this).val();
                    var before_extratime;
                    if ($('.dayInputBefore').is(':checked')) {
                        before_extratime = $('.before-extratime-' + day_id).val();
                    } else {
                        before_extratime = 'decline';
                    }

                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        data: {
                            dayId: day_id,
                            before_extratime: before_extratime,
                        },
                        success: function (data) {
                            if (!data) {
                            } else {
                            }
                        }
                    });
                });

                $('.dayInputAfter').click(function () {
                    var day_id = $(this).val();
                    var after_extratime;
                    if ($('.dayInputAfter').is(':checked')) {
                        after_extratime = $('.after-extratime-' + day_id).val();
                    } else {
                        after_extratime = 'decline';
                    }

                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        data: {
                            dayId: day_id,
                            after_extratime: after_extratime,
                        },
                        success: function (data) {
                            if (data) {

                            }
                        }
                    });
                });

                changeReports(function () {
                    $('.update-day').unbind();
                    $('#saveUpdateDay').unbind();

                    changeReports(function () {
                        $('.update-day').unbind();
                        $('#saveUpdateDay').unbind();
                    });

                });

                $('.log-day').click(function () {
                    var logDay = this.id;
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        // dataType: 'json',
                        data: {
                            logDay: logDay,
                        },
                        success: function (data) {
                            if (data) {
                                $('#exampleModalCenter-2 .modal-body .col-md-12 .user-log-modal').remove();
                                $('#exampleModalCenter-2 .modal-body .col-md-12').append(data).html();
                            }
                        }
                    });
                });

                $('.employees_date').click(function () {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        // dataType: 'json',
                        data: {
                            ids: selected_ids,
                            datepicker: datepicker,
                            day_period: day_period,
                            group_by_date: 'date'
                        },
                        success: function (data) {
                            if (data) {
                                $('.full-selection .groyp_by_employees').remove();
                                $('.full-selection .groyp_by_date').remove();
                                $('.full-selection').append(data).html();

                                $('.group_by_active').removeClass('group_by_active');
                                $('.employees_date').removeClass('cursor-pointer');

                                $('.employees_date').addClass('group_by_active');
                                $('.employees').addClass('group_by_style');
                                $('.employees').addClass('cursor-pointer');

                                $('.exel-btn').click(function () {
                                    var user_id = $('.exel-btn').attr('id');
                                    e.preventDefault();
                                    $.ajax({
                                        type: "POST",
                                        url: "reports",
                                        // dataType: 'json',
                                        data: {
                                            generate_excel: "excel",
                                            user_id: user_id,
                                            datepicker: datepicker,
                                            day_period: day_period,
                                        },
                                        success: function (data) {
                                            if (data) {
                                                console.log(data);
                                                // $('.full-selection .groyp_by_date').remove();
                                                // $('.full-selection .groyp_by_employees').remove();
                                                $('.full-selection').append(data).html();
                                            }
                                        }
                                    })
                                });


                                $('.dayInputBefore').click(function () {
                                    var day_id = $(this).val();
                                    var before_extratime;
                                    if ($('.dayInputBefore').is(':checked')) {
                                        before_extratime = $('.before-extratime-' + day_id).val();
                                    } else {
                                        before_extratime = 'decline';
                                    }

                                    e.preventDefault();
                                    $.ajax({
                                        type: "POST",
                                        url: "reports",
                                        data: {
                                            dayId: day_id,
                                            before_extratime: before_extratime,
                                        },
                                        success: function (data) {
                                            if (!data) {
                                            } else {
                                            }
                                        }
                                    });
                                });

                                $('.dayInputAfter').click(function () {
                                    var day_id = $(this).val();
                                    var after_extratime;
                                    if ($('.dayInputAfter').is(':checked')) {
                                        after_extratime = $('.after-extratime-' + day_id).val();
                                    } else {
                                        after_extratime = 'decline';
                                    }

                                    e.preventDefault();
                                    $.ajax({
                                        type: "POST",
                                        url: "reports",
                                        data: {
                                            dayId: day_id,
                                            after_extratime: after_extratime,
                                        },
                                        success: function (data) {
                                            if (data) {

                                            }
                                        }
                                    });
                                });

                                $('.log-day').click(function () {
                                    var logDay = this.id;
                                    e.preventDefault();
                                    $.ajax({
                                        type: "POST",
                                        url: "reports",
                                        // dataType: 'json',
                                        data: {
                                            logDay: logDay,
                                        },
                                        success: function (data) {
                                            if (data) {
                                                $('#exampleModalCenter-2 .modal-body .col-md-12 .user-log-modal').remove();
                                                $('#exampleModalCenter-2 .modal-body .col-md-12').append(data).html();
                                            }
                                        }
                                    });
                                });
                            }
                        }

                    });
                });


                $('.employees').click(function () {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        // dataType: 'json',
                        data: {
                            ids: selected_ids,
                            datepicker: datepicker,
                            day_period: day_period,
                            group_by_employees: 'employees'
                        },
                        success: function (data) {
                            if (data) {
                                $('.full-selection .groyp_by_date').remove();
                                $('.full-selection .groyp_by_employees').remove();
                                $('.full-selection').append(data).html();

                                $('.employees').removeClass('cursor-pointer');
                                $('.employees').addClass('group_by_active');

                                $('.employees_date').removeClass('group_by_active');
                                $('.employees_date').addClass('group_by_style');
                                $('.employees_date').addClass('cursor-pointer');


                                $('.exel-btn').click(function () {
                                    var user_id = $('.exel-btn').attr('id');
                                    e.preventDefault();
                                    $.ajax({
                                        type: "POST",
                                        url: "reports",
                                        // dataType: 'json',
                                        data: {
                                            generate_excel: "excel",
                                            user_id: user_id,
                                            datepicker: datepicker,
                                            day_period: day_period,
                                        },
                                        success: function (data) {
                                            if (data) {
                                                console.log(data);
                                                // $('.full-selection .groyp_by_date').remove();
                                                // $('.full-selection .groyp_by_employees').remove();
                                                $('.full-selection').append(data).html();
                                            }
                                        }
                                    })
                                });
                            }
                        }
                    })
                });

                $('.exel-btn').click(function () {
                    var user_id = $('.exel-btn').attr('id');
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        // dataType: 'json',
                        data: {
                            generate_excel: "excel",
                            user_id: user_id,
                            datepicker: datepicker,
                            day_period: day_period,
                        },
                        success: function (data) {
                            if (data) {
                                console.log(data);
                                $('.full-selection .groyp_by_date').remove();
                                $('.full-selection .groyp_by_employees').remove();
                                $('.full-selection').append(data).html();
                            }
                        }
                    })
                });
            }
        },
        error: function (data) {
            console.log(data + 'error');
        }
    });
});



$('.group-name').click(function (e) {
    $('.menu .active').removeClass('active');
    $(this).addClass('active');
    $('.table-users-all').remove();

    var groupId = this.id.substring(6);
    if (groupId == '') {
        groupId = 0;
    }

    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "reports",
        // dataType: 'json',
        data: {
            groupId: groupId,
        },
        success: function (data) {
            if (data) {
                document.getElementById('user-list').innerHTML = data;
                $(".user-check-all").click(function(){
                    $('.user_reports_ids').click();
                });
                $('.username-group').click(function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "reports",
                        data: {
                            username_user_id: $(this).attr('data-id'),
                        },
                        success: function (data) {
                            if (!data) {
                                // vue.shakeAtWrong();
                            } else {
                                document.getElementById('user-info').innerHTML = data;
                            }
                        }
                    });
                });
            }
        }
    });
});

$('.username-group').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "reports",
        data: {
            username_user_id: $(this).attr('data-id'),
        },
        success: function (data) {
            if (!data) {
                // vue.shakeAtWrong();
            } else {
                document.getElementById('user-info').innerHTML = data;
            }
        }
    });
});

$(".user-check-all").click(function(){
    $('.user_reports_ids').click();
});



$(function() {
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function(start, end, label) {

    });
});