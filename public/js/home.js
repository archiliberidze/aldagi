
var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope) {

    $(".column-50").sortable({
        connectWith: ".column-50",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all"
    });
    $(".column-100").sortable({
        connectWith: ".column-100",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all"
    });

    $(".portlet")
        .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<span class='fa fa-close portlet-toggle'></span>");

    $(".portlet-toggle").on("click", function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
    });

    // users searching

    $(document).on("keyup",".filter",function () {
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $("nav ul li").each(function(){

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();

                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
    });
    //end user searching

    // employee searching
    $(document).on("keyup",".filter_employee",function () {
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $(".employees_schedule").each(function(){

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();

                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
    });
    $(document).on("keyup",".filter_employee_next",function () {
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;

        // Loop through the comment list
        $(".employees_next_schedule").each(function(){

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();

                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
    });
    //end employee searching

    //add notes
    $(document).on("keyup","#mynote",function (e) {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 13) {  // Enter keycode
            param= new Object();
            param.action="add_note";
            param.text=this.value;
            this.value='';
            $.ajax({
                type: "POST",
                url: "home",
                dataType: 'json',
                data: param,
                success: function (data) {

                    $(".notes").append(` <div class="note" id="note_${data.result}">
                            <span onclick="delete_note(this)" note_id="${data.result}"  class="fa fa-close portlet-note-toggle "></span>
                            <div class="portlet-content portlet-border">
                               ${param.text}
                            </div>
                        </div>`);
                }
            });
        }
    });

    //Delete note

    $(document).on("click",".portlet-note-toggle",function () {

        Swal.fire({
            title: 'Are you sure you want to delete this note?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                param= new Object();
                param.action="delete_note";
                param.note_id=$(this).attr("note_id")
                $.ajax({
                    type: "POST",
                    url: "home",
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        $("#note_"+param.note_id).remove();
                    }
                });
            }
        })

    });

    // current timepunch widget  data

    param= new Object();
    param.action="get_current_timepunch";
    param.group_id=$('#inputGroup').val();
    $.ajax({
        type: "POST",
        url: "home",
        dataType: 'json',
        data: param,
        success: function (data) {
            $scope.$apply(function(){
                $scope.users=data.result;
            });
        }
    });

    // end current timepunch widget

    // attendance widget  data

    param= new Object();
    param.action="get_attendance";
    $.ajax({
        type: "POST",
        url: "home",
        dataType: 'json',
        data: param,
        success: function (data) {
            if(data.result != null && data.result !='') {
                var options1 = {
                    series: data.result[0].attandance_in_week,
                    colors: ['#2E93fA', '#c0d7fa', '#92d4ff', '#3FBFF5', '#7ab0ec', '#5e8cbf', '#90a4ae'],
                    chart: {
                        type: 'donut',
                    },
                    dataLabels: {
                        enabled: false
                    },
                    responsive: [{
                        breakpoint: 10,
                        options: {
                            chart: {
                                width: 100
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }],
                    labels: data.result[0].week_days,
                    title: {
                        text: 'Attendance this week',
                        align: 'center',
                        margin: 10,
                        offsetX: 0,
                        offsetY: 0,
                        floating: false,
                        style: {
                            fontSize: '18px',
                            color: '#6c757d'
                        },
                    },
                    legend: {
                        show: true,
                        position: 'bottom'

                    },
                    plotOptions: {
                        pie: {
                            customScale: 1,
                            offsetX: 0,
                            offsetY: 0,
                            expandOnClick: true,
                            dataLabels: {
                                offset: 0,
                                minAngleToShowLabel: 10
                            },
                            donut: {
                                size: '50%',
                                background: 'transparent',
                                labels: {
                                    show: true,
                                    name: {
                                        show: true,
                                        fontSize: '22px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: -10
                                    },
                                    value: {
                                        show: true,
                                        fontSize: '16px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: 16,
                                        formatter: function (val) {
                                            return val
                                        }
                                    },
                                    total: {
                                        show: true,
                                        showAlways: true,
                                        label: 'Total',
                                        color: '#3fbff5',
                                        formatter: function (w) {
                                            return w.globals.seriesTotals.reduce((a, b) => {
                                                return a + b
                                            }, 0)
                                        }
                                    }
                                }
                            },
                        }
                    }

                };
                var options2 = {
                    series: data.result[0].absence_in_week,
                    colors: ['#2E93fA', '#c0d7fa', '#92d4ff', '#3FBFF5', '#7ab0ec', '#5e8cbf', '#90a4ae'],
                    chart: {
                        type: 'donut',
                    },
                    dataLabels: {
                        enabled: false
                    },
                    responsive: [{
                        breakpoint: 10,
                        options: {
                            chart: {
                                width: 100
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }],
                    labels: data.result[0].week_days,
                    title: {
                        text: 'Absence this week',
                        align: 'center',
                        margin: 10,
                        offsetX: 0,
                        offsetY: 0,
                        floating: false,
                        style: {
                            fontSize: '18px',
                            color: '#6c757d'
                        },
                    },
                    legend: {
                        show: true,
                        position: 'bottom'

                    },
                    plotOptions: {
                        pie: {
                            customScale: 1,
                            offsetX: 0,
                            offsetY: 0,
                            expandOnClick: true,
                            dataLabels: {
                                offset: 0,
                                minAngleToShowLabel: 10
                            },
                            donut: {
                                size: '50%',
                                background: 'transparent',
                                labels: {
                                    show: true,
                                    name: {
                                        show: true,
                                        fontSize: '22px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: -10
                                    },
                                    value: {
                                        show: true,
                                        fontSize: '16px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: 16,
                                        formatter: function (val) {
                                            return val
                                        }
                                    },
                                    total: {
                                        show: true,
                                        showAlways: true,
                                        label: 'Total',
                                        color: '#3fbff5',
                                        formatter: function (w) {
                                            return w.globals.seriesTotals.reduce((a, b) => {
                                                return a + b
                                            }, 0)
                                        }
                                    }
                                }
                            },
                        }
                    }

                };
                var options3 = {
                    series: data.result[0].late_time_in_week,
                    colors: ['#2E93fA', '#c0d7fa', '#92d4ff', '#3FBFF5', '#7ab0ec', '#5e8cbf', '#90a4ae'],
                    chart: {
                        type: 'donut',
                    },
                    dataLabels: {
                        enabled: false
                    },
                    responsive: [{
                        breakpoint: 10,
                        options: {
                            chart: {
                                width: 100
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }],
                    labels: data.result[0].week_days,
                    title: {
                        text: 'Late time this week (minute)',
                        align: 'center',
                        margin: 10,
                        offsetX: 0,
                        offsetY: 0,
                        floating: false,
                        style: {
                            fontSize: '18px',
                            color: '#6c757d'
                        },
                    },
                    legend: {
                        show: true,
                        position: 'bottom'

                    },
                    plotOptions: {
                        pie: {
                            customScale: 1,
                            offsetX: 0,
                            offsetY: 0,
                            expandOnClick: true,
                            dataLabels: {
                                offset: 0,
                                minAngleToShowLabel: 10
                            },
                            donut: {
                                size: '50%',
                                background: 'transparent',
                                labels: {
                                    show: true,
                                    name: {
                                        show: true,
                                        fontSize: '22px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: -10
                                    },
                                    value: {
                                        show: true,
                                        fontSize: '16px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: 16,
                                        formatter: function (val) {
                                            return val
                                        }
                                    },
                                    total: {
                                        show: true,
                                        showAlways: true,
                                        label: 'Total',
                                        color: '#3fbff5',
                                        formatter: function (w) {
                                            return w.globals.seriesTotals.reduce((a, b) => {
                                                return a + b
                                            }, 0)
                                        }
                                    }
                                }
                            },
                        }
                    }

                };
                var options4 = {
                    series: data.result[0].overtime_in_week,
                    colors: ['#2E93fA', '#c0d7fa', '#92d4ff', '#3FBFF5', '#7ab0ec', '#5e8cbf', '#90a4ae'],
                    chart: {
                        type: 'donut',
                    },
                    dataLabels: {
                        enabled: false
                    },
                    responsive: [{
                        breakpoint: 10,
                        options: {
                            chart: {
                                width: 100
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }],
                    labels: data.result[0].week_days,
                    title: {
                        text: 'Overtime this week(minute)',
                        align: 'center',
                        margin: 10,
                        offsetX: 0,
                        offsetY: 0,
                        floating: false,
                        style: {
                            fontSize: '18px',
                            color: '#6c757d'
                        },
                    },
                    legend: {
                        show: true,
                        position: 'bottom'

                    },
                    plotOptions: {
                        pie: {
                            customScale: 1,
                            offsetX: 0,
                            offsetY: 0,
                            expandOnClick: true,
                            dataLabels: {
                                offset: 0,
                                minAngleToShowLabel: 10
                            },
                            donut: {
                                size: '50%',
                                background: 'transparent',
                                labels: {
                                    show: true,
                                    name: {
                                        show: true,
                                        fontSize: '22px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: -10
                                    },
                                    value: {
                                        show: true,
                                        fontSize: '16px',
                                        fontFamily: 'Helvetica, Arial, sans-serif',
                                        color: '#3fbff5',
                                        offsetY: 16,
                                        formatter: function (val) {
                                            return val
                                        }
                                    },
                                    total: {
                                        show: true,
                                        showAlways: true,
                                        label: 'Total',
                                        color: '#3fbff5',
                                        formatter: function (w) {
                                            return w.globals.seriesTotals.reduce((a, b) => {
                                                return a + b
                                            }, 0)
                                        }
                                    }
                                }
                            },
                        }
                    }

                };

                var chart1 = new ApexCharts(document.querySelector("#chart1"), options1);
                var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
                var chart3 = new ApexCharts(document.querySelector("#chart3"), options3);
                var chart4 = new ApexCharts(document.querySelector("#chart4"), options4);
                chart1.render();
                chart2.render();
                chart3.render();
                chart4.render();
                $scope.$apply(function () {
                    $scope.attendance = data.result[0];
                });
            }
        }
    });

    // end attendance timepunch widget



    $(document).on("click","#inputGroup",function () {
        param= new Object();
        param.action="get_current_timepunch";
        param.group_id=$('#inputGroup').val();
        $.ajax({
            type: "POST",
            url: "home",
            dataType: 'json',
            data: param,
            success: function (data) {
                $scope.$apply(function(){
                    $scope.users=data.result;
                });
            }
        });
    });

    $(document).on("click",".click_next_schedule",function () {
        $('#schedule').removeClass("show");
    });
    $(document).on("click",".click_schedule",function () {
        $('#next_schedule').removeClass("show");
    });




});



