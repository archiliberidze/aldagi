
$(document).on("click","#image",function () {
    document.querySelector("#upload_image").click();
});
$(document).on("click","#image_group_setting",function () {
    document.querySelector("#upload_image_group_setting").click();
});

$(document).on("change","#upload_image",function () {
    //on change event
    formdata = new FormData();
    if ($(this).prop('files').length > 0) {
        file = $(this).prop('files')[0];
        formdata.append("upload_image", file);
    }

    jQuery.ajax({
        type: "POST",
        url: "settings",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (data) {
            data=JSON.parse(data);
            var image='public/images/users/'+data.image_name;
            document.querySelector("#personal > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(1)").innerHTML = `
                                <div class="card col-7" >
                                    <img src=`+`${image}`+` class="card-img" alt="...">
                                 </div>
                                <div class="col-5">
                                    <a id="image">Change Photo</a>
                                    <br>
                                    <hr>
                                    <a id="delete_image"> Delete Photo</a>
                                    <hr>
                                    <div>If you are unable to change your photo, you do not have permission to do so.
                                        Please
                                        contact your manager
                                    </div>   
                                    </div>`;

        }
    });
});
$(document).on("click","#delete_image",function () {
    jQuery.ajax({
        type: "POST",
        url: "settings",
        data: {action: "delete_image"},
        success: function (data) {

            document.querySelector("#personal > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(1)").innerHTML = `
                                                           <div class="card" id="image_content">
                                                            <div id="image"> + Upload an image</div>
                                                        </div>`;
        }
    });
});


$(document).on("click","#pswsubmit",function () {
    param= new Object();
    param.action="change_password";
    param.currentpassword=$('#currentpassword').val();
    param.inputpassword=$('#inputpassword').val();
    param.repeatpassword=$('#repeatpassword').val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            if (data.error!='' && data.error!= null){
                Swal.fire({
                    type: 'error',
                    text:  data.error,
                    icon: 'error'
                })
            }else {
                Swal.fire({
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 900
                })
            }

        }
    });
});

$(document).on("change","#email",function () {
    param= new Object();
    param.action="change_user_email";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            if (data.error!='' && data.error!= null){
                Swal.fire({
                    type: 'error',
                    text:  data.error,
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 900
                })
                document.querySelector("#email").value=data.result[0]['email'];
            }else {
                Swal.fire({
                    icon: 'success',
                    title: '',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }
    });
});
$(document).on("change","#name",function () {
    param= new Object();
    param.action="change_user_name";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            if (data.error!='' && data.error!= null){
                Swal.fire({
                    type: 'error',
                    text:  data.error,
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 900
                })
                document.querySelector("#name").value=data.result[0]['name'];
            }else {
                Swal.fire({
                    icon: 'success',
                    title: '',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }
    });
});
$(document).on("change","#pin",function () {
    param= new Object();
    param.action="change_user_pin";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                title: '',
                showConfirmButton: false,
                timer: 1500
            })
        }
    });
});
$(document).on("change","#company_name",function () {
    param= new Object();
    param.action="company_name";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                title: '',
                showConfirmButton: false,
                timer: 1500
            })
        }
    });
});
$(document).on("change","#fax",function () {
    param= new Object();
    param.action="change_user_fax";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                title: '',
                showConfirmButton: false,
                timer: 1500
            })
        }
    });
});
$(document).on("change","#address",function () {
    param= new Object();
    param.action="change_user_address";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                title: '',
                showConfirmButton: false,
                timer: 1500
            })
        }
    });
});
$(document).on("change","#phone_number",function () {
    param= new Object();
    param.action="change_user_phone_number";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                title: '',
                showConfirmButton: false,
                timer: 1500
            })
        }
    });
});
$(document).on("change","#zip_code",function () {
    param= new Object();
    param.action="change_user_zip_code";
    param.val=$(this).val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                title: '',
                showConfirmButton: false,
                timer: 1500
            })
        }
    });
});

$(document).on("mouseover",".settings_logo",function () {
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div").style.backgroundColor="black";
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div").style.backgroundBlendMode="color-burn";
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div").style.opacity="0.8";
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div > span").style.display="block";
})
$(document).on("mouseout",".settings_logo",function () {
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div").style.backgroundColor="white";
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div").style.backgroundBlendMode="unset";
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div").style.opacity="1";
    document.querySelector("#quick-timepunch > div > div.col-12.row > div.col-2 > div > span").style.display="none";
})

$(document).on("click","#logo_image",function () {
    document.querySelector("#upload_logo_image").click();
});

$(document).on("change","#upload_logo_image",function () {
    formdata = new FormData();
    if ($(this).prop('files').length > 0) {
        file = $(this).prop('files')[0];
        formdata.append("upload_logo_image", file);
    }

    jQuery.ajax({
        type: "POST",
        url: "settings",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (data) {
            var image=data;
            image =image.replace('"','');
            image=image.substring(0, image.length - 1);
            document.querySelector("#logo_image_content").innerHTML = `
                               
                                <div class="settings_logo" id="logo_image" style="background-image: url('public/images/quick/${image}')">
                                <span id="">Change</span>
                                <input id="upload_logo_image" type="file" accept="image/*"/>
                               </div>`;

        }
    });
});

$(document).on("change","#timezone-offset",function () {
    param= new Object();
    param.action="update_time_zone";
    param.time_zone=$('#timezone-offset').val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                showConfirmButton: false,
                timer: 800
            })
        }
    });
});
$(document).on("change","input[type=radio][name=them]",function () {
    param= new Object();
    param.action="update_company_color";
    param.theme_color=$("input[name='them']:checked").val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                showConfirmButton: false,
                timer: 800
            })
        }
    });
});

$(document).on("change","input[type=radio][name=background]",function () {
    param= new Object();
    param.action="update_company_background";
    param.background=$("input[name='background']:checked").val();
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            Swal.fire({
                icon: 'success',
                showConfirmButton: false,
                timer: 800
            })
        }
    });
});

function change_group_options() {
    $(document).on("click",".hvr-grow",function () {

        $('.hvr-grow').removeClass('active');
        $('.user_groups_individuals').removeClass('active');
        $(this).addClass('active');
        $('#users-groups > div.row > div.col-9.row > div.text-center.col-4.icon-plussign').removeClass('invisible');
        $("#users-groups > div.row > div.col-9.row > div.text-center.col-4.icon-plussign").addClass('visible');

    })
    $(document).on("click",".user_groups_individuals",function () {
        $('.hvr-grow').removeClass('active');
        $(this).addClass('active');
        $('#users-groups > div.row > div.col-9.row > div.text-center.col-4.icon-plussign').removeClass('visible');
        $("#users-groups > div.row > div.col-9.row > div.text-center.col-4.icon-plussign").addClass('invisible');

    })
}

$(document).ready(function(){
    param= new Object();
    param.action="get_theme_color";
    jQuery.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            colorPicker = new iro.ColorPicker(".colorPicker", {
                width: 150,
                color: data.result[0].theme_color,
                borderWidth: 1,
                borderColor: "#fff",
            });

            colorPicker.on(["color:change"], function(color){
                var values = document.getElementById("color_values");
                $('#mydiv').data('myval',"#fff");
                $('#mydiv').data('myval',color.hexString);
                values.style.backgroundColor=color.hexString;
                param= new Object();
                param.action="update_company_color";
                param.theme_color=color.hexString;
                jQuery.ajax({
                    type: "POST",
                    url: "settings",
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        // deselect a radio button group
                        var radList = document.getElementsByName('them');
                        for (var i = 0; i < radList.length; i++) {
                            if(radList[i].checked) radList[i].checked = false;
                        }
                    }
                });
            });
            var values = document.getElementById("color_values");
            values.style.backgroundColor=data.result[0].theme_color;
        }
    });

  change_group_options();

});


// users and groups

$(document).on("click","#select_all",function () {
    document.querySelector("#select_all_input").click();
});
$(document).on("click","#select_all_input",function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});

// add users select all

$(document).on("click","#Select_All_Users",function () {
    document.querySelector("#select_all_input_users").click();
});
$(document).on("click","#select_all_input_users",function () {
    $("input[type=checkbox][class='checkSingleUsersAdd']").not(this).prop('checked', this.checked);
});

// Creat copy button

function copy_link() {
    /* Get the text field */
    var copyText = document.getElementById("jamp_url");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");

}

//////////////////////////


//angular js

var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope) {
    $scope.group_id='0';
    param= new Object();
    param.action="get_groups";
    $.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            if(data.result==null || data.result.length==0){
                document.querySelector("#users-groups > div > div.col-9.row > div.col-4.icon-plussign").style.display='none';
                document.querySelector("#users-groups > div > div.col-9.row > div.col-4.icon-deactivate").style.display='none';
            }else {
                param= new Object();
                param.action="get_users";
                // param.group_id=data.result[0].id;
                // $scope.group_id=data.result[0].id;
                $.ajax({
                    type: "POST",
                    url: "settings",
                    dataType: 'json',
                    data: param,
                    success: function (data1) {

                            $scope.users=data1.result;
                            $scope.groups=data.result;
                            $scope.counts=data1.counts;


                    },
                    async:false
                });
            }

        },
        async:false
    });

    param= new Object();
    param.action="get_position";
    $.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            $scope.$apply(function(){
                $scope.positions=data.result;
            });
            $("#JobPosition_chosen > div ").append("<li id='add_position'><span>&#x2b;</span> <b>Add position</b></li>");
        }
    });

    param= new Object();
    param.action="get_overtime";
    $.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            $scope.$apply(function(){
                $scope.overtimes=data.result;
            });
        }
    });


    $scope.call_users=function(group_id){
        param= new Object();
        param.action="get_users";
        param.group_id=group_id;
        $scope.group_id=group_id;
        $.ajax({
            type: "POST",
            url: "settings",
            dataType: 'json',
            data: param,
            success: function (data) {
                change_group_options();
                $scope.$apply(function(){
                    $scope.users=data.result;
                    $scope.counts=data.counts;
                });
            }
        });
    }

    $scope.SaveEmployee=function(){
        param= new Object();
        param.action="create_new_employee";
        param.FirstName=$scope.FirstName;
        param.LastName=$scope.LastName;
        param.Email=$scope.Email;
        param.position=$scope.position;
        param.group=$scope.group;
        param.PhoneNumber=$scope.PhoneNumber;
        param.Fax=$scope.Fax;
        param.Address=$scope.Address;
        param.Password=$scope.Password;
        param.Repeatpassword=$scope.Repeatpassword;
        param.overtime=$scope.overtime;
        $.ajax({
            type: "POST",
            url: "settings",
            dataType: 'json',
            data: param,
            success: function (data) {
                if (data.error!='' && data.error!= null){
                    Swal.fire({
                        type: 'error',
                        text:  data.error,
                        icon: 'error'
                    })
                }else {
                    $('#CreateNewEmployeeModal').modal('hide');
                    Swal.fire({
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900
                    })
                    $scope.$apply(function(){
                        $scope.FirstName='';
                        $scope.LastName='';
                        $scope.Email='';
                        $scope.position='';
                        $scope.group='';
                        $scope.PhoneNumber='';
                        $scope.Fax='';
                        $scope.Address='';
                        $scope.Password='';
                        $scope.Repeatpassword='';
                        $scope.overtime='';
                    });
                    $scope.call_users($scope.group_id);
                }


            }
        });
    }

    $(document).on("click","#add_position",function () {
        param= new Object();
        param.action="add_position";
        param.position=$("#JobPosition_chosen [type=text]").val();

        $.ajax({
            type: "POST",
            url: "settings",
            dataType: 'json',
            data: param,
            success: function (data) {
                $scope.$apply(function(){
                    $scope.positions=data.result;
                    $scope.position=data.result[0];
                });

            }
        });
    });

    $(document).on("click","#deactivate_user",function () {

        var selected = new Array();

        $(".table input[type=checkbox][class='checkSingle']:checked").each(function () {
            selected.push(this.value);
        });
        if (selected.length > 0) {
            Swal.fire({
                title: 'Are you sure you want to deactivate user?',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3FBFF5',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    param= new Object();
                    param.action="deactivate_user";
                    param.deactivate_user=selected;
                    param.group_id= $scope.group_id;
                    $.ajax({
                        type: "POST",
                        url: "settings",
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            $scope.$apply(function(){
                                $scope.users=data.result;
                                $scope.counts=data.counts;
                            });
                        }
                    });
                }
            })

        }
    });
    $(document).on("click","#activate_user",function () {

        var selected = new Array();

        $(".table input[type=checkbox][class='checkSingle']:checked").each(function () {
            selected.push(this.value);
        });
        if (selected.length > 0) {

            Swal.fire({
                title: 'Are you sure you want to activate user?',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3FBFF5',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    param= new Object();
                    param.action="activate_user";
                    param.activate_user=selected;
                    param.group_id= $scope.group_id;
                    $.ajax({
                        type: "POST",
                        url: "settings",
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            $scope.$apply(function(){
                                $scope.users=data.result;
                                $scope.counts=data.counts;
                            });
                        }
                    });
                }
            })

        }
    });

    $scope.changebutton=function (status) {
      if(status=='1'){
          document.querySelector("#users-groups > div > div.col-9.row > div.col-4.icon-deactivate").style.display='block';
          document.querySelector("#users-groups > div > div.col-9.row > div.col-4.icon-activate").style.display='none';
      }else {
          document.querySelector("#users-groups > div > div.col-9.row > div.col-4.icon-deactivate").style.display='none';
          document.querySelector("#users-groups > div > div.col-9.row > div.col-4.icon-activate").style.display='block';
      }
    }

    $(document).on("click","#creat_group",function () {
        $scope.addesd_users=[];
        param= new Object();
        param.action="get_all_users";
        $.ajax({
            type: "POST",
            url: "settings",
            dataType: 'json',
            data: param,
            success: function (data) {
                $scope.$apply(function(){
                    $scope.all_users=data.result;
                });
            }
        });
    });
    $scope.AddUser=function (id) {
        $scope.addesd_users.push(id);
        document.querySelector("#CreateGroupModal > div > div > div.modal-body > div > nav > ul > li[remove_id='"+id+"'] > button").remove();
        $("#CreateGroupModal > div > div > div.modal-body > div > nav > ul > li[remove_id='"+id+"']").append('<div><i class="fa fa-check"></i> Added</div>')
    }
    $scope.SaveGroup=function () {
        param= new Object();
        param.action="creat_group";
        param.users=$scope.addesd_users;
        param.title=$('#GroupName').val();
        if(param.title==''){
            document.querySelector("#GroupName").style.borderColor='red';
        }else {
            $.ajax({
                type: "POST",
                url: "settings",
                dataType: 'json',
                data: param,
                success: function (data) {
                    if (data.error!='' && data.error!= null){
                        Swal.fire({
                            type: 'error',
                            text:  data.error,
                            icon: 'error'
                        })
                    }else {
                        $('#CreateGroupModal').modal('hide');
                        $scope.$apply(function(){
                            $scope.groups=data.result;
                            $scope.counts=data.counts;
                        });
                    }

                }
            });
        }

    }
     $scope.get_users_for_add=function () {
         param= new Object();
         param.action="get_users_with_group_id";
         param.group_id=$scope.group_id;
         $.ajax({
             type: "POST",
             url: "settings",
             dataType: 'json',
             data: param,
             success: function (data) {
                 $scope.$apply(function(){
                     $scope.all_users_for_add_users=data.result;
                 });
             }
         });
     }

     $scope.add_user_in_group=function () {
         var selected = new Array();

         $("input[type=checkbox][class='checkSingleUsersAdd']:checked").each(function () {
             selected.push(this.value);
         });
         param= new Object();
         param.action="add_users_in_group";
         param.group_id=$scope.group_id;
         param.users=selected;
         $.ajax({
             type: "POST",
             url: "settings",
             dataType: 'json',
             data: param,
             success: function (data) {
                 $('#AdduserModal').modal('hide');
                 $scope.$apply(function(){
                     $scope.users=data.result;
                 });
             }
         });
     }

//     Group Setting Modal

$scope.get_user_group_setting = function(user_id) {

    param= new Object();
    param.action="get_user_group_setting";
    param.group_id=$scope.group_id;
    param.user_id=user_id;
    $scope.user_id=user_id;
    $.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            $scope.schedule_group_setting=data.result_schedule;
            $("#GroupSettingsModal").modal('show');

            $scope.$apply(function(){
                $scope.group_setting=data.result[0];
                $scope.group_setting_name=data.result[0].name;
                $scope.group_setting_last_name=data.result[0].last_name;
                $scope.group_setting_email=data.result[0].email;
                $scope.group_setting_pin=data.result[0].pin;
                if(data.result[0].group_id!='' && data.result[0].group_id!= null){
                    group_result=data.result[0].group_id.split(',');
                    var group_result = group_result.map(function (x) {
                        return parseInt(x, 10);
                    });
                    $scope.group_setting_group_id=group_result;
                }else {
                    $scope.group_setting_group_id=0;
                }

                if(data.result[0].manages_group!='' && data.result[0].manages_group!= null){
                    manages_group=data.result[0].manages_group.split(',');
                    var manages_group = manages_group.map(function (x) {
                        return parseInt(x, 10);
                    });
                    $scope.group_setting_manag_group=manages_group;
                }else {
                    $scope.group_setting_manag_group=0;
                }

                if(data.result[0].position_id!='' && data.result[0].position_id!= null) {
                    position_id = data.result[0].position_id.split(',');
                    var position_id = position_id.map(function (x) {
                        return parseInt(x, 10);
                    });
                    $scope.group_setting_position = position_id;
                }else {
                    $scope.group_setting_position = 0;
                }

                if(data.result[0].overtime!='' && data.result[0].overtime!= null){
                    overtime=data.result[0].overtime.split(',');
                    var overtime = overtime.map(function (x) {
                        return parseInt(x, 10);
                    });
                    $scope.group_setting_overtime=overtime;
                }else {
                    $scope.group_setting_overtime=0;
                }

                $scope.group_setting_schedule_id=data.result[0].schedule_id;
                $scope.group_setting_next_schedule=data.result[0].next_schedule;
                $scope.group_setting_start_date=data.result[0].start_date;
                $scope.group_setting_active_since=data.result[0].active_since;
                $scope.group_setting_phone_number=data.result[0].phone_number;
                $scope.group_setting_fax=data.result[0].fax;
                $scope.group_setting_address=data.result[0].address;
                $scope.group_setting_zip_code=data.result[0].zip_code;

            });

        }
    });
}

$scope.SaveGroupSetting=function(){
    param= new Object();
    param.action="edit_group_setting";
    param.user_id=$scope.user_id;
    param.group_id=$scope.group_id;

    param.name=$scope.group_setting_name;
    param.last_name=$scope.group_setting_last_name;
    param.email=$scope.group_setting_email;
    param.pin=$scope.group_setting_pin;
    param.schedule_id=$scope.group_setting_schedule_id;
    param.phone_number=$scope.group_setting_phone_number;
    param.fax=$scope.group_setting_fax;
    param.address=$scope.group_setting_address;
    param.zip_code=$scope.group_setting_zip_code;

    param.groups=$scope.group_setting_group_id;
    param.manag_group=$scope.group_setting_manag_group;
    param.overtime=$scope.group_setting_overtime;
    param.positions=$scope.group_setting_position;

    $.ajax({
        type: "POST",
        url: "settings",
        dataType: 'json',
        data: param,
        success: function (data) {
            $('#GroupSettingsModal').modal('hide');
            $scope.$apply(function(){
                $scope.users=data.result;
            });
        }
    });
}

    $(document).on("change","#upload_image_group_setting",function () {
        //on change event
        formdata = new FormData();
        if ($(this).prop('files').length > 0) {
            file = $(this).prop('files')[0];
            formdata.append("upload_image", file);
            formdata.append("user_id", $scope.group_setting.user_id);
        }

        jQuery.ajax({
            type: "POST",
            url: "settings",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (data) {
                data=JSON.parse(data);
                var image='public/images/users/'+data.image_name;
                image =image.replace('"','');
                document.querySelector(".group_setting_image").innerHTML = `
                                <div class="card col-7" >
                                    <img src=`+`${image}`+` class="card-img" alt="...">
                                 </div>
                                <div class="col-5">
                                    <a id="image_group_setting">Change Photo</a>
                                    <br>
                                    <hr>
                                    <a id="delete_image_group_setting"> Delete Photo</a>
                                    <hr>
                                     <div>
                                        Current photo Uploaded
                                     </div>
                                    <div>
                                        ${data.uploaded_date}
                                    </div> 
                                    </div>`;

            }
        });
    });
    $(document).on("click","#delete_image_group_setting",function () {
        jQuery.ajax({
            type: "POST",
            url: "settings",
            data: {action: "delete_image", user_id: $scope.group_setting.user_id},
            success: function (data) {

                document.querySelector(".group_setting_image").innerHTML = `
                                                           <div class="card" id="image_content">
                                                            <div id="image_group_setting"> + Upload an image </div>
                                                        </div>`;
            }
        });
    });



});



// users searching
$(document).on("keyup",".filter",function () {
    // Retrieve the input field text and reset the count to zero
    var filter = $(this).val(), count = 0;

    // Loop through the comment list
    $("nav ul li").each(function(){

        // If the list item does not contain the text phrase fade it out
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut();

            // Show the list item if the phrase matches and increase the count by 1
        } else {
            $(this).show();
            count++;
        }
    });


});

///////////////////////////

function chosen($timeout) {

        var EVENTS, scope, linker, watchCollection;


        /*
         * List of events and the alias used for binding with angularJS
         */
        EVENTS = [{
            onChange: 'change'
        }, {
            onReady: 'chosen:ready'
        }, {
            onMaxSelected: 'chosen:maxselected'
        }, {
            onShowDropdown: 'chosen:showing_dropdown'
        }, {
            onHideDropdown: 'chosen:hiding_dropdown'
        }, {
            onNoResult: 'chosen:no_results'
        }];

        /*
         * Items to be added in the scope of the directive
         */
        scope = {
            options: '=', // the options array
            ngModel: '=', // the model to bind to,,
            ngDisabled: '='
        };

        /*
         * initialize the list of items
         * to watch to trigger the chosen:updated event
         */
        watchCollection = [];
        Object.keys(scope).forEach(function (scopeName) {
            watchCollection.push(scopeName);
        });

        /*
         * Add the list of event handler of the chosen
         * in the scope.
         */
        EVENTS.forEach(function (event) {
            var eventNameAlias = Object.keys(event)[0];
            scope[eventNameAlias] = '=';
        });

        /* Linker for the directive */
        linker = function ($scope, iElm, iAttr) {
            var maxSelection = parseInt(iAttr.maxSelection, 10),
                searchThreshold = parseInt(iAttr.searchThreshold, 10);

            if (isNaN(maxSelection) || maxSelection === Infinity) {
                maxSelection = undefined;
            }

            if (isNaN(searchThreshold) || searchThreshold === Infinity) {
                searchThreshold = undefined;
            }

            var allowSingleDeselect = iElm.attr('allow-single-deselect') !== undefined ? true : false;
            var noResultsText = iElm.attr('no-results-text') !== undefined ? iAttr.noResultsText : "No results found.";
            var disableSearch = iElm.attr('disable-search') !== undefined ? JSON.parse(iAttr.disableSearch) : false;
            var placeholderTextSingle = iElm.attr('placeholder-text-single') !== undefined ? iAttr.placeholderTextSingle : "";
            var placeholderTextMultiple = iElm.attr('placeholder-text-multiple') !== undefined ? iAttr.placeholderTextMultiple : "";
            var displayDisabledOptions = iElm.attr('display-disabled-options') !== undefined ? JSON.parse(iAttr.displayDisabledOptions) : true;
            var displaySelectedOptions = iElm.attr('display-selected-options') !== undefined ? JSON.parse(iAttr.displaySelectedOptions) : true;

            iElm.chosen({
                width: '100%',
                max_selected_options: maxSelection,
                disable_search_threshold: searchThreshold,
                search_contains: true,
                allow_single_deselect: allowSingleDeselect,
                no_results_text: noResultsText,
                disable_search: disableSearch,
                placeholder_text_single: placeholderTextSingle,
                placeholder_text_multiple: placeholderTextMultiple,
                display_disabled_options: displayDisabledOptions,
                display_selected_options: displaySelectedOptions
            });

            iElm.on('change', function () {
                iElm.trigger('chosen:updated');
            });


            $scope.$watchGroup(watchCollection, function () {
                $timeout(function () {
                    iElm.trigger('chosen:updated');
                }, 100);
            });

            // assign event handlers
            EVENTS.forEach(function (event) {
                var eventNameAlias = Object.keys(event)[0];

                if (typeof $scope[eventNameAlias] === 'function') { // check if the handler is a function
                    iElm.on(event[eventNameAlias], function (event) {
                        $scope.$apply(function () {
                            $scope[eventNameAlias](event);
                        });
                    }); // listen to the event triggered by chosen
                }
            });
        };

        // return the directive
        return {
            name: 'chosen',
            scope: scope,
            restrict: 'A',
            link: linker
        };


}
app.directive('chosen', ['$timeout', chosen]);
